

import { Formik } from "formik";
import { Alert, Button, Col, Container, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./login.css"

export const Login = (props: any) => {



  return (
    <>



      <Formik
        enableReinitialize={true}
        validationSchema={props.schema}
        initialValues={props.state}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            actions.setSubmitting(false);
            props.handleSubmit(values)
          }, 1000);
        }}
      >
        {fromProps => (

          <Col md={5} >
            <div className=" mt-5 login-custom mb-5">
                <Form noValidate onSubmit={fromProps.handleSubmit} onKeyDown={(e) => {
                    if (e.key === 'Enter') {
                      fromProps.handleSubmit()
                    }
                  }}>
                  <div className="pa-5">
                    <h3 className="">SIGN IN </h3>
                    <div className="login-alert">
                      <Alert show={props.showFail} variant="danger" >
                        Invalid Username / Password
                      </Alert>
                    </div>
                    <Form.Group controlId="email" className=""  >
                      <Form.Label className="m-0 mb-1"><b>Email ID</b> </Form.Label>
                      {/* <i className="fas fa-user"></i> */}
                      <Form.Control
                      
                        placeholder="abc@dgnote.com"
                        name="email"
                        required
                        maxLength={255}
                        value={fromProps.values.email}
                        
                        onChange={fromProps.handleChange}
                        isInvalid={props.isInvalidField(fromProps, 'email')}
                      />
                      <Form.Control.Feedback type="invalid">
                        {props.getInvalidText(fromProps, 'email')}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group controlId="password" className="mt-4">
                      <Form.Label className="m-0">
                        <b>Password</b>
                      </Form.Label>
                      <Form.Control
                        type={props.isShowPassword?'text':'password'}
                        placeholder="*******"
                        name="password"
                        value={fromProps.values.password }
                        onChange={fromProps.handleChange}
                        maxLength={255}
                        required
                        isInvalid={props.isInvalidField(fromProps, 'password')}
                        />
                        <img className="pwd-eye" onClick={ () => props.showHidePwd()} alt="" src="https://img.icons8.com/material-outlined/50/000000/visible--v1.png"/>
                        <Form.Control.Feedback type="invalid">
                          {props.getInvalidText(fromProps, 'password')}
                        </Form.Control.Feedback>
                      </Form.Group>
                      {/* <div className="trms-cond-lnk">
                      <a target="_blank" href="https://dgnote.com/privacy-policy/download">Terms and conditions</a>
                      </div> */}
                    <Button type="submit" >Login</Button>
                    <Form.Group controlId="remember" className="d-inline-block mt-2">
                      {/* <i className="fas fa-user"></i> */}
                      <Form.Check type="checkbox" label="Remember me" />
                    </Form.Group>

                    <Form.Label className="float-end mt-1 f14">
                      <Link className="p-0" to="/password/forgot">

                      Forgot password?
                      </Link>
                    </Form.Label>

                    {/* <Container className="bookdemo">
                      {" "}
                      <Button variant="link" href="#">
                        Book A Demo
                      </Button>{" "}
                    </Container> */}
                  </div>
                </Form>
            </div>
          </Col>
        )}

      </Formik>
  

      

    </>
  );
};
