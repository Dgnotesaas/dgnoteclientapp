import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { login, updateProperties } from "../../redux/actions/login.action";
import { IAppState, ILoginModel } from "../../models";
import { IGenericReducerState } from "../../utilities";
import * as yup from 'yup';
import { Login } from "./login";
import { getIn } from "formik";
class LoginContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {

  invalidUser:Boolean=false;
  private formSchema: any;
  isShowPassword =false;
  public constructor(props) {
    super(props);

    this.showHidePwd = this.showHidePwd.bind(this);
    this.isInvalidField = this.isInvalidField.bind(this);
    this.getInvalidText = this.getInvalidText.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange=this.handleChange.bind(this);
    
  }

  showHidePwd(){
    this.isShowPassword = !this.isShowPassword;
    this.setState({isShowPassword: this.isShowPassword});
  }

  public handleChange(e:any) {
    const { name, value } = e.target;
    //this.props.loginState.Payload[name]=value
    this.manageTargetValue(name, value);
    //this.props.dispatch(updateProperties(this.props.loginState.Payload));
  }

  manageTargetValue(name, value){
    this.props.loginState.Payload[name] = value;
    this.props.dispatch(updateProperties(this.props.loginState.Payload));
  }

  loadSchema() {
    return (this.formSchema = yup.object().shape({
      email: yup.string().required("Enter Email").email("Enter Valid email"),
      password: yup.string().required("Enter Password"),
    })
    )
  }

  isInvalidField(fromProps, name): boolean {
    return getIn(fromProps.touched, name) && getIn(fromProps.errors, name);
  }

  getInvalidText(fromProps, name) {
    return this.isInvalidField(fromProps, name)
      ? getIn(fromProps.errors, name)
      : null;
  }

  public componentWillMount() {
    
  }
  
  public handleSubmit(e) {
 
    let hostName: any;

    if(window.location.hostname == 'localhost') {
      hostName = "dev1.tms.dgnote.com";
    } else {
      hostName = window.location.hostname
    }
    this.props.loginState.Payload.email = e.email;
    this.props.loginState.Payload.password = e.password;
    this.props.loginState.Payload.domain = hostName;
    this.invalidUser = false;
    this.props.dispatch(login(this.props.loginState.Payload));  
  }

  public render() {

    if (this.props.loginState.Error && this.props.loginState.Error === "invalid model") {
      this.invalidUser = true;
      setTimeout(() => {
        this.props.loginState.Error= "";
        this.invalidUser = false;
        this.setState({});
      }, 3000);
    }

    const model = {
      isLoading: this.props.loginState.Fetching,
      showFail:this.invalidUser,
      state: {...this.props.loginState.Payload},
      handleSubmit: this.handleSubmit,
      handleChange: this.handleChange,
      schema: this.loadSchema(),
      isInvalidField: this.isInvalidField,
      getInvalidText: this.getInvalidText,
      showHidePwd: this.showHidePwd,
      isShowPassword : this.isShowPassword,
    };
    return <Login {...model} />;
  }
}
interface IConnectState {
  loginState: IGenericReducerState<ILoginModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
  loginState: state.Login
});

interface IConnectDispatch {
  dispatch: any;
}
export default connect(mapStateToProps)(LoginContainer);
