import * as React from "react";
import { Button, ButtonGroup, Col, Form, Row, ToggleButton } from "react-bootstrap";
import { MomentTypeEnum } from "../../utilities/enum";


export const TemplateSetting = (props: any) => {




    return (




        <Row>

            <Col md={12}>
                

                    <Row>
                        <Col md={12}>
                            <h3 className="font-weight-bold mb-0">Movement details</h3>
                        </Col>
                       
                    </Row>

                    <Form noValidate >
                        <Row>
                            <Col className="pl-3 pt-5">
                                <button type="button" className="btn btn-warning btn-rounded btn-fw btn-sm">Single Trip</button>
                                <button type="button" className="btn btn-rounded btn-fw btn-sm btn-light">Return Trip</button>
                                {/* <button type="button" className="btn btn-rounded btn-fw btn-sm btn-light">Multiple Trip</button> */}
                            </Col>
                        </Row>
                        <Row className=" mt-3 mb-3">
                            <Col md={4}>
                                <Col className="border-r p-3">
                                    <Form.Group className="mt-3"  >
                                        <Form.Label className="m-0 mb-1">From Location</Form.Label>
                                        <Form.Control
                                            placeholder="Mumbai"
                                        />

                                    </Form.Group>


                                </Col>
                            </Col>
                            <Col md={1} className=" text-center pt-5"><i className="ti-angle-double-right"></i></Col>

                            <Col md={4}>
                                <Col className="border-r p-3">
                                    <Form.Group className="mt-3"  >
                                        <Form.Label className="m-0 mb-1">To Location</Form.Label>
                                        <Form.Control
                                            placeholder="Delhi"

                                        />

                                    </Form.Group>


                                </Col>
                            </Col>
                        </Row>
                        <Row className=" pt-4 pb-4">
                            <Col md={12} className=" d-flex flex-column justify-content-center">
                                <h5>Transit Details</h5>

                                <Col className="border p-3">
                                    <Row>
                                        <Col md={11}>
                                            <Row>
                                                {props.state.tripFields.map((trip, index) =>

                                                    <Col md={3} className="mt-2" key={index}>
                                                        <Form.Group  >
                                                            <Form.Label className="col-form-label">{props.convertCamelCaseToText(trip.fieldName)} </Form.Label>
                                                            {/* <Form.Label className="col-form-label">{props.convertCamelCaseToText(trip.fieldName) + "-" + trip.isShow} </Form.Label> */}
                                                            {
                                                                !props.isHideToggle(trip.fieldName) ?
                                                                    <div>
                                                                        <ToggleButton className="mr-1"
                                                                            id={"toggle-check-show-t-" + trip.fieldName}
                                                                            type="checkbox"
                                                                            variant={trip.isShow ? "success" : "secondary"}
                                                                            checked={trip.isShow}
                                                                            value={trip.isShow}
                                                                            onChange={(e) => props.setSetting(e.currentTarget.checked, "tripFields", trip.fieldName, "show")}
                                                                        >
                                                                            Show
                                                                        </ToggleButton>

                                                                        <ToggleButton
                                                                        disabled= {!trip.isShow}
                                                                            id={"toggle-check-isrequired-t-" + trip.fieldName}
                                                                            type="checkbox"
                                                                            variant={trip.isRequired ? "success" : "secondary"}
                                                                            checked={trip.isRequired}
                                                                            value={trip.isRequired}
                                                                            onChange={(e) => props.setSetting(e.currentTarget.checked, "tripFields", trip.fieldName, "required")}
                                                                        >
                                                                            Required
                                                                        </ToggleButton>
                                                                    </div>
                                                                    : ""

                                                            }

                                                            {
                                                                props.getElementType(trip.fieldName) === "select" ?
                                                                    <Form.Select
                                                                    >
                                                                        <option>Select value</option>

                                                                    </Form.Select>
                                                                    :
                                                                    <Form.Control
                                                                        placeholder={props.convertCamelCaseToText(trip.fieldName)}
                                                                        type={props.getElementType(trip.fieldName)}
                                                                    />
                                                            }


                                                        </Form.Group>
                                                    </Col>
                                                )}
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>

                            </Col>
                        </Row>
                        <Col className="border-bottom-1 pb-4"></Col>
                        <Row className=" pb-4 pt-4">
                            <Col md={12} className=" d-flex flex-column justify-content-center">
                                <h5>LR Details</h5>
                                <Row>


                                    {props.state.movementDetailsFields.map((m, index) =>

                                        <Col md={3} key={index}>
                                            <Form.Group  >
                                                <Form.Label className="col-form-label">{props.convertCamelCaseToText(m.fieldName)}</Form.Label>

                                                <div>
                                                    <ToggleButton className="mr-1"
                                                        id={"toggle-check-show-m-" + m.fieldName}
                                                        type="checkbox"
                                                        variant={m.isShow ? "success" : "secondary"}
                                                        checked={m.isShow}
                                                        value={m.isShow}
                                                        onChange={(e) => props.setSetting(e.currentTarget.checked, "movementDetailsFields", m.fieldName, "show")}
                                                    >
                                                        Show
                                                    </ToggleButton>

                                                    <ToggleButton
                                                    disabled= {!m.isShow}
                                                        id={"toggle-check-isrequired-m-" + m.fieldName}
                                                        type="checkbox"
                                                        variant={m.isRequired ? "success" : "secondary"}
                                                        checked={m.isRequired}
                                                        value={m.isRequired}
                                                        onChange={(e) => props.setSetting(e.currentTarget.checked, "movementDetailsFields", m.fieldName, "required")}
                                                    >
                                                        Required
                                                    </ToggleButton>
                                                </div>


                                                {
                                                    props.getElementType(m.fieldName) === "select" ?
                                                        <Form.Select
                                                        >
                                                            <option>Select Value</option>
                                                        </Form.Select>
                                                        :
                                                        <Form.Control
                                                            placeholder={props.convertCamelCaseToText(m.fieldName)}
                                                            type={props.getElementType(m.fieldName)}
                                                        />
                                                }


                                            </Form.Group>
                                        </Col>


                                    )}


                                </Row>
                            </Col>
                        </Row>
                        <Col className="border-bottom-1 pb-4"></Col>
                        <Row className="pb-4 pt-4">
                            <Col md={12}>
                                <h5>Additional Details</h5>
                                <Row>

                                    {props.state.additionalDetailsFields.map((m, index) =>

                                        <Col md={3} key={index}>
                                            <Form.Group  >
                                                <Form.Label className="col-form-label">{props.convertCamelCaseToText(m.fieldName)}</Form.Label>


                                                <div>
                                                    <ToggleButton className="mr-1"
                                                        id={"toggle-check-show-a-" + m.fieldName}
                                                        type="checkbox"
                                                        variant={m.isShow ? "success" : "secondary"}
                                                        checked={m.isShow}
                                                        value={m.isShow}
                                                        onChange={(e) => props.setSetting(e.currentTarget.checked, "additionalDetailsFields", m.fieldName, "show")}
                                                    >
                                                        Show
                                                    </ToggleButton>

                                                    <ToggleButton
                                                     disabled= {!m.isShow}
                                                        id={"toggle-check-isrequired-a-" + m.fieldName}
                                                        type="checkbox"
                                                        variant={m.isRequired ? "success" : "secondary"}
                                                        checked={m.isRequired}
                                                        value={m.isRequired}
                                                        onChange={(e) => props.setSetting(e.currentTarget.checked, "additionalDetailsFields", m.fieldName, "required")}
                                                    >
                                                        Required
                                                    </ToggleButton>
                                                </div>


                                                {
                                                    props.getElementType(m.fieldName) === "select" ?
                                                        <Form.Select
                                                        >
                                                            <option>Select Value</option>
                                                        </Form.Select>
                                                        :
                                                        <Form.Control
                                                            placeholder={props.convertCamelCaseToText(m.fieldName)}
                                                            type={props.getElementType(m.fieldName)}
                                                        />
                                                }


                                            </Form.Group>
                                        </Col>


                                    )}


                                </Row>
                            </Col>
                        </Row>
                        <Col className="border-bottom-1"></Col>
                        <Row className="pb-4 pt-4">
                            <Col md={12} className=" d-flex flex-column justify-content-center">
                                <Row className=" align-self-center">
                                    <Button type="button" onClick={() => props.submit()} className="btn button-theme btn-rounded text-uppercase mr-3">
                                        Save
                                    </Button>
                                    <Button type="button" className="btn btn-secondary btn-rounded text-uppercase mt-2">
                                        Cancel
                                    </Button>
                                </Row>
                            </Col>
                        </Row>
                    </Form>



              
            </Col>
        </Row>



    );

};