
import * as React from "react";
import { connect } from "react-redux";
import { AppContainer } from "../app.container";
import {  IAppState } from "../../models";
import { IGenericReducerState } from "../../utilities";
import { getTemplateSettings, updateProperties, updateTemplate } from "../../redux/actions/template-settings.action";
import { ITemplateSettingModel } from "../../models/template-setting.model";
import { TemplateSetting } from "./template-setting";

class TemplateSettingContainer extends AppContainer<IConnectState> {

  fixedControls :string[];

  public constructor(props) {
    super(props);
    this.fixedControls = ['customerName', 'driverMobileNumber', 'vehicleNumber','transporterName','transitStartDate','transitEstimatedEndDate'];
    this.submit = this.submit.bind(this);
    this.isHideToggle = this.isHideToggle.bind(this);
    this.setSetting=this.setSetting.bind(this);
    this.convertCamelCaseToText=this.convertCamelCaseToText.bind(this);
    this.getElementType=this.getElementType.bind(this);
    this.getSettings();
  }


  public getSettings(){
   this.props.dispatch(getTemplateSettings());
  }
  public convertCamelCaseToText(field: string): string {
    const result = field.replace(/([A-Z])/g, " $1");
    let finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    if(finalResult.includes('Validaity')){
      finalResult = finalResult.replace('Validaity', 'Validity')
    }
    return finalResult;
}

public isHideToggle(fieldName){
  return this.fixedControls.includes(fieldName);
}

public getElementType(field: string): string {
    field = field.toLowerCase();
    if (field.includes("pinginterval") || field.includes("movementtype")) {
        return "select"
    }
    else if (field.includes("date")) {
        return "date";
    } else {
        return "text";
    }
}
 public setSetting(value, objectName, field,type){
     let state = this.props.templateSettingState.Payload;
     state[objectName] = [
         ...state[objectName].map(x => {
             if (x.fieldName === field) {
                 if(type==="show"){
                  x.isShow= value;
                  x.isRequired= value ? x.isRequired : false;
                 }
                 else if (type === "required"){
                  x.isRequired= value;
                 }
             }
             return x;
         })
        ]; 
        this.props.dispatch(updateProperties(this.props.templateSettingState.Payload));
 }


 


  public submit(values: any) {

    this.props.dispatch(updateTemplate(this.props.templateSettingState.Payload.id,this.props.templateSettingState.Payload));
  }

 
  

  public render() {

    let model = {
      state: this.props.templateSettingState.Payload,
      submit: this.submit,
      setSetting: this.setSetting,
      getElementType:this.getElementType,
      convertCamelCaseToText:this.convertCamelCaseToText,
      isHideToggle: this.isHideToggle
    
    };
 
    return <TemplateSetting {...model} />;
  }
}
interface IConnectState {
  templateSettingState: IGenericReducerState<ITemplateSettingModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
  templateSettingState: state.TemplateSetting
});

export default connect(mapStateToProps)(TemplateSettingContainer);