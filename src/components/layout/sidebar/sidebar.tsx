import * as React from "react";
import { Link } from "react-router-dom";
// import { ProSidebar, Menu, MenuItem, SubMenu,SidebarHeader,SidebarContent,SidebarFooter } from 'react-pro-sidebar';
// import 'react-pro-sidebar/dist/css/styles.css';


export const SideBar = (props:any) => {

  return (
    <>
      {/* <ProSidebar >
<SidebarHeader>
    
  </SidebarHeader>
  <SidebarContent>
  <Menu iconShape="square">
                  {
                       props.paths.map((p,i) =>
                       <MenuItem key="i">  {p.displayName}
                       <Link to="{p.route}" />
                       </MenuItem>
                       )
                  }
                 
                 
              </Menu>
  </SidebarContent>
  <SidebarFooter>
  
  </SidebarFooter>

             
          </ProSidebar> */}

      <nav className="sidebar sidebar-offcanvas" id="sidebar">
        <ul className="nav">
          {props.paths.map((p, i) => {
            if (p.child.length > 0) {
             return <li key={i} className="nav-item">
                <a className="nav-link nav-collapser" data-toggle="collapse" aria-expanded="false" aria-controls={p.areaControl} onClick={(e)=>{e.preventDefault();props.showSubMenu(e, (p.child && p.child.length > 0 ) ? true : false)}} href={"#"+p.areaControl}>
                  <i className={p.iconClass}></i>
                  <span className="menu-title">{p.displayName}</span><i className="menu-arrow"></i> 
                </a>
                <div className="collapse" id={p.areaControl}>
                  <ul className="nav flex-column sub-menu">
                    {p.child.map((c, j) => {
                      return <li key={j} className="nav-item">
                        <Link className="nav-link" to={c.route}>
                          {c.displayName}
                        </Link>
                      </li>;
                    })}
                  </ul>
                </div>
              </li>;
            } else {
              return <li key={i} className="nav-item" onClick={(e)=>{e.preventDefault();props.showSubMenu(e, (p.child && p.child.length > 0 ) ? true : false)}}>
                <Link className="nav-link nav-link-no-child " to={p.route}>
                  <i className={p.iconClass}></i>
                  <span className="menu-title">{p.displayName}</span>
                </Link>
              </li>;
            }
          })}
          {/* <li className="nav-item"> <a className="nav-link" href="javascript:void(0)"> <i className="ti-ticket menu-icon"></i> <span className="menu-title">Dashboard</span> </a> </li>
      <li className="nav-item active"> <a className="nav-link" href="javascript:void(0)"> <i className="ti-file menu-icon"></i> <span className="menu-title">Transists</span> </a> </li>
      <li className="nav-item"> <a className="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic"> <i className="ti-palette menu-icon"></i> <span className="menu-title">Tracking</span> <i className="menu-arrow"></i> </a>
          <div className="collapse" id="ui-basic">
              <ul className="nav flex-column sub-menu">
                  <li className="nav-item"> <a className="nav-link" href="#">Demo 1</a></li>
                  <li className="nav-item"> <a className="nav-link" href="#">Demo 2</a></li>
              </ul>
          </div>
      </li>
      <li className="nav-item"> <a className="nav-link" href="#"> <i className="ti-layout-list-post menu-icon"></i> <span className="menu-title">Consent Intelligence & Rules</span> </a> </li>
      <li className="nav-item"> <a className="nav-link" href="#"> <i className="ti-pie-chart menu-icon"></i> <span className="menu-title">Notification Panel</span> </a> </li>
      <li className="nav-item"> <a className="nav-link" href="#"> <i className="ti-view-list-alt menu-icon"></i> <span className="menu-title">Digital Documents</span> </a> </li>
      <li className="nav-item"> <a className="nav-link" href="#"> <i className="ti-user menu-icon"></i> <span className="menu-title">Customer Management</span> </a> </li>
      <li className="nav-item"> <a className="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth"> <i className="ti-user menu-icon"></i> <span className="menu-title">Vendor Management</span> <i className="menu-arrow"></i> </a>
          <div className="collapse" id="auth">
              <ul className="nav flex-column sub-menu">
                  <li className="nav-item"> <a className="nav-link" href="#"> Demo 1 </a></li>
                  <li className="nav-item"> <a className="nav-link" href="#"> Demo 2 </a></li>
                  <li className="nav-item"> <a className="nav-link" href="#"> Demo 3 </a></li>
                  <li className="nav-item"> <a className="nav-link" href="#"> Demo 4 </a></li>
                  <li className="nav-item"> <a className="nav-link" href="#"> Demo 5 </a></li>
              </ul>
          </div>
      </li>
      <li className="nav-item"> <a className="nav-link" href="#"> <i className="ti-settings menu-icon"></i> <span className="menu-title">Transporter & vehical</span> </a> </li>
      <li className="nav-item"> <a className="nav-link" href="#"> <i className="ti-folder menu-icon"></i> <span className="menu-title">User Management</span> </a> </li>
      <li className="nav-item"> <a className="nav-link" href="#"> <i className="ti-wallet menu-icon"></i> <span className="menu-title">MIS & Reporting</span> </a> </li> */}
        </ul>
      </nav>
    </>
  );
}
