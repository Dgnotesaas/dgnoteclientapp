import * as React from "react";
import $ from "jquery";

import { SideBar } from "./sidebar";
import { IGenericReducerState } from "../../../utilities";
import { IAppState, IUserModel } from "../../../models";

import { RouteComponentProps } from "react-router-dom";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { UserPermissions } from "../../../utilities/enum/user_permission.enum";
class SideBarContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {
  currentPath: string = window.location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '')
  constructor(props) {
    super(props);
    this.addActiveClass = this.addActiveClass.bind(this);
  }
  componentDidMount() {

    var body = $('body');
    var contentWrapper = $('.content-wrapper');
    var scroller = $('.container-scroller');
    var footer = $('.footer');
    var sidebar = $('.sidebar');
    $(document).on('mouseenter mouseleave', '.sidebar .nav-item', function (ev) {

      var sidebarIconOnly = body.hasClass("sidebar-icon-only");
      var sidebarFixed = body.hasClass("sidebar-fixed");
      if (!('ontouchstart' in document.documentElement)) {
        if (sidebarIconOnly) {
          if (sidebarFixed) {
            if (ev.type === 'mouseenter') {
              body.removeClass('sidebar-icon-only');
            }
          } else {

            var $menuItem = $(ev.currentTarget);
            if (ev.type === 'mouseenter') {
              $menuItem.addClass('hover-open')
            } else {
              $menuItem.removeClass('hover-open')
            }
          }
        }
      }
    });
    var anchors = $(".nav", sidebar).find("li a");
    anchors.map((i, item) => {
      var $this = $(item);
      this.addActiveClass($this);
    })
    //Close other submenu in sidebar on opening any
    $(sidebar).on('show.bs.collapse', '.collapse', function () {
      sidebar.find('.collapse.show').collapse('hide');
    });
    $('[data-toggle="minimize"]').on("click", function () {

      body.toggleClass('sidebar-icon-only');
    });


  }
  //Add active class to nav-link based on url dynamically
  //Active class can be hard coded directly in html file also as required

  addActiveClass(element) {
    this.currentPath = window.location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '');
    if (this.currentPath === "") {
      //for root url
      if (element.attr('href').indexOf("/dashboard") !== -1) {
        element.parents('.nav-item').last().addClass('active');
        element.attr("aria-expanded", true);
        if (element.parents('.sub-menu').length) {
          element.closest('.collapse').addClass('show');
          element.addClass('active');

        }
      }
    } else {
      //for other url
      if (element.attr('href').indexOf(this.currentPath) !== -1) {
        element.parents('.nav-item').last().addClass('active');
        element.attr("aria-expanded", true);
        if (element.parents('.sub-menu').length) {
          element.closest('.collapse').addClass('show');
          element.addClass('active');

        }
        if (element.parents('.submenu-item').length) {
          element.addClass('active');
        }
      }
    }
  }
  showSubMenu(e, hasChild) {
    if (hasChild) {
      let subMenu = e.currentTarget.nextElementSibling;
      if ($(e.currentTarget).attr("aria-expanded") === 'false') {
        $('.sidebar').find('.collapse.show').toggleClass('show');
        $('.sidebar').find('.nav-collapser').removeClass("collapsed").attr("aria-expanded", false);

        // $(e.currentTarget).removeClass("collapsed")
        // $(e.currentTarget).attr("aria-expanded",true);
        // $(subMenu).addClass("show");   
        $('.sidebar').find('.nav-link-no-child').removeClass("collapsed").attr("aria-expanded", false);
      } else {
        $(subMenu).removeClass("show");
        $(e.currentTarget).addClass("collapsed")
        $(e.currentTarget).attr("aria-expanded", false);

      }
    }
    else {
      $('.sidebar').find('.collapse.show').toggleClass('show');
      $('.sidebar').find('.nav-collapser').removeClass("collapsed").attr("aria-expanded", false);
      $('.sidebar').find('.nav-link-no-child').removeClass("collapsed").attr("aria-expanded", false);
    }


  }
  public render() {

    let sidebar = $('.sidebar');
    var anchors = $(".nav", sidebar).find("li a");
    anchors.map((i, item) => {
      var $this = $(item);
      $this.parents('.nav-item').last().removeClass("active");
      $this.removeClass("active");
      this.addActiveClass($this);
      return item;
    })

    let permissionType = ''; 
    if (this.props.userState.Payload && this.props.userState.Payload.role) {
      permissionType = this.props.userState.Payload.role.name;
    } 
    console.log(permissionType);
    
    let props : any = [];

    if(permissionType != 'SUPERADMIN' ) {
      props = {

        paths: [
          { key: 'User Dashboard', isModule: false, areaControl: "User_Dashboard", displayName: 'Dashboard', route: '/dashboard', permission: [UserPermissions.User_Dashboard], iconClass: 'ti-home menu-icon ml-1', child: [] },
          {
            key: 'Booking', isModule: true, areaControl: "booking", displayName: 'Telematics', route: '/booking', iconClass: 'ti-agenda menu-icon ml-1', 
            child: [
              { key: 'Add Booking', permission: [UserPermissions.Add_Booking, UserPermissions.Modify_Booking], areaControl: "Add_Booking", displayName: 'Add Booking', route: '/booking/add', child: [] },
              { key: 'List Bookings', permission: [UserPermissions.List_Bookings], areaControl: "Active", displayName: 'Listing', route: '/booking-list/active', child: [] },
              { key: 'Track Vehicle', permission: [UserPermissions.Track_Vehicle, UserPermissions.Share_Tracking], areaControl: "All_Vehicle", displayName: 'Tracking', route: '/tracking/all_vehicle', child: [] },
              { key: 'Consent Log', permission: [UserPermissions.Send_Consent], areaControl: "Consent_Log", displayName: 'Consent', route: '/consentlog', child: [] },
              { key: 'Template Setting', isModule: false, areaControl: "Template_Setting", displayName: 'Template Setting', route: '/template-setting', iconClass: 'ti-panel menu-icon ml-1', child: [] }
              // { key: 'List Bookings', permission: [UserPermissions.List_Bookings], areaControl: "Completed", displayName: 'Completed', route: '/booking-list/completed', child: [] },
            ]
          },
          // {
          //   key: 'Tracking', isModule: true, areaControl: "tracking", displayName: 'Tracking', route: '/trackings', iconClass: 'ti-map-alt menu-icon ml-1',
          //   child: [
          //     { key: 'Track Vehicle', permission: [UserPermissions.Track_Vehicle, UserPermissions.Share_Tracking], areaControl: "All_Vehicle", displayName: 'All Vehicle', route: '/tracking/all_vehicle', child: [] },
          //     { key: 'Track Vehicle', permission: [UserPermissions.Track_Vehicle, UserPermissions.Share_Tracking], areaControl: "Track_Vehicle", displayName: 'Track Vehicle', route: '/tracking/tracking-vehicle', child: [] },
          //   ]
          // },
          {
            key: 'User Management', isModule: true, areaControl: "usermanagement", displayName: 'User Management', route: '/usermanagement', iconClass: 'ti-user menu-icon ml-1',
            child: [
              { key: 'Create User',permission:[UserPermissions.Create_User,UserPermissions.Modify_User],areaControl:"All_user", displayName: 'Add User', route: '/usermanagement/user/create-user', child: [] },
              { key: 'List Users',permission:[UserPermissions.List_Users],areaControl:"All_user", displayName: 'Users List', route: '/usermanagement/user/user-list', child: [] },
              { key: 'Create Role',permission:[UserPermissions.Create_Role,UserPermissions.Modify_Role],areaControl:"All_user", displayName: 'Add Role', route: '/usermanagement/role/create-role', child: [] },
              { key: 'List Roles',permission:[UserPermissions.List_Roles],areaControl:"All_user", displayName: 'Roles List', route: '/usermanagement/role/role-list', child: [] },
            ]
          },

          // {
          //   key: 'Consent', isModule: true, areaControl: "Consent", displayName: 'Consent', route: '/consentlog', iconClass: 'ti-menu menu-icon ml-1',
          //   child: [
          //     { key: 'Consent Log', permission: [UserPermissions.Send_Consent], areaControl: "Consent_Log", displayName: 'Consent Log', route: '/consentlog', child: [] },
          //   ]
          // },
          
          // { key: 'List Alert', isModule: false, areaControl: "Alert_Module", displayName: 'Alert Module', route: '/alert-module/alertmodule', iconClass: 'ti-bell menu-icon ml-1', child: [] },

          // { key: 'Template Setting', isModule: false, areaControl: "Template_Setting", displayName: 'Template Setting', route: '/template-setting', iconClass: 'ti-panel menu-icon ml-1', child: [] }


        ],

        showSubMenu: this.showSubMenu.bind(this)
      } 
    } else {
      props = {

        paths: [  
            {
                key: 'Health List', isModule: true, areaControl:"health_list",displayName: 'Health Check', route: '/health',iconClass:'ti-truck menu-icon ml-1',
                child: [ 
                  { key: 'Health List',permission:[UserPermissions.Health_List], areaControl:"health_list",displayName: 'TMS Health Checkup', route: '/health-list/healthlist/active', child: [] }, 
                  { key: 'Health Log',permission:[UserPermissions.Health_List], areaControl:"health_list",displayName: 'TMS Health Log', route: '/health-list/healthlist/completed', child: [] }, 
                  // { key: 'Consent List',permission:[UserPermissions.Send_Consent], areaControl:"health_list",displayName: 'Consent Checkup', route: '/consent-list/healthlog', child: [] }, 
                ]
            } 


        ],

        showSubMenu: this.showSubMenu.bind(this)
      }
    }

    if (this.props.userState.Payload && this.props.userState.Payload.permissions) {

      const userPermissions = [...this.props.userState.Payload.permissions];
      const allowedScreens = userPermissions.map(m => m.name);
      let paths = props.paths;

      // props.perm = userPermissions;
      paths = paths.filter(x => (!x.isModule && userPermissions.map(m => m.name)) || (x.isModule && userPermissions.map(m => m.moduleName)));
      let filteredPaths = paths.filter(r => {
        if (r.child && r.child.length > 0) {
          r.child = r.child.filter(x => allowedScreens.includes(x.key) || x.permission.some(t => allowedScreens.includes(t)));
          if (r.child.length > 0) {
            return r;
          }

        } else if (allowedScreens.includes(r.key)) {
          return r;
        }

      });


      props.paths = filteredPaths;

    }
 
    // props.paths.push({ key: 'Change Password', isModule: false, areaControl: "Change_Password", displayName: 'Change Password', route: '/password/change', iconClass: 'ti-key menu-icon ml-1', child: [] })

    return <SideBar {...props}/>;
  }
}
interface IConnectState {
  userState: IGenericReducerState<IUserModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
  userState: state.User
});

interface IConnectDispatch {
  dispatch: any;
}
export default withRouter(connect(mapStateToProps)(SideBarContainer));