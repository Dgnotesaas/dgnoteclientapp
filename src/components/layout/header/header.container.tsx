import * as React from "react";
import { logout } from "../../../redux/actions/login.action";
import { connect } from "react-redux";

import { Header } from "./header";
import { IGenericReducerState } from "../../../utilities";
import { IAppState, IUserModel } from "../../../models";
import { withRouter } from "react-router";
import { RouteComponentProps } from "react-router-dom";
import $ from "jquery";
class HeaderContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {

    public constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
      }

    logout(){
       this.props.dispatch(logout());  
    }

    public render() {

        let model= {
            user:this.props.userState.Payload,
            logout:this.logout
        }

        return <Header {...model} />;
    }
}
interface IConnectState {
    userState: IGenericReducerState<IUserModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
    userState: state.User
});

interface IConnectDispatch {
    dispatch: any;
}
export default  withRouter(connect(mapStateToProps)(HeaderContainer));