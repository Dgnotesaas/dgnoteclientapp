import * as React from "react";
import { logout } from "../../../redux/actions/login.action";
import { NavDropdown} from 'react-bootstrap';

export const Header = (props:any) => {
    const navDropdownTitle = (<i style={{'fontSize': '20px', color: '#333', 'border': '2px solid #333'}} className="rounded-circle ti-user menu-icon ml-1 p-1 "></i>)
    return (
        <nav className="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row bg-menu">
        <div className="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center"> <a className="navbar-brand brand-logo mr-5" href="index.html"><img src="https://dgnote.com//images/logo.jpg" className="mr-2" alt="logo" /></a> <a className="navbar-brand brand-logo-mini" href="index.html"><img src="https://dgnote.com//images/logo.jpg" alt="logo" /></a> </div>
        <div className="navbar-menu-wrapper d-flex align-items-center justify-content-end">
            <button className="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize"> <span className="ti-view-list"></span> </button>
            <ul className="navbar-nav mr-lg-2 navbar-nav-right">

               
                <li className="nav-item nav-profile dropdown">
                   {/* <span title="logout" className="crsr-ptr" onClick={props.logout}>
                   <img alt="logout"  src="https://img.icons8.com/ios-glyphs/30/000000/logout-rounded--v1.png"/>
                       </span>  */}
                </li> 
            </ul>
            <NavDropdown title={navDropdownTitle} id="collasible-nav-dropdown">
                <NavDropdown.Item href={'../../password/change'}>Change Password</NavDropdown.Item>
                <NavDropdown.Item onClick={props.logout}>Logout</NavDropdown.Item>
            </NavDropdown>
            <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas"> <span className="ti-view-list"></span> </button>
        </div>
    </nav>
  );
};