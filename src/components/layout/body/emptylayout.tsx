import * as React from "react";
import { Col, Container, Row } from "react-bootstrap";

export class EmptyLayout  extends React.Component {
 
  render(){
      return (
    <div className="content">
      <Container>
      <Row>
      <Col md={7} className="pt-5 mb-3">
              <img src="https://dgnote.com//images/logo.jpg" width="137" height="40" alt="logo1" />
         </Col>   
      </Row>
        <Row>
          <Col md={7} className="pt-4">
            
            <h2 className="pt-2 pb-2">
              Find Easier,
              <br />
              Efficient &amp; Effective
              <br />
              ways of doing Business!
            </h2>
            <div>
              <img src="/images/l5.png" alt="Image2" className="img-fluid" />{" "}
            </div>
          </Col>
          {this.props.children}
        </Row>
      </Container>
    </div>
  );
  }
}


