import * as React from "react";
import { Col, Container, Row } from "react-bootstrap";
import SideBarContainer from "../sidebar/sidebar.container";
import HeaderContainer from "../header/header.container";
import { RouteComponentProps, withRouter } from "react-router";
import { IUserModel, IAppState } from "../../../models";
import { IGenericReducerState } from "../../../utilities";
import { connect } from "react-redux";
 export class DashboardLayout  extends React.Component<{}> { 
    constructor(props) {
    super(props);
}
    public render() {
  return (
        <div className="container-scroller">
       <HeaderContainer></HeaderContainer>
        <div className="container-fluid page-body-wrapper"> 
          
           <SideBarContainer></SideBarContainer>
            <div className="main-panel">
                <div className="content-wrapper">
                   {this.props.children}
              
            </div>
            
        </div>
       
    </div>
      </div>
      );
  }

}
