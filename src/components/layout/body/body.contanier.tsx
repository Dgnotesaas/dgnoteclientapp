
import * as React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { routes } from "../../../route.config";
import { connect } from "react-redux";
import { IAppState, IUserModel } from "../../../models";
import { EmptyLayout } from "./emptylayout";
import { DashboardLayout } from "./dashboardlayout";
import {
  IGenericReducerState
} from "../../../utilities";
import { ToastContainer, } from 'react-toastify';
import { Spinner } from "react-bootstrap";
import { Permissions } from "../../../utilities/app.config"

let Routes: any[] = [];
class MainLayout extends React.Component<IConnectState & IConnectDispatch> {
  constructor(props) {
    super(props);

    if (process.env.NODE_ENV === "development") {
      //localStorage.setItem("tenant","tenant_b");
    }

  }

  public render() {
    let isLogin = localStorage.getItem("Authorize") === "True"
    let geturl = window.location.href;
    if (geturl.search("tracksharing") == -1) {
      if (isLogin) { 
        let userPermissions: any = this.props.userState.Payload.permissions.map(x => {
          return x.name
        })
        Routes = routes
        .filter((x) => x.protected === true && x.permissions.some(t => userPermissions.includes(t)))
        .map((x, i) => (
          <Route key={i} path={x.Path} exact={true} component={x.Component} />
        ));
        return (
          <>
            {this.props.loadingState ?
              <div className="spinner-p">
                <Spinner animation="border" variant="primary" /> <br />
              </div>
              : ""}
            <DashboardLayout>
  
              <Switch>
                {Routes}
                <Redirect to="/" />
              </Switch>
              <ToastContainer limit={1} autoClose={2000}></ToastContainer>
            </DashboardLayout>
          </>
        );
    } else {

      Routes = routes
        .filter((x) => x.protected === false)
        .map((x, i) => (
          <Route key={i} path={x.Path} exact={true} component={x.Component} />
        )); 
        return (
          <>
            {this.props.loadingState ?
              <div className="spinner-p">
                <Spinner animation="border" variant="primary" /> <br />
              </div>
              : ""}
            <EmptyLayout>
              <Switch>
                {Routes}
                <Redirect to="/login" />
              </Switch>
              <ToastContainer limit={1} autoClose={2000}></ToastContainer>
            </EmptyLayout>
          </>
        );
    }
  } else {
    Routes = routes.map((x, i) => (
      <Route key={i} path={x.Path} exact={true} component={x.Component} />
    ));
    return (
      <>
        {this.props.loadingState ?
          <div className="spinner-p">
            <Spinner animation="border" variant="primary" /> <br />
          </div>
          : ""}
          <Switch>
            {Routes}
            <Redirect to="/" />
          </Switch>
          <ToastContainer limit={1} autoClose={2000}></ToastContainer>
      </>
    );
  }
  }
}
interface IConnectState {
  userState: IGenericReducerState<IUserModel>;
  loadingState: boolean;
}
interface IConnectDispatch {
  dispatch: any;
}
export const LoadMainLayout = connect((state: IAppState): IConnectState => ({
  userState: state.User,
  loadingState: getLoadingState(state)

})
)(MainLayout);

export class Layout extends React.Component<{}> {

  public render() {
    return <LoadMainLayout>{Routes}</LoadMainLayout>;
  }
}
function getLoadingState(state: IAppState): boolean {

  let loadingState: boolean = false;
  for (const [key, value] of Object.entries(state)) {
    if (value.Fetching === true) {
      loadingState = true;

      break;
    }
  }
  return loadingState;
}
