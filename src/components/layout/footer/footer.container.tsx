import * as React from "react";
import { Footer } from "./footer";

export class FooterContainer extends React.Component {
    public render() {
        return <Footer />;
    }
}