import * as React from "react";
import { RouteComponentProps } from "react-router-dom";


interface IConnectDispatch {
    dispatch: any;
}

export class AppContainer<T> extends React.Component<IConnectDispatch & T & RouteComponentProps<{}>> {
    public constructor(props) {
        super(props);
    }

}