import { Container, Row } from 'react-bootstrap';

function Footer() { 

    return (
        <div className="user-mgt-custom">
            <Container>
                <Row className="mt-4">
                    <div className="col-md-12 mb-4">
                        <p>Regards,</p>
                        <p>DgNote Team</p>
                    </div> 
                </Row>
            </Container>
        </div>
    )
}

export default Footer;