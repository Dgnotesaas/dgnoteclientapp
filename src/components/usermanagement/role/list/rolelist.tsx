import { useState, useEffect } from 'react';
import { Container, Row, Form, Button, Modal } from 'react-bootstrap';
import '../css/user.css';
import { Link } from "react-router-dom";

function Rolelist({ rolelist }) { 

    let count = 1;

    const [show, setShow] = useState({modals: false, permissions: []});

    const handleClose = () => setShow({modals: false, permissions: []});
    const handleShow = (permission) => setShow({modals : true, permissions: permission });

    return (
        <div>
            <Container>
                <div className="row"> 

                    <Modal show={show.modals} onHide={handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Permissions</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                        <table className="table table-bordered">
                                <thead className="thead-dark">
                                    <tr>
                                        <th>Permission</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                            {
                                show.permissions.map((perm: any) => {
                                    return(<tr><td>{perm.name}</td><td>{perm.description}</td></tr>)
                                })
                            }
                            </table>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <div className="col-md-12 mb-4">
                        <h3 className="font-weight-bold mb-0">Roles List</h3>
                    </div>
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <table className="table table-bordered">
                                <thead className="thead-dark list-table">
                                    <tr>
                                        <th>No.</th>
                                        <th>Roles</th>
                                        <th>Created By</th>
                                        <th>Email Id</th>
                                        <th>Created Date</th>
                                        <th>Modify Date</th>
                                        <th>Permission</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                {Object.values(rolelist).map((item: any) => {
                                    if (item.name !== 'SUPERADMIN' && item.name !== 'ADMIN') {
                                    return (<tr>
                                        <td>{count++}</td>
                                        <td>{item.name}</td>
                                        <td>Kamlesh</td>
                                        <td>kamlesh@gmail.com</td>
                                        <td>20-09-2021</td>
                                        <td>21-09-2021</td>
                                        <td><Button href="#" className="btn btn-sm button-theme btn-rounded text-white" style={{ background: "#248afd" }} onClick={() => handleShow(item.permissions)}>View</Button></td>
                                        <td><Link  className="btn btn-sm button-theme btn-rounded text-white" style={{ background: "#082e39"}} to={'../../usermanagement/role/edit/'+ item.id} >Edit</Link></td>
                                    </tr>)
                                    }
                                })}
                            </table>
                        </div>
                    </div>
                </div>
            </Container>
        </div>
    )
}

export default Rolelist;