import * as React from "react";
import { connect } from "react-redux";
import { IAppState, defaultPermission } from "../../../../models";

import { IGenericReducerState } from "../../../../utilities";
import { RouteComponentProps } from "react-router-dom";
import { IRoleAllModel } from "../../../../models/roleall.model";
import { Subject } from "rxjs";
import Rolelist from "./rolelist";
import { getAllRole } from "../../../../redux/actions/role.action";

class RoleListContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {
 
  public constructor(props) {
    super(props);
    this.fetchList();
  }
  fetchList() {
    this.props.dispatch(getAllRole());
  } 

  public render() {
    let getRoleList = this.props.roleListState.Payload;
    return (<Rolelist rolelist={getRoleList} />);
  }

}
interface IConnectState {
  roleListState: IGenericReducerState<IRoleAllModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
  roleListState: state.RoleAll,
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(RoleListContainer);