import * as React from "react";
import { connect } from "react-redux";
import { IAppState, defaultPermission, defaultRole } from "../../../../models";

import { IGenericReducerState } from "../../../../utilities";
import { RouteComponentProps } from "react-router-dom";
import { IPermissionModel } from "../../../../models/permission.model";
import { IRoleAllModel } from "../../../../models/roleall.model";
import { Subject } from "rxjs";
import Role from "./role";
import { getAllPermission } from "../../../../redux/actions/permission.action";
import { postActiveRole, getSingleRole, updateRole } from "../../../../redux/actions/role.action";
import  { Redirect } from 'react-router-dom'

let listPermission: any[] = [];
let checklistID: any[] = [];

interface MyState {
  name: any,
  description: any,
  errors: {
    name: any,
    description: any,
  }
}

class RoleContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps, MyState> {

  searchModel = defaultPermission;
  searchString$: Subject<string> = new Subject<string>();
  listType: number = 1;
  roleId: string = '';
  isEditPage: boolean = false;

  public constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      errors: {
        name: '',
        description: '',
      }
    }
    this.fetchList();
    this.roleId = this.props.match.params["id"];
    if (this.roleId) {
      this.isEditPage = true;
    }
    if(!this.isEditPage) {
      this.props.roleListState.Payload = {...defaultRole};
    }
    this.handleChange = this.handleChange.bind(this);
  }

  async getRoleDetail() {
    if (this.isEditPage) {
      await this.props.dispatch(getSingleRole(this.roleId));
    }
  }

  public componentDidMount() {
    this.getRoleDetail()
  }

  public handleChange(e) {
    // e.preventDefault();
    const { name, value, checked } = e.target;
  let errors = this.state.errors;
  const specialChar = RegExp(/^[a-zA-Z ]{2,30}$/)

  switch (name) {
    case 'name':
      errors.name =
        specialChar.test(value)
          ? ''
          : 'Please enter valid name';
      this.setState({ name: value })
      break;
    case 'description':
      errors.description =
        (value > 2)
          ? 'Please enter valid description'
          : '';
      this.setState({ description: value })
      break;
     default:
      break;
  }

  this.setState({ errors: errors })
    if (name === 'permissions') {
      let checklistPermission: any[] = [];

      if (checked === true) {
        checklistID.push(value)
      } else {
        const index = checklistID.indexOf(value);
        if (index > -1) {
          checklistID.splice(index, 1);
        }
      }
      checklistID.map(getId => {
        checklistPermission.push(listPermission.filter(itemInArray => itemInArray.id === getId)[0]);
      })
      this.props.roleListState.Payload.permissions = checklistPermission;
    } else {
      this.props.roleListState.Payload[name] = value;
    }
  }

  public addRole() {
    
  }

  fetchList() {
    this.props.dispatch(getAllPermission());
  }

  handleSubmit = (event) => {

    event.preventDefault();

    if (!this.isEditPage) {
      let seterrors = this.state.errors;
      seterrors.name = (this.state.name === '') ? 'Please enter role name' : '';
      seterrors.description = (this.state.description === '') ? 'Please add description' : '';

      this.setState({ errors: seterrors })
    }

    if (this.validateForm(this.state.errors)) {
      console.info('Valid Form')
      if (this.props.roleListState.Payload.permissions.length > 0) {
        if (this.isEditPage) {
          this.props.dispatch(updateRole(this.props.roleListState.Payload));
        } else {
          this.props.dispatch(postActiveRole(this.props.roleListState.Payload));
        }
        this.props.history.push('/usermanagement/role/role-list')
      } else {
        alert('Please select role permission!')
        return false;
      }
    } else {
      console.error('Invalid Form')
      return false;
    }
  }

  validateForm(errors) {
    let valid = true;
    Object.values(errors).forEach(
      (val: any) => val.length > 0 && (valid = false)
    );
    return valid;
  }
  
  cancel() { 
    window.location.reload();
  }

  checked(id) {
    if (this.isEditPage) {
      let getAllrole = this.props.roleListState.Payload.permissions
      if (getAllrole) {
        if (getAllrole.some(el => el.id === id)) {
          return true
        } else {
          return false
        }
      }
    }
  }

  public render() {
    listPermission = Object.values(this.props.permissionListState.Payload);
    let model = {
      getPermissionList: this.props.permissionListState.Payload,
      initValue: this.props.roleListState.Payload,
      handleChange: this.handleChange,
      handleSubmit: this.handleSubmit,
      cancel: this.cancel,
      isEdit: this.isEditPage,
      errors: this.state.errors,
      checked: this.checked.bind(this),
      addRole: this.addRole.bind(this),
    };
    return <Role {...model} />;
  }
}
interface IConnectState {
  permissionListState: IGenericReducerState<IPermissionModel>;
  roleListState: IGenericReducerState<IRoleAllModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
  permissionListState: state.UserPermission,
  roleListState: state.RoleAll,
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(RoleContainer);