import * as React from "react";
import { connect } from "react-redux";
import { IAppState, defaultRole, defaultUsermanagement } from "../../../../models";

import { IGenericReducerState } from "../../../../utilities";
import { RouteComponentProps } from "react-router-dom";
import { IRoleAllModel } from "../../../../models/roleall.model";
import { Subject } from "rxjs";
import User from "./user";
import { getAllRole } from "../../../../redux/actions/role.action";
import { postUser, getSingleUser, getAllUsers, updateUser } from "../../../../redux/actions/usermanagement.action";
import { IUsermanagementModel } from "../../../../models/usermanagement.model";
import { IUserModel } from "../../../../models/user.model";

let listRole: any[] = [];
let checklistID: any[] = [];

interface MyState {
  firstName: any,
  lastName: any,
  email: any,
  password: any,
  reppassword: any,
  errors: {
    firstName: any,
    lastName: any,
    email: any,
    password: any,
    reppassword: any,
  }
}


class UserContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps, MyState> {

  searchModel = defaultRole;
  searchString$: Subject<string> = new Subject<string>();
  listType: number = 1;
  userId: string = '';
  isEditPage: boolean = false;

  public constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      reppassword: '',
      errors: {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        reppassword: '',
      }
    };

    this.userId = this.props.match.params["id"];
    if (this.userId) {
      this.isEditPage = true;
    }

    if(!this.isEditPage) {
      this.props.SendUser.Payload = {...defaultUsermanagement};
    }
    this.fetchList();
    this.handleChange = this.handleChange.bind(this);
  }

  async getUserDetail() {
    if (this.isEditPage) {
      await this.props.dispatch(getSingleUser(this.userId));
    }
  }

  public componentDidMount() {
    this.getUserDetail()
  }

  fetchList() {
    this.props.dispatch(getAllRole());
  }

  public handleChange(e) {

    // e.preventDefault();
    const { name, value, checked } = e.target;
    const validEmailRegex =
      RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
    let errors = this.state.errors;
    const specialChar = RegExp(/^[a-zA-Z ]{2,30}$/)
    const passwordExp = RegExp(/^(?=.*\d)(?=.*[!@#$%^&*]).{8,}$/)

    switch (name) {
      case 'firstName':
        errors.firstName =
          specialChar.test(value)
            ? ''
            : 'Please enter valid first name';
        this.setState({ firstName: value })
        break;
      case 'lastName':
        errors.lastName =
          specialChar.test(value)
            ? ''
            : 'Please enter valid last name';
        this.setState({ lastName: value })
        break;
      case 'email':
        errors.email =
          validEmailRegex.test(value)
            ? ''
            : 'Email is not valid!';
        this.setState({ email: value })
        break;
      case 'password':
        errors.password =
          passwordExp.test(value)
            ? '' : 'Password must be 8 characters long, one Number and one special case character!'
          ;
        this.setState({ password: value })
        break;
      case 'reppassword':
        errors.reppassword =
          (value === this.props.SendUser.Payload['password'])
            ? "" : "Passwords don't match"
          ;
        this.setState({ reppassword: value })
        break;
      default:
        break;
    }

    this.setState({ errors: errors })

    if (name === 'role') {
      let checklistRole: any[] = [];

      if (checked === true) {
        checklistID.push(value)
      } else {
        const index = checklistID.indexOf(value);
        if (index > -1) {
          checklistID.splice(index, 1);
        }
      }
      checklistID.map(getId => {
        checklistRole = listRole.filter(itemInArray => itemInArray.id === getId)[0];
      })
      this.props.SendUser.Payload.role = checklistRole;
    } else {
      if (name !== 'reppassword') {
        this.props.SendUser.Payload[name] = value;
      }
    }

    this.props.SendUser.Payload.companyDto = this.props.UserList.Payload['companyDto']
  }

  public addUser() {

  }

  handleSubmit = (event) => {

    event.preventDefault();

    if (!this.isEditPage) {
      let seterrors = this.state.errors;
      seterrors.firstName = (this.state.firstName === '') ? 'Please enter first name' : '';
      seterrors.lastName = (this.state.lastName === '') ? 'Please enter last name' : '';
      seterrors.email = (this.state.email === '') ? 'Please enter email' : '';
      seterrors.password = (this.state.password === '') ? 'Please enter password' : '';
      seterrors.reppassword = (this.state.reppassword === '') ? 'Please enter confirm password' : '';

      this.setState({ errors: seterrors })
    } 

    if (this.validateForm(this.state.errors)) {
      console.info('Valid Form', this.props.SendUser.Payload)

      // let getRoles : any[] = [];
      let getRoles : any = this.props.SendUser.Payload.role
      if (getRoles.length !== 0) {
        if (this.isEditPage) {
          this.props.dispatch(updateUser(this.props.SendUser.Payload));
        } else {
          this.props.dispatch(postUser(this.props.SendUser.Payload));
        }
        this.props.dispatch(getAllUsers());
        this.props.history.push('/usermanagement/user/user-list')
      } else {
        alert('Please select user role!')
        return false;
      }
    } else {
      console.error('Invalid Form')
      return false;
    }
  }

  cancel() { 
    window.location.reload();
  }

  validateForm(errors) {
    let valid = true;
    Object.values(errors).forEach(
      // if we have an error string set valid to false
      (val: any) => val.length > 0 && (valid = false)
    );

    return valid;
  }

  checked(id) {

    let getAlluser = this.props.SendUser.Payload.role
    if (getAlluser) {
      if (getAlluser['id'] === id) {
        return true;
      } else {
        return false
      }
    }
  }

  public render() {
    listRole = Object.values(this.props.roleListState.Payload);
    let model = {
      getRoleList: this.props.roleListState.Payload,
      initValue: this.props.SendUser.Payload,
      handleChange: this.handleChange,
      handleSubmit: this.handleSubmit,
      cancel: this.cancel,
      isEdit: this.isEditPage,
      errors: this.state.errors,
      checked: this.checked.bind(this),
      addUser: this.addUser.bind(this),
    };
    return <User {...model} />;
  }

}
interface IConnectState {
  roleListState: IGenericReducerState<IRoleAllModel>;
  SendUser: IGenericReducerState<IUsermanagementModel>;
  UserList: IGenericReducerState<IUserModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
  roleListState: state.RoleAll,
  SendUser: state.UserManagement,
  UserList: state.User,
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(UserContainer);