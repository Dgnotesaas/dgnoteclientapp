import { useState, useEffect } from 'react';
import { Container, Row, Form, Button } from 'react-bootstrap';
import '../../role/css/user.css';
import { Link } from 'react-router-dom';

function User(props: any) {

    return (
        <div>
            <div>
                <Row className="">
                    <div className="">
                        <h3 className="font-weight-bold mb-0" >User Details</h3>
                    </div>
                    <div className="formsection mt-4">
                        <Form onSubmit={props.handleSubmit} autoComplete="off">
                            <Row>
                                <div className="col-md-4">
                                    <Form.Group className="mb-2" controlId="formBasicEmail">
                                        <Form.Label className="small">First Name</Form.Label>
                                        <Form.Control type="text" name="firstName" onChange={props.handleChange} defaultValue={props.initValue.firstName} placeholder="Enter First Name" />
                                        <div className='errors'>
                                            {props.errors.firstName.length > 0 && props.errors.firstName}
                                        </div>
                                    </Form.Group>
                                </div>
                                <div className="col-md-4">
                                    <Form.Group className="mb-2" controlId="formBasicEmail">
                                        <Form.Label className="small">Last Name</Form.Label>
                                        <Form.Control type="text" name="lastName" onChange={props.handleChange} defaultValue={props.initValue.lastName} placeholder="Enter Last Name" />
                                        <div className='errors'>
                                            {props.errors.lastName.length > 0 && props.errors.lastName}
                                        </div>
                                    </Form.Group>
                                </div>
                                <div className="col-md-4">
                                    <Form.Group className="mb-2" controlId="formBasicEmail">
                                        <Form.Label className="small">Email</Form.Label>
                                        <Form.Control type="email" name="email" onChange={props.handleChange} defaultValue={props.initValue.email} placeholder="Enter Email" />
                                        <div className='errors'>
                                            {props.errors.email.length > 0 && props.errors.email}
                                        </div>
                                    </Form.Group>
                                </div>
                                <>
                                    {(!props.isEdit) ? (<><div className="col-md-4">
                                    <Form.Group className="mb-2" controlId="formBasicEmail">
                                        <Form.Label className="small">Password</Form.Label>
                                        <Form.Control type="password" name="password" onChange={props.handleChange} defaultValue={props.initValue.password} placeholder="Enter Password" />
                                        <div className='errors'>
                                            {props.errors.password.length > 0 && props.errors.password}
                                        </div>
                                    </Form.Group>
                                </div>
                                <div className="col-md-4">
                                    <Form.Group className="mb-2" controlId="formBasicEmail">
                                        <Form.Label className="small">Confirm Password</Form.Label>
                                        <Form.Control type="password" name="reppassword" onChange={props.handleChange} placeholder="Enter Confirm Password" />
                                        <div className='errors'>
                                            {props.errors.reppassword.length > 0 && props.errors.reppassword}
                                        </div>
                                    </Form.Group>
                                </div></>) : (<></>)}
                                </>
                                
                            </Row>
                            <hr />
                            <div className="mt-4 mb-2"><b>Roles Assignment</b></div>
                            <Row>
                                <div className="col-md-12">
                                    <table className="table table-bordered">
                                        <thead className="thead-dark">
                                            <tr className="list-table">
                                                <th className="tablehead">Roles</th>
                                                {/* <th > */}
                                                {/* <Form.Check
                                                    type="checkbox" onChange={props.handleChange}
                                                    id="all_check"
                                                    label="All Role"
                                                    /> */}
                                                <th style={{ 'textAlign': 'center' }} >
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {Object.values(props.getRoleList).map((allitem: any) => {
                                                if (allitem.name !== 'SUPERADMIN' && allitem.name !== 'ADMIN') {
                                                    return (
                                                        <tr>
                                                            <td className="tabledata">{allitem.name}</td>
                                                            <td>
                                                                <div className="custom-control custom-switch">
                                                                    <Form.Check
                                                                        type="checkbox"
                                                                        id="single_check"
                                                                        name="role"
                                                                        checked={props.checked(allitem.id)}
                                                                        // defaultChecked={true}
                                                                        value={allitem.id}
                                                                        onChange={props.handleChange}
                                                                        label=""
                                                                    />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                }
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="col-md-12 mt-3">
                                    <Button className="btn btn-sm button-theme btn-rounded text-white sub-button" type="submit" onClick={() => props.addUser()}>Submit</Button>
                                </div>
                            </Row>
                        </Form>
                        <div className="absub-button">
                            {(!props.isEdit) ? (<Button className="btn btn-sm button-theme btn-rounded text-white" type="submit" onClick={() => props.cancel()}>Cancel</Button>) : (<Link className="btn btn-sm button-theme btn-rounded text-white" style={{ background: "#082e39" }} to={'../user-list'} >Cancel</Link>)}
                        </div>
                    </div>
                </Row>
            </div>
        </div>
    )
}

export default User;