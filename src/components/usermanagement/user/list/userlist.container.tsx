import * as React from "react";
import { connect } from "react-redux";
import { IAppState, defaultPermission } from "../../../../models";

import { IGenericReducerState } from "../../../../utilities";
import { RouteComponentProps } from "react-router-dom";
import { IUsermanagementModel } from "../../../../models/usermanagement.model";
import Userlist from "./userlist";
import { getAllUsers } from "../../../../redux/actions/usermanagement.action";

class RoleListContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {
 
  public constructor(props) {
    super(props);
    this.fetchList();
  }
  fetchList() {
    this.props.dispatch(getAllUsers());
  } 

  public render() {
    let getUserList = this.props.userListState.Payload;
    return (<Userlist userlist={getUserList} />);
  }

}
interface IConnectState {
  userListState: IGenericReducerState<IUsermanagementModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
  userListState: state.UserManagement,
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(RoleListContainer);