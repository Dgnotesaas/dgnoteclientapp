import { useState, useEffect } from 'react';
import { Container, Row, Form, Button, Modal } from 'react-bootstrap';
import '../../role/css/user.css';
import { Link } from 'react-router-dom';

function Userlist({ userlist }) {

    let count = 1;

    const [show, setShow] = useState({ modals: false, roles: [] });

    const handleClose = () => setShow({ modals: false, roles: [] });
    const handleShow = (role) => setShow({ modals: true, roles: role });

    const setbody = () => {
        if (show.roles != null) {
            show.roles.map((getitem: any) => {
                // return(<tr><td>{getitem.name}</td><td>{getitem.description}</td></tr>)
            })
        } else {
            return (<tr></tr>)
        }
    }

    return (
        <div>
            <div>
                <div className="row">

                    <Modal show={show.modals} onHide={handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Roles</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <table className="table table-bordered">
                                <thead className="thead-dark">
                                    <tr>
                                        <th>Role</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                {setbody()}
                            </table>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <div className="col-md-12 mb-4">
                        <h3 className="font-weight-bold mb-0">User List</h3>
                    </div>
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <table className="table table-bordered">
                                <thead className="thead-dark list-table">
                                    <tr>
                                        <th>No.</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Permission</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                {Object.values(userlist).map((item: any) => {
                                    if (item.role != '' && typeof item.role !== 'undefined' && item.role != null) {
                                        if (item.role.name !== 'SUPERADMIN' && item.role.name !== 'ADMIN') {
                                            return (<tr>
                                                <td>{count++}</td>
                                                <td>{item.firstName}</td>
                                                <td>{item.lastName}</td>
                                                <td>{item.email}</td>
                                                <td>{item.role.name}</td>
                                                {/* <td><Button href="#" className="btn btn-sm button-theme btn-rounded text-white" style={{ background: "#248afd" }} onClick={() => handleShow(item.role)}>View</Button></td> */}
                                                <td><Link className="btn btn-sm button-theme text-white" style={{ background: "#082e39" }} to={'../../usermanagement/user/edit/' + item.id} >Edit</Link></td>
                                            </tr>)
                                        }
                                    }
                                })}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Userlist;