import React, { Component, useState, useEffect } from 'react';
import styled from 'styled-components';
import Cookies from 'js-cookie'
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
import filterFactory from 'react-bootstrap-table2-filter'
import Button from "react-bootstrap/esm/Button";
import { Form, Modal } from "react-bootstrap";
import { CSVLink } from "react-csv";


const defaultSorted = [{
  dataField: 'bookingDate',
  order: 'desc'
}];

const pagination = paginationFactory({
  page: 1,
  sizePerPage: 5,
  lastPageText: '>>',
  firstPageText: '<<',
  nextPageText: '>',
  prePageText: '<',
  showTotal: true,
  alwaysShowAllBtns: true,
  onPageChange: function (page, sizePerPage) {
    console.log('page', page);
    console.log('sizePerPage', sizePerPage);
  },
  onSizePerPageChange: function (page, sizePerPage) {
    console.log('page', page);
    console.log('sizePerPage', sizePerPage);
  }
});

const cellEditProps = {
  mode: 'click',
  blurToSave: true
};

let columnsval: any[] = [];


export const ConsentCheckup = ({ data, onTableChange,
  onSearchTextChange, listType, columnsDef, showCancelModal, closeModal,
  confirmCancelModal, selectedTrips, getColums, showCompletedModal, confirmCompletedModal, handleRemarksChange, csvData, csvName }) => {
  const [show, setShow] = useState({ modals: false });
  let columns: any[] = columnsDef.filter(x => x.listType.indexOf(listType) > -1);
  // Cookies.remove('setcols');
  if (Cookies.get('setcols')) {
    let jsonStr: string = Cookies.get('setcols');
    columnsval = JSON.parse(jsonStr);
    let getkeycolumn: any[] = [];
    let copycolumn: any[] = [];
    columns.map((olditem, getkey) => {
      if(olditem) {
        if(olditem.text == "Consent Action") {
          columns[getkey].dataField = 'consent'; 
        }
        columnsval.map((item: any, key) => {
          if (item) {
            if (olditem.text == item.text) {
              getkeycolumn.push(key)
            }
          }
        })
      }
    })

    if (getkeycolumn) {
      getkeycolumn.map((setitem: any, key) => {
        copycolumn[key] = columns[setitem];
        copycolumn[key].hide = columnsval[setitem].hide; 
      })
      columns = copycolumn;
    }
  }
  const [cols, setCols] = useState<any>(columns);
  const [dragOver, setDragOver] = useState("");

  const csvHeaders = [...[...columns].map(m => { return { label: m.text, key: m.dataField } }).filter(x => !(x.label.includes('Action') || x.label.includes('Track')))];
  let tableRef = React.createRef();

  const handleDragStart = e => {
    const { id } = e.target;
    const idx = cols.indexOf(id);
    e.dataTransfer.setData("colIdx", id);
  };

  const handleDragOver = e => e.preventDefault();
  const handleDragEnter = e => {
    const { id } = e.target;
    setDragOver(id);
  };

  const setColumn = e => {
    const { value, checked } = e.target; 

    cols.map((item, key)=>{
      if(item.text == value) {
        cols[key].hide = checked;  
      }
      // if(item.text == "Consent Action") {
      //   cols[key].dataField = 'consent'; 
      // }

    })

    setCols(cols);
    Cookies.remove('setcols');
    Cookies.set('setcols', JSON.stringify(cols))    
  }

  const handleOnDrop = e => {
    const { id } = e.target;
    const droppedColIdx = cols.indexOf(id);
    const draggedColIdx = e.dataTransfer.getData("colIdx");
    const tempCols = [...cols];
    console.log('drop', id);
    console.log('drag', draggedColIdx);

    tempCols[draggedColIdx] = cols[id];
    tempCols[id] = cols[draggedColIdx];

    const filtertempCols = tempCols.filter(x => x)

    if (filtertempCols.length == columns.length) {
      Cookies.remove('setcols');
      Cookies.set('setcols', JSON.stringify(filtertempCols))

      setCols(filtertempCols);
      setDragOver("");
    }
  };

  const handleClose = () => setShow({ modals: false });
  const handleShow = () => setShow({ modals: true });

  let setColumns = cols.filter(function(item) {
      return item.hide !== true;
  })
    
    console.log('setColumns', setColumns);
    console.log('cols', cols);
    console.log('columns', columns);
    

  return (


    <div className="bt-data-table-custom">

      <div className="">
        <div className="row">
          <div className="col-md-6">

            <CSVLink id="csvlinkid" className="hidden" filename={csvName + "_" + new Date().toDateString().replaceAll(' ', '_') + ".csv"} data={csvData} headers={csvHeaders}></CSVLink>
            <h3 className="font-weight-bold mb-0">{listType === 1 ? 'Health Listing ' : 'Health Listing Log'} </h3>
          </div>
        </div>
        <div className="row pb-3 pt-5 d-block txt-ryt">

          <div className="col-md-12 d-inline-block txt-lft">
            {/* <Button className="btn btn-sm button-theme btn-rounded mb-1" onClick={e => {
            let table = tableRef as any;
            table.current.sortContext.handleSort(defaultSorted);
              }}>
                Reset Sort
          </Button> */}
            <input type="text"
              onChange={onSearchTextChange}
              style={{ 'borderRadius': '10px' }}
              className="form-control form-control-sm"
              placeholder="Search" />
          </div> 
        </div>

        <div className="row pb-3">
          <div className="col-md-12 booking-table">
            <div className="responsive-table">
              <BootstrapTable
                noDataIndication={() => { return 'No records to display'; }}
                ref={tableRef}
                // remote
                keyField="tripNumber"
                data={data}
                columns={setColumns}
                striped
                hover
                condensed
                // filter={filterFactory()}
                defaultSorted={defaultSorted}
                cellEdit={cellEditFactory(cellEditProps)}
                onTableChange={onTableChange}
                pagination={paginationFactory({ sizePerPage: 5 })}
              />
            </div>
            <div className="fs-12">
              {"Legends: ST -> Single Trip / RT-> Return Trip."}

            </div>
          </div>
        </div>

      </div>
      {
        selectedTrips.length > 0 ?
          <Modal
            show={showCancelModal}
            onHide={closeModal}
            backdrop="static"
            keyboard={false}
            dialogClassName="modal-booking-popup"
          >
            <Modal.Header closeButton>
              <Modal.Title>Transit Cancelled!</Modal.Title>
            </Modal.Header>
            <Modal.Body >
              <div className="bt-data-table-custom">
                <div className="col-md-12 booking-table">
                  <div className="responsive-table">

                    <Form>
                      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <BootstrapTable
                          key="1"
                          keyField="id"
                          data={selectedTrips}
                          columns={getColums()}
                        />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Transit Cancellation Reason</Form.Label>

                        <Form.Control
                          name="remarks"
                          required
                          onChange={handleRemarksChange}
                          value={selectedTrips[0].remarks}

                          as="textarea" rows={3} />
                      </Form.Group>
                    </Form>
                  </div> </div></div>
            </Modal.Body>
            <Modal.Footer>

              <Button variant="primary" disabled={!selectedTrips[0].remarks} onClick={() => confirmCancelModal()}>Submit</Button>
            </Modal.Footer>
          </Modal>
          : ""}

      <Modal
        show={showCompletedModal}
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
        dialogClassName="modal-booking-popup"
      >
        <Modal.Header closeButton>
          <Modal.Title>Transit Complete!</Modal.Title>
        </Modal.Header>
        <Modal.Body >
          <div className="bt-data-table-custom">
            <div className="col-md-12 booking-table">
              <div className="responsive-table">

                <BootstrapTable
                  key="1"
                  keyField="id"
                  data={selectedTrips}
                  columns={getColums()}
                />

              </div> </div></div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Close
          </Button>
          <Button variant="primary" onClick={() => confirmCompletedModal()}>Submit</Button>
        </Modal.Footer>
      </Modal>

      <Modal show={show.modals} onHide={handleClose} className="bookingmodel">
        <Modal.Body>
          <table className="table table-bordered">
            <thead className="thead-light">
              {
                cols.map((perm: any, key) => {
                  if (perm) {
                    if (perm.text != "No." && perm.text != "Trip No.")
                      return (<tr style={{ 'border': 'solid white 10px' }}>
                        <StyledTh
                          id={key}
                          key={key}
                          draggable
                          onDragStart={handleDragStart}
                          onDragOver={handleDragOver}
                          onDrop={handleOnDrop}
                          onDragEnter={handleDragEnter}
                          dragOver={perm.text === dragOver}
                        >
                          <th style={{ 'borderRadius': '50px', 'width':'250px', 'borderColor': '#e9ecef', 'paddingTop': '0px', 'paddingBottom': '10px'}}>{perm.text}
                            <Form className="align-right" style={{'textAlign': 'right', 'marginTop': '-18px'}}>
                              <Form.Check
                                type="switch"
                                value={perm.text}
                                defaultChecked={perm.hide}
                                id="custom-switch"
                                onChange={setColumn}
                              /> 
                            </Form>
                          </th>
                        </StyledTh>
                      </tr>)
                  }
                })
              }
            </thead>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );


};

const StyledTh = styled.th`
  white-space: nowrap;
  color: #716f88;
  letter-spacing: 1.5px;
  font-weight: 600;
  font-size: 14px;
  text-align: left;
  text-transform: capitalize;
  vertical-align: middle;
  padding: 20px;
  border: 0px solid #fff;
  text-transform: uppercase;
  border-left: ${({ dragOver }) => dragOver && "5px solid red"};
`;