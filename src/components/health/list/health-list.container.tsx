
import * as React from "react";
import { connect } from "react-redux";
import { defaultListSearchRequestModel, IAppState } from "../../../models";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import moment from "moment";
import { IGenericReducerState } from "../../../utilities";
import { Link, RouteComponentProps } from "react-router-dom";
import filterFactory, { textFilter, dateFilter, Comparator} from "react-bootstrap-table2-filter";

import 'font-awesome/css/font-awesome.min.css'; 
import { HealthList } from "./health-list";
// import { defaultBookingListValue, IBookingListModel } from "../../../models/booking-list.model";
import { defaultHealthValue, IHealthModel } from "../../../models/healthlist.model";
// import { getBookings, getBookingsForexport, updateBookingListProperties } from "../../../redux/actions/booking-list.actions";
import { getAllHealth } from "../../../redux/actions/health.action";
import { IListSearchRequstModel } from "../../../models/list-search-request.model";
import { debounceTime, distinctUntilChanged, Subject } from "rxjs";
import { defaultBookingValue, IBookingModel, ITrip } from "../../../models/booking.model";
import { checkConsent, updateTrip } from "../../../redux/actions/trip.action";
import { Button, ButtonProps, Image, OverlayTrigger, Tooltip } from "react-bootstrap";
import { updateBookingProperties } from "../../../redux/actions/booking.action";
import { toast } from "react-toastify";

class HealthListContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {

  searchModel = defaultListSearchRequestModel;
  searchString$: Subject<string> = new Subject<string>();
  listType: number = 1;
  showCancelModal: boolean = false;
  totalRowCountAfterFilter: number = 0;
  selectedTrips: any = [];
  showCompletedModal: boolean = false;
  csvData: any[] = [];
  csvName: string = "";

  bookdateFilter: any;
  tripnoFilter: any;
  customerFilter: any;
  vehiclenoFilter: any;
  drivernoFilter: any;


  public constructor(props) {
    super(props);
    this.handleTableChange = this.handleTableChange.bind(this);
    this.onSearchTextChange = this.onSearchTextChange.bind(this);
    this.inItSearch();
    if (window.location.href.indexOf("/health-list/healthlist/active") > -1) {
      this.listType = 1
      this.searchModel.status = "ACTIVE";
      this.csvName = "Active_Booking_Trips";
    } else if (window.location.href.indexOf("/health-list/healthlist/completed") > -1) {
      this.listType = 3;
      this.searchModel.status = "COMPLETED";
      this.csvName = "Complete_Booking_Trips";
    } 
    this.searchModel.searchToken = "";
    this.fetchList(this.searchModel);
  }

  inItSearch() {
    this.searchString$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
    ).subscribe(text => {
      this.searchModel.searchToken = text;
      this.fetchList(this.searchModel);
    })
  }
  closeModal() {
    this.showCompletedModal = false;
    this.showCancelModal = false;
    this.selectedTrips = [];
    this.setState({ showCancelModal: false, showCompletedModal: false })
  }

  confrimModalAction(id) {

  }

  isValidEndDate = (date: Date) => {
    const previousDay = moment().subtract(1, "days");
    const lastDay = moment().add(10, "days");
    return date.getTime() > previousDay.toDate().getTime() && date.getTime() < lastDay.toDate().getTime();
  };

  isValidEndTime = (date: Date) => {

    return date.getHours() > new Date().getHours();
  };
  onSearchTextChange(e) {
    this.searchString$.next(e.target.value.trim())
  }

  fetchList(model) { 
    this.props.dispatch(getAllHealth(model.status));
  } 
 

  handleTableChange = (type, { page, sizePerPage, filters, sortField, sortOrder, cellEdit }) => {

    // if (type === 'pagination') {
    //   this.searchModel.pageNumber = page - 1;
    //   if (this.searchModel.pageSize !== sizePerPage) {
    //     this.searchModel.pageNumber = 0;
    //   }
    //   this.searchModel.pageSize = sizePerPage;
    //   this.fetchList(this.searchModel);
    // }
    // else if (type === "sort") {
    //   this.searchModel.sortDirection = sortOrder ? sortOrder.toUpperCase() : "ASC";
    //   this.searchModel.sortProperty = sortField;
    //   this.fetchList(this.searchModel);
    // }  
    // else if(filters){
    //   if(filters.transitStartDate){
    //     let fromDate = new Date(filters.transitStartDate.filterVal.date);
    //     fromDate.setHours(0);
    //     fromDate.setMinutes(0);
    //     let toDate = new Date(filters.transitStartDate.filterVal.date);
    //     toDate.setHours(23);
    //     toDate.setMinutes(59);
    //     this.searchModel.fromDate = fromDate.getTime();
    //     this.searchModel.toDate = toDate.getTime();
    //   }
    // } else {
    //   this.searchModel.fromDate = undefined;
    //     this.searchModel.toDate = undefined;
    // }

  }


  onCompleteTripClicked(id, trip) {
    this.showCompletedModal = true;
    this.selectedTrips.push(trip);
    this.setState({ showCompletedModal: true })
  }

  onCancelTripClicked(id, trip) {
    this.showCancelModal = true;
    this.selectedTrips.push(trip);
    this.setState({ showCancelModal: true })
  }


  async onRefreshConsent(id) {
    await this.props.dispatch(checkConsent(id));
  }

  async onResendConsent(id) {
    await this.props.dispatch(checkConsent(id));
  }

  getColumnsDefination() {
    const allColumns = [
      {
        dataField: 'consentStatus',
        text: 'Health Type',
        hide: false,
        sort: false,
        editable: false, 
        listType: [1, 2, 3],
        formatter: (cell, row) => {
          return <>
            <span>
              {cell === "NOT_INITIATED" || cell === "PENDING" || row.firstSuccessFullTracingFlag === "PENDING" || row.telenityTripCreationFlag === "PENDING" || cell === "EXPIRED" ? <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-2x text-danger"></i> </span> : cell === "ALLOWED" ? <span className="custom-t-btn">  <i className="fa fa-check-circle-o fa-2x text-success"></i> </span> : <span className="custom-t-btn"><i className="fa fa-trash-o fa-2x"></i> </span>}
            </span>
          </>
        }, 
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },{
        dataField: 'bookingDate',
        text: 'Date',
        hide: false,
        sort: true,
        editable: false,
        listType: [1, 2, 3],
        formatter: (cell) => {
          return <>{moment(cell).format('DD-MM-YY hh:mm a')}</>
        },
        filter: dateFilter({
          delay: 400,
          placeholder: 'custom placeholder',
          withoutEmptyComparatorOption: true,
          comparators: [Comparator.EQ, Comparator.GT, Comparator.LT],
          className: 'custom-datefilter-class',
          comparatorStyle: { display: 'none' },
          comparatorClassName: 'custom-comparator-class',
          dateStyle: { marginTop: '10px' },
          dateClassName: 'custom-date-class'
        }), 
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center' };
        },
        tdStyle: (colum, colIndex) => {
          return { textAlign: 'center' };
        }
      },
      {
        dataField: 'tripNumber',
        text: 'Trip ID',
        hide: false,
        sort: true,
        editable: false,
        listType: [1, 2, 3],
        formatter: (cell, row) => {

          return <>
            {/* <Link className="custom-t-btn" to={`/booking/view/${row.bookingId}/${row.id}`}  > */}
              {cell}
            {/* </Link> */}
          </>
        },
        filter: textFilter({
          getFilter: filter => {
            this.tripnoFilter = filter;
          }
        }) 
      },
      {
        dataField: 'companyName',
        text: 'Customer',
        hide: false,
        sort: true,
        editable: false,
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '60px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        filter: textFilter({
          getFilter: filter => {
            this.customerFilter = filter;
          }
        }),
        tdStyle: (colum, colIndex) => {
          return { width: '60px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }

      },
      {
        dataField: 'vehicleNumber',
        text: 'Vehicle No.',
        hide: false,
        sort: true,
        editable: false,
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        filter: textFilter({
          getFilter: filter => {
            this.vehiclenoFilter = filter;
          }
        }),
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      {
        dataField: 'driverMobileNumber',
        text: 'Driver  No.',
        hide: false,
        sort: true,
        editable: false,
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        filter: textFilter({
          getFilter: filter => {
            this.drivernoFilter = filter;
          }
        }),
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      // {
      //   dataField: 'driverMobileNumber',
      //   text: 'Carrier',
      //   hide: false,
      //   sort: true,
      //   editable: false,
      //   listType: [1, 2, 3],
      //   headerStyle: (colum, colIndex) => {
      //     return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
      //   },
      //   tdStyle: (colum, colIndex) => {
      //     return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
      //   }
      // },
      {
        dataField: 'telenityTripCreationFlag',
        text: 'Trip Created',
        hide: false,
        sort: false,
        editable: false,
        formatter: (cell, row) => {
          return <>
            <span>
              {cell === "PENDING" ? 
              <OverlayTrigger
              placement="bottom"
              overlay={<Tooltip id="button-tooltip-2">{row.createTripAPICallLog?.errorMessage}</Tooltip>}
            >
              {({ ref, ...triggerHandler }) => (
                <Button
                  ref={ref}
                  variant="light"
                  {...triggerHandler}
                  className="d-inline-flex p-0 bg-transparent border-0"
                > 
                  <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-10x text-danger" style={{fontSize: "24px"}}></i> </span>
                </Button>
              )}
            </OverlayTrigger>
              // <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-2x text-danger"></i> </span> 
              : <span className="custom-t-btn align-items-center"><i className="fa fa-check-circle-o fa-2x text-success"></i> </span>}
            </span>
          </>
        },
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      {
        dataField: 'consentStatus',
        text: 'Consent',
        hide: false,
        sort: false,
        editable: false,
        formatter: (cell,row) => {
          return <>
            <span>
              {cell === "NOT_INITIATED" || cell === "PENDING" || cell === "EXPIRED" ? 
              <OverlayTrigger
              placement="bottom"
              overlay={<Tooltip id="button-tooltip-2">Consent Status is {cell}</Tooltip>}
            >
              {({ ref, ...triggerHandler }) => (
                <Button
                  ref={ref}
                  variant="light"
                  {...triggerHandler}
                  className="d-inline-flex p-0 bg-transparent border-0"
                > 
                  <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-10x text-danger" style={{fontSize: "24px"}}></i> </span>
                </Button>
              )}
            </OverlayTrigger>
              // <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-2x text-danger"></i> </span> 
              : <span className="custom-t-btn">  <i className="fa fa-check-circle-o fa-2x text-success"></i> </span>}
            </span>
          </>
        },
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      {
        dataField: 'firstSuccessFullTracingFlag',
        text: 'Tracking',
        hide: false,
        sort: false,
        editable: false,
        formatter: (cell, row) => {
          return <>
            <span>
              {cell === "PENDING" ?
              <OverlayTrigger
              placement="bottom"
              overlay={<Tooltip id="button-tooltip-2">{row.lastTrackingError}</Tooltip>}
            >
              {({ ref, ...triggerHandler }) => (
                <Button
                  ref={ref}
                  variant="light"
                  {...triggerHandler}
                  className="d-inline-flex p-0 bg-transparent border-0"
                > 
                  <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-10x text-danger" style={{fontSize: "24px"}}></i> </span>
                </Button>
              )}
            </OverlayTrigger> 
              // <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-2x text-danger"></i> </span> 
              : <span className="custom-t-btn">  <i className="fa fa-check-circle-o fa-2x text-success"></i> </span>}
            </span>
          </>
        },
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      {
        dataField: 'consentStatus',
        text: 'Locate Vehicle',
        hide: false,
        sort: false,
        editable: false,
        formatter: (cell, row) => {
          console.log('row', row);
          
          return <>
             
            {cell === "NOT_INITIATED" || cell === "PENDING" || cell === "EXPIRED" || row.firstSuccessFullTracingFlag === "PENDING" ? <span className="consent-st-trips"> </span> : <Link className="custom-t-btn" to={'../../tracking/single-tracking/' + row.id +'/' + row.tenantName} ><img className="bk-list-loc-icn" alt="" src="/images/location-icon.png" /></Link>}
            
            
            {/* <Link className="custom-t-btn" to="/tracking/{cell}" >  <img alt="" src="/images/location-icon.png"> </img></Link> */}
          </>
        },
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '40px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '40px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        }
      },
      {
        dataField: 'status',
        text: 'DgNote Trip Comp.',
        hide: false,
        sort: false,
        editable: false,
        formatter: (cell) => {
          return <>
            <span>
              {cell === "ACTIVE" ? <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-2x text-danger"></i> </span> : <span className="custom-t-btn">  <i className="fa fa-check-circle-o fa-2x text-success"></i> </span>}
            </span>
          </>
        },
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      {
        dataField: 'telenityTripStatus',
        text: 'Telenity Trip Comp.',
        hide: false,
        sort: false,
        editable: false,
        formatter: (cell) => {
          return <>
            <span>
              {cell === "ACTIVE" ? <span className="custom-t-btn">  <i className="fa fa-check-circle-o fa-2x text-success"></i> </span> : <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-2x text-danger"></i> </span>}
            </span>
          </>
        },
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },

      {
        dataField: 'action',
        text: 'Action',
        hide: false,
        sort: false,
        editable: false,
        listType: [1],
        headerStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        },
        formatter: (cell, row) => {

          return <>
            {/* <Link className="custom-t-btn" to={`/booking/view/${row.bookingId}`}  >View</Link> */}
            {/* <Button className="d-block custom-t-btn"  variant="link">Edit</Button> */}

            <Button className="d-inline-block custom-t-btn" onClick={() => this.onCancelTripClicked(row.id,row)} variant="link">Close</Button>
            <Button className="d-inline-block custom-t-btn" onClick={() => this.onCompleteTripClicked(row.id, row)} variant="link">Complete</Button>
          </>
        },
      },
      {
        dataField: 'lastModifiedDate',
        text: 'Close Date',
        hide: false,
        sort: true,
        editable: false,
        listType: [2,3],
        formatter: (cell) => {
          let dateObj = cell;
          if (typeof cell !== 'object') {
            dateObj = new Date(cell);
          }
          // return  `${('0' + dateObj.getDate()).slice(-2)}/${('0' + (dateObj.getMonth() + 1)).slice(-2)}/${dateObj.getFullYear()}`;
          return moment(dateObj).format('DD-MM-YY hh:mm a')
        },
        headerStyle: (colum, colIndex) => {
          return { width: '100px', textAlign: 'center', 'whiteSpace': 'nowrap', 'overflow': 'hidden', 'textOverflow': 'ellipsis' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '100px', textAlign: 'center', 'whiteSpace': 'nowrap', 'overflow': 'hidden', 'textOverflow': 'ellipsis' };
        }
      },

    ];

    return allColumns;
  }

  getselectedTripColumns() {
    const selectedTripColumns = [
      {
        dataField: 'tripNumber',
        text: 'Trip No.',
        sort: false,
        editable: false
      }, 
      {
        dataField: 'transitStartDate',
        text: 'Start Date',
        sort: false,
        formatter: (cell) => {
          let dateObj = cell;
          if (typeof cell !== 'object') {
            dateObj = new Date(cell);
          }
          // return  `${('0' + dateObj.getDate()).slice(-2)}/${('0' + (dateObj.getMonth() + 1)).slice(-2)}/${dateObj.getFullYear()}`;
          return moment(dateObj).format('DD-MM-YY hh:mm a')
        },
        editable: false,
        listType: [1, 2, 3],
        headerStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        classes: 'date-custom-cell'
      },
      {
        dataField: 'transitEstimatedEndDate',
        text: 'End Date',
        sort: false,
        formatter: (cell) => {
          let dateObj = cell;
          if (typeof cell !== 'object') {
            dateObj = new Date(cell);
          }
          // return `${('0' + dateObj.getDate()).slice(-2)}/${('0' + (dateObj.getMonth() + 1)).slice(-2)}/${dateObj.getFullYear()}`;
          return moment(dateObj).format('DD-MM-YY hh:mm a')
        },
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        classes: 'date-custom-cell'
      },

      {
        dataField: 'customerName',
        text: 'Customer',
        sort: false,
        editable: false,

        headerStyle: (colum, colIndex) => {
          return { width: '80px', textAlign: 'center', 'whiteSpace': 'nowrap', 'textOverflow': 'ellipsis' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '80px', textAlign: 'center', 'whiteSpace': 'nowrap', 'textOverflow': 'ellipsis' };
        }

      },
      {
        dataField: 'transporterName',
        text: 'Transporter',
        sort: false,
        editable: false,

        headerStyle: (colum, colIndex) => {
          return { width: '80px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '80px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        }
      },
      {
        dataField: 'vehicleNumber',
        text: 'Vehicle No.',
        sort: false,
        editable: false,

        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      {
        dataField: 'driverMobileNumber',
        text: 'Driver  No.',
        sort: false,
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      {
        dataField: 'containerNumber',
        text: 'Container No.',
        sort: false,
        editable: false,

        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      }


    ];
    return selectedTripColumns;
  }

  public handleRemarksChange(e) {
    const { value } = e.target;
    this.selectedTrips[0].remarks = value;
    this.setState({ showCancelModal: true })

  }
  public async submitCancelTrip() {
    var trip = this.selectedTrips[0] as ITrip;
    trip.status = "CANCELLED";
    await this.props.dispatch(updateTrip(trip));
    this.fetchList(this.searchModel);
    this.closeModal();
  }
  async submitCompleteTrip() {
    var trip = this.selectedTrips[0] as ITrip;
    trip.status = "COMPLETED";
    await this.props.dispatch(updateTrip(trip));
    this.fetchList(this.searchModel);
    this.closeModal();
  }
  componentWillUnmount() {
    // this.props.dispatch(updateBookingListProperties({ ...defaultHealthValue }))
    this.props.dispatch(updateBookingProperties(JSON.parse(JSON.stringify({ ...defaultBookingValue }))))
  }
  public render() {

    let data = this.props.healthListState.Payload;
    
    // if (!data)
    // data = [];
    // this.totalRowCountAfterFilter = totalElements;
    let getData = Object.values(data).map(x => {
      return x;
    })

    // console.log('data', data);
    
    return <HealthList
      listType={this.listType}
      data={getData}
      onTableChange={this.handleTableChange}
      onSearchTextChange={this.onSearchTextChange}
      columnsDef={this.getColumnsDefination()}
      closeModal={this.closeModal.bind(this)}
      confirmCancelModal={this.submitCancelTrip.bind(this)}
      selectedTrips={this.selectedTrips}
      showCancelModal={this.showCancelModal}
      showCompletedModal={this.showCompletedModal}
      confirmCompletedModal={this.submitCompleteTrip.bind(this)}
      getColums={this.getselectedTripColumns.bind(this)}
      handleRemarksChange={this.handleRemarksChange.bind(this)}
      csvData={this.csvData}
      csvName={this.csvName}

    />;
  }





}
interface IConnectState {
  // bookingListState: IGenericReducerState<IBookingListModel>;
  healthListState: IGenericReducerState<IHealthModel>;
  // bookingState: IGenericReducerState<IBookingModel>;
  loadingState: boolean
}


const mapStateToProps = (state: IAppState): IConnectState => ({
  healthListState: state.HealthList,
  // bookingState: state.Booking,
  loadingState: true
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(HealthListContainer);