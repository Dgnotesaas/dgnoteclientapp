import { useState, useEffect } from 'react';
import { Container, Row, Form, Button } from 'react-bootstrap';
import '../css/user.css';
import { useFormik } from 'formik';
import { Link } from 'react-router-dom';

function Company(props: any) {

    let arrPermissionlist = Object.values(props.getPermissionList)
    let getmoduleArray = arrPermissionlist.map((item: any) => {
        return (item.moduleName)
    })

    let getUniquemodule = Array.from(new Set(getmoduleArray));

    const formik = useFormik({
        initialValues: {
            roleName: '',
            description: '',
            checked: [],
        },
        onSubmit: values => {
            alert(JSON.stringify(values, null, 2));
        },
    });

    console.log('arrPermissionlist', props.initValue.name);


    return (
        <div className="user-mgt-custom">
            <Container>
                <Row className="mt-4">
                    <div className="col-md-12 mb-4">
                        <h3 className="font-weight-bold mb-0">Roles & Permission</h3>
                    </div>
                    <div className="formsection mt-4">
                        <Form onSubmit={props.handleSubmit}>
                            <Row>
                                <div className="col-md-6">
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label className="small">Role Name</Form.Label>
                                        <Form.Control type="text" name="name" onChange={props.handleChange} defaultValue={props.initValue.name} placeholder="Enter Role Name" />
                                        <div className='errors'>
                                            {props.errors.name.length > 0 && props.errors.name}
                                        </div>
                                    </Form.Group>
                                </div>
                                <div className="col-md-6">
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label className="small">Description</Form.Label>
                                        <Form.Control type="text" name="description" onChange={props.handleChange} defaultValue={props.initValue.description} placeholder="Enter Description" />
                                        <div className='errors'>
                                            {props.errors.description.length > 0 && props.errors.description}
                                        </div>
                                    </Form.Group>
                                </div>
                            </Row>
                            <hr />
                            <div className="mt-4 mb-2"><b>Permission</b></div>
                            <Row>
                                <div className="col-md-12">
                                    <table className="table table-bordered">
                                        <thead className="thead-dark list-table">
                                            <tr>
                                                <th >Modules and Pages</th>
                                                <th >Action</th>
                                            </tr>
                                        </thead>
                                        {getUniquemodule.map((item: any) => {
                                            // if (item.moduleName = 'Administration') {
                                            return (
                                                <tbody>
                                                    <tr>
                                                        <td align="center"><strong>{item}</strong></td>
                                                        <td>
                                                            {/* <div className="custom-control custom-switch">
                                                                <Form.Check
                                                                    type="checkbox"
                                                                    id="all_check"
                                                                    label="All Permission"
                                                                /> 
                                                            </div> */}
                                                        </td>
                                                    </tr>
                                                    {arrPermissionlist.map((allitem: any) => {
                                                        if (allitem.moduleName === item)
                                                            return (
                                                                <tr>
                                                                    <td className="tabledata">{allitem.name}</td>
                                                                    <td>
                                                                        <div className="custom-control custom-switch">
                                                                            <Form.Check
                                                                                type="checkbox"
                                                                                id="single_check"
                                                                                label=""
                                                                                defaultChecked={props.checked(allitem.id)}
                                                                                name="permissions"
                                                                                onChange={props.handleChange}
                                                                                value={allitem.id}
                                                                            />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            )
                                                    })}
                                                </tbody>
                                            )
                                            // }
                                        })}
                                    </table>
                                </div>
                                <div className="col-md-12 mt-3">
                                    <Button className="btn btn-sm button-theme btn-rounded text-white sub-button" type="submit" onClick={() => props.addRole()}>Submit</Button> 
                                </div>
                            </Row>
                        </Form>   
                        <div className="absub-button">
                             {(!props.isEdit) ? (<Button className="btn btn-sm button-theme btn-rounded text-white" type="submit" onClick={() => props.cancel()}>Cancel</Button>) : (<Link className="btn btn-sm button-theme btn-rounded text-white" style={{ background: "#082e39"}} to={'../role-list'} >Cancel</Link>) }
                         </div>
                    </div>
                </Row>
            </Container>
        </div>
    )
}

export default Company;