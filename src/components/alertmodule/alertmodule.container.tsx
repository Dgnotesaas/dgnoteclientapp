import * as React from "react";
import { connect } from "react-redux";
import { IAppState, IUserModel} from "../../models";

import { IGenericReducerState } from "../../utilities";
import { RouteComponentProps } from "react-router-dom";
import { IAlertList, defaultAlertList, IUserRule } from "../../models/alertmodule.model";
import Alertmodule from "./alertmodule";
import { getAlertModule, postAlertModule, getAlertModulebyId } from "../../redux/actions/alertmodule.action";

let listPermission: any[] = [];
let checklistID: any[] = [];

interface MyState {
  rule: any,
  ruleid: any,
  description: any,
  emailStatus: any,
  smsStatus: any,
  whatsappStatus: any, 
}

class AlertModuleContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps, MyState> {

  listType: number = 1;
  alertid: string = '';
  isEditPage: boolean = false;

  public constructor(props) {
    super(props);
    this.state = {
      rule: '',
      ruleid: '',
      description: '',
      emailStatus: '',
      smsStatus: '',
      whatsappStatus: '', 
    }
    this.fetchList();
    this.alertid = this.props.match.params["id"];
    if (this.alertid) {
      this.isEditPage = true;
    }
    if(!this.isEditPage) {
      this.props.alertModuleList.Payload = {...defaultAlertList};
    }
    this.handleChange = this.handleChange.bind(this)
  }

  fetchList() {
    this.props.dispatch(getAlertModule('ACTIVE'));
  }  

  // async getAlertModuleDetails() {
  //   if (this.isEditPage) {
  //     await this.props.dispatch(getAlertModulebyId(this.alertid));
  //   }
  // }

  // public componentDidMount() {
  //   this.getAlertModuleDetails()
  // }
  
  public handleChange(e, key) {
    const { name, value, checked} = e.target;  
    if(name == 'whatsappStatus' || name == 'emailStatus' || name == 'smsStatus') {
      let getchecked = (checked)?'ACTIVE':'INACTIVE';
      this.props.alertModuleList.Payload[key].userRule[name] = getchecked;
    } else {
      this.props.alertModuleList.Payload[key].userRule[name] = value;
    }
    const user = this.props.userState.Payload as any;

    this.props.alertModuleList.Payload[key].userRule['companyId'] = user.companyDto.id;
    this.props.alertModuleList.Payload[key].userRule['description'] = this.props.alertModuleList.Payload[key].description;
    this.props.alertModuleList.Payload[key].userRule['rule'] = this.props.alertModuleList.Payload[key].rule;
    this.props.alertModuleList.Payload[key].userRule['ruleId'] = this.props.alertModuleList.Payload[key].id;
    this.props.alertModuleList.Payload[key].userRule['tenantName'] = localStorage.getItem("tenant");   
    
  }

  handleSubmit = (event) => {

    event.preventDefault();

    // if (!this.isEditPage) {
    //   let seterrors = this.state.errors;
    //   seterrors.name = (this.state.name === '') ? 'Please enter role name' : '';
    //   seterrors.description = (this.state.description === '') ? 'Please add description' : '';

    //   this.setState({ errors: seterrors })
    // }

    // if (this.validateForm(this.state.errors)) {
    //   console.info('Valid Form')
    //   if (this.props.roleListState.Payload.permissions.length > 0) {
        // if (this.isEditPage) {
        //   this.props.dispatch(updateRole(this.props.roleListState.Payload));
        // } else {
        //   if(this.props.alertModuleList.Payload.length >= ) {
        //   let userRole = this.props.alertModuleList.Payload.map(getitem: any) {
        //     return (

        //     )
        //   }
        //   console.log('lot', this.props.alertModuleList.Payload);
        // } 
          this.props.dispatch(postAlertModule(this.props.alertModuleList.Payload))
        // }
    //     this.props.history.push('/usermanagement/role/role-list')
    //   } else {
    //     alert('Please select role permission!')
    //     return false;
    //   }
    // } else {
    //   console.error('Invalid Form')
    //   return false;
    // }
  }

  public render() {  
    console.log('tsetst', this.props.alertModuleList.Payload);
       
    let model = {
      getAlertModuleList: this.props.alertModuleList.Payload,
      handleChange: this.handleChange,
      handleSubmit: this.handleSubmit,
      isEdit: this.isEditPage,
      initValue: this.props.alertModulePost.Payload,
    };
    return <Alertmodule {...model} />;
  }
}
interface IConnectState {
  alertModuleList: IGenericReducerState<IAlertList>;
  alertModulePost: IGenericReducerState<IUserRule>;
  userState: IGenericReducerState<IUserModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
  alertModuleList: state.AlertList,
  alertModulePost: state.AlertPost,
  userState: state.User
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(AlertModuleContainer);