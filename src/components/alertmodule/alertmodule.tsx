import { useState, useEffect } from 'react';
import { Container, Row, Form, Button, Modal } from 'react-bootstrap';
import '../usermanagement/role/css/user.css';
import { Link } from 'react-router-dom';

function Alertmodule(props: any) {

    // let getUniquemodule = Array.from(new Set(getmoduleArray)); 

    // const [show, setShow] = useState({modals: false, alertmoduleform: ""});

    // const handleClose = () => setShow({modals: false, alertmoduleform: ""});
    // const handleShow = () => setShow({modals : true, alertmoduleform: 'permission' });

    const inputbox = (value, key) => {
    return (
        <input className="form-control table-input" name="value" type="number" pattern="[0-9]*" defaultValue={value} onChange={(e) => props.handleChange(e, key)} required />)
    }

    return (
        <div className="user-mgt-custom">
            <Container>
                <Row className="mt-4">
                    {/* <Modal show={show.modals} onHide={handleClose}>
                    <div className="modal-header text-center">
                        <h4 className="modal-title w-100"><b>Add Rules</b></h4>
                    </div>
                        <Modal.Body>
                            <Form onSubmit={props.handleSubmit}>
                                <Row>
                                    <div className="col-md-6">
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label className="small">New Header</Form.Label>
                                            <Form.Control type="text" name="name" onChange={(e) => props.handleChange(e, key)} defaultValue={props.initValue.name} placeholder="Enter New Header" /> 
                                        </Form.Group>
                                    </div>
                                    <div className="col-md-6">
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label className="small">Select Header</Form.Label>
                                            <Form.Select aria-label="Default select example">
                                                <option>Open this select menu</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </Form.Select> 
                                        </Form.Group>
                                    </div>
                                    <div className="col-md-6">
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label className="small">Email Subject Line</Form.Label>
                                            <Form.Control type="text" name="description" onChange={(e) => props.handleChange(e, key)} defaultValue={props.initValue.description} placeholder="Enter Email Subject Line" /> 
                                        </Form.Group>
                                    </div>
                                    <div className="col-md-6">
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label className="small">Rule Description</Form.Label> 
                                            <Form.Control as="textarea" rows={1} style={{ "padding": "0.54rem 0.6rem"}} /> 
                                        </Form.Group>
                                    </div>
                                    <div className="col-md-12 mt-3">
                                        <Button className="btn btn-sm button-theme btn-rounded text-white sub-button" type="submit" onClick={() => props.addRole()}>Create</Button> 
                                    </div>
                                </Row>
                            </Form>
                        </Modal.Body> 
                    </Modal> */}
                    <div className="col-md-12 mb-4">
                        <h3 className="font-weight-bold mb-0">Alert Configuration</h3>
                    </div>
                    <div className="formsection mt-4">
                        <Form onSubmit={(e) => props.handleSubmit(e)} >
                            <Row>
                                <div className="col-md-12">
                                    <table className="table table-bordered">
                                        <thead >
                                            <tr>
                                                <th >Rule Description</th>
                                                <th >Email Subject</th>
                                                <th >Whatsapp</th>
                                                <th >Email</th>
                                                <th >Message</th>
                                                {/* <th >Action</th> */}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {Object.values(props.getAlertModuleList).map((allitem: any, key) => {
                                                
                                                let getDescriptin = allitem.description 
                                                let setString = [];
                                                if (getDescriptin) {
                                                    setString = getDescriptin.split('{TEXTBOX}')
                                                }
                                                return (
                                                    <tr>
                                                        <td style={{ "minWidth": "200px" }}>{getDescriptin&&setString[0]} {inputbox((allitem.userRule)?allitem.userRule.value:'', key)} {getDescriptin&&setString[1]}</td> 
                                                        <td>
                                                            <input type="text" name="emailSubject" style={{ "minWidth": "150px", "height": "30px", "display": "inline-block" }} 
                                                            defaultValue={(allitem.userRule)?allitem.userRule.emailSubject:''} onChange={(e) => props.handleChange(e, key)}
                                                            className='form-control' required/>
                                                        </td>
                                                        <td>
                                                            <Form.Check
                                                                type="switch"
                                                                id="custom-switch"
                                                                name="whatsappStatus"
                                                                style={{ "marginTop": "-12px" }}
                                                                onChange={(e) => props.handleChange(e, key)}
                                                                defaultChecked={(allitem.userRule)?(allitem.userRule.whatsappStatus == 'ACTIVE')?allitem.userRule.whatsappStatus:'' :''}
                                                                disabled
                                                            />
                                                        </td>
                                                        <td>
                                                            <Form.Check
                                                                type="switch"
                                                                name="emailStatus"
                                                                id="custom-switch"
                                                                style={{ "marginTop": "-12px" }}
                                                                onChange={(e) => props.handleChange(e, key)} 
                                                                defaultChecked={(allitem.userRule)?(allitem.userRule.emailStatus == 'ACTIVE')?allitem.userRule.emailStatus:'' :''}
                                                            />
                                                        </td>
                                                        <td>
                                                            <Form.Check
                                                                type="switch"
                                                                name="smsStatus"
                                                                id="custom-switch"
                                                                style={{ "marginTop": "-12px" }}
                                                                onChange={(e) => props.handleChange(e, key)} 
                                                                defaultChecked={(allitem.userRule)?(allitem.userRule.smsStatus == 'ACTIVE')?allitem.userRule.smsStatus:'' :''}
                                                                disabled
                                                            />
                                                        </td>
                                                        {/* <td>
                                                            {allitem.userRule&&(allitem.userRule.companyId !== '')?<Link className="btn btn-sm button-theme btn-rounded text-black" style={{ background: "transparent" }} to={'../../alertmodule/alert-module/edit' + allitem.userRule.id} >Edit</Link>:''}
                                                        </td> */}
                                                    </tr>
                                                ) 
                                            })}
                                        </tbody>

                                    </table>
                                </div>
                                <div className="col-md-12 mt-3">
                                    <Button className="btn btn-sm button-theme btn-rounded text-white sub-button" type="submit">Submit</Button> 
                                </div>
                            </Row>
                        </Form> 
                    </div>
                </Row>
            </Container>
        </div>
    )
}

export default Alertmodule;