import { Formik } from "formik";
import { Alert, Button, Col, Container, Form } from "react-bootstrap";
export const Forgot = (props: any) => {
  return (
    <>  
      <Col md={5} className=" mt-5">
        <Form noValidate onSubmit={()=>props.submit()}>
          <div className="login-custom mb-5">
            <h3 className="pb-2">FORGOT PASSWORD</h3>
            <Form.Group controlId="uname" className="mt-3"  >
              <Form.Label className="m-0 mb-1"><b>Email / Username</b> </Form.Label>
              {/* <i className="fas fa-user"></i> */}
              <Form.Control 
                placeholder="abc@dgnote.com"
                name="email"
                type="email"
                required
                value={props.state.Email}
                onChange={props.handleChange} 
              /> 
              <div style={{"fontSize": "13px", "color": "#ee5151", "minHeight": "15px"}}>
                  {props.errors.email.length > 0 && props.errors.email}
              </div>
            </Form.Group>
           
            <Button type="button" onClick={()=>props.submit()} >Forgot Password</Button>
            {/* <Button className="mt-4" type="submit"  >Forgot Password</Button> */}
           
          </div>
        </Form>
     </Col>     
    </>
  );
};
