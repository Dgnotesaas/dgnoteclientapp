import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { invalidPasswordModel, passwordForgot, updateProperties } from "../../redux/actions/password.action";
import { IAppState, IPasswordModel, IForgetPasswordModel} from "../../models";
import * as yup from 'yup';
import { getIn } from "formik";
import { IGenericReducerState } from "../../utilities";
import { Forgot } from "./forgotPassword";

interface MyState {
  email: any,
  errors: {
    email: any,
  }
}

class ForgotPasswordContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps, MyState> {
    
    private formSchema: any;
    
    public constructor(props) {
        super(props);
        this.state = {
          email: '',
          errors: {
            email: '',
          }
        };

        this.handleChange = this.handleChange.bind(this);
        this.submit = this.submit.bind(this);
    }
    
    validateForm(errors) {
      let valid = true;
      Object.values(errors).forEach(
        (val: any) => val.length > 0 && (valid = false)
      );
      return valid;
    }

    public handleChange(e) {
      const { name, value } = e.target;
      this.props.forgetPasswordState.Payload[name] = value;
      
      const validEmailRegex =
      RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
      let errors = this.state.errors;

      switch (name) { 
        case 'email':
          errors.email =
            validEmailRegex.test(value)
              ? ''
              : 'Email is not valid!';
          this.setState({ email: value })
          break; 
        default:
          break;
      }

      this.setState({ errors: errors })
    }

    public submit() {  

      let seterrors = this.state.errors;
      seterrors.email = (this.state.email === '') ? 'Please enter email' : '';
      this.setState({ errors: seterrors })
      
      if (this.validateForm(this.state.errors)) {
        this.props.dispatch(passwordForgot(this.props.forgetPasswordState.Payload));
      } else {
        return false
      }
    }

    public render() {
        
            const model = {
            isLoading: this.props.passwordState.Fetching,
            errors: this.state.errors,
            handleChange: this.handleChange,
            state: this.props.forgetPasswordState.Payload,
            submit: this.submit
        };
        return <Forgot {...model} />;
    }
}

interface IConnectState {
    passwordState: IGenericReducerState<IPasswordModel>,
    forgetPasswordState: IGenericReducerState<IForgetPasswordModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
    passwordState: state.Password,
    forgetPasswordState: state.ForgetPassword
});

interface IConnectDispatch {
    dispatch: any;
}
export default connect(mapStateToProps)(ForgotPasswordContainer);