import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { changePassword, clearPasswordModel,  updateProperties } from "../../redux/actions/password.action";
import { IAppState, IPasswordModel } from "../../models";
import { IGenericReducerState } from "../../utilities";
import * as yup from 'yup';

import { ChangePassword } from "./changepassword";
import { getIn } from "formik";

class ChangePasswordContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {
    public passwordLinks;
    private formSchema: any;
    isShowHidePwd= {
        current:false,
        new: false,
        confirm:false
    }
    public constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.showHidePwd = this.showHidePwd.bind(this);
    }

    showHidePwd(type){
        this.isShowHidePwd[type] = !this.isShowHidePwd[type];
        this.setState({isShowHidePwd: {...this.isShowHidePwd}});
    }
    public handleChange(e) {
        const { name, value } = e.target;
        this.props.passwordState.Payload[name] = value;
        this.props.dispatch(updateProperties(this.props.passwordState.Payload));
    }
    public componentWillUnmount() {
        this.props.dispatch(clearPasswordModel({} as IPasswordModel));
    }
    public handleSubmit(e) {
        this.props.passwordState.Payload.confirmPassword = e.confirmPassword;
        this.props.passwordState.Payload.newPassword = e.newPassword;
        this.props.passwordState.Payload.existingPassword = e.existingPassword;
         this.props.dispatch(changePassword(this.props.passwordState.Payload));
    }

    loadSchema() {
        return (this.formSchema = yup.object().shape({
            existingPassword: yup.string().required("Enter existing password"),
            newPassword: yup.string().required('Enter password')
            .matches(new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"),"Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"),
            confirmPassword: yup.string().required("Confirm password is required")
                .oneOf([yup.ref('newPassword'), null], 'Confirm password should same as a new password')
        })
        )
    }

    isInvalidField(fromProps, name): boolean {
        return getIn(fromProps.touched, name) && getIn(fromProps.errors, name);
      }
    
      getInvalidText(fromProps, name) {
        return this.isInvalidField(fromProps, name)
          ? getIn(fromProps.errors, name)
          : null;
      }

    public render() {
        const model = {
            isLoading: this.props.passwordState.Fetching,
            links: this.passwordLinks,
            handleChange: this.handleChange,
            state: this.props.passwordState.Payload,
            handleSubmit: this.handleSubmit,
            schema: this.loadSchema(),
            isInvalidField: this.isInvalidField,
            getInvalidText: this.getInvalidText,
            showHidePwd: this.showHidePwd,
            isShowHidePwd: {...this.isShowHidePwd}
        };
        return <ChangePassword  {...model} />;
    }
}
interface IConnectState {
    passwordState: IGenericReducerState<IPasswordModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
    passwordState: state.Password
});

interface IConnectDispatch {
    dispatch: any;
}
export default connect(mapStateToProps)(ChangePasswordContainer);
