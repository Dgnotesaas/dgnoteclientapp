

import { Formik } from "formik";
import React from "react";
import { Button, Col, Container, Form } from "react-bootstrap";
export const ChangePassword = (props: any) => {
  return (
    <>

      <Formik
        enableReinitialize={true}
        validationSchema={props.schema}
        initialValues={props.state}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            actions.setSubmitting(false);
            props.handleSubmit(values)
          }, 1000);
        }}
      >
        {fromProps => (

         
            <Form className="c-p-form" noValidate onSubmit={fromProps.handleSubmit} onKeyDown={(e) => {
              if (e.key === 'Enter') {
                fromProps.handleSubmit()
              }
            }}>
             
                <h3 className="pb-2 pt-3">CHANGE PASSWORD </h3>
                <Form.Group controlId="existingPassword" className="mt-3"  >
                  <Form.Label className="m-0 mb-1"><b>Current Password</b> </Form.Label>
               
                  <Form.Control
                   type={props.isShowHidePwd.current ?'text':'password'} required name="existingPassword" placeholder="**********"
                    value={fromProps.values.existingPassword}
                    onChange={fromProps.handleChange}
                    isInvalid={props.isInvalidField(fromProps, 'existingPassword')}
                  />
                    <img className="pwd-eye" onClick={ () => props.showHidePwd('current')} alt="" src="https://img.icons8.com/material-outlined/50/000000/visible--v1.png"/>
                  <Form.Control.Feedback type="invalid">
                    {props.getInvalidText(fromProps, 'existingPassword')}
                  </Form.Control.Feedback>

                </Form.Group>
                <Form.Group controlId="newPassword" className="mt-3"  >
                  <Form.Label className="m-0 mb-1"><b>New Password</b> </Form.Label>
                  
                  <Form.Control
                    type={props.isShowHidePwd.new ?'text':'password'} required name="newPassword" placeholder="**********"
                    value={fromProps.values.newPassword}
                    onChange={fromProps.handleChange}
                    isInvalid={props.isInvalidField(fromProps, 'newPassword')}
                  />
                    <img className="pwd-eye" onClick={ () => props.showHidePwd('new')} alt="" src="https://img.icons8.com/material-outlined/50/000000/visible--v1.png"/>
                  <Form.Control.Feedback type="invalid">
                    {props.getInvalidText(fromProps, 'newPassword')}
                  </Form.Control.Feedback>
                </Form.Group>

                <Form.Group controlId="confirmPassword" className="mt-3"  >
                  <Form.Label className="m-0 mb-1"><b>Confirm Password</b> </Form.Label>
                  
                  <Form.Control
                     type={props.isShowHidePwd.confirm ?'text':'password'} required name="confirmPassword" placeholder="**********"
                    value={fromProps.values.confirmPassword}
                    onChange={fromProps.handleChange}
                    isInvalid={props.isInvalidField(fromProps, 'confirmPassword')}
                  />
                    <img className="pwd-eye" onClick={ () => props.showHidePwd('confirm')} alt="" src="https://img.icons8.com/material-outlined/50/000000/visible--v1.png"/>
                  <Form.Control.Feedback type="invalid">
                    {props.getInvalidText(fromProps, 'confirmPassword')}
                  </Form.Control.Feedback>
                </Form.Group>
                <div className="text-right mt-4">
                <Button type="submit">Change Password</Button>
                </div>
                

            
            </Form>
      


        )}

      </Formik>
    
    </>
  );
};


