import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { createPassword} from "../../redux/actions/password.action";
import { IAppState, ICreatePasswordModel} from "../../models";
import { IGenericReducerState } from "../../utilities";
import { Reset } from "./resetPassword";

interface MyState {
    isShowPassword : any,
    code: any,
    email: any,
    newPassword: any,
    errors: {
        code: any,
        email: any,
        newPassword: any,
    }
}

class ResetPasswordContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps, MyState> {
    
    isShowPassword =false;
    
    public constructor(props) {
        super(props);
        this.state = {
            isShowPassword: false,
            code: '',
            email: '',
            newPassword: '',
            errors: {
                code: '',
                email: '',
                newPassword: '',
            }
        };
        this.showHidePwd = this.showHidePwd.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.submit = this.submit.bind(this);
    }
    
    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
          (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    public handleChange(e) {
        const { name, value } = e.target;
        
        this.props.createPasswordState.Payload[name] = value;

        const validEmailRegex =
        RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
        let errors = this.state.errors;
        const passwordExp = RegExp(/^(?=.*\d)(?=.*[!@#$%^&*]).{8,}$/)
        const numberExp = RegExp("\\d{6}")

        switch (name) { 
            case 'code':
            errors.code =
            numberExp.test(value)
                ? ''
                : 'Code is not valid!';
            this.setState({ code: value })
            break; 
            case 'email':
            errors.email =
                validEmailRegex.test(value)
                ? ''
                : 'Email is not valid!';
            this.setState({ email: value })
            break; 
            case 'newPassword':
            errors.newPassword =
            passwordExp.test(value)
                ? ''
                : 'Password must be 8 characters long, one Number and one special case character!';
            this.setState({ newPassword: value })
            break; 
            default:
            break;
        }

      this.setState({ errors: errors })
    }

    showHidePwd(){
        this.isShowPassword = !this.isShowPassword;
        this.setState({isShowPassword: this.isShowPassword});
    }

    public submit() {
        
        let seterrors = this.state.errors;
        seterrors.code = (this.state.code === '') ? 'Please enter code' : '';
        seterrors.email = (this.state.email === '') ? 'Please enter email' : '';
        seterrors.newPassword = (this.state.newPassword === '') ? 'Please enter Password' : '';
        this.setState({ errors: seterrors })

        if (this.validateForm(this.state.errors)) {
            this.props.dispatch(createPassword(this.props.createPasswordState.Payload));
          } else {
            return false
          }
    }

    public render() {
            const model = {
            // isLoading: this.props.passwordState.Fetching,
           
            handleChange: this.handleChange,
            state: this.props.createPasswordState.Payload,
            errors: this.state.errors,
            showHidePwd: this.showHidePwd,
            submit: this.submit,
            isShowPassword : this.isShowPassword,
        };
        return <Reset {...model} />;
    }
}

interface IConnectState {
    createPasswordState: IGenericReducerState<ICreatePasswordModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
    createPasswordState: state.CreatePassword
});

interface IConnectDispatch {
    dispatch: any;
}
export default connect(mapStateToProps)(ResetPasswordContainer);