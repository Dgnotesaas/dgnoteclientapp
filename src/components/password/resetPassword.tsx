

import { Alert, Button, Col, Container, Form } from "react-bootstrap";
export const Reset = (props: any) => {
  console.log('props', props);
  
  return (
    <>
      <Col md={5} className=" mt-5">
        <Form noValidate onSubmit={()=>props.submit()} >
          <div className="login-custom mb-5">
            <h3 className="pb-2 pt-3">FORGOT PASSWORD</h3>
            <Form.Group controlId="uname" className="mt-3"  >
              <Form.Label className="m-0 mb-1"><b>Code</b> </Form.Label>
              {/* <i className="fas fa-user"></i> */}
              <Form.Control 
                type="text" name="code" placeholder="123456" onChange={props.handleChange} defaultValue={props.state.code} 
                required 
              />
              <div style={{"fontSize": "13px", "color": "#ee5151", "minHeight": "15px"}}>
                  {props.errors.code.length > 0 && props.errors.code}
              </div>
            </Form.Group>
            <Form.Group controlId="uname" className="mt-3"  >
              <Form.Label className="m-0 mb-1"><b>Email ID</b> </Form.Label>
              {/* <i className="fas fa-user"></i> */}
              <Form.Control 
                type="email" onChange={props.handleChange} defaultValue={props.state.email}
                placeholder="abc@dgnote.com"
                name="email"
                required 
              />
              <div style={{"fontSize": "13px", "color": "#ee5151", "minHeight": "15px"}}>
                  {props.errors.email.length > 0 && props.errors.email}
              </div>
            </Form.Group>
            <Form.Group controlId="uname" className="mt-3"  >
              <Form.Label className="m-0 mb-1"><b>New Password</b> </Form.Label>
              {/* <i className="fas fa-user"></i> */}
              <Form.Control 
                type={props.isShowPassword?'text':'password'} name="newPassword" placeholder="**********" onChange={props.handleChange} defaultValue={props.state.newPassword} 
              />
              <img className="pwd-eye" onClick={ () => props.showHidePwd()} alt="" src="https://img.icons8.com/material-outlined/50/000000/visible--v1.png"/>
              <div style={{"fontSize": "13px", "color": "#ee5151", "minHeight": "15px"}}>
                  {props.errors.newPassword.length > 0 && props.errors.newPassword}
              </div>
            </Form.Group> 
            <Button type="button" onClick={()=>props.submit()} >Change Password</Button>
           
          </div>
        </Form>
     </Col>
    </>
  );
};

