import * as React from "react";
import { connect } from "react-redux";
import { defaultListSearchRequestModel, IAppState } from "../../../models";

import { IGenericReducerState } from "../../../utilities";
import { Link, RouteComponentProps } from "react-router-dom";

import { Type } from 'react-bootstrap-table2-editor';
import 'font-awesome/css/font-awesome.min.css';

import { ConsentLog } from "./consentlog";
import { defaultBookingListValue, IBookingListModel } from "../../../models/booking-list.model";
import { getBookings, updateBookingListProperties } from "../../../redux/actions/booking-list.actions";
import { IListSearchRequstModel } from "../../../models/list-search-request.model";
import { debounceTime, distinctUntilChanged, Subject } from "rxjs";
import { defaultBookingValue, IBookingModel, ITrip } from "../../../models/booking.model";
import { checkConsent, updateTrip } from "../../../redux/actions/trip.action";
import { Button } from "react-bootstrap";
import { updateBookingProperties } from "../../../redux/actions/booking.action";

class ConsentLogContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {

    searchModel = defaultListSearchRequestModel;
    searchString$: Subject<string> = new Subject<string>();
    listType: number = 1;
    showCancelModal: boolean = false;

    selectedTrips: any = [];
    showCompletedModal: boolean = false;
    public constructor(props) {
        super(props);
        this.handleTableChange = this.handleTableChange.bind(this);
        this.onSearchTextChange = this.onSearchTextChange.bind(this);
        this.inItSearch();
        if (window.location.href.indexOf("/booking-list/active") > -1) {
            this.listType = 1
            this.searchModel.status = "ACTIVE";
        } else if (window.location.href.indexOf("/booking-list/cancelled") > -1) {
            this.listType = 3;
            this.searchModel.status = "CANCELLED";
        }
        else if (window.location.href.indexOf("/booking-list/completed") > -1) {
            this.listType = 2
            this.searchModel.status = "COMPLETED";
        }

        this.fetchList(this.searchModel);
    }

    inItSearch() {
        this.searchString$.pipe(
            debounceTime(1000),
            distinctUntilChanged(),
        ).subscribe(text => {
            this.searchModel.searchToken = text;
            this.fetchList(this.searchModel);
        })
    }
    closeModal() {
        this.showCompletedModal = false;
        this.showCancelModal = false;
        this.selectedTrips = [];
        this.setState({ showCancelModal: false, showCompletedModal: false })
    }

    confrimModalAction(id) {

    }
    onSearchTextChange(e) {
        this.searchString$.next(e.target.value.trim())
    }

    fetchList(model: IListSearchRequstModel) {

        this.props.dispatch(getBookings(model));
    }


    async updateTransistActualDate(tripId, newDate) {

        let trips = [...this.props.bookingListState.Payload.content] as ITrip[];
        let trip = trips.find(x => x.id === tripId) as ITrip;
        trip.transitActualEndDate = new Date(newDate).getTime();
        await this.props.dispatch(updateTrip(trip));
        this.fetchList(this.searchModel);
    }

    handleTableChange = (type, { page, sizePerPage, filters, sortField, sortOrder, cellEdit }) => {

        if (type === 'pagination') {
            this.searchModel.pageNumber = page - 1;
            this.searchModel.pageSize = sizePerPage;
            this.fetchList(this.searchModel);
        }
        else if (type === "sort") {
            this.searchModel.sortDirection = sortOrder ? sortOrder.toUpperCase() : "ASC";
            this.searchModel.sortProperty = sortField;
            this.fetchList(this.searchModel);
        }
        else if (type === "cellEdit" && cellEdit && cellEdit.dataField === 'transitActualEndDate') {
            this.updateTransistActualDate(cellEdit.rowId, cellEdit.newValue);
        }
    }

    onCompleteTripClicked(id, trip) {
        this.showCompletedModal = true;
        this.selectedTrips.push(trip);
        this.setState({ showCompletedModal: true })
    }

    onCancelTripClicked(id, trip) {
        this.showCancelModal = true;
        this.selectedTrips.push(trip);
        this.setState({ showCancelModal: true })
    }

    async onRefreshConsent(id) {
        await this.props.dispatch(checkConsent(id));
    }

    async onResendConsent(id) {

    }

    getColumnsDefination() {
        const allColumns = [
            {
                dataField: 'tripNumber',
                text: 'No.',
                sort: true,
                editable: false,
                listType: [1, 2, 3],

            }, {
                dataField: 'bookingDate',
                text: 'Date',
                sort: true,
                formatter: (cell) => {
                    return <>{new Date(cell).toLocaleDateString()}</>
                }
                ,
                editable: false,
                listType: [1, 2, 3]
            },
            {
                dataField: 'tripNumber',
                text: 'Trip No.',
                sort: true,
                editable: false,
                listType: [1, 2, 3],

            }, 
            {
                dataField: 'fromLocation.city',
                text: 'From Loc.',
                sort: true,
                editable: false,
                listType: [1, 2, 3]
            },
            {
                dataField: 'toLocation.city',
                text: 'To Loc.',
                sort: true,
                editable: false,
                listType: [1, 2, 3]
            },
            {
                dataField: 'transporterName',
                text: 'Transpoter',
                sort: true,
                editable: false,
                listType: [1, 2, 3],
                headerStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
                  },
                  tdStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
                  }
            },  
            {
                dataField: 'vehicleNumber',
                text: 'Vehicle No.',
                sort: true,
                editable: false,
                listType: [1, 2, 3],
                headerStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
                  },
                  tdStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
                  }
            },
            {
                dataField: 'driverMobileNumber',
                text: 'Driver  No.',
                sort: true,
                editable: false,
                listType: [1, 2, 3],
                headerStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
                  },
                  tdStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
                  }
            },
            {
                dataField: 'transporterName',
                text: 'Operator',
                sort: true,
                editable: false,
                listType: [1, 2, 3],
                headerStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
                  },
                  tdStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
                  }
            },
            {
                dataField: 'consentStatus',
                text: 'Send Status',
                sort: true,
                editable: false,
                formatter: (cell) => {
                    return <>
                        <span>
                            {cell === "NOT_INITIATED" || cell === "PENDING" || cell === "EXPIRED" ? <span>  Resend </span> : cell === "ALLOWED" ? <span> Send </span> : <span className="custom-t-btn">  </span>}
                        </span>
                    </>
                },
                listType: [1],
                headerStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
                  },
                  tdStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
                  }
            },  
            {
                dataField: 'consentStatus',
                text: 'Revoke Status',
                sort: true,
                editable: false,
                formatter: (cell) => {
                    return <>
                        <span>
                            {cell === "NOT_INITIATED" || cell === "PENDING" || cell === "EXPIRED" ? <span>  Resend </span> : cell === "ALLOWED" ? <span> Send </span> : <span className="custom-t-btn">  </span>}
                        </span>
                    </>
                },
                listType: [1],
                headerStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
                  },
                  tdStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
                  }
            },  
            {
                dataField: 'transporterName',
                text: 'Consent Count',
                sort: true,
                editable: false,
                listType: [1, 2, 3],
                headerStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
                  },
                  tdStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
                  }
            },   
            {
                dataField: 'consentStatus',
                text: 'Consent Status',
                sort: true,
                editable: false,
                formatter: (cell) => {
                    return <>
                        <span>
                            {cell === "NOT_INITIATED" || cell === "PENDING" || cell === "EXPIRED" ? <span className="consent-st-trips">  <i className="fa fa-times-circle-o fa-2x text-danger"></i> </span> : cell === "ALLOWED" ? <span className="custom-t-btn">  <i className="fa fa-check-circle-o fa-2x text-success"></i> </span> : <span className="custom-t-btn"><i className="fa fa-trash-o fa-2x"></i> </span>}
                        </span>
                    </>
                },
                listType: [1],
                headerStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
                  },
                  tdStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
                  }
            }, 
            {
                dataField: 'id',
                text: 'Consent Action',
                sort: false,
                editable: false,
                formatter: (cell) => {
                
                  return <>
                    <Button className="d-inline-block custom-t-btn" onClick={() => this.onRefreshConsent(cell)} variant="link">Refresh</Button>
                    {/* <Button className="d-inline-block custom-t-btn" onClick={() => this.onResendConsent(cell)} variant="link">Resend</Button> */}
                  </>
                },
                listType: [1],
                headerStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
                  },
                  tdStyle: (colum, colIndex) => {
                    return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
                  }
              },

        ];

        return allColumns;
    }

    getselectedTripColumns() {
        const selectedTripColumns = [
            {
                dataField: 'tripNumber',
                text: 'No.',
                sort: true,
                editable: false

            }, {
                dataField: 'tripNumber',
                text: 'Trip No.',
                sort: true,
                editable: false

            }, {
                dataField: 'bookingDate',
                text: 'Date',
                sort: true,
                formatter: (cell) => {
                    return <>{new Date(cell).toLocaleDateString()}</>
                }
                ,
                editable: false
            }, {
                dataField: 'tripType',
                text: 'Trip Type',
                //filter: textFilter(),
                sort: true,
                formatter: (cell) => {
                    return <>{cell.replace("_", " ")}</>
                },
                editable: false

            },
            {
                dataField: 'fromLocation.city',
                text: 'From Loc.',
                sort: true,
                editable: false

            },
            {
                dataField: 'toLocation.city',
                text: 'To Loc.',
                sort: true,
                editable: false

            },
            {
                dataField: 'transitStartDate',
                text: 'Start Date',
                sort: true,
                formatter: (cell) => {
                    let dateObj = cell;
                    if (typeof cell !== 'object') {
                        dateObj = new Date(cell);
                    }
                    return `${('0' + dateObj.getDate()).slice(-2)}/${('0' + (dateObj.getMonth() + 1)).slice(-2)}/${dateObj.getFullYear()}`;
                },
                editable: false,
                listType: [1, 2, 3]
            },
            {
                dataField: 'transitEstimatedEndDate',
                text: 'End Date',
                sort: true,
                formatter: (cell) => {
                    let dateObj = cell;
                    if (typeof cell !== 'object') {
                        dateObj = new Date(cell);
                    }
                    return `${('0' + dateObj.getDate()).slice(-2)}/${('0' + (dateObj.getMonth() + 1)).slice(-2)}/${dateObj.getFullYear()}`;
                },
                editable: false,

            },

            {
                dataField: 'customerName',
                text: 'Customer',
                sort: true,
                editable: false,

                headerStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'nowrap', 'textOverflow': 'ellipsis' };
                },
                tdStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'nowrap', 'textOverflow': 'ellipsis' };
                }

            },
            {
                dataField: 'transporterName',
                text: 'Transpoter',
                sort: true,
                editable: false,

                headerStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'normal' };
                },
                tdStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'normal' };
                }
            },
            {
                dataField: 'vehicleNumber',
                text: 'Vehicle No.',
                sort: true,
                editable: false,

                headerStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'normal' };
                },
                tdStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'normal' };
                }
            },
            {
                dataField: 'driverMobileNumber',
                text: 'Driver  No.',
                sort: true,
                editable: false,

                headerStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'normal' };
                },
                tdStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'normal' };
                }
            },
            {
                dataField: 'containerNumber',
                text: 'Container No.',
                sort: true,
                editable: false,

                headerStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'normal' };
                },
                tdStyle: (colum, colIndex) => {
                    return { width: '40px', textAlign: 'center', 'whiteSpace': 'normal' };
                }
            }


        ];
        return selectedTripColumns;
    }

    public handleRemarksChange(e) {
        const { name, value } = e.target;
        this.selectedTrips[0].remarks = value;
        this.setState({ showCancelModal: true })

    }
    public async submitCancelTrip() {
        var trip = this.selectedTrips[0] as ITrip;
        trip.status = "CANCELLED";
        await this.props.dispatch(updateTrip(trip));
        this.fetchList(this.searchModel);
        this.closeModal();
    }
    async submitCompleteTrip() {
        var trip = this.selectedTrips[0] as ITrip;
        trip.status = "COMPLETED";
        await this.props.dispatch(updateTrip(trip));
        this.fetchList(this.searchModel);
        this.closeModal();
    }
    componentWillUnmount() {
        this.props.dispatch(updateBookingListProperties({ ...defaultBookingListValue }))
        this.props.dispatch(updateBookingProperties(JSON.parse(JSON.stringify({...defaultBookingValue}))));
    }
    public render() {

        let { content, currentPage, currentPageSize, totalElements } = this.props.bookingListState.Payload;
        currentPage = currentPage + 1;
        if (!content)
            content = [];

        content = content.map(x => {
            x.transitActualEndDate = new Date(x.transitActualEndDate).toLocaleDateString();
            return x;
        })

        return <ConsentLog
            listType={this.listType}
            data={content}
            page={currentPage}
            sizePerPage={currentPageSize}
            totalSize={totalElements}
            onTableChange={this.handleTableChange}
            onSearchTextChange={this.onSearchTextChange}
            columnsDef={this.getColumnsDefination()}
            closeModal={this.closeModal.bind(this)}
            confirmCancelModal={this.submitCancelTrip.bind(this)}
            selectedTrips={this.selectedTrips}
            showCancelModal={this.showCancelModal}
            showCompletedModal={this.showCompletedModal}
            confirmCompletedModal={this.submitCompleteTrip.bind(this)}
            getColums={this.getselectedTripColumns.bind(this)}
            handleRemarksChange={this.handleRemarksChange.bind(this)}
        />;
    } 
}

interface IConnectState {
    bookingListState: IGenericReducerState<IBookingListModel>;
    bookingState: IGenericReducerState<IBookingModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
    bookingListState: state.BookingList,
    bookingState: state.Booking
});

interface IConnectDispatch {
    dispatch: any;
}

export default connect(mapStateToProps)(ConsentLogContainer);