import * as React from "react";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
import filterFactory from 'react-bootstrap-table2-filter'
import Button from "react-bootstrap/esm/Button";
import { Form, Modal} from "react-bootstrap";



const defaultSorted = [{
  dataField: 'bookingDate',
  order: 'desc'
}];

const cellEditProps = {
  mode: 'click',
  blurToSave: true
};


export const ConsentLog = ({ data, page, sizePerPage, onTableChange, totalSize, 
  onSearchTextChange,listType,columnsDef,showCancelModal,closeModal, 
  confirmCancelModal,selectedTrips,getColums,showCompletedModal,confirmCompletedModal,handleRemarksChange }) => {
  const columns=columnsDef.filter(x=>x.listType.indexOf(listType)>-1);

  let tableRef = React.createRef();
  
  return (


    <div className="bt-data-table-custom">

      <div className="">
        <div className="row">
          <div className="col-md-6">
            <h3 className="font-weight-bold mb-0">Consent Log</h3>
          </div>
          <div className="col-md-6">
            <div className="text-right">
              {/* <button className="btn btn-sm button-theme btn-rounded text-uppercase mr-2">
                <a href="#" className="text-white">Column Arragment</a>
              </button>
              <button className="btn btn-sm button-theme btn-rounded text-uppercase">
                <a href="#" className="text-white">Export</a>
              </button> */}
            </div>
          </div>
        </div>
        {/* <div className="row pb-3 pt-5">

          <div className="col-md-3">
            <input type="text"
              onChange={onSearchTextChange}
              className="form-control form-control-sm"
              placeholder="Search" />
          </div>
        </div>
        <div>
          <Button className="btn btn-sm button-theme btn-rounded mb-1" onClick={e => {
            let table = tableRef as any;
            table.current.sortContext.handleSort(defaultSorted);
              }}>
                Reset Sort
          </Button>
        </div> */}

        <div className="row pb-3 pt-5 d-block txt-ryt">

          <div className="col-md-3 d-inline-block txt-lft">
            <Button className="btn btn-sm button-theme btn-rounded mb-1" onClick={e => {
              let table = tableRef as any;
              table.current.sortContext.handleSort(defaultSorted);
            }}>
              Reset Sort
            </Button>
          </div>
          <div className="col-md-6 d-inline-block">
          </div>
          <div className="col-md-3 d-inline-block">
            <input type="text"
              onChange={onSearchTextChange}
              className="form-control form-control-sm"
              placeholder="Search" />
          </div>
        </div>
        <div className="row pb-3">
          <div className="col-md-12 booking-table">
            <div className="responsive-table">
              <BootstrapTable
               ref={tableRef }
                remote
                keyField="tripNumber"
                data={data}
                columns={columns}
                striped
                hover
                condensed
                filter={filterFactory()}
                pagination={paginationFactory({ page, sizePerPage, totalSize, sizePerPageList: [{
                  text: '5', value: 5
                }, {
                  text: '10', value: 10
                }, {
                  text: '25', value: 25
                },
                {
                  text: '100', value: 100
                } ],
              })}
                cellEdit={cellEditFactory(cellEditProps)}
                onTableChange={onTableChange}
              />
            </div>
          </div>
        </div>

      </div>
     {
       selectedTrips.length>0?
      <Modal
        show={showCancelModal}
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
        dialogClassName="modal-90w"
      >
        <Modal.Header closeButton>
          <Modal.Title>Transit Cancelled!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
  <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
        <BootstrapTable
        key="1"
                keyField="id"
                data={selectedTrips}
                columns={getColums()}
              />
  </Form.Group>
  <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
    <Form.Label>Cancel Reason</Form.Label>
     
    <Form.Control
      name="remarks"
      required
      onChange={handleRemarksChange}
      value={selectedTrips[0].remarks}
      
    as="textarea" rows={3} />
  </Form.Group>
</Form>
</Modal.Body>
        <Modal.Footer>
        
          <Button variant="primary"  disabled={!selectedTrips[0].remarks}  onClick={()=>confirmCancelModal()}>Submit</Button>
        </Modal.Footer>
      </Modal>
  :""}
      <Modal
        show={showCompletedModal}
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
        dialogClassName="modal-90w"
      >
        <Modal.Header closeButton>
          <Modal.Title>Transit Complete!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
     
 
        <BootstrapTable
                  key="1"
                keyField="id"
                data={selectedTrips}
                columns={getColums()}
              />
  
 
</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Close
          </Button>
          <Button variant="primary" onClick={()=>confirmCompletedModal()}>Submit</Button>
        </Modal.Footer>
      </Modal>
  
    </div>
  );


};