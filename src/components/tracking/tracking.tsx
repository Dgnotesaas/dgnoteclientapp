import { useState, useEffect } from 'react';
import './css/tracking.css';
import { GoogleMap, LoadScript, Marker, InfoWindow } from '@react-google-maps/api';
import { Container, Row, Dropdown, Form, Modal, Table, Button, InputGroup, FormControl } from 'react-bootstrap';
import LoadScriptOnlyIfNeeded from './LoadScriptOnlyIfNeeded';
import Scroll from "./Search/Scroll";
import Geocode from "react-geocode";

const mapStyles = {
    height: "80vh",
    width: "100%"
};

Geocode.setApiKey("AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU");

const defaultCenter = {
    lat: 20.593683, lng: 78.962883
}

function Tracking({ locationsval }) {
    const [selected, setSelected] = useState<any>({});
    const [searchField, setSearchField] = useState("");

    const onSelect = item => {
        setSelected(item);
    };

    // Converts numeric degrees to radians
    function toRad(Value) {
        return Value * Math.PI / 180;
    }

    let count = 0;
    let filteredLocations : any[] = [];
    
    if (locationsval != '' && typeof locationsval !== 'undefined') { 
        
        const handleChange = e => {
            setSearchField(e.target.value);
        };

        filteredLocations = Object.values(locationsval).filter(
            (location: any) => {
                return (
                    (location.transporterName || '').toLowerCase().includes(searchField.toLowerCase())
                    || (location.vehicleNumber || '').toLowerCase().includes(searchField.toLowerCase())
                    || (location.driverMobileNumber || '').toLowerCase().includes(searchField.toLowerCase())
                    || (location.fromLocation.addressLine1 || '').toLowerCase().includes(searchField.toLowerCase())
                    || (location.toLocation.addressLine1 || '').toLowerCase().includes(searchField.toLowerCase())
                );
            }
        );

        return (
            <div>
                
                    <Row className="mt-4">
                        <div className="col-md-12 mb-4">
                            <h3 className="font-weight-bold mb-0" >All Vehicle</h3>
                        </div>
                        <div className="col-md-12">
                            <Form.Group className="mb-3" controlId="formBasicSearch">
                                <InputGroup className="mb-3">

                                    <FormControl
                                        type="search"
                                        placeholder="Search"
                                        aria-label="Search"
                                        aria-describedby="basic-addon1"
                                        onChange={handleChange}
                                    /> 
                                </InputGroup>
                            </Form.Group>
                        </div>
                        {/* <div className="col-md-12">
                            <p><b>Legends: DN - </b>Driver No. <b> / FL - </b> From Location <b>/ CL - </b>Current Location <b> / TL - </b> To Location <b>/ ETA - </b> Estimate Time of Arrival</p>
                        </div> */}
                        <div className="col-md-12 mb-4">
                            <LoadScriptOnlyIfNeeded
                                googleMapsApiKey='AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU'>
                                <GoogleMap
                                    mapContainerStyle={mapStyles}
                                    zoom={5}
                                    center={defaultCenter}>
                                    {
                                        filteredLocations.map((item: any) => {
                                            count = count + 0.0001;
                                            if (item.lastSuccessfullTrackingHit != null) {
                                                return (
                                                    <Marker
                                                        key={item.bookingId}
                                                        position={{ lat: item.lastSuccessfullTrackingHit['latitude'] + count, lng: item.lastSuccessfullTrackingHit['longitude'] + count }}
                                                        onMouseOver={() => onSelect(item)}
                                                        onClick={()=> window.open('../tracking/single-tracking/'+item.id+'/type', "")}
                                                    />
                                                )
                                            } else {
                                                return (<></>)
                                            }
                                        })
                                    }
                                    {
                                        selected?.lastSuccessfullTrackingHit &&
                                        (
                                            <InfoWindow
                                                position={{ lat: selected.lastSuccessfullTrackingHit.latitude, lng: selected.lastSuccessfullTrackingHit.longitude }} 
                                                options={{ pixelOffset: new window.google.maps.Size(0, -40) }}
                                                // clickable={true}
                                                onCloseClick={() => setSelected({})}
                                            >
                                                <div className="infowindowcont">
                                                    <Table hover bordered> 
                                                        <tbody>
                                                            <tr>
                                                                <td><b style={{'fontWeight': 'bold'}}>Vehicle No. </b></td>
                                                                <td><b>{selected.lastSuccessfullTrackingHit.tripDto.vehicleNumber}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b style={{'fontWeight': 'bold'}}>Driver No. </b></td>
                                                                <td><b>{selected.lastSuccessfullTrackingHit.tripDto.driverMobileNumber}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b style={{'fontWeight': 'bold'}}>From Loc.</b></td>
                                                                <td><b>{selected.fromLocation.city} {selected.fromLocation.state} {selected.fromLocation.pinCode}</b></td>
                                                            </tr>
                                                            <tr style={{"color": "rgb(255, 11, 11)"}}>
                                                                <td><b style={{'fontWeight': 'bold'}}>Current Loc.</b></td>
                                                                <td><b>{selected.lastSuccessfullTrackingHit.address}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b style={{'fontWeight': 'bold'}}>To Loc.</b></td>
                                                                <td><b>{selected.toLocation.city} {selected.toLocation.state} {selected.toLocation.pinCode}</b></td>
                                                            </tr>
                                                            <tr>    
                                                                <td><b style={{'fontWeight': 'bold'}}>Speed </b></td>
                                                                <td><b>{selected.speed} Km/hr</b></td>
                                                            </tr>
                                                        </tbody>
                                                    </Table>  
                                                </div>
                                            </InfoWindow>
                                        )
                                    }
                                </GoogleMap>
                            </LoadScriptOnlyIfNeeded>
                        </div>
                        <VehicleDetailTable />
                    </Row>
               
            </div>
        );
    } else {
        return (<div><VehicleDetailTable /></div>)
    }

    function VehicleDetailTable() {
        return (
            <div className="mt-2">
                <Scroll>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Trip No.</th>
                                <th>Trip Type</th>
                                <th>From Loc.</th>
                                <th>To Loc.</th>
                                <th>Vehicle No.</th>
                                <th>Total Distance (KM)</th>
                                <th>Travelled (KM)</th>
                                <th>Distance Left (KM)</th>
                                <th>Current Loc.</th>
                                <th>ETA</th>
                            </tr>
                        </thead>
                        <TableBody />
                    </Table>
                </Scroll>
            </div>
        );
    }

    function TableBody() {

        if (locationsval) {
            console.log(locationsval);
            
            return (
                <tbody>
                    {filteredLocations.map((item: any) => {

                        if (item.lastSuccessfullTrackingHit != null) {
                            var totaldis : any = calcCrow(item.fromLocation.latitude, item.fromLocation.longitude, item.toLocation.latitude, item.toLocation.longitude).toFixed(1);
                            var travelled : any = calcCrow(item.fromLocation.latitude, item.fromLocation.longitude, item.lastSuccessfullTrackingHit.latitude, item.lastSuccessfullTrackingHit.longitude).toFixed(1)
                            var leftdis : any = totaldis - travelled;

                            return (
                                <tr key={item.lastSuccessfullTrackingHit.tripDto.id}>
                                    <td>{item.lastSuccessfullTrackingHit.tripDto.tripNumber}</td>
                                    <td>{item.lastSuccessfullTrackingHit.tripDto.tripType}</td>
                                    <td>{item.fromLocation.city} {item.fromLocation.pinCode} {item.fromLocation.state} </td>
                                    <td>{item.toLocation.city} {item.toLocation.pinCode} {item.toLocation.state}</td>
                                    <td>{item.lastSuccessfullTrackingHit.tripDto.vehicleNumber}</td>
                                    <td>{totaldis}</td>
                                    <td>{travelled}</td>
                                    <td>{leftdis.toFixed(1)}</td>
                                    <td>{item.lastSuccessfullTrackingHit.address}</td>
                                    <td>{calcTime(item.fromLocation.latitude, item.fromLocation.longitude, item.toLocation.latitude, item.toLocation.longitude).toFixed(1)} Hours</td>
                                </tr>
                            )
                        } else {
                            return (<></>);
                        }
                    }
                    )}
                </tbody>
            )
        } else {
            return (
                <tbody>
                    <tr>No Data found</tr>
                </tbody>
            )
        }
    }

    function calcCrow(lat1: any, lon1, lat2: any, lon2: any) {
        var R = 6371; // km
        var dLat = toRad(lat2 - lat1);
        var dLon = toRad(lon2 - lon1);
        var lat1: any = toRad(lat1);
        var lat2: any = toRad(lat2);

        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    }

    function calcTime(lat1: any, lon1, lat2: any, lon2: any) {
        var R = 6371; // km
        var dLat = toRad(lat2 - lat1);
        var dLon = toRad(lon2 - lon1);
        var lat1: any = toRad(lat1);
        var lat2: any = toRad(lat2);

        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d/20;
    }

};

export default Tracking;