import React from "react";
import { connect } from "react-redux";
import { defaultTrackingListValue, IAppState } from "../../models";
import { IGenericReducerState } from "../../utilities";
import { RouteComponentProps } from "react-router-dom";
import { toast } from "react-toastify";
import moment from "moment";
import { Subject } from "rxjs";
import { IAllHitListModel, defaultAllHitListValue} from "../../models/allactivetrips.model";
import { IAllActiveTripModel } from "../../models/allactivetrips.model"; 
import { IEmailTemplateModel } from "../../models/emailtemplate.model";
import { postEmail } from "../../redux/actions/emailtemplate.action"; 
import { getActiveLocateTripsById, getAllHitTripsById, getHealthActiveLocateTripsById, getHealthAllHitTripsById} from "../../redux/actions/allactivetrips.action"; 
import SingleSearch from "./Search/SingleSearch";

interface Trackdetail { selItem: string[]; locationsval: any[]; handleChangeVal: (e: any) => void; showmodel: any; }
type MyState = { value: [string]};

class TrackingSingleVehicleContainer extends React.Component<Trackdetail &  IConnectState & IConnectDispatch & RouteComponentProps, MyState> {

  searchModel = defaultTrackingListValue;
  searchString$: Subject<string> = new Subject<string>();
  listType: number = 1;

  public constructor(props:any) {
    super(props); 
    this.state = {
      value: [''],
    };
    this.props.allHitState.Payload = {...defaultAllHitListValue};
    this.fetchFilterList();
    this.handleChange = this.handleChange.bind(this);
  } 

  async fetchFilterList() {
    if(this.props.match.params["type"] == 'type') {
      var tripId = {
          "tripIds": [
              this.props.match.params["id"]
            ]
      }
      await this.props.dispatch(getActiveLocateTripsById(tripId));
      await this.props.dispatch(getAllHitTripsById(tripId));
    } else {
      var settripId = {
        "tenantName": this.props.match.params["type"],
        "tripIds": [
            this.props.match.params["id"]
          ]
      }
      await this.props.dispatch(getHealthActiveLocateTripsById(settripId));
      await this.props.dispatch(getHealthAllHitTripsById(settripId));
    }
  }

  shareMap = (e, getArray) => {
    e.preventDefault(); 
      let today : any = new Date();
      let dd = String(today.getDate()).padStart(2, '0');
      let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = today.getFullYear(); 
      
      today = dd + '-' + mm + '-' + yyyy; 

      this.props.emailTemplateSend.Payload.email_to = this.state.value    

      this.props.emailTemplateSend.Payload.subject = "TMS | Tracking | "+ today    
      let header = "<div style='width: 100%; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;'><div style='display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;'><div style='margin-bottom: 1.5em; flex: 0 0 100%; max-width: 100%;'><p>Dear ,</p><p>Please find vehicle status below:</p></div></div></div>"
      
      let body1 = "<div style='width: 100%; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;'><div style='display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;'><div style=' flex: 0 0 100%; max-width: 100%;'><h3 style='margin-bottom: 5px;'><b>Transit Details:</b></h3><table border='1' style='border-collapse: collapse; margin-bottom: 0; width: 100%; margin-bottom: 1rem; color: #212529;'><thead>"

      let body2 =  ""
      let id = this.props.match.params["id"];       
      let hostName = ""
      if(window.location.hostname == 'localhost') {
        hostName = "dev.tms.dgnote.com";
      } else {
        hostName = window.location.hostname
      }

      let customer = '';
      Object.values(getArray).map((item: any) => {
        customer = (item.customer)? item.customer : ''; 
        body2 = body2 + "<tr><th style='padding: 10px;' colspan='4'><span style='float: left; margin-right: 50px;'>"+ item.vehicleNumber +"</span> <span style='float: right;'>"+item.current.address+"</span> </th><th style='padding: 10px; text-align: center;' rowspan='3'>Map</th></tr></thead><tbody><tr><td style='padding: 10px'><b>From Location</b></td><td style='padding: 10px'>"+item.from.city+" "+item.from.state+"</td><td style='padding: 10px'><b>To Location</b></td><td style='padding: 10px'>"+item.to.city+" "+item.to.state+"</td><td style='padding: 10px' rowspan='3'><a href='https://"+hostName+"/tracksharing/"+id+"/type'><img width='50' height='50' src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Google_Maps_icon_%282015-2020%29.svg/2048px-Google_Maps_icon_%282015-2020%29.svg.png'/></a></td></tr><tr><td style='padding: 10px'><b>Customer</b></td><td style='padding: 10px'>"+customer+"</td><td style='padding: 10px'><b>Transit End Date</b></td><td style='padding: 10px'>"+moment(item.transitEndDate).format('DD-MM-YYYY')+"</td></tr>"
      })

      let body3 = "</tbody></table></div></div></div>"
  
      let footer = "<div style='width: 100%; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;'><div style='margin-top: 1.5rem !important; display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;'><div style=' flex: 0 0 100%; max-width: 100%;'><p>Regards,</p><p>DgNote Team</p></div></div></div>"
  
      this.props.emailTemplateSend.Payload.content = header +""+ body1 +""+ body2 +""+ body3 +""+  footer
      
      this.setState({
        value: ['']
      })

      this.props.dispatch(postEmail(this.props.emailTemplateSend.Payload));
  } 

  handleChange = (event) => {
    event.preventDefault();
    const { value } = event.target;
    if(this.state.value[0] == '') {
      this.setState({
        value: [value]
      })
    } else {
      if(event.target.value != '') {
        const updatearray: any[string] = [...this.state.value, event.target.value]
        this.setState({
          value: updatearray
        })
    }
    } 
  } 

  public render() {
    let filterloc = this.props.trackingState.Payload;
    let allhit = this.props.allHitState.Payload; 

    return (<div><SingleSearch filterLocationval={filterloc} allhit={allhit} shareMap = {this.shareMap} handleChange = {this.handleChange} showmodel={this.state}/></div>);
  }

}
interface IConnectState {
  trackingState: IGenericReducerState<IAllActiveTripModel[]>;
  allHitState: IGenericReducerState<IAllHitListModel[]>;
  emailTemplateSend: IGenericReducerState<IEmailTemplateModel>; 
}


const mapStateToProps = (state: IAppState): IConnectState => ({
  trackingState: state.AllActiveTrip,
  allHitState: state.AllHitTripById,
  emailTemplateSend: state.EmailTemplate,
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(TrackingSingleVehicleContainer);