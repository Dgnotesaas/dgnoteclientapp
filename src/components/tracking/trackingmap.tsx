import React, { Component, useEffect, useState } from "react";
import { Container, Row, Table } from "react-bootstrap";
import { GoogleMap, LoadScript, Marker, InfoWindow, DirectionsRenderer, Polyline} from '@react-google-maps/api';
import './css/tracking.css';

import mapimg from './image/mapiconssmall.png'
import fromimg from './image/fromsmall.png'
import toimg from './image/tosmall.png'
import direction from './image/direction.png'
import LoadScriptOnlyIfNeeded from "./LoadScriptOnlyIfNeeded";

const mapStyles = {
    height: "80vh",
    width: "100%"
};

const defaultCenter = {
    lat: 20.593683, lng: 78.962883
}

var getDirections: any[] = [];
var getConstval: any[] = []

function MapDetails({ path, hitPath }) {
    
    const [directions, setDirection] = useState<any>([]);
    const [selected, setSelected] = useState<any>({
        item: [],
        lat: '',
        lng: ''
    });
    const [typecall, setCall] = useState<any>(true);

    const onSelect = (item, lat, lng) => {
        setSelected({ item, lat, lng });
    };

    useEffect(() => {
        if (typecall) {
            mapDirection();
        }
    });
    
    // debugger 
    function mapDirection() {
        if (typeof window.google !== 'undefined' && window.google != null) {
            const directionsService = new window.google.maps.DirectionsService();
            path.map(getLocation => {
                if (getLocation != '' && typeof getLocation !== 'undefined' && getLocation != null) {
                    const origin = { lat: getLocation.from.lat, lng: getLocation.from.lng };
                    const destination = { lat: getLocation.to.lat, lng: getLocation.to.lng };
                    directionsService.route(
                        {
                            origin: origin,
                            destination: destination,
                            travelMode: window.google.maps.TravelMode.DRIVING,
                            waypoints: [
                                {
                                    location: new google.maps.LatLng(getLocation.current.lat, getLocation.current.lng)
                                }]
                        },
                        (result, status) => {
                            if (status === window.google.maps.DirectionsStatus.OK) {
                                if (result != null) {

                                    if (!('id' in result)) {
                                        result['id'] = getLocation.id
                                        getDirections.push(result)
                                            setDirection(olddata => [...olddata, result])
                                            // setDirection(result);
                                        // setDirection(directions.filter(item => item.id !== result['id']));
                                        setCall(false)
                                    }
                                }
                            } else {
                                console.error(`error fetching directions ${result}`);
                            }
                        }
                    );
                }
            })
        }
    }
 
    let travLoc : any[] = []; 

    return (
        <div>
            <Container>
                <Row className="mt-4">
                    
                    <LoadScriptOnlyIfNeeded
                        googleMapsApiKey='AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU'>
                        <GoogleMap
                            mapContainerStyle={mapStyles}
                            zoom={5}
                            center={defaultCenter}>
                            {/* {setCall(true)} */}
                            {
                                Object.values(path).map((item: any) => {
                                    if (item != '' && typeof item !== 'undefined' && item != null) {
                                        return (
                                            <div> 
                                                <Marker
                                                    key={item.current.lat}
                                                    position={{ lat: item.current.lat, lng: item.current.lng }}
                                                    onMouseOver={() => onSelect(item, item.current.lat, item.current.lng)}
                                                    // onClick={() => onSelect(item, item.current.lat, item.current.lng)}
                                                    icon={{
                                                        url: mapimg,

                                                    }}
                                                />
                                                <Marker
                                                    // key={item.vehicleinfo}
                                                    position={{ lat: item.from.lat, lng: item.from.lng }}
                                                    onMouseOver={() => onSelect(item, item.from.lat, item.from.lng)}
                                                    // onClick={() => onSelect(item, item.from.lat, item.from.lng)}
                                                    icon={{
                                                        url: toimg,
                                                    }}
                                                />
                                                <div style={{"height": "250px", padding: '10px', width: "200px", borderRadius: '3px', background: '#f5f5f5', opacity: '0.9', border: '2px solid #bbb',zIndex: 1000, position: 'absolute', right: '15%', top: '120px'}}>
                                                    <h5>{item.vehicleNumber}</h5>
                                                    <p><b>DN - </b> {item.driverMobileNumber}</p>
                                                    <p><b>FL - </b> {item.from.city} {item.from.state} {item.from.pinCode}</p>
                                                    {/* <p className="cl"><b>CL - {item.current.address}</b></p> */}
                                                    <p><b>CL - </b> {item.to.city} {item.to.state} {item.to.pinCode} </p>
                                                    <p><b>TL - </b> {item.to.city} {item.to.state} {item.to.pinCode} </p>
                                                </div>
                                                <Marker
                                                    // key={item.vehicleinfo}
                                                    position={{ lat: item.to.lat, lng: item.to.lng }}
                                                    onMouseOver={() => onSelect(item, item.to.lat, item.to.lng)}
                                                    // onClick={() => onSelect(item, item.to.lat, item.to.lng)}
                                                    icon={{
                                                        url: fromimg,
                                                    }}
                                                />
                                            </div>
                                        )
                                    }
                                })
                            } 
                            {
                                hitPath && Object.values(hitPath).map((hititem: any) => {
                                    if (hititem != '' && typeof hititem !== 'undefined' && hititem != null) {
                                        travLoc = hititem.map((getitem: any) => {  
                                            return ({lat: getitem.latitude, lng:getitem.longitude}
                                            )
                                        })

                                        travLoc.sort((n1,n2) => n1.lat - n2.lat);

                                        console.log('travLoc', hititem[0].tripDto.tripType);
                                        let linecolor = (hititem[0].tripDto.tripType == 'SINGLE_TRIP')? "#ff2527" : "#111127"

                                        return (
                                            <div>
                                                <Polyline
                                                    path={travLoc}
                                                    options={{
                                                        strokeColor: linecolor,
                                                        strokeOpacity: 0.75,
                                                        strokeWeight: 7
                                                    }}
                                                />
                                            </div>
                                        )
                                    }
                                })
                            }
                            {
                                // selected.item?.current &&
                                // (
                                //     <InfoWindow
                                //         position={{ lat: selected.lat, lng: selected.lng }}
                                //         // clickable={true}
                                //         onCloseClick={() => setSelected({})}
                                //     >
                                //         <div className="infowindowcont">
                                //             <h5>{selected.item.vehicleNumber}</h5>
                                //             <p><b>DN - </b> {selected.item.driverMobileNumber}</p>
                                //             <p><b>FL - </b> {selected.item.from.city} {selected.item.from.state} {selected.item.from.pinCode}</p>
                                //             {/* <p className="cl"><b>CL - {selected.item.current.address}</b></p> */}
                                //             <p><b>TL - </b> {selected.item.to.city} {selected.item.to.state} {selected.item.to.pinCode} </p>
                                //         </div>
                                //     </InfoWindow>
                                // )
                            } 
                            {
                                directions.map((location, key) => {
                                    // alert(location.id)
                                    return (

                                        (<DirectionsRenderer key={key}
                                            directions={location}
                                            options={{
                                                markerOptions: { title: 'blackMarker', icon: '' },
                                                polylineOptions: {
                                                    strokeOpacity: 0.7,
                                                    strokeColor: '#007fff',
                                                },
                                                suppressMarkers: true
                                            }}
                                        />
                                        )
                                    )
                                })
                            }
                        </GoogleMap>
                    </LoadScriptOnlyIfNeeded>
                </Row>
            </Container>
        </div>
    );
}

export default MapDetails;