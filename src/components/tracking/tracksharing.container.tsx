import React from "react";
import { connect } from "react-redux";
import { defaultTrackingListValue, IAppState } from "../../models";
import { IGenericReducerState } from "../../utilities";
import { RouteComponentProps } from "react-router-dom";
import { Subject } from "rxjs";
import { IAllHitListModel, defaultAllHitListValue} from "../../models/allactivetrips.model";
import { IAllActiveTripModel } from "../../models/allactivetrips.model";
import { getSharingActiveLocateTripsById, getSharingAllHitTripsById} from "../../redux/actions/allactivetrips.action";
import TrackSharing from "./Search/tracksharing/tracksharing";

interface Trackdetail { selItem: string[]; locationsval: any[]; handleChangeVal: (e: any) => void; }

class TrackSharingContainer extends React.Component<Trackdetail &  IConnectState & IConnectDispatch & RouteComponentProps<{}>> {

  searchModel = defaultTrackingListValue;
  searchString$: Subject<string> = new Subject<string>();
  listType: number = 1;

  public constructor(props:any) {
    super(props); 
    this.props.allHitState.Payload = {...defaultAllHitListValue};
    this.fetchFilterList();
  } 

  async fetchFilterList() {
      console.log('this.props.match.params["id"]', this.props.match.params["id"]);
      
    var tripId = {
        "tripIds": [
            this.props.match.params["id"]
          ]
    }
    await this.props.dispatch(getSharingActiveLocateTripsById(tripId));
    await this.props.dispatch(getSharingAllHitTripsById(tripId));
  } 

  public render() {
    let filterloc = this.props.trackingState.Payload;
    let allhit = this.props.allHitState.Payload;
    return (<div><TrackSharing filterLocationval={filterloc} allhit={allhit}/></div>);
  }

}
interface IConnectState {
  trackingState: IGenericReducerState<IAllActiveTripModel[]>;
  allHitState: IGenericReducerState<IAllHitListModel[]>; 
}


const mapStateToProps = (state: IAppState): IConnectState => ({
  trackingState: state.AllActiveTrip,
  allHitState: state.AllHitTripById,
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(TrackSharingContainer);