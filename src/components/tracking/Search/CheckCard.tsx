import React from 'react';
import moment from "moment";
import Geocode from "react-geocode";
Geocode.setApiKey("AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU");
Geocode.enableDebug();

function CheckCard({ location }) {
  console.log(location);
  
  if (location.lastSuccessfullTrackingHit != null) {

    return (
      <tr key={location.lastSuccessfullTrackingHit.tripDto.id}>
        <td>{location.lastSuccessfullTrackingHit.tripDto.tripNumber}</td>
        <td>{moment(location.lastSuccessfullTrackingHit.tripDto.transitStartDate).format("DD-MM-YYYY")}</td>
        <td>{location.lastSuccessfullTrackingHit.tripDto.tripType}</td>
        <td>{moment(location.lastSuccessfullTrackingHit['timeStamp']).format("hh:mm")}</td>
        <td>{location.lastSuccessfullTrackingHit.tripDto.transporterName}</td>
        <td>{location.lastSuccessfullTrackingHit.tripDto.vehicleNumber}</td>
        <td>{location.lastSuccessfullTrackingHit.tripDto.driverMobileNumber}</td>
        <td>{location.lastSuccessfullTrackingHit.tripDto.containerNumber}</td>
        <td>{location.lastSuccessfullTrackingHit['address']}</td>
        <td>{location.lastSuccessfullTrackingHit['latitude']}</td>
        <td>{location.lastSuccessfullTrackingHit['longitude']}</td>
      </tr>
    );
  } else {
    return (<tr key={location.id}>
      <td>{location.tripNumber}</td>
      <td>{moment(location.transitStartDate).format("DD-MM-YYYY")}</td>
      <td>{location.tripType}</td>
      <td>{moment(location.transitStartDate).format("hh:mm")}</td>
      <td>{location.transporterName}</td>
      <td>{location.vehicleNumber}</td>
      <td>{location.driverMobileNumber}</td>
      <td>{location.containerNumber}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>)
  }
}

export default CheckCard;