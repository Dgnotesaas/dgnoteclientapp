import React, { useState } from 'react';
import { Button, Col, Container, Form, Row, Table } from "react-bootstrap";
import SingelCard from './SingelCard';

function SingleSearchList({ filteredLocations }) {
    let filtered;
    
    if (Object.values(filteredLocations)[0] != '') {
        filtered = Object.values(filteredLocations).map((location: any) => 
        <SingelCard key={location.bookingId} location={location} />);
    }


    return (
        <div>
            <Container>
                <Row>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Transporter</th>
                                <th>Vehicle No.</th>
                                <th>Driver No.</th>
                                <th>From Loc.</th>
                                <th>To Loc.</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filtered}
                        </tbody>
                    </Table>
                </Row>
            </Container>
        </div>
    );
}

export default SingleSearchList;