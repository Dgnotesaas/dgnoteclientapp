import React, { Component, useEffect, useState } from "react";
import { Container, Row, Table } from "react-bootstrap";
import { GoogleMap, LoadScript, Marker, InfoWindow, DirectionsRenderer, Polyline } from '@react-google-maps/api';
import moment from "moment";

import mapimg from '../image/currloc.png'
import fromimg from '../image/fromsmall.png'
import toimg from '../image/tosmall.png'
import direction from '../image/direction.png'
// import toimg from '../image/to-locoation-icon.png'
import LoadScriptOnlyIfNeeded from "../LoadScriptOnlyIfNeeded";

const mapStyles = {
    height: "80vh",
    width: "100%"
};

const defaultCenter = {
    lat: 20.593683, lng: 78.962883
}

var getDirections: any[] = [];
var getConstval: any[] = []

function SingleMap({ path, locationID, hitPath }) {
    const [directions, setDirection] = useState<any>([]);
    const [selected, setSelected] = useState<any>({
        item: [],
        lat: '',
        lng: ''
    }); 
    
    const [tripselected, setTripSelected] = useState<any>({
        item: [],
        lat: '',
        lng: ''
    });

    const [typecall, setCall] = useState<any>(true);

    const onSelect = (item, lat, lng) => {
        setSelected({ item, lat, lng });
    };

    const onTripSelect = (item, lat, lng) => {
        setTripSelected({ item, lat, lng });
    };

    useEffect(() => {
        if (typecall) {
            getRoute();
        }
    });
    // debugger 
    function getRoute() {
        if (typeof window.google !== 'undefined' && window.google != null) {
            const directionsService = new window.google.maps.DirectionsService();
            path.map(getLocation => {
                if (getLocation != '' && typeof getLocation !== 'undefined' && getLocation != null) {
                    const origin = { lat: getLocation.from.lat, lng: getLocation.from.lng };
                    const destination = { lat: getLocation.to.lat, lng: getLocation.to.lng };
                    directionsService.route(
                        {
                            origin: origin,
                            destination: destination,
                            travelMode: window.google.maps.TravelMode.DRIVING,
                            waypoints: [
                                {
                                    location: new google.maps.LatLng(getLocation.current.lat, getLocation.current.lng)
                                }]
                        },
                        (result, status) => {
                            if (status === window.google.maps.DirectionsStatus.OK) {
                                if (result != null) {

                                    if (!('id' in result)) {
                                        result['id'] = getLocation.id
                                        getDirections.push(result)
                                        if (locationID != '' && typeof locationID !== 'undefined' && locationID != null) {
                                            setDirection(directions.filter(item => item.id !== locationID[0]));
                                        } else {
                                            setDirection(olddata => [...olddata, result])
                                        }
                                        setCall(false)
                                    }
                                }
                            } else {
                                console.error(`error fetching directions ${result}`);
                            }
                        }
                    );
                }
            })
        }
    }   

    console.log('hitPath', tripselected.item);
    

    let travLoc : any[] = []; 

    return (
        <div>
            <Container>
                <Row className="mt-4">
                    <LoadScriptOnlyIfNeeded
                        googleMapsApiKey='AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU'>
                        <GoogleMap
                            mapContainerStyle={mapStyles}
                            zoom={5}
                            center={defaultCenter}>
                            {/* {setCall(true)} */}
                            {
                                Object.values(path).map((item: any) => {
                                    if (item != '' && typeof item !== 'undefined' && item != null) {
                                        return (
                                            <div>
                                                <Marker
                                                    // key={item.vehicleinfo}
                                                    position={{ lat: item.current.lat, lng: item.current.lng }}
                                                    // onMouseOver={() => onSelect(item, item.current.lat, item.current.lng)}
                                                    onClick={() => onSelect(item, item.current.lat, item.current.lng)}
                                                    icon={{
                                                        url: mapimg,

                                                    }}
                                                />
                                                <Marker
                                                    // key={item.vehicleinfo}
                                                    position={{ lat: item.from.lat, lng: item.from.lng }}
                                                    // onMouseOver={() => onSelect(item, item.from.lat, item.from.lng)}
                                                    onClick={() => onSelect(item, item.from.lat, item.from.lng)}
                                                    icon={{
                                                        url: fromimg,
                                                    }}
                                                />
                                                <Marker
                                                    // key={item.vehicleinfo}
                                                    position={{ lat: item.to.lat, lng: item.to.lng }}
                                                    // onMouseOver={() => onSelect(item, item.to.lat, item.to.lng)}
                                                    onClick={() => onSelect(item, item.to.lat, item.to.lng)}
                                                    icon={{
                                                        url: toimg,
                                                    }}
                                                />
                                            </div>
                                        )
                                    }
                                })
                            }
                            {
                                hitPath && Object.values(hitPath).map((hititem: any) => {
                                    if (hititem != '' && typeof hititem !== 'undefined' && hititem != null) {
                                        hititem.sort((n1,n2) => n1.timeStamp < n2.timeStamp);
                                        travLoc = hititem.map((getitem: any) => {  
                                            return ({lat: getitem.latitude, lng:getitem.longitude}
                                            )
                                        })

                                        let linecolor = (hititem[0].tripDto.tripType == 'SINGLE_TRIP')? "#ff2527" : "#111127"

                                        return (
                                            <div>
                                                {/* <Polyline
                                                    path={travLoc}
                                                    options={{
                                                        strokeColor: linecolor,
                                                        strokeOpacity: 0.75,
                                                        strokeWeight: 3
                                                    }}
                                                /> */}
                                                {hititem.map((getitem: any) => { 
                                                    return (
                                                        <div>
                                                            <Marker
                                                                // key={hititem.vehicleinfo}
                                                                position={{ lat: getitem.latitude, lng: getitem.longitude }} 
                                                                icon={{
                                                                    url: direction, 
                                                                }}
                                                                // icon={{
                                                                //     path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                                                                //     scale: 7,
                                                                //     fillColor: 'red',
                                                                // fillOpacity: 0.8,
                                                                // strokeWeight: 2,
                                                                // rotation: marker.rotation,
                                                                //   }}
                                                                // onMouseOver={() => onTripSelect(getitem, getitem.latitude, getitem.longitude)}
                                                                onClick={() => onTripSelect(getitem, getitem.latitude, getitem.longitude)}
                                                            /> 
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        )
                                    }
                                })
                            }
                            {
                                selected.item?.current &&
                                (
                                    <InfoWindow
                                        position={{ lat: selected.lat, lng: selected.lng }}
                                        // clickable={true}
                                        onCloseClick={() => setSelected({})}
                                    >
                                        <div className="infowindowcont">
                                            <Table hover bordered> 
                                                <tbody>
                                                    <tr>
                                                        <td><b style={{'fontWeight': 'bold'}}>Vehicle No. </b></td>
                                                        <td><b>{selected.item.vehicleNumber}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b style={{'fontWeight': 'bold'}}>Driver No. </b></td>
                                                        <td><b>{selected.item.driverMobileNumber}</b></td>
                                                    </tr>
                                                    {/* <tr>
                                                        <td><b>Date </b></td>
                                                        <td><b>{moment(tripselected.fromLocation.createdDate).format("DD-MM-YYYY")} {moment(tripselected.fromLocation.timeStamp).format("hh:mm")}</b></td>
                                                    </tr> */}
                                                    <tr>
                                                        <td><b style={{'fontWeight': 'bold'}}>From Loc.</b></td>
                                                        <td><b>{selected.item.from.city} {selected.item.from.state} {selected.item.from.pinCode}</b></td>
                                                    </tr>
                                                    <tr style={{"color": "rgb(255, 11, 11)"}}>
                                                        <td><b style={{'fontWeight': 'bold'}}>Current Loc.</b></td>
                                                        <td><b>{selected.item.current.address}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b style={{'fontWeight': 'bold'}}>To Loc.</b></td>
                                                        <td><b>{selected.item.to.city} {selected.item.to.state} {selected.item.to.pinCode}</b></td>
                                                    </tr> 
                                                </tbody>
                                            </Table>
                                        </div>
                                    </InfoWindow>
                                )
                            }
                            {
                                tripselected.item?.latitude &&
                                (
                                    <InfoWindow
                                        position={{ lat: tripselected.lat, lng: tripselected.lng }}
                                        // clickable={true}
                                        onCloseClick={() => setTripSelected({})}
                                    >
                                        <div className="infowindowcont">
                                            <Table hover bordered> 
                                                <tbody>
                                                    <tr>
                                                        <td><b style={{'fontWeight': 'bold'}}>Vehicle No. </b></td>
                                                        <td><b>{tripselected.item.tripDto.vehicleNumber}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b style={{'fontWeight': 'bold'}}>Date </b></td>
                                                        <td><b>{moment(tripselected.item.createdDate).format("DD-MM-YYYY")} {moment(tripselected.item.timeStamp).format("hh:mm")}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b style={{'fontWeight': 'bold'}}>Driver No. </b></td>
                                                        <td><b>{tripselected.item.tripDto.driverMobileNumber}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b style={{'fontWeight': 'bold'}}>From Loc.</b></td>
                                                        <td><b>{tripselected.item.tripDto.fromLocation.city} {tripselected.item.tripDto.fromLocation.state} {tripselected.item.tripDto.fromLocation.pinCode}</b></td>
                                                    </tr>
                                                    <tr style={{"color": "rgb(255, 11, 11)"}}>
                                                        <td><b style={{'fontWeight': 'bold'}}>Current Loc.</b></td>
                                                        <td><b>{tripselected.item.address}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b style={{'fontWeight': 'bold'}}>To Loc.</b></td>
                                                        <td><b>{tripselected.item.tripDto.toLocation.city} {tripselected.item.tripDto.toLocation.state} {tripselected.item.tripDto.toLocation.pinCode}</b></td>
                                                    </tr>
                                                    {/* <tr>
                                                        <td><b>ETA </b></td>
                                                        <td><b>{calcTime(selected.fromLocation.latitude, selected.fromLocation.longitude, selected.toLocation.latitude, selected.toLocation.longitude).toFixed(1)} Hours</b></td>
                                                    </tr> */}
                                                </tbody>
                                            </Table>   
                                        </div>
                                    </InfoWindow>
                                )
                            }
                            {
                                directions.map((location, key) => {
                                    return (

                                        (<DirectionsRenderer key={key}
                                            directions={location}
                                            options={{
                                                markerOptions: { title: 'blackMarker', icon: '' },
                                                polylineOptions: {
                                                    strokeOpacity: 0.7,
                                                    strokeColor: '#007fff',
                                                },
                                                suppressMarkers: true
                                            }}
                                        />
                                        )
                                    )
                                })
                            }
                        </GoogleMap>
                    </LoadScriptOnlyIfNeeded>
                </Row>
            </Container>
        </div>
    );

    function toRad(Value) {
        return Value * Math.PI / 180;
    }

    function calcTime(lat1: any, lon1, lat2: any, lon2: any) {
        var R = 6371; // km
        var dLat = toRad(lat2 - lat1);
        var dLon = toRad(lon2 - lon1);
        var lat1: any = toRad(lat1);
        var lat2: any = toRad(lat2);

        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d/20;
    }
}

export default SingleMap;