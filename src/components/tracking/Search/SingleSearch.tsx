import React, { useState } from 'react';
import { Container, Dropdown, Row, Form, Modal, Button, InputGroup, FormControl } from 'react-bootstrap';
import moment from "moment";
import { CSVLink } from "react-csv";
import Scroll from './Scroll';
import SingleMap from './SingleMap';
import SingleSearchList from './SingleSearchList';
import { toast } from "react-toastify";
import CheckList from './CheckList';
import Geocode from "react-geocode";
Geocode.setApiKey("AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU");
Geocode.enableDebug();

function Search({ filterLocationval, allhit, shareMap, handleChange, showmodel}) {

    const [show, setShow] = useState({ modals: false, type: '' });
    const [showicon, setShowicon] = useState({ modals: false});
    const [inputList, setInputList] = useState([{ email: ""}]);

    const handleClose = () => setShow({ modals: false, type: '' });
    const handleShow = (permission) => {
        setShow({ modals: true, type: permission })
        setShowicon({ modals: false})
    };

    const handleCloseicon = () => setShowicon({ modals: false});
    const handleShowicon = () => setShowicon({ modals: true});

    const handleRemoveClick = (e,index) => {
        e.preventDefault();
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };

    const onClickButton = (e, path) => {

        e.preventDefault();
        const validEmailRegex =
        RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
  
        let validmail = true;

        showmodel.value.map((item : any, key) => {
            if(!validEmailRegex.test(item) || item == '') { 
                validmail = false; 
            } else {
                validmail = true; 
            }
        })

        if(validmail) {
            setShow({ modals: false, type: '' })
            setInputList([{email: ""}]);
            shareMap(e, path) 
        } else {
            toast.error('Please Enter Valid Email Id', {
                toastId: 'Please Enter Valid Email Id'
            })
        } 
    }

    const handleAddClick = () => {
        setInputList([...inputList, { email: ""}]);
    };

    if (filterLocationval) {

        let path: any[] = [];
        let locationID: any[] = [];
        let expportData;
        let hitPath: any;
        let single = true;

        if (filterLocationval != '' && typeof filterLocationval !== 'undefined') {
            path = Object.values(filterLocationval).map((pathval: any) => {
                if (pathval.lastSuccessfullTrackingHit != null) {

                    return (
                        {
                            from: { lat: parseFloat(pathval.fromLocation.latitude), lng: parseFloat(pathval.fromLocation.longitude), typeloc: 'from', city: pathval.fromLocation.city, state: pathval.fromLocation.state, pinCode: pathval.fromLocation.pinCode },
                            current: { lat: parseFloat(pathval.lastSuccessfullTrackingHit.latitude), lng: parseFloat(pathval.lastSuccessfullTrackingHit.longitude), typeloc: 'current', address: pathval.lastSuccessfullTrackingHit.address },
                            to: { lat: parseFloat(pathval.toLocation.latitude), lng: parseFloat(pathval.toLocation.longitude), typeloc: 'to', city: pathval.toLocation.city, state: pathval.toLocation.state, pinCode: pathval.toLocation.pinCode },
                            id: pathval.bookingId, vehicleNumber: pathval.vehicleNumber, customer: pathval.customerName, transitEndDate: pathval.transitEstimatedEndDate, driverMobileNumber: pathval.driverMobileNumber
                        }
                    )
                } else {
                    return (
                        {
                            from: { lat: parseFloat(pathval.fromLocation.latitude), lng: parseFloat(pathval.fromLocation.longitude), typeloc: 'from', city: pathval.fromLocation.city, state: pathval.fromLocation.state, pinCode: pathval.fromLocation.pinCode },
                            current: { lat: parseFloat(pathval.fromLocation.latitude), lng: parseFloat(pathval.fromLocation.longitude), typeloc: 'current', address: '' },
                            to: { lat: parseFloat(pathval.toLocation.latitude), lng: parseFloat(pathval.toLocation.longitude), typeloc: 'to', city: pathval.toLocation.city, state: pathval.toLocation.state, pinCode: pathval.toLocation.pinCode },
                            id: pathval.bookingId, vehicleNumber: pathval.vehicleNumber, customer: pathval.customerName, transitEndDate: pathval.transitEstimatedEndDate, driverMobileNumber: pathval.driverMobileNumber
                        }
                    )
                }
            })

            hitPath = Object.values(allhit).map((hitval: any) => {
                return (hitval.allHits)
            })

        } else {
            filterLocationval = [];
        }

        const getFiltervalue = Object.values(filterLocationval);
        expportData = getFiltervalue.map((pathval: any) => {
            if (pathval.lastSuccessfullTrackingHit != null) {
                return (
                    {
                        'Trip No.': pathval.tripNumber,
                        'Date': moment(pathval.lastSuccessfullTrackingHit.tripDto.transitActualEndDate).format("DD-MM-YYYY"),
                        'Trip Type': pathval.tripType,
                        'Time': moment(pathval.lastSuccessfullTrackingHit['timeStamp']).format("hh:mm"),
                        'Transporter': pathval.transporterName,
                        'Vehicle No.': pathval.vehicleNumber,
                        'Driver No.': pathval.driverMobileNumber,
                        'Container No.': pathval.containerNumber,
                        'Current Loc.': pathval.lastSuccessfullTrackingHit['address'],
                        'Latitude': pathval.lastSuccessfullTrackingHit['latitude'],
                        'Longitude': pathval.lastSuccessfullTrackingHit['longitude'],
                    }
                )
            } else {
                return (
                    {
                        'Trip No.': pathval.tripNumber,
                        'Date': moment(pathval.transitStartDate).format("DD-MM-YYYY"),
                        'Trip Type': pathval.tripType,
                        'Time': moment(pathval.transitStartDate).format("hh:mm"),
                        'Transporter': pathval.transporterName,
                        'Vehicle No.': pathval.vehicleNumber,
                        'Driver No.': pathval.driverMobileNumber,
                        'Container No.': pathval.containerNumber,
                        'Current Loc.': '',
                        'Latitude': '',
                        'Longitude': '',
                    }
                )
            }
        }); 
        return (
            <div>
                <Container className="mt-4">
                    <Row>
                        <div className="col-md-12 mb-4">
                            <h3 className="font-weight-bold mb-0" >Track Vehicle</h3>
                        </div>
                        <div className="col-md-12">
                            <div style={{ 'overflowY': 'scroll' }}>
                                <SingleSearchList filteredLocations={filterLocationval} />
                            </div>
                            <div className="col-md-12 d-inline-block">
                                <div className="text-right mt-3"> 
                                    {/* <Dropdown className="d-inline-block">
                                        <Dropdown.Toggle className="btn btn-sm button-theme btn-rounded text-white mr-1" style={{ width: "90px" }} id="dropdown-autoclose-true">
                                            Share
                                        </Dropdown.Toggle>

                                        <Dropdown.Menu className="text-center">
                                            <Dropdown.Item className="py-2" value="item" onClick={() => handleShow('Email')}><div className="btn btn-sm button-theme btn-rounded text-white"><i className="ti-email"></i> Email</div></Dropdown.Item> 
                                        </Dropdown.Menu>
                                    </Dropdown> */}
                                    <Button className="btn btn-sm button-theme btn-rounded text-uppercase text-white mr-1" onClick={() => handleShowicon()}>Share</Button>
                                    <CSVLink data={expportData} filename={"track_vehicles.csv"} className="btn btn-sm button-theme btn-rounded text-white d-inline-block" >Export</CSVLink>
                                </div>
                            </div>
                            <CheckList
                                checkFilteredLocations={filterLocationval} single={single} />
                            <SingleMap
                                path={path}
                                locationID={locationID}
                                hitPath={hitPath}
                            />
                        </div>
                    </Row>
                </Container>
                <Modal show={show.modals} onHide={handleClose} size="sm" className="sharingform">
                    <Modal.Header closeButton>
                        <Modal.Title><i className="ti-sharethis menu-icon ml-1"> Share Location </i></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <div className='col-md-1'></div>
                            <div className='col-md-11'>
                                <Form onSubmit={(e) => onClickButton(e, path)}>
                                    <Row>
                                        <div className="col-md-12">
                                            <Form.Group className="mb-3 mt-3"  controlId="formBasicEmail">
                                                    {inputList.map((x, i) => {
                                                        return (
                                                            <Row>
                                                                <Form.Control className='col-sm-8 mb-1' type="email" name="email" onBlur={(e) => handleChange(e)} placeholder="Enter Email Id" defaultValue="" required/>  
                                                                <div className='col-sm-4 mb-3'>
                                                                    {inputList.length !== 1 && <button className="btn btn-sm button-theme btn-rounded text-white mt-1" onClick={(e) => handleRemoveClick(e,i)}><i className="ti-minus menu-icon"></i></button>}
                                                                    {inputList.length - 1 === i && <button className="btn btn-sm button-theme btn-rounded text-white mt-1 ml-1" onClick={handleAddClick}><i className="ti-plus menu-icon"></i></button>}
                                                                </div> 
                                                            </Row>
                                                        );
                                                    })}
                                            </Form.Group>
                                        </div>
                                        <div className="pt-2">
                                            <Button className="btn btn-sm button-theme btn-rounded text-white sub-button" type="submit" >Send</Button>
                                        </div>
                                    </Row>
                                </Form>
                            </div>
                        </Row>
                    </Modal.Body> 
                </Modal>
                <Modal show={showicon.modals} onHide={handleCloseicon} size="sm" className="sharingform">
                    {/* <Modal.Header closeButton>
                        <Modal.Title><i className="ti-sharethis menu-icon ml-1"> Share Loaction </i></Modal.Title>
                    </Modal.Header> */}
                    <Modal.Body>
                        <Row>
                            <div className='col-md-1'></div>
                            <div className='col-md-11'>
                                <Form>
                                    <Row>
                                        <div className="col-md-12 text-center">
                                            <Button className="btn btn-sm button-theme btn-rounded text-uppercase text-white mr-1" style={{height: '60px', 'width':'60px', 'borderRadius': '5px'}} onClick={() => handleShow('Email')}><i className="ti-email menu-icon" style={{'fontSize': '30px'}}></i></Button>
                                        </div>
                                    </Row>
                                </Form>
                            </div>
                        </Row>
                    </Modal.Body> 
                </Modal>
            </div>

        );
    } else {
        return (
            <Container className="mt-5">
                <Row>
                    <div className="mr-5">
                        No Data Found
                    </div>
                </Row>
            </Container>
        );
    }
}

export default Search