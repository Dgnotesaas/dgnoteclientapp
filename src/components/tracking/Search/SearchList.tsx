import React, {useState} from 'react';
import { Button, Col, Container, Form, Row, Table } from "react-bootstrap";
import Card from './Card';

function SearchList({ handleChangeVal, filteredLocations }) {
    const filtered = filteredLocations.map(location => <Card handleChangeVal={handleChangeVal} key={location.id} location={location} />); 

    return (
        <div>
            <Container>
                <Row>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Transporter</th>
                                <th>Vehicle No.</th>
                                <th>Driver No.</th>
                                <th>From Loc.</th>
                                <th>To Loc.</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filtered}
                        </tbody>
                    </Table>
                </Row>
            </Container>
        </div>
    );
}

export default SearchList;