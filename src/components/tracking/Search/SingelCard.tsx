import React, {useState} from 'react';

function SingleCard({location}) { 

// console.log(data);
  if(location.driverMobileNumber != null) {
  return( 
    <tr key={location.bookingId}> 
        <td>{location.transporterName}</td>
        <td>{location.vehicleNumber}</td>
        <td>{location.driverMobileNumber}</td>
        <td>{location.fromLocation.city} {location.fromLocation.state} {location.fromLocation.state}</td>
        <td>{location.toLocation.city} {location.toLocation.state} {location.toLocation.state}</td>
    </tr>
  )
  } else {
    return (<></>)
  }
}

export default SingleCard;