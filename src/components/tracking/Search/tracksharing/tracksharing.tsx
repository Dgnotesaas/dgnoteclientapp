import { Container, Row } from 'react-bootstrap';
import SingleMap from '../SingleMap';
import SingleSearchList from '../SingleSearchList';
import CheckList from '../CheckList';
import Geocode from "react-geocode";
Geocode.setApiKey("AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU");
Geocode.enableDebug();

function TrackSharing({ filterLocationval, allhit}) { 
    
    if (filterLocationval) {

        let path: any[] = [];
        let locationID: any[] = [];
        let hitPath: any;
        let single = true; 

        if (filterLocationval != '' && typeof filterLocationval !== 'undefined') {
            path = Object.values(filterLocationval).map((pathval: any) => {
                if (pathval.lastSuccessfullTrackingHit != null) { 

                    return (
                        {
                            from: { lat: parseFloat(pathval.fromLocation.latitude), lng: parseFloat(pathval.fromLocation.longitude), typeloc: 'from', city: pathval.fromLocation.city, state: pathval.fromLocation.state, pinCode: pathval.fromLocation.pinCode },
                            current: { lat: parseFloat(pathval.lastSuccessfullTrackingHit.latitude), lng: parseFloat(pathval.lastSuccessfullTrackingHit.longitude), typeloc: 'current', address: pathval.lastSuccessfullTrackingHit.address },
                            to: { lat: parseFloat(pathval.toLocation.latitude), lng: parseFloat(pathval.toLocation.longitude), typeloc: 'to', city: pathval.toLocation.city, state: pathval.toLocation.state, pinCode: pathval.toLocation.pinCode },
                            id: pathval.bookingId, vehicleNumber: pathval.vehicleNumber, customer: pathval.customerName, transitEndDate: pathval.transitEstimatedEndDate, driverMobileNumber: pathval.driverMobileNumber
                        }
                    )
                } else {
                    return (
                        {
                            from: { lat: parseFloat(pathval.fromLocation.latitude), lng: parseFloat(pathval.fromLocation.longitude), typeloc: 'from', city: pathval.fromLocation.city, state: pathval.fromLocation.state, pinCode: pathval.fromLocation.pinCode },
                            current: { lat: parseFloat(pathval.fromLocation.latitude), lng: parseFloat(pathval.fromLocation.longitude), typeloc: 'current', address: '' },
                            to: { lat: parseFloat(pathval.toLocation.latitude), lng: parseFloat(pathval.toLocation.longitude), typeloc: 'to', city: pathval.toLocation.city, state: pathval.toLocation.state, pinCode: pathval.toLocation.pinCode },
                            id: pathval.bookingId, vehicleNumber: pathval.vehicleNumber, customer: pathval.customerName, transitEndDate: pathval.transitEstimatedEndDate,driverMobileNumber: pathval.driverMobileNumber
                        }
                    )
                }
            })

            hitPath = Object.values(allhit).map((hitval : any) => {
                return (hitval.allHits) 
             })

        } else {
            filterLocationval = [];
        } 

        return (
            <div>
                <Container className="mt-4">
                    <Row>
                        <div className="col-md-12 mb-4">
                            <h3 className="font-weight-bold mb-0" >Track Vehicle</h3>
                        </div>
                        <div className="col-md-12">
                            <div style={{'overflowY': 'scroll'}}>
                                <SingleSearchList filteredLocations={filterLocationval} />
                            </div>
                            <div> 
                                <CheckList
                                    checkFilteredLocations={filterLocationval} single={single} />
                                <SingleMap
                                    path={path}
                                    locationID={locationID}
                                    hitPath={hitPath}
                                />
                            </div>
                        </div>
                    </Row>
                </Container> 
            </div>

        );
    } else {
        return (
            <Container className="mt-5">
                <Row>
                    <div className="mr-5">
                        No Data Found
                    </div>
                </Row>
            </Container>
        );
    }
}

export default TrackSharing