// import * as React from "react";
// import { connect } from "react-redux";
// import { defaultListSearchRequestModel, IAppState } from "../../../models";

// import { IGenericReducerState } from "../../../utilities";
// import { RouteComponentProps } from "react-router-dom";

// import { IBookingListModel } from "../../../models/booking-list.model";
// import { getBookings } from "../../../redux/actions/booking-list.actions";
// import { IListSearchRequstModel } from "../../../models/list-search-request.model";
// import { debounceTime, distinctUntilChanged, Subject } from "rxjs";
// import { IBookingModel } from "../../../models/booking.model";
// import Search from "./Search";
// import Scroll from "./Scroll";
// import MapDirection from "./MapDirection";
// import CheckList from "./CheckList";

// class GetCheckList extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {

//     searchModel = defaultListSearchRequestModel;
//     searchString$: Subject<string> = new Subject<string>();
//     listType: number = 1;
//     public constructor(props) {
//         super(props);
//         this.inItSearch();
//         this.fetchList(this.searchModel);
//     }

//     inItSearch() {
//         this.searchString$.pipe(
//             debounceTime(1000),
//             distinctUntilChanged(),
//         ).subscribe(text => {
//             this.searchModel.searchToken = text;
//             this.fetchList(this.searchModel);
//         })
//     }

//     fetchList(model: IListSearchRequstModel) {
//         this.props.dispatch(getBookings(model));
//     }

//     public render() {

//         let { content } = this.props.bookingListState.Payload;
//         return (<CheckListCont checkFilterLocation={content} />);
//     }

// }

// interface IConnectState {
//     bookingListState: IGenericReducerState<IBookingListModel>;
//     bookingState: IGenericReducerState<IBookingModel>;
// }


// const mapStateToProps = (state: IAppState): IConnectState => ({
//     bookingListState: state.BookingList,
//     bookingState: state.Booking
// });

// interface IConnectDispatch {
//     dispatch: any;
// }

// function CheckListCont({ checkFilterLocation }) {
//     if (checkFilterLocation != '') {
//         const checkFilteredLocations = checkFilterLocation.filter(
//             location => {
//                 return (
//                     location.vehicleinfo.id.includes(checkFilterLocation.toLowerCase())
//                 );
//             }
//         );

//         const path = checkFilteredLocations.map(pathval => {
//             return (pathval.location)
//         })

//         return (
//             <div>
//                 {/* <CheckList
//                     // className="mb-4" 
//                     checkFilteredLocations={checkFilteredLocations} />
//                 <MapDirection path={path} checkFilteredLocations={checkFilteredLocations} /> */}
//             </div>
//         );
//     } else {
//         return (
//             <Scroll>
//             </Scroll>
//         );
//     }
// }

// export default connect(mapStateToProps)(GetCheckList);