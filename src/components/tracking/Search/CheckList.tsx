import { Container, Row, Table} from "react-bootstrap";
import CheckCard from './CheckCard';
import '../css/tracking.css';

function CheckList({ checkFilteredLocations, single }) {
    let filtered;
    const getFiltervalue = Object.values(checkFilteredLocations);
    if (getFiltervalue.length > 0 && getFiltervalue[0] != '') {
        filtered = getFiltervalue.map((location: any) => <CheckCard key={location.bookingId} location={location} />); 
    } else {
        filtered = [];
    }

    return (
        <div className="mt-4">
            <Container>
                <Row>
                    <div className="col-md-8"></div>
                    <div style={{ 'overflowY': 'scroll', padding: 0 }}>
                        <Table striped bordered hover className="px-3">
                            <thead>
                                <tr>
                                    <th>Trip NO.</th>
                                    <th>Date</th>
                                    <th>Trip Type</th>
                                    <th>Time</th>
                                    <th>Transporter</th>
                                    <th>Vehicle No.</th>
                                    <th>Driver No.</th>
                                    <th>Container No.</th>
                                    <th>Current Loc.</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                </tr>
                            </thead>
                            <tbody>
                                {filtered}
                            </tbody>
                        </Table>
                    </div>
                </Row>
            </Container> 
        </div>
    );
}

export default CheckList;