import React, {useState} from 'react';

function Card({handleChangeVal, location}) { 

// console.log(data);
  if(location.driverMobileNumber != null) {
  return( 
    <tr key={location.id}>
        <td>
        <input
                  type="checkbox"
                  value={location.id}
                  onChange={handleChangeVal}
                  // checked={selItem === location.id}
                /> 
        </td>
        <td>{location.transporterName}</td>
        <td>{location.vehicleNumber}</td>
        <td>{location.driverMobileNumber}</td>
        <td>{location.fromLocation.city} {location.fromLocation.state} {location.fromLocation.pinCode}</td>
        <td>{location.toLocation.city} {location.toLocation.state} {location.toLocation.pinCode}</td>
    </tr>
  )
  } else {
    return (<></>)
  }
}

export default Card;