import React, { Component, useEffect, useState } from "react";
import { Container, Row, Table } from "react-bootstrap";
import { GoogleMap, LoadScript, Marker, InfoWindow, DirectionsRenderer, Polyline} from '@react-google-maps/api';
import moment from "moment";

import mapimg from '../image/mapiconssmall.png'
import fromimg from '../image/fromsmall.png'
import toimg from '../image/tosmall.png'
import direction from '../image/direction.png'
import LoadScriptOnlyIfNeeded from "../LoadScriptOnlyIfNeeded";

const mapStyles = {
    height: "80vh",
    width: "100%"
};

const defaultCenter = {
    lat: 20.593683, lng: 78.962883
}

var getDirections: any[] = [];
var getConstval: any[] = [1]

function Map({ directions, path, hitPath}) {
    // const [directions, setDirection] = useState<any>([]);
    const [selected, setSelected] = useState<any>({
        item: [],
        lat: '',
        lng: ''
    });

    const [tripselected, setTripSelected] = useState<any>({
        item: [],
        lat: '',
        lng: ''
    });
    
    console.log('hitPath', hitPath);
    
    const [typecall, setCall] = useState<any>(true);

    const onSelect = (item, lat, lng) => {
        setSelected({ item, lat, lng });
    };

    const onTripSelect = (item, lat, lng) => {
        setTripSelected({ item, lat, lng });
    };

    if(path.length == 0) {
        directions = [];
    }
    
    let travLoc : any[] = []; 

    return (
        <div>
            <Container>
                <Row className="mt-4">
                    <LoadScriptOnlyIfNeeded
                        googleMapsApiKey='AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU'>
                        <GoogleMap
                            mapContainerStyle={mapStyles}
                            zoom={5}
                            center={defaultCenter}>
                            {/* {setCall(true)} */}
                            {
                                Object.values(path).map((item: any) => {
                                    if (item != '' && typeof item !== 'undefined' && item != null) {
                                        return (
                                            <div>
                                                <Marker
                                                    key={item.current.lat}
                                                    position={{ lat: item.current.lat, lng: item.current.lng }}
                                                    // onMouseOver={() => onSelect(item, item.current.lat, item.current.lng)}
                                                    onClick={() => onSelect(item, item.current.lat, item.current.lng)}
                                                    icon={{
                                                        url: mapimg,

                                                    }}
                                                />
                                                <Marker
                                                    key={item.from.lat}
                                                    position={{ lat: item.from.lat, lng: item.from.lng }}
                                                    // onMouseOver={() => onSelect(item, item.from.lat, item.from.lng)}
                                                    onClick={() => onSelect(item, item.from.lat, item.from.lng)}
                                                    icon={{
                                                        url: toimg,
                                                    }}
                                                />
                                                <Marker
                                                    key={item.to.lat}
                                                    position={{ lat: item.to.lat, lng: item.to.lng }}
                                                    // onMouseOver={() => onSelect(item, item.to.lat, item.to.lng)}
                                                    onClick={() => onSelect(item, item.to.lat, item.to.lng)}
                                                    icon={{
                                                        url: fromimg,
                                                    }}
                                                />
                                            </div>
                                        )
                                    }
                                })
                            }
                            {
                                hitPath && Object.values(hitPath).map((hititem: any) => {
                                    if (hititem != '' && typeof hititem !== 'undefined' && hititem != null) {
                                        hititem.sort((n1,n2) => n1.timeStamp < n2.timeStamp);
                                        travLoc = hititem.map((getitem: any) => {  
                                            return ({lat: getitem.latitude, lng:getitem.longitude}
                                            )
                                        })

                                        let linecolor = (hititem[0].tripDto.tripType == 'SINGLE_TRIP')? "#ff2527" : "#111127"

                                        return (
                                            <div>
                                                {/* <Polyline
                                                    path={travLoc}
                                                    options={{
                                                        strokeColor: linecolor,
                                                        strokeOpacity: 0.75,
                                                        strokeWeight: 3
                                                    }}
                                                /> */}
                                                {hititem.map((getitem: any) => { 
                                                    return (
                                                        <div>
                                                            <Marker
                                                                // key={hititem.vehicleinfo}
                                                                position={{ lat: getitem.latitude, lng: getitem.longitude }} 
                                                                icon={{
                                                                    url: direction, 
                                                                }}
                                                                // onMouseOver={() => onTripSelect(getitem, getitem.latitude, getitem.longitude)}
                                                                onClick={() => onTripSelect(getitem, getitem.latitude, getitem.longitude)}
                                                            /> 
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        )
                                    }
                                })
                            }
                            {
                                selected.item?.current &&
                                (
                                    <InfoWindow
                                        position={{ lat: selected.lat, lng: selected.lng }}
                                        // clickable={true}
                                        onCloseClick={() => setSelected({})}
                                    >
                                        <div className="infowindowcont">
                                            <h5>{selected.item.vehicleNumber}</h5>
                                            <p><b>DN - </b> {selected.item.driverMobileNumber}</p>
                                            <p><b>FL - </b> {selected.item.from.city} {selected.item.from.state} {selected.item.from.pinCode}</p>
                                            <p className="cl"><b>CL - {selected.item.current.address}</b></p>
                                            <p><b>TL - </b> {selected.item.to.city} {selected.item.to.state} {selected.item.to.pinCode} </p>
                                        </div>
                                    </InfoWindow>
                                )
                            }
                            {
                                tripselected.item?.latitude &&
                                (
                                    <InfoWindow
                                        position={{ lat: tripselected.lat, lng: tripselected.lng }}
                                        // clickable={true}
                                        onCloseClick={() => setTripSelected({})}
                                    >
                                        <div className="infowindowcont"> 
                                            <h5>{tripselected.item.tripDto.vehicleNumber}</h5>
                                            <p><b>DN - </b> {tripselected.item.tripDto.driverMobileNumber}</p>
                                            <p><b>FL - </b> {tripselected.item.tripDto.fromLocation.city} {tripselected.item.tripDto.fromLocation.state} {tripselected.item.tripDto.fromLocation.pinCode}</p>
                                            <p><b>HT - </b> {moment(tripselected.item.createdDate).format("DD-MM-YYYY")} {moment(tripselected.item.timeStamp).format("hh:mm")}</p>
                                            <p className="cl"><b>CL - {tripselected.item.address}</b></p>
                                            <p><b>TL - </b> {tripselected.item.tripDto.toLocation.city} {tripselected.item.tripDto.toLocation.state} {tripselected.item.tripDto.toLocation.pinCode} </p>
                                        </div>
                                    </InfoWindow>
                                )
                            }
                            {
                                directions.map((location, key) => {
                                    return (

                                        (<DirectionsRenderer key={key}
                                            directions={location}
                                            options={{
                                                markerOptions: { title: 'blackMarker', icon: '' },
                                                polylineOptions: {
                                                    strokeOpacity: 0.7,
                                                    strokeColor: '#007fff',
                                                },
                                                suppressMarkers: true
                                            }}
                                        />
                                        )
                                    ) 
                                })
                            }
                        </GoogleMap>
                    </LoadScriptOnlyIfNeeded>
                </Row>
            </Container>
        </div>
    );
}

export default Map;