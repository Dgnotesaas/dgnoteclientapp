import * as React from "react";
import { connect } from "react-redux";
import { IAppState, defaultAllTrackingListValue } from "../../models";
import { IGenericReducerState } from "../../utilities";
import { RouteComponentProps } from "react-router-dom";
import { IAllActiveTripModel } from "../../models/allactivetrips.model";
import { Subject } from "rxjs";
import Tracking from "./tracking";
import { getAllActiveLocateTrips } from "../../redux/actions/allactivetrips.action";

class TrackingContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps<{}>> {

  searchModel = defaultAllTrackingListValue;
  searchString$: Subject<string> = new Subject<string>();
  listType: number = 1;
  public constructor(props) {
    super(props);
    this.fetchList();
  } 

   fetchList() {
    this.props.dispatch(getAllActiveLocateTrips()); 
  }

  public render() {
    let getTracking = this.props.trackingList.Payload;
    console.log(getTracking);
    
    // return (<div>test</div>); 
    return (<Tracking locationsval={getTracking} />); 
  }

}
interface IConnectState {
  trackingList: IGenericReducerState<IAllActiveTripModel[]>;
}

const mapStateToProps = (state: IAppState) => ({
  trackingList: state.AllActiveTrip
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(TrackingContainer);