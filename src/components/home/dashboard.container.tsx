import React from "react";
import { connect } from "react-redux";
import { defaultTrackingListValue, IAppState } from "../../models";
import { IGenericReducerState } from "../../utilities";
import { RouteComponentProps } from "react-router-dom";
import { ITrackingListModel } from "../../models/tracking-list.model";
import { IBookingListModel } from "../../models/booking-list.model";
import { getBookings } from "../../redux/actions/booking-list.actions";
import { IAllActiveTripModel } from "../../models/allactivetrips.model";
import { defaultBookingValue, IBookingModel, ITrip } from "../../models/booking.model";
import { IConsentCountModel, ITransitCountModel } from "../../models/dashboard.model";
import { getAllActiveLocateTrips, getActiveLocateTripsById, getTransitCount, getConsentCount} from "../../redux/actions/allactivetrips.action";
import { updateTrip } from "../../redux/actions/trip.action";
import Dashboard from "./dashboard";

var filterLocId: string[] = [];
var selItem: string;
interface Trackdetail { selItem: string[]; locationsval: any[]; handleChangeVal: (e: any) => void; shareMap: any[]; }

interface MyState { checkFilterLocation: any };

class DashbaordContainer extends React.Component<Trackdetail & MyState & IConnectState & IConnectDispatch & RouteComponentProps<{}>> {

  searchModel = defaultTrackingListValue;
  listType: number = 1;

  public constructor(props:any) {
    super(props);
    this.state = {
      checkFilterLocation: 'test'
    }
    this.handleChangeVal = this.handleChangeVal.bind(this)
    this.fetchList(this.searchModel);
    filterLocId = [];
  }

  async fetchList(model: ITrackingListModel) {
    await this.props.dispatch(getBookings(model));
    await this.props.dispatch(getAllActiveLocateTrips()); 
    await this.props.dispatch(getTransitCount()); 
    await this.props.dispatch(getConsentCount()); 
  }

  async fetchFilterList(itemId) {
    var tripId = {
      "tripIds": itemId
    }
    await this.props.dispatch(getActiveLocateTripsById(tripId));
  } 

  async  submitCompleteTrip(trip){
   await this.props.dispatch(updateTrip(trip));
   this.fetchList(this.searchModel); 
  }



  handleChangeVal = (e: any) => {
    var isChecked = e.target.checked;
    var item = e.target.value;
    if (isChecked === true) {
      filterLocId.push(item)
      this.fetchFilterList(filterLocId)
    } else { 
      const index = filterLocId.indexOf(item);
      if (index > -1) {
        filterLocId.splice(index, 1);
      }
      this.fetchFilterList(filterLocId)
    }
  };

  public render() {
    let { content } = this.props.bookingListState.Payload;
    let getTracking = this.props.trackingList.Payload;
    let getConsentCount = this.props.consentcount.Payload;
    let getTransitCount = this.props.transitcount.Payload;
    
    return (<div><Dashboard locationsval={content} handleChangeVal={this.handleChangeVal} listlocation={getTracking} getConsentCount={getConsentCount} getTransitCount={getTransitCount} confirmCompletedModal={this.submitCompleteTrip.bind(this)} /></div>);
  }

}

interface IConnectState {
  bookingListState: IGenericReducerState<IBookingListModel>;
  trackingList: IGenericReducerState<IAllActiveTripModel[]>;
  consentcount: IGenericReducerState<IConsentCountModel>;
  transitcount: IGenericReducerState<ITransitCountModel>;
}

const mapStateToProps = (state: IAppState): IConnectState => ({
  bookingListState: state.BookingList,
  trackingList: state.AllActiveTrip,
  consentcount: state.ConsentCount,
  transitcount: state.TransitCount,
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(DashbaordContainer);