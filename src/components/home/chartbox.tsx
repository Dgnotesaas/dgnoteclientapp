import React from "react";
import { Doughnut } from "react-chartjs-2";
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { Container, Row, Dropdown, Form, Modal, Button, InputGroup, FormControl } from 'react-bootstrap';
import {Chart, ArcElement} from 'chart.js'
Chart.register(ArcElement);
Chart.register(ChartDataLabels); 

const options = {
  aspectRatio: 1.5,
  layout: {
      padding: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          color: '#fff',
      }
  },
  responsive: true,
  color: '#fff',
  cutoutPercentage: 90,
  legend: {
      display: false,
  },
  title: {
    color: '#fff',
      display: false,
  },
    datalabels: {
      color: '#fff',
      align: 'center',
      anchor: 'center',
      font: { weight: 'bold' },
    }
}

export function TransitCount(getTransitCount) {
  let transitval = (getTransitCount)
  const transitdata = {
    datasets: [
      {
        label: "My First dataset",
        fill: false,
        lineTension: 0.1,
        backgroundColor: ["#4bc0c0", "#ffda6c"],
        // borderColor: "#4bc0c0",
        borderDashOffset: 0.0,
        // borderJoinStyle: "miter",
        pointBorderColor: "rgba(255, 205, 205, 0)",
        pointBackgroundColor: "#fff",
        // pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "#4bc0c0",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 200,
        pointRadius: 100,
        pointHitRadius: 10,
        data: [getTransitCount['getTransitCount']['activeTransit'], getTransitCount['getTransitCount']['upcomingTransit']],
        datalabels: { 
          color: '#000',
        }
      }
    ]
  };
  return (<>
    <Row> 
      <div className="col-md-8"><Doughnut data={transitdata} options={options} /></div> 
      <div className="col-md-4">
          <ul className="list-unstyled mt-3">
            <li>
              <p className="d-inline mr-2" style={{"backgroundColor": "#4bc0c0", "color": "#4bc0c0"}}>sd</p>
              <small className="d-inline">Active Transit</small>
            </li>
            <li>
              <p className="d-inline mr-2" style={{"backgroundColor": "#ffda6c", "color": "#ffda6c"}}>sd</p>
              <small className="d-inline">Upcoming Transit</small>
            </li>
          </ul>
      </div>
    </Row> 
  </>);
}


export function ConsentCount(getConsentCount) {
  const consentdata = {
    datasets: [
      {
        label: "My First dataset",
        fill: false,
        lineTension: 0.1,
        backgroundColor: ["#ff7f42", "#dc3545"],
        // borderColor: "#4bc0c0",
        borderDashOffset: 0.0,
        // borderJoinStyle: "miter",
        pointBorderColor: "rgba(255, 205, 205, 0)",
        pointBackgroundColor: "#fff",
        // pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "#4bc0c0",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 200,
        pointRadius: 100,
        pointHitRadius: 10,
        data: [getConsentCount['getConsentCount']['allowedConsent'], getConsentCount['getConsentCount']['pendingConsent']],
        datalabels: { 
          color: '#000',
        }
      }
    ]
  };
  return (<>
    <Row> 
      <div className="col-md-8"><Doughnut data={consentdata} options={options} /></div> 
      <div className="col-md-4">
          <ul className="list-unstyled mt-3">
            <li>
              <p className="d-inline mr-2" style={{"backgroundColor": "#ff7f42", "color": "#ff7f42"}}>sd</p>
              <small className="d-inline">Allowed</small>
            </li>
            <li>
              <p className="d-inline mr-2" style={{"backgroundColor": "#dc3545", "color": "#dc3545"}}>sd</p>
              <small className="d-inline">Pending</small>
            </li>
          </ul>
      </div>
    </Row> 
  </>);
}
