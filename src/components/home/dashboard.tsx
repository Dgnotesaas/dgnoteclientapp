import React, { useState } from 'react';
import { Container, Row, Dropdown, Form, Modal, Button, InputGroup, FormControl } from 'react-bootstrap';
import SearchList from './SearchList';
import {TransitCount, ConsentCount} from './chartbox';
import MapStr from './map'
import  './dashboard.css';

var getDirections: any[] = [];

function Dashboard({ locationsval, handleChangeVal, listlocation, getConsentCount, getTransitCount, confirmCompletedModal}) {
    const [searchField, setSearchField] = useState("");
    let setlistlocation: any;

    if (locationsval) {
        const handleChange = e => {
            setSearchField(e.target.value);
        }; 

        const filteredLocations = locationsval.filter(
            location => {
                return (
                    (location.transporterName || '').toLowerCase().includes(searchField.toLowerCase())
                    || (location.vehicleNumber || '').toLowerCase().includes(searchField.toLowerCase())
                    || (location.driverMobileNumber || '').toLowerCase().includes(searchField.toLowerCase())
                    || (location.fromLocation.addressLine1 || '').toLowerCase().includes(searchField.toLowerCase())
                    || (location.toLocation.addressLine1 || '').toLowerCase().includes(searchField.toLowerCase())
                );
            }
        );      
        
        return (
            <div>
                <Row> 
                    <div className="col-md-12">
                        <div >
                            <Form.Group className="mb-3" controlId="formBasicSearch">
                                <InputGroup className="mb-3"> 
                                    <FormControl
                                        type="search"
                                        placeholder="Search"
                                        aria-label="Search"
                                        aria-describedby="basic-addon1"
                                        onChange={handleChange}
                                        className="shadow-sm"
                                        style={{ height: '35px', 'borderRadius': '5px' }}
                                    />
                                    {/* <InputGroup.Text id="basic-addon1" style={{ padding: '0px 10px', 'borderRadius': '25px' }} className="btn btn-sm button-theme text-white shadow" >Add Widget</InputGroup.Text> */}

                                </InputGroup>
                            </Form.Group>
                        </div>
                        <Row className='my-4'>
                            <div className='col-md-6 mt-1'>
                                <div className='border p-2 rounded bg-white shadow'>
                                    <div className="widget-header pt-2 mx-4 mb-3">
                                        <h6 className='text-center pl-5'><b>Tracking</b>
                                        {/* <i className="float-right mr-2 fa fa-close"></i><i className="float-right mr-2 fa fa-minus-square"></i>  */}
                                        </h6>
                                         
                                    </div>
                                    <TransitCount getTransitCount={getTransitCount} />
                                </div>
                            </div>
                            <div className='col-md-6 mt-1'>
                                <div className='border p-2 rounded bg-white shadow'>
                                    <div className="widget-header pt-2 mx-4 mb-3">
                                        <h6 className='text-center pl-5'><b>Consent</b>
                                        {/* <i className="float-right mr-2 fa fa-close"></i><i className="float-right mr-2 fa fa-minus-square"></i> */}
                                        </h6>
                                    </div>
                                    <ConsentCount  getConsentCount={getConsentCount}/>
                                </div>
                            </div> 
                        </Row> 
                        {/* <div className="widget-header pl-4 pt-2 mt-4 mb-3">
                            <h6><b>Post Estimated End Date</b><i className="float-right mr-2 fa fa-close"></i><i className="float-right mr-2 fa fa-minus-square"></i></h6>
                        </div>
                        <SearchList confirmCompletedModal={confirmCompletedModal} handleChangeVal={handleChangeVal} filteredLocations={filteredLocations} />  */}
                        <div className="widget-header pl-4 pt-2 mt-4 mb-3 shadow">
                            <h6><b>Vehicle Status</b>
                            {/* <i className="float-right mr-2 fa fa-close"></i><i className="float-right mr-2 fa fa-minus-square"></i> */}
                            </h6>
                        </div>
                        <MapStr setlistlocation={listlocation} />
                    </div>
                </Row> 
            </div>
        );
    } else {
        return (
            <div className="mt-5">
                <Row>
                    <div className="mr-5">
                        No Data Found
                    </div>
                </Row>
            </div>
        );
    }
}

export default Dashboard
// connect(null, mapDispatchToProps)(Dashboard);