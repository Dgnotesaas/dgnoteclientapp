import { useState, useEffect } from 'react';
import { Container, Row, Table } from "react-bootstrap";
import '../tracking/css/tracking.css';
import { GoogleMap, LoadScript, Marker, InfoWindow } from '@react-google-maps/api';
import LoadScriptOnlyIfNeeded from '../tracking/LoadScriptOnlyIfNeeded';
import Geocode from "react-geocode";

const mapStyles = {
    height: "50vh",
    width: "100%"
};

Geocode.setApiKey("AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU");

const defaultCenter = {
    lat: 20.593683, lng: 78.962883
}

function MapStr({ setlistlocation }) {
    const [selected, setSelected] = useState<any>({});

    const onSelect = item => {
        setSelected(item);
    };

    // Converts numeric degrees to radians
    function toRad(Value) {
        return Value * Math.PI / 180;
    }

    let count = 0;

    if (setlistlocation != '' && typeof setlistlocation !== 'undefined') { 

        return (
            <div>
                    <Row className="mt-4"> 
                        <div className="col-md-12 mb-4">
                            <LoadScriptOnlyIfNeeded
                                googleMapsApiKey='AIzaSyACijrul-ZdcGvAmiG_Clk-5I_V-WOibwU'>
                                <GoogleMap
                                    mapContainerStyle={mapStyles}
                                    zoom={5}
                                    center={defaultCenter} 
                                    >
                                    {
                                        Object.values(setlistlocation).map((item: any) => {
                                            count = count + 0.0001;
                                            if (item.lastSuccessfullTrackingHit != null) {
                                                return (
                                                    <Marker
                                                        key={item.bookingId}
                                                        position={{ lat: item.lastSuccessfullTrackingHit['latitude'] + count, lng: item.lastSuccessfullTrackingHit['longitude'] + count }}
                                                        onClick={() => onSelect(item)}
                                                    />
                                                )
                                            } else {
                                                return (<></>)
                                            }
                                        })
                                    }
                                    {
                                        selected?.lastSuccessfullTrackingHit &&
                                        ( 
                                            <InfoWindow
                                            position={{ lat: selected.lastSuccessfullTrackingHit.latitude, lng: selected.lastSuccessfullTrackingHit.longitude }}
                                            // clickable={true}
                                            onCloseClick={() => setSelected({})}
                                            >
                                                <div className="infowindowcont">
                                                <Table hover bordered> 
                                                        <tbody>
                                                            <tr>
                                                                <td><b style={{'fontWeight': 'bold'}}>Vehicle No. </b></td>
                                                                <td><b>{selected.lastSuccessfullTrackingHit.tripDto.vehicleNumber}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b style={{'fontWeight': 'bold'}}>Driver No. </b></td>
                                                                <td><b>{selected.lastSuccessfullTrackingHit.tripDto.driverMobileNumber}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b style={{'fontWeight': 'bold'}}>From Loc.</b></td>
                                                                <td><b>{selected.fromLocation.city} {selected.fromLocation.state} {selected.fromLocation.pinCode}</b></td>
                                                            </tr>
                                                            <tr style={{"color": "rgb(255, 11, 11)"}}>
                                                                <td><b style={{'fontWeight': 'bold'}}>Current Loc.</b></td>
                                                                <td><b>{selected.lastSuccessfullTrackingHit.address}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b style={{'fontWeight': 'bold'}}>To Loc.</b></td>
                                                                <td><b>{selected.toLocation.city} {selected.toLocation.state} {selected.toLocation.pinCode}</b></td>
                                                            </tr>
                                                            <tr>    
                                                                <td><b style={{'fontWeight': 'bold'}}>Speed </b></td>
                                                                <td><b>{selected.speed} Km/hr</b></td>
                                                            </tr>
                                                        </tbody>
                                                    </Table> 
                                                 </div> 
                                            </InfoWindow>

                                        )
                                    }
                                </GoogleMap>
                            </LoadScriptOnlyIfNeeded>
                        </div>
                        {/* <div className="col-md-3">
                        {
                            (selected.lastSuccessfullTrackingHit)?
                            selected?.lastSuccessfullTrackingHit &&
                            ( 
                                <div className="infowindowcont" style={{ padding: '10px', borderRadius: '2px', background: '#f5f5f5', opacity: '1', border: '2px solid #bbb',zIndex: 1000, 'overflowX': 'auto'}}>
                                    
                                    <Table striped bordered hover className='mt-1'> 
                                        <tbody>
                                            <tr>
                                                <td>Vehicle No.</td>
                                                <td>{selected.lastSuccessfullTrackingHit.tripDto.vehicleNumber}</td>
                                            </tr>
                                            <tr>
                                                <td>Start Loc.</td>
                                                <td>{selected.fromLocation.city} {selected.fromLocation.state} {selected.fromLocation.pinCode}</td>
                                            </tr>
                                            <tr>
                                                <td>Current Loc.</td>
                                                <td>{selected.lastSuccessfullTrackingHit.address}</td>
                                            </tr>
                                            <tr>
                                                <td>End Loc.</td>
                                                <td>{selected.toLocation.city} {selected.toLocation.state} {selected.toLocation.pinCode}</td>
                                            </tr>
                                            <tr>
                                                <td>ETA</td>
                                                <td>{calcTime(selected.fromLocation.latitude, selected.fromLocation.longitude, selected.toLocation.latitude, selected.toLocation.longitude).toFixed(1)} Hours</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </div>
                            ) : <div className="infowindowcont" style={{ padding: '10px', borderRadius: '2px', background: '#f5f5f5', opacity: '1', border: '2px solid #bbb',zIndex: 1000}}> 
                                <Table striped bordered hover className='mt-1'> 
                                    <tbody>
                                        <tr>
                                            <td>Vehicle No.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Start Loc.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Current Loc.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>End Loc.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>ETA</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </div>
                        }
                        </div> */}
                    </Row>
               
            </div>
        );
    } else {
        return (<div></div>)
    } 

    function calcCrow(lat1: any, lon1, lat2: any, lon2: any) {
        var R = 6371; // km
        var dLat = toRad(lat2 - lat1);
        var dLon = toRad(lon2 - lon1);
        var lat1: any = toRad(lat1);
        var lat2: any = toRad(lat2);

        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    }

    function calcTime(lat1: any, lon1, lat2: any, lon2: any) {
        var R = 6371; // km
        var dLat = toRad(lat2 - lat1);
        var dLon = toRad(lon2 - lon1);
        var lat1: any = toRad(lat1);
        var lat2: any = toRad(lat2);

        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d/20;
    }

};

export default MapStr;