import React, {useState} from 'react';
import { Button } from 'react-bootstrap';
import moment from "moment";

function Card({handleChangeVal, location, confirmCompletedModal}) { 
  location.status = "COMPLETED"
  if(location.driverMobileNumber != null) {
  return( 
    <tr key={location.id}> 
        <td>{location.tripNumber}</td>
        <td>{location.fromLocation.city} {location.fromLocation.state} {location.fromLocation.pinCode}</td>
        <td>{location.toLocation.city} {location.toLocation.state} {location.toLocation.pinCode}</td>
        <td>{moment(location.transitEstimatedEndDate).format("DD-MM-YYYY")}</td>
        <td>{location.vehicleNumber}</td>
        <td>{location.driverMobileNumber}</td> 
        <td><div style={{color: "#004385"}}  onClick={() => confirmCompletedModal(location)}>Complete</div></td> 
    </tr>
  )
  } else {
    return (<></>)
  }
}

export default Card;