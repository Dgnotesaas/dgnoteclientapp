import React, {useState} from 'react';
import { Button, Col, Container, Form, Row, Table } from "react-bootstrap";
import Card from './Card';

function SearchList({ handleChangeVal, filteredLocations, confirmCompletedModal }) {
    const filtered = filteredLocations.map(location => <Card handleChangeVal={handleChangeVal} key={location.id} location={location} confirmCompletedModal={confirmCompletedModal} />); 
    console.log('log', filtered);
    let setfiltered = (filtered[0])?filtered:(<tr><td colSpan={7}>No records to display</td></tr>)
    return (
        <div>
            <Container>
                <Row style={{'overflowX': 'auto'}}>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Trip No.</th>
                                <th>From Loc.</th>
                                <th>To Loc.</th>
                                <th>Estimate End Date</th>
                                <th>Vehicle No.</th>
                                <th>Driver No.</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {setfiltered}
                        </tbody>
                    </Table>
                </Row>
            </Container>
        </div>
    );
}

export default SearchList;