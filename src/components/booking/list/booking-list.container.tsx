
import * as React from "react";
import { connect } from "react-redux";
import { defaultListSearchRequestModel, IAppState } from "../../../models";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import moment from "moment";
import '../booking.css'
import { IGenericReducerState } from "../../../utilities";
import { Link, RouteComponentProps } from "react-router-dom";

import 'font-awesome/css/font-awesome.min.css';
import { dateFilter } from 'react-bootstrap-table2-filter';
import { BookingList } from "./booking-list";
import { defaultBookingListValue, IBookingListModel } from "../../../models/booking-list.model";
import { getBookings, getBookingsForexport, updateBookingListProperties } from "../../../redux/actions/booking-list.actions";
import { IEmailTemplateModel } from "../../../models/emailtemplate.model";
import { postEmail } from "../../../redux/actions/emailtemplate.action";
import { IListSearchRequstModel } from "../../../models/list-search-request.model";
import { debounceTime, distinctUntilChanged, Subject } from "rxjs";
import { defaultBookingValue, IBookingModel, ITrip } from "../../../models/booking.model";
import { checkConsent, updateTrip } from "../../../redux/actions/trip.action";
import { Button, ButtonProps, Image, OverlayTrigger, Tooltip, Dropdown } from "react-bootstrap";
import { updateBookingProperties } from "../../../redux/actions/booking.action";
import { toast } from "react-toastify";


type MyState = { value: [string], showCancelModal: boolean, showCompletedModal, csvData : any[], handleShowicon: boolean};


class BookingListContainer extends React.Component<IConnectState & IConnectDispatch & RouteComponentProps, MyState> {

  searchModel = defaultListSearchRequestModel;
  searchString$: Subject<string> = new Subject<string>();
  listType: number = 1;
  showCancelModal: boolean = false;
  totalRowCountAfterFilter: number = 0;
  selectedTrips: any = [];
  showCompletedModal: boolean = false;
  handleShowicon: boolean = false
  csvData: any[] = [];
  csvName: string = "";

  public constructor(props:any) {
    super(props);
    this.state = {
      handleShowicon: false,
      value: [''],
      showCancelModal: false,
      showCompletedModal: false,
      csvData: [],
    }
    this.handleTableChange = this.handleTableChange.bind(this);
    this.onSearchTextChange = this.onSearchTextChange.bind(this);
    this.onExportClicked = this.onExportClicked.bind(this);
    this.handleChangeModelIcon = this.handleChangeModelIcon.bind(this);
    this.inItSearch();
    if (window.location.href.indexOf("/booking-list/active") > -1) {
      this.listType = 1
      this.searchModel.status = "ACTIVE";
      this.csvName = "All_Booking_Trips";
    } else if (window.location.href.indexOf("/booking-list/planned") > -1) {
      this.listType = 4
      this.searchModel.status = "ALL";
      this.csvName = "Active_Booking_Trips";
    } else if (window.location.href.indexOf("/booking-list/cancelled") > -1) {
      this.listType = 3;
      this.searchModel.status = "CANCELLED";
      this.csvName = "Cancelled_Booking_Trips";
    }
    else if (window.location.href.indexOf("/booking-list/completed") > -1) {
      this.listType = 2
      this.searchModel.status = "COMPLETED";
      this.csvName = "Completed_Booking_Trips";
    }
    this.searchModel.searchToken = "";

    this.setState({ handleShowicon: false})
    this.fetchList(this.searchModel);
  }

  inItSearch() {
    this.searchString$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
    ).subscribe(text => {
      this.searchModel.searchToken = text;
      this.fetchList(this.searchModel);
    })
  }
  closeModal() {
    this.showCompletedModal = false;
    this.showCancelModal = false;
    this.selectedTrips = [];
    this.setState({ showCancelModal: false, showCompletedModal: false })
  }

  confrimModalAction(id) {

  }

  handleChangeModelIcon = (e, type) => {
    e.preventDefault(); 
    this.setState({ handleShowicon: type})
  }

  isValidEndDate = (date: Date) => {
    const previousDay = moment().subtract(1, "days");
    const lastDay = moment().add(10, "days");
    return date.getTime() > previousDay.toDate().getTime() && date.getTime() < lastDay.toDate().getTime();
  };

  isValidEndTime = (date: Date) => {

    return date.getHours() > new Date().getHours();
  };
  onSearchTextChange(e) {
    this.searchString$.next(e.target.value.trim())
  }

  fetchList(model: IListSearchRequstModel) {
    this.props.dispatch(getBookings(model));
  }

  onExportClicked() {
    //this.
    const searchModel = { ...this.searchModel };
    searchModel.pageSize = this.totalRowCountAfterFilter;
    getBookingsForexport(searchModel).then(response => {
      if (response.data.status) {
        const bookings = response.data.data as IBookingListModel;
        if (bookings.content && bookings.content.length > 0) {
          this.csvData = bookings.content;
          this.csvData = this.csvData.map((m, i) => {
            m.transitStartDate = moment(m.transitStartDate).format("DD-MM-YYYY hh:mm a") + " - " + moment(m.transitEstimatedEndDate).format("DD-MM-YYYY hh:mm a");
            m.transitEstimatedEndDate = moment(m.transitEstimatedEndDate).format("DD-MM-YYYY hh:mm a");
            m.transitActualEndDate = moment(m.transitActualEndDate).format("DD-MM-YYYY hh:mm a");
            m.bookingDate = moment(m.bookingDate).format("DD-MM-YYYY hh:mm a");
            m.serialNum = i + 1;
            m.fromLocation = m.fromLocation.city ? m.fromLocation.city + "," + m.fromLocation.state + " - " + m.toLocation.city + "," + m.toLocation.state : m.toLocation.state + " - " + m.toLocation.state;
            m.toLocation = m.toLocation.city ? m.toLocation.city + "," + m.toLocation.state : m.toLocation.state;
            return m;
          })
          this.setState({ csvData: this.csvData }, () => {
            // click the CSVLink component to trigger the CSV download
            setTimeout(() => {
              const el = document.getElementById('csvlinkid') as HTMLElement;
              el.click();
            }, 1000);
          });
        } else {
          toast.error("No trips to export", { toastId: 'No trips to export' });
        }


      }

    });
  }


  async updateTransistActualDate(_d, _trip) {
    if (typeof _d === 'object') {

      let trips = [...this.props.bookingListState.Payload.content] as ITrip[];
      let trip = trips.find(x => x.id === _trip.id) as ITrip;
      trip.transitActualEndDate = new Date(_d).getTime();
      await this.props.dispatch(updateTrip(trip));
      this.fetchList(this.searchModel);
      // } else {
      //   this.props.bookingListState.Payload.content = this.props.bookingListState.Payload.content.map(x => {
      //     if(x => x.id === _trip.id){
      //       x.transitActualEndDate = new Date(_d).getTime();
      //     }
      //     return x;
      //   })
      // }
    }

  }

  handleTableChange = (type, { page, sizePerPage, filters, sortField, sortOrder, cellEdit }) => {

    if (type === 'pagination') {
      this.searchModel.pageNumber = page - 1;
      if (this.searchModel.pageSize !== sizePerPage) {
        this.searchModel.pageNumber = 0;
      }
      this.searchModel.pageSize = sizePerPage;
      this.fetchList(this.searchModel);
    }
    else if (type === "sort") {
      this.searchModel.sortDirection = sortOrder ? sortOrder.toUpperCase() : "ASC";
      this.searchModel.sortProperty = sortField;
      this.fetchList(this.searchModel);
    }

  }


  onCompleteTripClicked(id, trip) {
    this.showCompletedModal = true;
    this.selectedTrips.push(trip);
    this.setState({ showCompletedModal: true })
  }

  onCancelTripClicked(id, trip) {
    this.showCancelModal = true;
    this.selectedTrips.push(trip);
    this.setState({ showCancelModal: true })
  }


  async onRefreshConsent(id) {
    await this.props.dispatch(checkConsent(id));
  }

  async onResendConsent(id) {
    await this.props.dispatch(checkConsent(id));
  }

  getColumnsDefination() {
    const allColumns = [
      {
        dataField: 'tripNumber',
        text: 'Trip No.',
        hide: true,
        sort: true,
        editable: false,
        listType: [1, 2, 3, 4],
        formatter: (cell, row) => {

          return <>
            <h6><b><Link to={`/booking/view/${row.bookingId}/${row.id}`}  >{cell}</Link></b></h6>
            <div className="mt-4">{moment(row.bookingDate).format('DD/MM/YY hh:mm a')}</div>
          </>
        },
        style: {
          'width': '16%', 'whiteSpace': 'break-spaces'
        }

      },
      {
        dataField: 'fromLocation',
        text: 'From - To Location',
        hide: true,
        sort: true,
        editable: false,
        formatter: (cell, row) => {
          return <> <div style={{ 'background': '#cedebb', 'color': '#222' }} className="badge badge-pill badge-danger">{cell.city ? cell.city + "," + cell.state : cell.state}</div> <div className="my-1" >to</div> <div style={{ 'background': '#fccdcd', 'color': '#222' }} className="badge badge-pill badge-success">{row.toLocation.city ? row.toLocation.city + "," + row.toLocation.state : row.toLocation.state}</div></>;
        },
        listType: [1, 2, 3, 4],
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', 'whiteSpace': 'break-spaces' };
        },
        // tdStyle: (colum, colIndex) => {
        //   return { width: '450px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        // },
        style: {
          'width': '20%', 'textAlign': 'center', 'whiteSpace': 'break-spaces'
        }
      },
      {
        dataField: 'transitStartDate',
        text: 'Start - End Date',
        hide: true,
        sort: true,
        // filter: dateFilter(),
        formatter: (cell, row) => {
          let dateObj = cell;
          let dateObjend = row.transitEstimatedEndDate;
          if (typeof cell !== 'object') {
            dateObj = new Date(cell);
            dateObjend = new Date(row.transitEstimatedEndDate);
          }
          return <><div>{moment(dateObj).format('DD/MM/YY hh:mm a')}</div>
            <div className="mt-4" >{moment(dateObjend).format('DD/MM/YY hh:mm a')}</div>
          </>
        },
        editable: false,
        listType: [1, 2, 3, 4],
        style: {
          width: '10%', 'textAlign': 'center', 'whiteSpace': 'break-spaces'
        }
      },
      {
        dataField: 'transitActualEndDate',
        text: 'Completed Date',
        hide: true,
        sort: true,
        formatter: (cell) => {
          let dateObj = cell;
          if (typeof cell !== 'object') {
            dateObj = new Date(cell);
          }
          //  return `${('0' + dateObj.getDate()).slice(-2)}/${('0' + (dateObj.getMonth() + 1)).slice(-2)}/${dateObj.getFullYear()}`;
          return moment(dateObj).format('DD-MM-YY hh:mm a')
        },
        editable: false,
        listType: [2],
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap', 'overflow': 'hidden' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap', 'overflow': 'hidden' };
        },
      },
      {
        dataField: 'driverMobileNumber',
        text: 'Mobile  No.',
        hide: true,
        sort: true,
        editable: false,
        listType: [1, 2, 3, 4],
        // headerStyle: (colum, colIndex) => {
        //   return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap'  };
        // },
        // tdStyle: (colum, colIndex) => {
        //   return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        // },
        formatter: (cell, row) => {
          return <>
            <div className=""><b>{cell}</b></div>
            {/* <div className="my-3">Airtel</div> */}
            <div className="mt-4">Consent {row.consentStatus === "NOT_INITIATED" || row.consentStatus === "PENDING" || row.consentStatus === "EXPIRED" ? <span className="consent-st-trips"><i className="fa fa-times-circle-o fa-1x text-danger"></i></span> : row.consentStatus === "ALLOWED" ? <span className="custom-t-btn"><i className="fa fa-check-circle-o fa-1x text-success"></i></span> : <span className="custom-t-btn"><i className="fa fa-trash-o fa-1x"></i></span>}</div>
          </>;
        },
        style: {
          'width': '5%', 'whiteSpace': 'break-spaces'
        }
      },
      {
        dataField: 'vehicleNumber',
        text: 'Vehicle No.',
        hide: true,
        sort: true,
        editable: false,
        listType: [1, 2, 3, 4],
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        formatter: (cell, row) => {
          return <>
            <div><span className="mr-1 ml-2" style={{ "fontSize": "16px", "fontWeight": "bold" }}>{cell}</span> <Link className="custom-t-btn" to={'../tracking/single-tracking/' + row.id + '/type'} ><img className="bk-list-loc-icn" alt="" src="/images/location-icon-2.png" /></Link></div>
            <div className="my-2"><b>Dis. Left : {Math.round(row.remainingDistance)}KM</b></div>
            <div style={{ 'background': '#f8dab7', lineHeight: '15px', 'color': '#222', width: "100%", maxWidth: "100%", wordBreak: 'break-all', whiteSpace: 'normal' }} className="my-2 px-3 py-2 badge badge-pill badge-danger">{row.currentLocation}</div>
          </>;
        },
        style: {
          'width': '27%', 'textAlign': 'center', 'whiteSpace': 'break-spaces'
        }
      },
      {
        dataField: 'action',
        text: 'Action',
        hide: true,
        sort: false,
        editable: false,
        listType: [1, 4],
        headerStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        },
        formatter: (cell, row) => {

          return <>

            <div className="d-inline mr-2 mb-2">
          {/* <button style={{"border": "0px", "width": "30px", "height": "30px", "borderRadius": "50px", "boxShadow": "rgb(221 221 221) 0px 0px 5px 2px", "background": "#fff"}} onClick={(e) => this.handleChangeModelIcon(e, true)} id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i className="fa fa-share-alt"></i>
          </button> */}
          </div>
            <div className="dropdown d-inline">
              <button style={{ "border": "0px", "width": "30px", "height": "30px", "borderRadius": "50px", "boxShadow": "rgb(221 221 221) 0px 0px 5px 2px", "background": "#fff" }} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i className="fa fa-ellipsis-h"></i>
              </button>
              <div className="dropdown-menu rounded" aria-labelledby="dropdownMenuButton">
                {row.consentStatus !== 'ALLOWED' ?
                  <Button className="dropdown-item" onClick={() => this.onRefreshConsent(cell)} variant="link">Resend</Button> : ""}
                <Link className="dropdown-item mt-2" to={`/booking/${row.bookingId}`}  >Edit</Link>
                <Button className="dropdown-item mt-2" onClick={() => this.onCancelTripClicked(row.id, row)} variant="link">Cancel</Button>
              </div>
            </div>
            <div className="mb-2">
              {/* <button style={{"border": "0px", "width": "30px", "height": "30px", "borderRadius": "50px", "boxShadow": "rgb(221 221 221) 0px 0px 5px 2px", "background": "#fff"}} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> */}
              <Button className="mt-3" style={{ "border": "0px", "height": "30px", "borderRadius": "50px", "boxShadow": "rgb(221 221 221) 0px 0px 5px 2px", color: "#666", "background": "#fff" }} onClick={() => this.onCompleteTripClicked(row.id, row)} variant="link">Complete</Button>
              {/* </button> */}
            </div>
          </>
        },
        style: {
          'width': '15%', 'textAlign': 'center', 'whiteSpace': 'break-spaces'
        }
      },
      {
        dataField: 'remarks',
        text: 'Reason to Cancel',
        hide: true,
        sort: true,
        editable: false,
        listType: [3],
        formatter: (cell, row) => {

          return <>
            <span title={cell} className="trips-cancel-text">
              {cell}
            </span>
          </>
        },
        headerStyle: (colum, colIndex) => {
          return { width: '100px', textAlign: 'center', 'whiteSpace': 'nowrap', 'overflow': 'hidden', 'textOverflow': 'ellipsis' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '100px', textAlign: 'center', 'whiteSpace': 'nowrap', 'overflow': 'hidden', 'textOverflow': 'ellipsis' };
        }
      },

    ];

    return allColumns;
  }

  getselectedTripColumns() {
    const selectedTripColumns = [
      {
        dataField: 'tripNumber',
        text: 'Trip No.',
        sort: false,
        editable: false,
      }, {
        dataField: 'bookingDate',
        text: 'Date',
        sort: false,
        formatter: (cell) => {
          return <>{moment(cell).format('DD-MM-YY hh:mm a')}</>
        }
        ,
        editable: false
      }, {
        dataField: 'tripType',
        text: 'Trip Type',
        //filter: textFilter(),
        sort: false,
        formatter: (cell) => {
          return <>{cell.replace("_", " ")}</>
        },
        editable: false

      },
      {
        dataField: 'fromLocation.city',
        text: 'From Loc.',
        sort: false,
        editable: false

      },
      {
        dataField: 'toLocation.city',
        text: 'To Loc.',
        sort: false,
        editable: false

      },
      {
        dataField: 'transitStartDate',
        text: 'Start Date',
        sort: false,
        formatter: (cell) => {
          let dateObj = cell;
          if (typeof cell !== 'object') {
            dateObj = new Date(cell);
          }
          // return  `${('0' + dateObj.getDate()).slice(-2)}/${('0' + (dateObj.getMonth() + 1)).slice(-2)}/${dateObj.getFullYear()}`;
          return moment(dateObj).format('DD-MM-YY hh:mm a')
        },
        editable: false,
        listType: [1, 2, 3, 4],
        headerStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        classes: 'date-custom-cell'
      },
      {
        dataField: 'transitEstimatedEndDate',
        text: 'End Date',
        sort: false,
        formatter: (cell) => {
          let dateObj = cell;
          if (typeof cell !== 'object') {
            dateObj = new Date(cell);
          }
          // return `${('0' + dateObj.getDate()).slice(-2)}/${('0' + (dateObj.getMonth() + 1)).slice(-2)}/${dateObj.getFullYear()}`;
          return moment(dateObj).format('DD-MM-YY hh:mm a')
        },
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '200px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        classes: 'date-custom-cell'
      },

      {
        dataField: 'customerName',
        text: 'Customer',
        sort: false,
        editable: false,

        headerStyle: (colum, colIndex) => {
          return { width: '80px', textAlign: 'center', 'whiteSpace': 'nowrap', 'textOverflow': 'ellipsis' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '80px', textAlign: 'center', 'whiteSpace': 'nowrap', 'textOverflow': 'ellipsis' };
        }

      },
      {
        dataField: 'transporterName',
        text: 'Transporter',
        sort: false,
        editable: false,

        headerStyle: (colum, colIndex) => {
          return { width: '80px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '80px', textAlign: 'center', 'whiteSpace': 'break-spaces' };
        }
      },
      {
        dataField: 'vehicleNumber',
        text: 'Vehicle No.',
        sort: false,
        editable: false,

        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      {
        dataField: 'driverMobileNumber',
        text: 'Driver  No.',
        sort: false,
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      },
      {
        dataField: 'containerNumber',
        text: 'Container No.',
        sort: false,
        editable: false,

        headerStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        },
        tdStyle: (colum, colIndex) => {
          return { width: '75px', textAlign: 'center', 'whiteSpace': 'nowrap' };
        }
      }


    ];
    return selectedTripColumns;
  }

  shareMap = (e, getArray) => {
    e.preventDefault(); 
      let today : any = new Date();
      let dd = String(today.getDate()).padStart(2, '0');
      let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = today.getFullYear();
      
      today = dd + '-' + mm + '-' + yyyy;
 
      this.props.emailTemplateSend.Payload.email_to = this.state.value    
      this.props.emailTemplateSend.Payload.subject = "TMS | Tracking | "+ today    
      let header = "<div style='width: 100%; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;'><div style='display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;'><div style='margin-bottom: 1.5em; flex: 0 0 100%; max-width: 100%;'><p>Dear ,</p><p>Please find vehicle status below:</p></div></div></div>"
      
      let body1 = "<div style='width: 100%; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;'><div style='display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;'><div style=' flex: 0 0 100%; max-width: 100%;'><h3 style='margin-bottom: 5px;'><b>Transit Details:</b></h3><table border='1' style='border-collapse: collapse; margin-bottom: 0; width: 100%; margin-bottom: 1rem; color: #212529;'><thead>"

      let body2 =  ""
      let id = ""
      let hostName = ""
      if(window.location.hostname == 'localhost') {
        hostName = "dev.tms.dgnote.com";
      } else {
        hostName = window.location.hostname
      }

      let customer = '';
      
      Object.values(getArray).map((item: any, key) => {
        id = 'test';
        customer = (item.customer)? item.customer : ''; 
        body2 = body2 + "<tr><th style='padding: 10px;' colspan='4'><span style='float: left; margin-right: 50px;'>"+ item.vehicleNumber +"</span> <span style='float: right;'>"+item.current.address+"</span> </th><th style='padding: 10px; text-align: center;'>Map</th></tr></thead><tbody><tr><td style='padding: 10px'><b>From Location</b></td><td style='padding: 10px'>"+item.from.city+" "+item.from.state+"</td><td style='padding: 10px'><b>To Location</b></td><td style='padding: 10px'>"+item.to.city+" "+item.to.state+"</td><td style='padding: 10px' rowspan='2'><a href='https://"+hostName+"/tracksharing/"+id+"'><img width='50' height='50' src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Google_Maps_icon_%282015-2020%29.svg/2048px-Google_Maps_icon_%282015-2020%29.svg.png'/></a></td></tr><tr><td style='padding: 10px'><b>Customer</b></td><td style='padding: 10px'>"+customer+"</td><td style='padding: 10px'><b>Transit End Date</b></td><td style='padding: 10px'>"+moment(item.transitEndDate).format('DD-MM-YYYY')+"</td></tr>"
      })

      let body3 = "</tbody></table></div></div></div>"

      let footer = "<div style='width: 100%; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;'><div style='margin-top: 1.5rem !important; display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;'><div style=' flex: 0 0 100%; max-width: 100%;'><p>Regards,</p><p>DgNote Team</p></div></div></div>"

      this.props.emailTemplateSend.Payload.content = header +""+ body1 +""+ body2 +""+ body3 +""+  footer
      
      this.setState({
        value: ['']
      })

      this.props.dispatch(postEmail(this.props.emailTemplateSend.Payload)); 
  } 

  handleChangemail = (event) => {
    event.preventDefault();
    const { value } = event.target;
    if(this.state.value[0] == '') {
      this.setState({
        value: [value]
      })
    } else {
      if(event.target.value != '') {
        const updatearray: any[string] = [...this.state.value, event.target.value]
        this.setState({
          value: updatearray
        })
      }
    }  
  }
  
  public handleRemarksChange(e) {
    const { value } = e.target;
    this.selectedTrips[0].remarks = value;
    this.setState({ showCancelModal: true })

  }

  public async submitCancelTrip() {
    var trip = this.selectedTrips[0] as ITrip;
    trip.status = "CANCELLED";
    await this.props.dispatch(updateTrip(trip));
    this.fetchList(this.searchModel);
    this.closeModal();
  }
  async submitCompleteTrip() {
    var trip = this.selectedTrips[0] as ITrip;
    trip.status = "COMPLETED";

    await this.props.dispatch(updateTrip(trip));
    this.fetchList(this.searchModel);
    this.closeModal();
  }
  componentWillUnmount() {
    this.props.dispatch(updateBookingListProperties({ ...defaultBookingListValue }))
    this.props.dispatch(updateBookingProperties(JSON.parse(JSON.stringify({ ...defaultBookingValue }))))
  }
  public render() {

    let { content, currentPage, totalElements } = this.props.bookingListState.Payload;
    currentPage = currentPage + 1;
    if (!content)
      content = [];
    this.totalRowCountAfterFilter = totalElements;
    content = content.map(x => {
      x.transitActualEndDate = moment(x.transitActualEndDate);
      return x;
    })
    console.log('this.state', this.state);
    return <BookingList
      listType={this.listType}
      data={content}
      page={currentPage}
      sizePerPage={this.searchModel.pageSize}
      totalSize={totalElements}
      onTableChange={this.handleTableChange}
      onSearchTextChange={this.onSearchTextChange}
      columnsDef={this.getColumnsDefination()}
      closeModal={this.closeModal.bind(this)}
      confirmCancelModal={this.submitCancelTrip.bind(this)}
      selectedTrips={this.selectedTrips}
      showCancelModal={this.showCancelModal}
      showCompletedModal={this.showCompletedModal}
      confirmCompletedModal={this.submitCompleteTrip.bind(this)}
      getColums={this.getselectedTripColumns.bind(this)}
      handleRemarksChange={this.handleRemarksChange.bind(this)}
      csvData={this.csvData}
      shareMap={this.shareMap} 
      showmodel={this.state.handleShowicon}
      handleChangemail={this.handleChangemail.bind(this)}
      onExportClicked={this.onExportClicked}
      csvName={this.csvName}

    />;
  }





}
interface IConnectState {
  bookingListState: IGenericReducerState<IBookingListModel>;
  bookingState: IGenericReducerState<IBookingModel>;
  emailTemplateSend: IGenericReducerState<IEmailTemplateModel>; 
  loadingState: boolean
}


const mapStateToProps = (state: IAppState): IConnectState => ({
  bookingListState: state.BookingList,
  bookingState: state.Booking,
  emailTemplateSend: state.EmailTemplate,
  loadingState: true
});

interface IConnectDispatch {
  dispatch: any;
}

export default connect(mapStateToProps)(BookingListContainer);