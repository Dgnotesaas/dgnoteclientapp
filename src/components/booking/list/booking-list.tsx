import React, { Component, useState, useEffect, } from 'react';
import styled from 'styled-components';
import Cookies from 'js-cookie'
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
import filterFactory from 'react-bootstrap-table2-filter'
import Button from "react-bootstrap/esm/Button";
import { Link } from 'react-router-dom';
import { Form, Modal, Row } from "react-bootstrap";
import { CSVLink } from "react-csv";


const defaultSorted = [{
  dataField: 'bookingDate',
  order: 'desc'
}];

const cellEditProps = {
  mode: 'click',
  blurToSave: true
};

let columnsval: any[] = [];


export const BookingList = ({ data, page, sizePerPage, onTableChange, totalSize,
  onSearchTextChange, listType, columnsDef, showCancelModal, closeModal,
  confirmCancelModal, selectedTrips, getColums, showCompletedModal, confirmCompletedModal, handleRemarksChange, csvData, shareMap, handleChangemail, showmodel, onExportClicked, csvName }) => {
  const [show, setShow] = useState({ modals: false });
  const [showshare, setShowShare] = useState({modals: false, type: ''});
  const [showicon, setShowicon] = useState({ modals: false});
  const [inputList, setInputList] = useState([{ email: ""}]);
  let columns: any[] = columnsDef.filter(x => x.listType.indexOf(listType) > -1);

  console.log('showmodel', showmodel);
  
  // Cookies.remove('setactivecols');
  let coockiesval: any = '';
  if (listType == 1) {
    coockiesval = Cookies.get('setactivecols')
  } else if (listType == 2) {
    coockiesval = Cookies.get('setcompletecols')
  } else if (listType == 3) {
    coockiesval = Cookies.get('setcancelcols')
  }
  if (coockiesval) {
    let jsonStr: string = coockiesval;
    columnsval = JSON.parse(jsonStr);
    console.log('columnsval', columnsval);
    let getkeycolumn: any[] = [];
    let copycolumn: any[] = [];
    columns.map((olditem, getkey) => {
      if (olditem) {
        if (olditem.text == "Consent Action") {
          columns[getkey].dataField = 'consent';
        }
        columnsval.map((item: any, key) => {
          if (item) {
            if (olditem.text == item.text) {
              getkeycolumn.push(key)
            }
          }
        })
      }
    })

    if (getkeycolumn) {
      console.log('getkeycolumn', getkeycolumn);
      getkeycolumn.map((setitem: any, key) => {
        copycolumn[key] = columns[setitem];
        copycolumn[key].hide = columnsval[setitem].hide;
      })
      columns = copycolumn;
    }
  }
  const [cols, setCols] = useState<any>(columns);
  const [dragOver, setDragOver] = useState("");

  const csvHeaders = [...[...columns].map(m => { return { label: m.text, key: m.dataField } }).filter(x => !(x.label.includes('Action') || x.label.includes('Track')))];
  let tableRef = React.createRef();

  const handleDragStart = e => {
    const { id } = e.target;
    const idx = cols.indexOf(id);
    e.dataTransfer.setData("colIdx", id);
  };

  const handleDragOver = e => e.preventDefault();
  const handleDragEnter = e => {
    const { id } = e.target;
    setDragOver(id);
  };

  const setColumn = e => {
    const { value, checked } = e.target;

    cols.map((item, key) => {
      if (item.text == value) {
        cols[key].hide = checked;
      }
    })

    setCols(cols);
    if (listType == 1) {
      Cookies.remove('setactivecols');
      Cookies.set('setactivecols', JSON.stringify(cols))
    } else if (listType == 2) {
      Cookies.remove('setcompletecols');
      Cookies.set('setcompletecols', JSON.stringify(cols))
    } else if (listType == 3) {
      Cookies.remove('setcancelcols');
      Cookies.set('setcancelcols', JSON.stringify(cols))
    }
  }

  const handleOnDrop = e => {
    const { id } = e.target;
    const droppedColIdx = cols.indexOf(id);
    const draggedColIdx = e.dataTransfer.getData("colIdx");
    const tempCols = [...cols];

    tempCols[draggedColIdx] = cols[id];
    tempCols[id] = cols[draggedColIdx];

    const filtertempCols = tempCols.filter(x => x)

    if (filtertempCols.length == columns.length) {
      if (listType == 1) {
        Cookies.remove('setactivecols');
        Cookies.set('setactivecols', JSON.stringify(filtertempCols))
      } else if (listType == 2) {
        Cookies.remove('setcompletecols');
        Cookies.set('setcompletecols', JSON.stringify(filtertempCols))
      } else if (listType == 3) {
        Cookies.remove('setcancelcols');
        Cookies.set('setcancelcols', JSON.stringify(filtertempCols))
      }
      setCols(filtertempCols);
      setDragOver("");
    }
  };

  const handleClose = () => setShow({ modals: false });
  const handleShow = () => setShow({ modals: true }); 

  const handleCloseShare = () => setShowShare({modals: false, type: ''});
  const handleShowShare = (permission) => {
      setShowShare({ modals: true, type: permission })
      setShowicon({ modals: false})
  };

  const handleCloseicon = () => setShowicon({ modals: false});
  const handleShowicon = () => setShowicon({ modals: true});

  const onClickButton = (e, path) => {

    e.preventDefault();
    
    const validEmailRegex =
    RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

    let validmail = true; 
    // console.log('showmodel', showmodel.value);
    
    // showmodel.value.map((item : any, key) => {
    //     if(!validEmailRegex.test(item) || item == '') { 
    //         validmail = false; 
    //     } else {
    //         validmail = true; 
    //     }
    // })

    // if(validmail) {
    //     setShow({ modals: false, type: '' })
    //     setInputList([{email: ""}]);
    //     shareMap(e, path) 
    // } else {
    //     toast.error('Please Enter Valid Email Id', {
    //         toastId: 'Please Enter Valid Email Id'
    //     })
    // } 
  }

  const handleRemoveClick = (e,index) => {
    e.preventDefault();
    const list = [...inputList];
    list.splice(index, 1);
    setInputList(list);
  };

  const handleAddClick = () => {
    setInputList([...inputList, { email: ""}]);
  };

  let setColumns = cols.filter(function (item) {
    return item.hide !== false;
  })

  return (

    <div className="bt-data-table-custom">

      <div className="">
        <div className="row">
          <div className="col-md-6">

            <CSVLink id="csvlinkid" className="hidden" filename={csvName + "_" + new Date().toDateString().replaceAll(' ', '_') + ".csv"} data={csvData} headers={csvHeaders}></CSVLink>
            <h3 className="font-weight-bold mb-0">{listType === 1 ? 'Active ' : listType === 2 ? 'Completed' : listType === 4 ? 'Planned ' : 'Cancelled'} Booking</h3>
          </div>
        </div>
        <div className="row pb-3 pt-4 d-block txt-ryt">
          <div className="col-md-9 d-inline-block txt-lft">
            {/* <Button className="btn btn-sm button-theme btn-rounded mb-1" onClick={e => {
            let table = tableRef as any;
            table.current.sortContext.handleSort(defaultSorted);
              }}>
                Reset Sort
          </Button> */}
            <input type="text"
              onChange={onSearchTextChange}
              style={{ 'borderRadius': '5px' }}
              className="form-control form-control-sm"
              placeholder="Search" />
          </div>
          <div className="col-md-3 d-inline-block">
            <div className="text-right mt-3">

              <Button className="btn btn-sm button-theme btn-rounded text-uppercase text-white" onClick={() => onExportClicked()}>
                Export
              </Button>
              <Button className="btn btn-sm button-theme btn-rounded text-uppercase text-white ml-2" onClick={() => handleShow()}>Setting</Button>
            </div>
          </div>
          <div className="col-md-12 d-inline-block">
            <div className="text-left mt-3" style={{"overflow": "auto", "margin": "4px 4px", "padding": "4px", "maxWidth": "100%", "overflowX": "auto", "overflowY": "hidden", "whiteSpace": "nowrap"}}>
              <Link className="btn btn-sm button-theme rounded px-4 mr-3 mt-2 text-white" style={{ background: (listType === 1) ? "#336666" : "#082e39" }} to={'../booking-list/active'} >All</Link>
              <Link className="btn btn-sm button-theme rounded px-3 mr-3 mt-2 text-white" style={{ background: (listType === 4) ? "#336666" : "#082e39" }} to={'../booking-list/planned'} >Planned</Link>
              <Link className="btn btn-sm button-theme rounded px-3 mr-3 mt-2 text-white" style={{ background: (listType === 3) ? "#336666" : "#082e39" }} to={'../booking-list/cancelled'} >Cancelled</Link>
              <Link className="btn btn-sm button-theme rounded px-3 mr-3 mt-2 text-white" style={{ background: (listType === 2) ? "#336666" : "#082e39" }} to={'../booking-list/completed'} >Completed</Link>
            </div>
          </div>
        </div>

        <div className="row pb-3">
          <div className="col-md-12 booking-table">
            <div className="responsive-table booking-table">
              <BootstrapTable
                noDataIndication={() => { return 'No records to display'; }}
                ref={tableRef}
                remote
                keyField="tripNumber"
                data={data}
                columns={setColumns}
                striped
                hover
                condensed

                filter={filterFactory()}
                pagination={paginationFactory({
                  page, sizePerPage, totalSize
                })}
                cellEdit={cellEditFactory(cellEditProps)}
                onTableChange={onTableChange}
              />
            </div>
            <div className="fs-12">
              {/* {"Legends: ST -> Single Trip / RT-> Return Trip."} */}

            </div>
          </div>
        </div>

      </div>
      {
        selectedTrips.length > 0 ?
          <Modal
            show={showCancelModal}
            onHide={closeModal}
            backdrop="static"
            keyboard={false}
            dialogClassName="modal-booking-popup"
          >
            <Modal.Header closeButton>
              <Modal.Title>Transit Cancelled!</Modal.Title>
            </Modal.Header>
            <Modal.Body >
              <div className="bt-data-table-custom">
                <div className="col-md-12 booking-table">
                  <div className="responsive-table">

                    <Form>
                      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <BootstrapTable
                          key="1"
                          keyField="id"
                          data={selectedTrips}
                          columns={getColums()}
                        />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Transit Cancellation Reason</Form.Label>

                        <Form.Control
                          name="remarks"
                          required
                          onChange={handleRemarksChange}
                          value={selectedTrips[0].remarks}

                          as="textarea" rows={3} />
                      </Form.Group>
                    </Form>
                  </div> </div></div>
            </Modal.Body>
            <Modal.Footer>

              <Button variant="primary" disabled={!selectedTrips[0].remarks} onClick={() => confirmCancelModal()}>Submit</Button>
            </Modal.Footer>
          </Modal>
          : ""}

      <Modal
        show={showCompletedModal}
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
        dialogClassName="modal-booking-popup"
      >
        <Modal.Header closeButton>
          <Modal.Title>Transit Complete!</Modal.Title>
        </Modal.Header>
        <Modal.Body >
          <div className="bt-data-table-custom">
            <div className="col-md-12 booking-table">
              <div className="responsive-table">

                <BootstrapTable
                  key="1"
                  keyField="id"
                  data={selectedTrips}
                  columns={getColums()}
                />

              </div> </div></div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Close
          </Button>
          <Button variant="primary" onClick={() => confirmCompletedModal()}>Submit</Button>
        </Modal.Footer>
      </Modal>

      <Modal show={show.modals} onHide={handleClose} className="bookingmodel">
        <Modal.Body>
          <table className="table table-bordered">
            <thead className="thead-light">
              {
                cols.map((perm: any, key) => {
                  if (perm) {
                    if (perm.text != "No." && perm.text != "Trip No.")
                      return (<tr style={{ 'border': 'solid white 10px' }}>
                        <StyledTh
                          id={key}
                          key={key}
                          draggable
                          onDragStart={handleDragStart}
                          onDragOver={handleDragOver}
                          onDrop={handleOnDrop}
                          onDragEnter={handleDragEnter}
                          dragOver={perm.text === dragOver}
                        >
                          <th style={{ 'borderRadius': '50px', 'width': '250px', 'borderColor': '#e9ecef', 'paddingTop': '0px', 'paddingBottom': '10px' }}>{perm.text}
                            <Form className="align-right" style={{ 'textAlign': 'right', 'marginTop': '-18px' }}>
                              <Form.Check
                                type="switch"
                                value={perm.text}
                                defaultChecked={perm.hide}
                                id="custom-switch"
                                onChange={setColumn}
                              />
                            </Form>
                          </th>
                        </StyledTh>
                      </tr>)
                  }
                })
              }
            </thead>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showshare.modals} onHide={handleCloseShare} style={{'maxWidth': '400px !important'}} size="sm" className="sharingform">
          <Modal.Header closeButton>
              <Modal.Title><i className="ti-sharethis menu-icon ml-1"> Share Location </i></Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Row>
                  <div className='col-md-1'></div>
                  <div className='col-md-11'>
                      <Form onSubmit={(e) => onClickButton(e, data)}>
                          <Row>
                              <div className="col-md-12">
                                  <Form.Group className="mb-3 mt-3"  controlId="formBasicEmail">
                                          {inputList.map((x, i) => {
                                              return (
                                                  <Row> 
                                                      <Form.Control className='col-sm-8 mb-3' type="email" name="email" onBlur={(e) => handleChangemail(e)} placeholder="Enter Email Id" required/> 
                                                      <div className='col-sm-4 mb-3'>
                                                          {inputList.length !== 1 && <button className="btn btn-sm button-theme btn-rounded text-white mt-1" onClick={(e) => handleRemoveClick(e,i)}><i className="ti-minus menu-icon"></i></button>}
                                                          {inputList.length - 1 === i && <button className="btn btn-sm button-theme btn-rounded text-white mt-1 ml-1" onClick={handleAddClick}><i className="ti-plus menu-icon"></i></button>}
                                                      </div>
                                                  </Row>
                                              );
                                          })}
                                  </Form.Group>
                              </div>
                              <div className="pt-2">
                                  <Button className="btn btn-sm button-theme btn-rounded text-white sub-button" type="submit">Send</Button>
                              </div>
                          </Row>
                      </Form>
                  </div>
              </Row>
          </Modal.Body> 
      </Modal>
      <Modal show={showmodel.handleShowicon} onHide={handleCloseicon} size="sm" className="sharingform">
          {/* <Modal.Header closeButton>
              <Modal.Title><i className="ti-sharethis menu-icon ml-1"> Share Loaction </i></Modal.Title>
          </Modal.Header> */}
          <Modal.Body>
              <Row>
                  <div className='col-md-1'></div>
                  <div className='col-md-11'>
                      <Form>
                          <Row>
                              <div className="col-md-12 text-center">
                                  <Button className="btn btn-sm button-theme btn-rounded text-uppercase text-white mr-1" style={{height: '60px', 'width':'60px', 'borderRadius': '5px'}} onClick={() => handleShowShare('Email')}><i className="ti-email menu-icon" style={{'fontSize': '30px'}}></i></Button>
                              </div>
                          </Row>
                      </Form>
                  </div>
              </Row>
          </Modal.Body> 
      </Modal>
    </div>
  );


};

const StyledTh = styled.th`
  white-space: nowrap;
  color: #716f88;
  letter-spacing: 1.5px;
  font-weight: 600;
  font-size: 14px;
  text-align: left;
  text-transform: capitalize;
  vertical-align: middle;
  padding: 20px;
  border: 0px solid #fff;
  text-transform: uppercase;
  border-left: ${({ dragOver }) => dragOver && "5px solid red"};
`;