
import * as React from "react";
import { connect } from "react-redux";
import * as yup from 'yup';
import { IAppState, IBookingModel } from "../../../models";
import { IGenericReducerState } from "../../../utilities";
import { AppContainer } from "../../app.container";
import { getBooking, getConfigSettings, SaveBooking, updateBooking, updateBookingProperties } from "../../../redux/actions/booking.action";
import { getAllHitTripsById} from "../../../redux/actions/allactivetrips.action";
import { Booking } from "./booking";
import { defaultBookingValue, defaultTripValue, ILocation, ITrip } from "../../../models/booking.model";
import { IAllHitListModel} from "../../../models/allactivetrips.model";
import { IUserModel } from "../../../models/user.model";
import { getIn } from "formik";
import { getTemplateSettings } from "../../../redux/actions/template-settings.action";
import { ITemplateSettingModel } from "../../../models/template-setting.model";
import { alphanumericValidate, combinationAlphanumaricValidate, gstNumValidate, onlyLettersValidate, onlyNumberValidate } from "../../../utilities/regex.util";
import moment from "moment";
import { toast } from "react-toastify";

class BookingContainer extends AppContainer<IConnectState> {
  private formSchema: any;
  private fromAddress: any;
  private toAddress: any;
  private fromField: any;
  private toField: any;
  private bookingId: string = "";
  private tripId: string = "";
  disableForm = false;
  isSubmitted = false;
  fromPlaceId: string = "";
  toPlaceId: string = "";
  pingIntervalsList: any[];
  distance: number = 0;
  isEditPage: boolean = false;
  public constructor(props) {
    super(props);
    // this.getSettings();
    this.onSubmitClicked = this.onSubmitClicked.bind(this);
    this.isShowField = this.isShowField.bind(this);
    this.submit = this.submit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.loadSchema = this.loadSchema.bind(this);
    this.fromPlaceSelected = this.fromPlaceSelected.bind(this);
    this.toPlaceSelected = this.toPlaceSelected.bind(this);
    this.isInvalidField = this.isInvalidField.bind(this);
    this.getInvalidText = this.getInvalidText.bind(this);
    this.manageTargetValue = this.manageTargetValue.bind(this);
    this.isValidEndTime = this.isValidEndTime.bind(this);
    this.isValidEndDate = this.isValidEndDate.bind(this);
    this.isReturnDatesInvalid = this.isReturnDatesInvalid.bind(this);
    this.bookingId = this.props.match.params["id"]
    this.tripId = this.props.match.params["tripid"]
    this.disableForm = this.props.match.path.includes("view");
    this.pingIntervalsList = [];
    if (this.bookingId) {
      this.isEditPage = true;
    }
  }



  public isRequiredField(fieldName, objectName): boolean {
    const templateSetting = { ...this.props.templateSettingState.Payload };
    const obj = templateSetting[objectName] as any[];
    if (!obj) {
      return false;
    }
    const field = obj.find(x => x.fieldName === fieldName);
    if (!field) {
      return false;
    }
    return (field.isRequired && field.isShow);
  }

  public isShowField(fieldName, objectName): boolean {
    const templateSetting = { ...this.props.templateSettingState.Payload };
    const obj = templateSetting[objectName] as any[];
    if (!obj) {
      return false;
    }
    const field = obj.find(x => x.fieldName === fieldName);
    if (!field) {
      return false;
    }
    return field.isShow;
  }

  onSubmitClicked() {
    this.isSubmitted = true;
  }

  async loadBooking() {
    await this.props.dispatch(getBooking(this.bookingId));
    var tripId = {
      "tripIds": [this.tripId]
    }
    await this.props.dispatch(getAllHitTripsById(tripId));
    this.props.bookingState.Payload.from_autocomplete = this.bindAddress(this.props.bookingState.Payload.fromLocation.addressLine1, this.props.bookingState.Payload.fromLocation.addressLine2, this.props.bookingState.Payload.fromLocation.city, this.props.bookingState.Payload.fromLocation.state);
    this.props.bookingState.Payload.to_autocomplete = this.bindAddress(this.props.bookingState.Payload.toLocation.addressLine1, this.props.bookingState.Payload.toLocation.addressLine2, this.props.bookingState.Payload.toLocation.city, this.props.bookingState.Payload.toLocation.state);
    this.props.dispatch(updateBookingProperties(this.props.bookingState.Payload));
  }
  loadSchema() {

    return (this.formSchema = yup.object().shape({

      from_autocomplete: !!this.bookingId ? yup.string() : yup.string().required("Enter From Location"),
      to_autocomplete: !!this.bookingId ? yup.string() : yup.string().required("Enter To Location"),
      tripType: yup.string().required("Trip type is required"),

      additionalDetails: yup.object().shape({
        email: this.isRequiredField('email', 'additionalDetailsFields') ? yup.string().required("Enter email id").email("Enter valid email Id") : yup.string().email("Enter valid email Id"),
        invoiceNumber: this.isRequiredField('invoiceNumber', 'additionalDetailsFields') ? yup.string().required("Enter invoice No.") : yup.string(),
        materialType: this.isRequiredField('materialType', 'additionalDetailsFields') ? yup.string().required("Enter material type") : yup.string(),
        mobileNumber: this.isRequiredField('mobileNumber', 'additionalDetailsFields') ? yup.string().required("Enter mobile No.").matches(onlyNumberValidate, "Enter valid mobile no.") : yup.string().matches(onlyNumberValidate, "Enter valid mobile no."),
        shipmentNumber: this.isRequiredField('shipmentNumber', 'additionalDetailsFields') ? yup.string().required("Enter shipment No.") : yup.string(),
      }),

      movementDetails: yup.object().shape({
        additionalInformation: this.isRequiredField('additionalInformation', 'movementDetailsFields') ? yup.string().required("Enter invoice No.") : yup.string(),
        consigneeBuyerAddress: this.isRequiredField('consigneeBuyerAddress', 'movementDetailsFields') ? yup.string().required("Enter address") : yup.string(),
        consigneeBuyerGstNumber: this.isRequiredField('consigneeBuyerGstNumber', 'movementDetailsFields') ? yup.string().required("Enter GST No.").matches(gstNumValidate, "Enter valid GST NO.").length(15, "Enter valid GST NO.") : yup.string().matches(gstNumValidate, "Enter valid GST NO.").length(15, "Enter valid GST NO."),
        consigneeBuyerName: this.isRequiredField('consigneeBuyerName', 'movementDetailsFields') ? yup.string().required("Enter name") : yup.string(),
        consignorSellerAddress: this.isRequiredField('consignorSellerAddress', 'movementDetailsFields') ? yup.string().required("Enter address") : yup.string(),
        consignorSellerGstNumber: this.isRequiredField('consignorSellerGstNumber', 'movementDetailsFields') ? yup.string().required("Enter GST No.").matches(gstNumValidate, "Enter valid GST NO.").length(15, "Enter valid GST NO.") : yup.string().matches(gstNumValidate, "Enter valid GST NO.").length(15, "Enter valid GST NO."),
        consignorSellerName: this.isRequiredField('consignorSellerName', 'movementDetailsFields') ? yup.string().required("Enter name") : yup.string(),
        cutOffDate: this.isRequiredField('cutOffDate', 'movementDetailsFields') ? yup.string().required("Enter cotoff date") : yup.date(),
        dispatchDate: this.isRequiredField('dispatchDate', 'movementDetailsFields') ? yup.string().required("Enter dispatch date") : yup.date(),
        doValidityDate: this.isRequiredField('doValidityDate', 'movementDetailsFields') ? yup.string().required("do validity date") : yup.date(),
        emptyYardGateCfs: this.isRequiredField('emptyYardGateCfs', 'movementDetailsFields') ? yup.string().required("Enter Address") : yup.string(),
        jobNumber: this.isRequiredField('jobNumber', 'movementDetailsFields') ? yup.string().required("Enter job No.") : yup.string(),
        lcDate: this.isRequiredField('lcDate', 'movementDetailsFields') ? yup.string().required("Enter lc date") : yup.date(),
        lcNumber: this.isRequiredField('lcNumber', 'movementDetailsFields') ? yup.string().required("Enter lc No.") : yup.string(),
        movementType: this.isRequiredField('movementType', 'movementDetailsFields') ? yup.string().required("Select moment type") : yup.string(),
        packages: this.isRequiredField('packages', 'movementDetailsFields') ? yup.string().required("Enter packages") : yup.string(),
        pickupDate: this.isRequiredField('pickupDate', 'movementDetailsFields') ? yup.string().required("Enter pickup date") : yup.date(),
        pickupYard: this.isRequiredField('pickupYard', 'movementDetailsFields') ? yup.string().required("Enter pickup yard") : yup.string(),
        port: this.isRequiredField('port', 'movementDetailsFields') ? yup.string().required("Enter port") : yup.string(),
        shippingLine: this.isRequiredField('shippingLine', 'movementDetailsFields') ? yup.string().required("Enter shipping line") : yup.string(),
        stuffingDate: this.isRequiredField('stuffingDate', 'movementDetailsFields') ? yup.string().required("Enter stuffing date") : yup.date(),
        validaityDate: this.isRequiredField('validaityDate', 'movementDetailsFields') ? yup.string().required("Enter validaity date") : yup.date(),
      }),

      trips: yup.array().of(
        yup.object().shape({
          customerName: yup.string().required("Enter Customer Name").matches(onlyLettersValidate, "Enter valid Name"),
          vehicleNumber: yup.string().required("Enter Vehicle No.").matches(alphanumericValidate, "Enter valid vehicle no.").matches(combinationAlphanumaricValidate, "Enter valid vehicle no."),
          driverMobileNumber: yup.string().required("Enter Driver No.").matches(onlyNumberValidate, "Enter valid mobile no.").min(10, "Enter valid mobile no."),
          transporterName: yup
            .string()
            .required("Enter Transporter Name").matches(onlyLettersValidate, "Enter valid Name"),
          transitStartDate: yup.date().required("Enter Transit Start Date").nullable(),
          transitEstimatedEndDate: yup.string().nullable().transform((curr, orig) => orig === '' ? null : curr).required("Enter Transit End Date"),
          returnStartDate: yup.string().when('tripType', { is: 'RETURN_TRIP', then: yup.string().required('Enter Return Start date'), otherwise: yup.string() }),
          returnEndDate: yup.string().when('tripType', { is: 'RETURN_TRIP', then: yup.string().required('Enter Return End date'), otherwise: yup.string() }),
          // returnStartDate: yup.string().when('tripType', { is: 'RETURN_TRIP', then: yup.string().required('Enter Return Start date'), otherwise: yup.string() }),
          // returnEndDate: yup.string().when('tripType', { is: 'RETURN_TRIP', then: yup.string().required('Enter Return End date'), otherwise: yup.string() }),

          podUploadURL: yup.string(),
          buying: this.isRequiredField('buying', 'tripFields') ? yup.number().required('Enter buying') : yup.number(),
          cargoWeight: this.isRequiredField('cargoWeight', 'tripFields') ? yup.string().required('Enter cargo weight') : yup.string(),
          containerNumber: this.isRequiredField('containerNumber', 'tripFields') ? yup.string().required("Enter container no.") : yup.string().matches(alphanumericValidate, "Enter valid container no."),
          containerSize: this.isRequiredField('containerSize', 'tripFields') ? yup.string().required('Enter container size') : yup.string(),
          customerAdvance: this.isRequiredField('customerAdvance', 'tripFields') ? yup.string().required('Enter customer advance') : yup.string(),
          pingInterval: this.isRequiredField('pingInterval', 'tripFields') ? yup.string().required("Select ping interval") : yup.string(),
          selling: this.isRequiredField('selling', 'tripFields') ? yup.number().required('Enter selling') : yup.number(),
          sealNumber: this.isRequiredField('sealNumber', 'tripFields') ? yup.string().required('Enter seal No.') : yup.string(),
          // uploadLRDocumentURL: this.isRequiredField('uploadLRDocumentURL','tripFields') ? yup.string().required('Enter document url.') : yup.string(),
          lrNumber: this.isRequiredField('lrNumber', 'tripFields') ? yup.string().required('Enter LR Number.') : yup.string(),
          // transitActualEndDate: this.isRequiredField('transitActualEndDate','tripFields') ? yup.string().required('Enter Transit Actual End Date.') : yup.string(),
          transporterAdvance: this.isRequiredField('transporterAdvance', 'tripFields') ? yup.string().required('Enter Transporter Advance') : yup.string(),
        })
      ),
    }));
  }

  isInvalidField(fromProps, name): boolean {
    return getIn(fromProps.touched, name) && getIn(fromProps.errors, name);
  }

  getInvalidText(fromProps, name) {
    return this.isInvalidField(fromProps, name)
      ? getIn(fromProps.errors, name)
      : null;
  }

  public handleChange(e, datetimfieldName = '') {

    if (datetimfieldName) {
      const d: Date = e;
      if (d.getHours() === 0 && d.getMinutes() === 0 && d.toDateString() === new Date().toDateString()) {
        d.setHours(new Date().getHours() + 1);
        e = d;
      }
      this.manageTargetValue(datetimfieldName, e);
    } else if (e.target) {

      const { name, value } = e.target;
      const onlyLettersControls = ['customerName', 'transporterName'];
      const onlyNumberControls = ['mobileNumber', 'driverMobileNumber'];
      const alphaNumericControls = ['vehicleNumber', 'containerNumber'];
      if (value) {
        if (onlyLettersControls.some(x => name.includes(x))) {
          if (!onlyLettersValidate.test(value)) {
            return false;
          }
        }

        if (onlyNumberControls.some(x => name.includes(x))) {
          if (!onlyNumberValidate.test(value)) {
            return false;
          }
        }

        if (alphaNumericControls.some(x => name.includes(x))) {
          if (!alphanumericValidate.test(value)) {
            return false;
          }
        }
      }

      this.manageTargetValue(name, value);
    }



  }
  manageTargetValue(name: any, value: any) {
    if (name === 'from_autocomplete') {
      this.props.bookingState.Payload.fromLocation.latitude = 0;
      this.props.bookingState.Payload.fromLocation.longitude = 0;
    }
    else if (name === 'to_autocomplete') {
      this.props.bookingState.Payload.toLocation.latitude = 0;
      this.props.bookingState.Payload.toLocation.longitude = 0;
    }
    var propArray = name.split(".");
    if (propArray.length === 1) {
      this.props.bookingState.Payload[name] = value;
      this.props.dispatch(updateBookingProperties(this.props.bookingState.Payload));
    }
    if (propArray.length > 1) {
      let rootProp = propArray[0];
      // let deepProp = "";
      if (rootProp.indexOf("trips[") > -1) {
        let tripIndex = parseInt(rootProp.replace("trips[", "").replace("]", ""));

        this.props.bookingState.Payload.trips[tripIndex][propArray[1]] = value;

      } else {
        switch (rootProp) {
          case "additionalDetails":
            this.props.bookingState.Payload.additionalDetails[
              propArray[propArray.length - 1]
            ] = value;
            break;

          case "fromLocation":
            this.props.bookingState.Payload.fromLocation[
              propArray[propArray.length - 1]
            ] = value;
            break;

          case "movementDetails":
            this.props.bookingState.Payload.movementDetails[
              propArray[propArray.length - 1]
            ] = value;
            break;

          case "toLocation":
            this.props.bookingState.Payload.toLocation[
              propArray[propArray.length - 1]
            ] = value;
            break;
        }
      }

      this.props.dispatch(updateBookingProperties(this.props.bookingState.Payload));

      if (this.isSubmitted) {
        const validateBtn = document.getElementById('validatebookingForm') as HTMLElement;
        if (validateBtn)
          validateBtn.click();
      }

    }

    if (this.props.bookingState.Payload && this.props.bookingState.Payload.tripType === "RETURN_TRIP") {
      this.isSubmitted = false;
    }

  }

  setDistance() {
    if (this.fromPlaceId && this.toPlaceId) {

      const directionsService = new google.maps.DirectionsService();

      directionsService.route(
        {
          origin: { placeId: this.fromPlaceId },
          destination: { placeId: this.toPlaceId },
          travelMode: google.maps.TravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.METRIC
        },
        (response, status) => {
          if (status === "OK") {
            if (response?.routes && response?.routes.length > 0 && response?.routes[0].legs && response?.routes[0].legs.length > 0 && response?.routes[0].legs && response?.routes[0].legs[0].distance) {
              this.distance = response?.routes[0].legs && response?.routes[0].legs[0].distance.value / 1000;
            }
          }
        }
      );
    }
  }




  bindAddress(...commaSepString): string {

    let result = "";
    commaSepString.forEach((e) => {
      if (e) result = result ? result + " " + e : result + e;
    });
    return result;
  }

  public submit(values: any) {

    this.isSubmitted = true;

    if (this.props.bookingState.Payload.trips.length > 1) {
      const trips = [...this.props.bookingState.Payload.trips];
      const hasDuplicateVehicleNumOrDriverNum = trips.some((x) =>
        (trips.filter(f => f.driverMobileNumber === x.driverMobileNumber).length > 1)
        || (trips.filter(f => f.vehicleNumber === x.vehicleNumber).length > 1)
      );
      if (hasDuplicateVehicleNumOrDriverNum) {
        toast.error('Driver phone Or vehicle no. must be unique for all trips', {
          toastId: 'duplicateValue', autoClose: 5000
        });
        return false;
      }
    }

    if (!this.props.bookingState.Payload.toLocation || !this.props.bookingState.Payload.toLocation.latitude || !this.props.bookingState.Payload.toLocation.longitude
      || !this.props.bookingState.Payload.fromLocation || !this.props.bookingState.Payload.fromLocation.latitude || !this.props.bookingState.Payload.fromLocation.longitude) {
      toast.error('Please select location from provided locations in the location search list', {
        toastId: 'locationError', autoClose: 10000
      });
      return false;
    }

    this.props.bookingState.Payload.tripType = values.tripType;
    this.props.bookingState.Payload.trips = this.props.bookingState.Payload.trips.map(m => {
      m.fromLocation = this.props.bookingState.Payload.fromLocation;
      m.toLocation = this.props.bookingState.Payload.toLocation;
      m.distance = this.distance ? this.distance : m.distance;
      return m;
    });

    // if (this.props.bookingState.Payload.movementDetails.lcDate) {
    //   this.props.bookingState.Payload.movementDetails.lcDate = new Date(this.props.bookingState.Payload.movementDetails.lcDate);
    // }

    if (this.props.bookingState.Payload.id !== this.bookingId) {

      if (this.props.bookingState.Payload.tripType === "RETURN_TRIP") {
        
        let singleTrips = JSON.parse(JSON.stringify([...this.props.bookingState.Payload.trips]));
        
        // const returnTrips = singleTrips.map(m => {

        //   const from = { ...this.props.bookingState.Payload.fromLocation };
        //   const to = { ...this.props.bookingState.Payload.toLocation };
        //   m.fromLocation = to;
        //   m.toLocation = from;
        //   m.transitStartDate = m.returnStartDate;
        //   m.transitActualEndDate = m.returnEndDate;
        //   m.transitEstimatedEndDate = m.returnEndDate;
        //   return m;
        // });

        // this.props.bookingState.Payload.trips = [...this.props.bookingState.Payload.trips, ...returnTrips];

        // console.log('first', this.props.bookingState.Payload.trips);

        this.props.bookingState.Payload.trips = this.props.bookingState.Payload.trips.map(m => {
          m.returnStartDate = "";
          m.returnEndDate = "";
          return m;
        })
        console.log('first', this.props.bookingState.Payload.trips);
      }
      this.props.dispatch(SaveBooking(this.props.bookingState.Payload));
    } else {
      this.props.dispatch(updateBooking(this.props.bookingState.Payload));
    }
  }

  public fromPlaceSelected(values: any) {
    const place: any = this.fromAddress.getPlace();
    this.fromPlaceId = place.place_id;
    this.setDistance();
    let fromlocation: ILocation = {
      addressLine1: "",
      addressLine2: "",
      pinCode: "",
      country: "",
      state: "",
      city: "",
    } as ILocation;
    if (!!place) {

      for (const component of place.address_components as google.maps.GeocoderAddressComponent[]) {
        // @ts-ignore remove once typings fixed
        const componentType = component.types[0];

        switch (componentType) {
          case "street_number": {
            fromlocation.addressLine1 = `${component.long_name} ${fromlocation.addressLine1}`;
            break;
          }

          case "route": {
            fromlocation.addressLine1 += component.long_name;
            break;
          }

          case "postal_code": {
            fromlocation.pinCode = component.long_name;
            break;
          }
          case "locality":
            fromlocation.city = component.long_name;
            break;

          case "administrative_area_level_1": {
            fromlocation.state = component.long_name;
            break;
          }

          case "country":
            fromlocation.country = component.long_name;
            break;
        }
      }

      fromlocation.latitude = place.geometry.location.lat();
      fromlocation.longitude = place.geometry.location.lng();

      this.props.bookingState.Payload.from_autocomplete = this.fromField.value;
      this.props.bookingState.Payload.fromLocation = { ...fromlocation };
      this.props.dispatch(updateBookingProperties(this.props.bookingState.Payload));
    }
  }

  componentWillUnmount() {

    this.resetForm();

  }

  public addTrip() {

    const lastTrpDetails = JSON.parse(JSON.stringify({ ...this.props.bookingState.Payload.trips[this.props.bookingState.Payload.trips.length - 1] })) as ITrip;

    const defaultTripFormValue = { ...defaultTripValue };
    if (lastTrpDetails.transitStartDate)
      defaultTripFormValue.transitStartDate = new Date(lastTrpDetails.transitStartDate);
    if (lastTrpDetails.transitEstimatedEndDate)
      defaultTripFormValue.transitEstimatedEndDate = new Date(lastTrpDetails.transitEstimatedEndDate);
    defaultTripFormValue.customerName = lastTrpDetails.customerName;
    defaultTripFormValue.pingInterval = lastTrpDetails.pingInterval;
    this.props.bookingState.Payload.trips.push({ ...defaultTripFormValue });
    this.props.dispatch(updateBookingProperties(this.props.bookingState.Payload));
  }
  public removeTrip(index) {
    this.props.bookingState.Payload.trips.splice(index, 1);
    this.props.dispatch(updateBookingProperties(this.props.bookingState.Payload));
  }
  public toPlaceSelected(values: any) {

    const place: any = this.toAddress.getPlace();
    this.toPlaceId = place.place_id;
    this.setDistance();
    let toLocation: ILocation = {} as ILocation;
    if (!!place) {
      for (const component of place.address_components as google.maps.GeocoderAddressComponent[]) {
        // @ts-ignore remove once typings fixed


        const componentType = component.types[0];

        switch (componentType) {
          case "street_number": {
            toLocation.addressLine1 = component.long_name;
            break;
          }

          case "sublocality_level_1": {
            toLocation.addressLine1 = toLocation.addressLine1 ? toLocation.addressLine1 + "," + component.long_name : component.long_name;
            break;
          }

          case "sublocality_level_2": {
            toLocation.addressLine2 = component.long_name;
            break;
          }

          case "route": {
            toLocation.addressLine2 = toLocation.addressLine2 ? toLocation.addressLine2 + "," + component.long_name : component.long_name;
            break;
          }




          case "postal_code": {
            toLocation.pinCode = component.long_name;
            break;
          }
          case "locality":
            toLocation.city = component.long_name;
            break;

          case "administrative_area_level_1": {
            toLocation.state = component.long_name;
            break;
          }

          case "country":
            toLocation.country = component.long_name;
            break;
        }
      }

      toLocation.latitude = place.geometry.location.lat();
      toLocation.longitude = place.geometry.location.lng();

      this.props.bookingState.Payload.to_autocomplete = this.toField.value;
      this.props.bookingState.Payload.toLocation = { ...toLocation };
    }
    this.props.dispatch(updateBookingProperties(this.props.bookingState.Payload));
  }

  public async componentWillMount() {
    await getConfigSettings().then(res => {
      if (res && res.data) {
        this.pingIntervalsList = res.data.data.pingIntervalTypes.map(m => {
          m.label = m.value.toLowerCase().split('_').map(t => {
            t = t.charAt(0).toUpperCase() + t.slice(1);
            return t;
          }).join(' ');
          return m;
        });

      }

      const user = this.props.userState.Payload as any;
      const defaultPingInterval = user.companyDto.defaultPingIntervalType;
      if (defaultPingInterval && this.pingIntervalsList.some(x => x.value === defaultPingInterval)) {
        this.pingIntervalsList = this.pingIntervalsList.filter(x => x.value === defaultPingInterval);
        if (this.props.bookingState.Payload.trips && this.props.bookingState.Payload.trips.length) {
          this.props.bookingState.Payload.trips = this.props.bookingState.Payload.trips.map(m => {
            m.pingInterval = defaultPingInterval;
            return m;
          })
        }
      }
    });

    await this.props.dispatch(getTemplateSettings());

    this.setState({ schema: this.loadSchema() });
  }


  isValidEndDate = (date: Date, tripIndex = 0, checkFieldName = '') => {
    let previousDay = (moment().subtract(1, "days")).toDate();
    let lastDay = (moment().add(10, "days")).toDate();
    if (tripIndex !== undefined && checkFieldName && checkFieldName !== undefined && this.props.bookingState.Payload && this.props.bookingState.Payload.trips && this.props.bookingState.Payload.trips.length) {
      const startDate = this.props.bookingState.Payload.trips[tripIndex][checkFieldName];
      if (startDate) {
        previousDay = (moment(startDate).subtract(1, "days")).toDate();
        lastDay = (moment(startDate).add(10, "days")).toDate();
      }
    }

    return date.getTime() > previousDay.getTime() && date.getTime() < lastDay.getTime();
  };
  isValidEndTime = (date: Date) => {

    return date.getTime() > new Date().getTime();
  };

  public componentDidMount() {

    this.fromField = document.querySelector(
      "[name=from_autocomplete]"
    ) as HTMLInputElement;
    this.toField = document.querySelector(
      "[name=to_autocomplete]"
    ) as HTMLInputElement;
    // this.fromField = document.querySelector("#from_autocomplete") as HTMLInputElement;
    // this.toField = document.querySelector("#to_autocomplete") as HTMLInputElement;
    setTimeout(() => {
      this.fromAddress = new google.maps.places.Autocomplete(this.fromField, {
        componentRestrictions: { country: ["in"] },
        fields: ["address_components", "geometry", "place_id"],
        // types: ["address"],
      });
      this.toAddress = new google.maps.places.Autocomplete(this.toField, {
        componentRestrictions: { country: ["in"] },
        fields: ["address_components", "geometry", "place_id"],
        // types: ["address"],
      });
      this.fromAddress.addListener("place_changed", this.fromPlaceSelected);
      this.toAddress.addListener("place_changed", this.toPlaceSelected);
    }, 700);
    if (!!this.bookingId) {
      this.loadBooking();
    }
  }


  isReturnDatesInvalid(index, fieldname): boolean {
    if (this.props.bookingState.Payload && this.isSubmitted && this.props.bookingState.Payload.tripType === "RETURN_TRIP" && this.props.bookingState.Payload.trips && this.props.bookingState.Payload.trips.length) {
      const t = this.props.bookingState.Payload.trips[index];
      if (!t) {
        return false;
      }
      return t[fieldname] ? false : true;
    } else {
      return false;
    }

  }

  resetForm() {

    let fromField = document.querySelector(
      "[name=from_autocomplete]"
    ) as HTMLInputElement;
    let toField = document.querySelector(
      "[name=to_autocomplete]"
    ) as HTMLInputElement;
    fromField.value = "";
    toField.value = "";

    let emptyFormValues = JSON.parse(JSON.stringify({ ...defaultBookingValue }));
    emptyFormValues.trips = [JSON.parse(JSON.stringify({ ...defaultTripValue }))];

    this.props.dispatch(updateBookingProperties(emptyFormValues));
    if (this.isEditPage || this.disableForm) {
      this.props.history.push("/booking-list/active")
    }
  }
  public render() {



    let model = {
      initialValues: { ...this.props.bookingState.Payload },
      tripValues: { ...this.props.allHitState.Payload },
      // defaultPingIntercal: this.props.UserList.Payload,
      schema: this.loadSchema(),
      handleChange: this.handleChange,
      handleSubmit: this.submit,
      isInvalidField: this.isInvalidField,
      getInvalidText: this.getInvalidText,
      addTrip: this.addTrip.bind(this),
      removeTrip: this.removeTrip.bind(this),
      tripModel: JSON.parse(JSON.stringify(defaultBookingValue.trips[0])),
      isShowField: this.isShowField,
      resetForm: this.resetForm.bind(this),
      disableForm: this.disableForm,
      onSubmitClicked: this.onSubmitClicked,
      pingIntervalsList: this.pingIntervalsList,
      isEditPage: this.isEditPage,
      isValidEndDate: this.isValidEndDate.bind(this),
      isValidEndTime: this.isValidEndTime.bind(this),
      isReturnDatesInvalid: this.isReturnDatesInvalid.bind(this),
    };
    // if (this.props.bookingState.Payload && this.props.bookingState.Payload.movementDetails && this.props.bookingState.Payload.movementDetails.lcDate) {

    //   this.props.bookingState.Payload.movementDetails.lcDate = moment(this.props.bookingState.Payload.movementDetails.lcDate).format('YYYY-MM-DD');
    // }Password122!
    if (this.props.bookingState.Payload && this.props.bookingState.Payload.trips && this.props.bookingState.Payload.trips.length) {

      this.props.bookingState.Payload.trips = this.props.bookingState.Payload.trips.map(m => {
        if (m.transitActualEndDate) {
          m.transitActualEndDate = new Date(m.transitActualEndDate);
        }
        if (m.transitEstimatedEndDate) {
          m.transitEstimatedEndDate = new Date(m.transitEstimatedEndDate);
        }
        if (m.transitStartDate) {
          m.transitStartDate = new Date(m.transitStartDate);
        }
        return m;
      })

      if(this.props.bookingState.Payload.movementDetails.lcDate) {
        this.props.bookingState.Payload.movementDetails.lcDate = new Date(this.props.bookingState.Payload.movementDetails.lcDate)      
      }
      if(this.props.bookingState.Payload.movementDetails.lcDate) {
        this.props.bookingState.Payload.movementDetails.pickupDate = new Date(this.props.bookingState.Payload.movementDetails.pickupDate)      
      }
      if(this.props.bookingState.Payload.movementDetails.validaityDate) {
        this.props.bookingState.Payload.movementDetails.validaityDate = new Date(this.props.bookingState.Payload.movementDetails.validaityDate)      
      }
      if(this.props.bookingState.Payload.movementDetails.dispatchDate) {
        this.props.bookingState.Payload.movementDetails.dispatchDate = new Date(this.props.bookingState.Payload.movementDetails.dispatchDate)      
      }
      if(this.props.bookingState.Payload.movementDetails.stuffingDate) {
        this.props.bookingState.Payload.movementDetails.stuffingDate = new Date(this.props.bookingState.Payload.movementDetails.stuffingDate)      
      }
      if(this.props.bookingState.Payload.movementDetails.cutOffDate) {
        this.props.bookingState.Payload.movementDetails.cutOffDate = new Date(this.props.bookingState.Payload.movementDetails.cutOffDate)      
      }
      if(this.props.bookingState.Payload.movementDetails.doValidityDate) {
        this.props.bookingState.Payload.movementDetails.doValidityDate = new Date(this.props.bookingState.Payload.movementDetails.doValidityDate)      
      }
    }
    return <Booking {...model} />;
  }

}
interface IConnectState {
  bookingState: IGenericReducerState<IBookingModel>;
  templateSettingState: IGenericReducerState<ITemplateSettingModel>;
  userState: IGenericReducerState<IUserModel>;
  allHitState: IGenericReducerState<IAllHitListModel[]>;

}

const mapStateToProps = (state: IAppState): IConnectState => ({
  bookingState: state.Booking,
  templateSettingState: state.TemplateSetting,
  userState: state.User,
  allHitState: state.AllHitTripById,
});

export default connect(mapStateToProps)(BookingContainer);