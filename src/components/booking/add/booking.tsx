import * as React from "react";
import { Button, ButtonProps, Col, Form, Row, Accordion} from "react-bootstrap";
import { FieldArray, Formik } from 'formik';
import { MomentTypeEnum } from "../../../utilities/enum";
import Datetime from "react-datetime";
import moment from "moment";
import '../booking.css'
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import MapDetails from "../../tracking/trackingmap";

export const Booking = (props: any) => { 
    let path: any[] = [];
    let hitPath: any;

    hitPath = Object.values(props.tripValues).map((hitval: any) => {
        return (hitval.allHits)
    })
    
    path = props.initialValues.trips.map((pathval: any) => {
        return (
            {
                from: { lat: parseFloat(pathval.fromLocation.latitude), lng: parseFloat(pathval.fromLocation.longitude), typeloc: 'from', city: pathval.fromLocation.city, state: pathval.fromLocation.state, pinCode: pathval.fromLocation.pinCode },
                current: { lat: parseFloat(pathval.toLocation.latitude), lng: parseFloat(pathval.toLocation.longitude), typeloc: 'current', city: pathval.toLocation.city, state: pathval.toLocation.state, pinCode: pathval.toLocation.pinCode },
                to: { lat: parseFloat(pathval.toLocation.latitude), lng: parseFloat(pathval.toLocation.longitude), typeloc: 'to', city: pathval.toLocation.city, state: pathval.toLocation.state, pinCode: pathval.toLocation.pinCode },
                id: pathval.id, vehicleNumber: pathval.vehicleNumber, driverMobileNumber: pathval.driverMobileNumber
            }
        )
    })

    // let dateRef: any[] = [];
    return (


        <Formik

            enableReinitialize={true}
            validationSchema={props.schema}
            initialValues={props.initialValues}
            validateOnChange={true}
            validateOnBlur={true}
            onSubmit={(values, actions) => {
                actions.setSubmitting(false);
                props.handleSubmit(values)

            }
            }

        >


            {fromProps => (

                <Row>
                    <Col md={12}>
                        <h3 className="font-weight-bold mb-0">
                            {props.disableForm ? 'Booking' : props.isEditPage ? 'Edit Booking' : 'Add Booking'} 
                        </h3>
                        </Col>
                    <Col md={12}>
                        

                            <Row>
                                <Col md={6}>

                                    <h5 className="movment-d-head">
                                        Movement Details
                                    </h5>
                                </Col>
                                {/* <Col md={6}>
                                    <Col className="text-right">
                                        <Button className="btn btn-sm button-theme btn-rounded text-uppercase mr-2 text-white">
                                            Download
                                        </Button>
                                        <Button className="btn btn-sm button-theme btn-rounded text-uppercase text-white">
                                            Upload Booking
                                        </Button>
                                    </Col>
                                </Col> */}
                            </Row>
                            <fieldset disabled={props.disableForm}>
                                <Form  noValidate onSubmit={fromProps.handleSubmit} autoComplete="off" className="add-book-custom"
                                onKeyDown={(e) => {
                                    if (e.key === 'Enter') {
                                        e.preventDefault();
                                    }
                                  }}
                                
                                >
                                    <Row>
                                        
                                        <Col md={11} className="pl-3 pt-5">
                                         <div className="fs-14 tttext">  Trip Type </div>
                                            <Form.Group className="trip-radio">
                                                <Form.Label className={fromProps.values.tripType === "SINGLE_TRIP" ? "trip-radio-active" : "trip-radio-inactive"}>Single Trip</Form.Label>
                                                <Form.Control type="radio"
                                                    id="SINGLE_TRIP"
                                                    name="tripType"
                                                    value="SINGLE_TRIP"
                                                    onChange={props.handleChange}
                                                    disabled={props.isEditPage}
                                                />
                                            </Form.Group>
                                  
                                     
                                      <Form.Group className="trip-radio">
                                                <Form.Label className={fromProps.values.tripType === "RETURN_TRIP" ? "trip-radio-active" : "trip-radio-inactive"}>Return Trip</Form.Label>
                                                <Form.Control type="radio"
                                                    id="RETURN_TRIP"
                                                    name="tripType"
                                                    value="RETURN_TRIP"
                                                    onChange={props.handleChange}
                                                    disabled={props.isEditPage}
                                                />
                                            </Form.Group>
                                  
                                            

                                            {/* <button type="button" className="btn btn-warning btn-rounded btn-fw btn-sm">Single Trip</button>
                                        <button type="button" className="btn btn-rounded btn-fw btn-sm btn-light">Return Trip</button> 
                                        <button type="button" className="btn btn-rounded btn-fw btn-sm btn-light">Multiple Trip</button> */}
                                        </Col>
                                    </Row>
                                    <Row className=" mt-3 mb-3">
                                        <Col md={4}>
                                            <Col className="border-r p-3">
                                                <Form.Group controlId="from_autocomplete" className="mt-3"  >
                                                    <Form.Label className="m-0 mb-1">From Location</Form.Label>
                                                    <Form.Control
                                                        placeholder="From Location"
                                                        name="from_autocomplete"
                                                        // id="from_autocomplete"
                                                        required
                                                        value={fromProps.values.from_autocomplete}
                                                        onChange={props.handleChange}
                                                        isInvalid={props.isInvalidField(fromProps, 'from_autocomplete')}
                                                        disabled={props.isEditPage}
                                                    />
                                                    <Form.Control.Feedback type="invalid">
                                                        {props.getInvalidText(fromProps, 'from_autocomplete')}
                                                    </Form.Control.Feedback>
                                                </Form.Group>


                                            </Col>
                                        </Col>
                                        <Col md={1} className=" text-center pt-5 deskarrow"><i className="ti-angle-double-right"></i></Col>
                                        <Col md={1} className=" text-center pt-3 pb-3 mobarrow"><i className="ti-angle-double-down"></i></Col>
                                        <Col md={4}>
                                            <Col className="border-r p-3">
                                                <Form.Group controlId="to_autocomplete" className="mt-3"  >
                                                    <Form.Label className="m-0 mb-1">To Location</Form.Label>
                                                    <Form.Control
                                                        placeholder="To Location"
                                                        name="to_autocomplete"
                                                        // id="to_autocomplete"
                                                        required
                                                        value={fromProps.values.to_autocomplete}
                                                        onChange={props.handleChange}
                                                        isInvalid={props.isInvalidField(fromProps, 'to_autocomplete')}
                                                        disabled={props.isEditPage}
                                                    />
                                                    <Form.Control.Feedback type="invalid">
                                                        {props.getInvalidText(fromProps, 'to_autocomplete')}
                                                    </Form.Control.Feedback>
                                                </Form.Group>


                                            </Col>
                                        </Col>
                                    </Row>
                                    <Row className=" pt-4 pb-4">
                                        <Col md={12} className=" d-flex flex-column justify-content-center">
                                            <h5>Transit Details</h5>

                                            <Col className="border ">

                                                <FieldArray
                                                    name="trips"
                                                    render={arrayHelpers => (
                                                        <div>
                                                            {fromProps.values.trips.map((trip, index) => (
                                                                <div key={index} className="mb-4 mt-4">
                                                                    <Row className={fromProps.values.trips.length > 1 && (fromProps.values.trips.length - 1) !== index ? 'trip-border pb-4' : 'pb-4'} >
                                                                        <Col md={11}>
                                                                            <Row>
                                                                                <Col md={3}>
                                                                                    <Form.Group controlId={`trips[${index}].customerName`} >
                                                                                        <Form.Label className="col-form-label">Customer* </Form.Label>
                                                                                        <Form.Control
                                                                                            placeholder="Customer"
                                                                                            name={`trips[${index}].customerName`}
                                                                                            required
                                                                                            maxLength={20}
                                                                                            value={fromProps.values.trips[index].customerName}
                                                                                            onChange={props.handleChange}
                                                                                            isInvalid={props.isInvalidField(fromProps, `trips[${index}].customerName`)}
                                                                                        />
                                                                                        <Form.Control.Feedback type="invalid">
                                                                                            {props.getInvalidText(fromProps, `trips[${index}].customerName`)}
                                                                                        </Form.Control.Feedback>
                                                                                    </Form.Group>
                                                                                </Col>

                                                                                <Col md={3}>
                                                                                    <Form.Group controlId={`trips[${index}].vehicleNumber`}>
                                                                                        <Form.Label className=" col-form-label">Vehicle No.*</Form.Label>
                                                                                        <Form.Control
                                                                                            placeholder="Vehicle No."
                                                                                            name={`trips[${index}].vehicleNumber`}
                                                                                            required
                                                                                            maxLength={10}
                                                                                            value={fromProps.values.trips[index].vehicleNumber}
                                                                                            onChange={props.handleChange}
                                                                                            isInvalid={props.isInvalidField(fromProps, `trips[${index}].vehicleNumber`)}
                                                                                        />
                                                                                        <Form.Control.Feedback type="invalid">
                                                                                            {props.getInvalidText(fromProps, `trips[${index}].vehicleNumber`)}
                                                                                        </Form.Control.Feedback>
                                                                                    </Form.Group>
                                                                                </Col>

                                                                                <Col md={3}>
                                                                                    <Form.Group controlId={`trips[${index}].driverMobileNumber`}>
                                                                                        <Form.Label className=" col-form-label">Driver No.*</Form.Label>
                                                                                        <Form.Control
                                                                                            placeholder="Driver No."
                                                                                            name={`trips[${index}].driverMobileNumber`}
                                                                                            required
                                                                                            maxLength={10}
                                                                                            value={fromProps.values.trips[index].driverMobileNumber}
                                                                                            onChange={props.handleChange}
                                                                                            isInvalid={props.isInvalidField(fromProps, `trips[${index}].driverMobileNumber`)}
                                                                                        />
                                                                                        <Form.Control.Feedback type="invalid">
                                                                                            {props.getInvalidText(fromProps, `trips[${index}].driverMobileNumber`)}
                                                                                        </Form.Control.Feedback>
                                                                                    </Form.Group>
                                                                                </Col>

                                                                                <Col md={3}>
                                                                                    <Form.Group controlId={`trips[${index}].transporterName`}>
                                                                                        <Form.Label className=" col-form-label">Transporter Name*</Form.Label>
                                                                                        <Form.Control
                                                                                            placeholder="Transporter Name"
                                                                                            name={`trips[${index}].transporterName`}
                                                                                            required
                                                                                            maxLength={20}
                                                                                            value={fromProps.values.trips[index].transporterName}
                                                                                            onChange={props.handleChange}
                                                                                            isInvalid={props.isInvalidField(fromProps, `trips[${index}].transporterName`)}
                                                                                        />
                                                                                        <Form.Control.Feedback type="invalid">
                                                                                            {props.getInvalidText(fromProps, `trips[${index}].transporterName`)}
                                                                                        </Form.Control.Feedback>
                                                                                    </Form.Group>
                                                                                </Col>

                                                                                <Col md={3} >
                                                                                    <Form.Group controlId={`trips[${index}].transitStartDate`} >
                                                                                        <Form.Label className=" col-form-label">Transit Start Date*  </Form.Label>
                                                                                        {/* 
                                                                                        <Datetime
                                                                                        ref={datePicker => {dateRef.push(datePicker)}}
                                                                                        
                                                                                        timeConstraints={{
                                                                                            hours: { min: 0, max: 23 , step:1},
                                                                                            minutes: { min: 0, max: 59 , step:30}
                                                                                          }}
                                                                                            value={fromProps.values.trips[index].transitStartDate}
                                                                                            onChange={(e) => props.handleChange(e, `trips[${index}].transitStartDate`)}
                                                                                            inputProps={{ placeholder: 'Transit Start Date', name: `trips[${index}].transitStartDate`, readOnly:!props.disableForm, disabled: props.disableForm}}
                                                                                            isValidDate={(current) => (current.isAfter(moment().subtract(1, 'day')) &&  current.isBefore(moment()))}
                                                                                            dateFormat="DD-MM-YYYY"
                                                                                            className={props.isInvalidField(fromProps, `trips[${index}].transitStartDate`) ? "is-invalid booking-add-date" : "booking-add-date"}
                                                                                        /> */}
                                                                                

                                                                                        <DatePicker
                                                                                            selected={fromProps.values.trips[index].transitStartDate}
                                                                                            onChange={(e) => props.handleChange(e, `trips[${index}].transitStartDate`)}
                                                                                            showTimeSelect
                                                                                            // excludeTimes={[
                                                                                            //     (new Date().setHours(0))
                                                                                               
                                                                                            //   ]}
                                                                                            timeIntervals={15}
                                                                                            placeholderText="Transit Start Date"
                                                                                            name={`trips[${index}].transitStartDate`}
                                                                                            id={`trips[${index}].transitStartDate`}
                                                                                            // disabledKeyboardNavigation
                                                                                            // onChangeRaw={(event) => this.updateTransistActualDate(event.target.value,row)}
                                                                                            //  shouldCloseOnSelect={false}
                                                                                            filterDate={(date) => props.isValidEndDate(date)}
                                                                                            filterTime={(date) => props.isValidEndTime(date)}
                                                                                            dateFormat="dd-MM-yy h:mm aa"
                                                                                            className={props.isInvalidField(fromProps, `trips[${index}].transitStartDate`) ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                                            isInvalid={props.isInvalidField(fromProps, `trips[${index}].transitStartDate`)}
                                                                                        // customInput={<ExampleCustomInput />}
                                                                                        />

                                                                                        <Form.Control.Feedback type="invalid" style={props.isInvalidField(fromProps, `trips[${index}].transitStartDate`) ? {display:"block"} : {display:"none"}}>
                                                                                            {props.getInvalidText(fromProps, `trips[${index}].transitStartDate`)}
                                                                                        </Form.Control.Feedback>
                                                                                    </Form.Group>
                                                                                </Col>

                                                                                <Col md={3}>
                                                                                    <Form.Group controlId={`trips[${index}].transitEstimatedEndDate`}>
                                                                                        <Form.Label className=" col-form-label">Transit End Date*</Form.Label>
                                                                                       
                                                                                        <DatePicker
                                                                                            selected={fromProps.values.trips[index].transitEstimatedEndDate}
                                                                                            onChange={(e) => props.handleChange(e, `trips[${index}].transitEstimatedEndDate`)}
                                                                                            showTimeSelect
                                                                                            timeIntervals={15}
                                                                                            placeholderText="Transit End Date"
                                                                                            // disabledKeyboardNavigation
                                                                                            // onChangeRaw={(event) => this.updateTransistActualDate(event.target.value,row)}
                                                                                            //  shouldCloseOnSelect={false}
                                                                                            filterDate={(date) => props.isValidEndDate(date,index, 'transitStartDate')}
                                                                                            filterTime={(date) => props.isValidEndTime(date)}
                                                                                            dateFormat="dd-MM-yy h:mm aa"
                                                                                            name={`trips[${index}].transitEstimatedEndDate`}
                                                                                            id={`trips[${index}].transitEstimatedEndDate`}
                                                                                            
                                                                                            className={props.isInvalidField(fromProps, `trips[${index}].transitEstimatedEndDate`) ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                                        // customInput={<ExampleCustomInput />}
                                                                                        />

                                                                                        <Form.Control.Feedback type="invalid" style={props.isInvalidField(fromProps, `trips[${index}].transitEstimatedEndDate`) ? {display:"block"} : {display:"none"}}>
                                                                                            {props.getInvalidText(fromProps, `trips[${index}].transitEstimatedEndDate`)}
                                                                                        </Form.Control.Feedback>
                                                                                    </Form.Group>
                                                                                </Col>
                                                                                           
                                                                                
                                                                                 {/* {
                                                                                    props.isShowField('uploadLRDocumentURL', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].uploadLRDocumentURL`}>
                                                                                                <Form.Label className=" col-form-label">Upload LR Document URL</Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Upload LR Document URL"
                                                                                                    name={`trips[${index}].uploadLRDocumentURL`}
                                                                                                    maxLength={15}
                                                                                                    value={fromProps.values.trips[index].uploadLRDocumentURL}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].uploadLRDocumentURL`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].uploadLRDocumentURL`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                } */}
                                                                                {
                                                                                    fromProps.values.tripType === "RETURN_TRIP" && !props.isEditPage ?
                                                                                        <>
                                                                                            {/* <Col md={3}> */}
                                                                                                {/* <Form.Group controlId={`trips[${index}].returnStartDate`}> */}
                                                                                                    {/* <Form.Label className=" col-form-label">Return Start Date*</Form.Label> */}

                                                                                                    {/* <Datetime
                                                                                                     ref={datePicker => {dateRef.push(datePicker)}}
                                                                                                     timeConstraints={{
                                                                                                        hours: { min: 0, max: 23 , step:1},
                                                                                                        minutes: { min: 0, max: 59 , step:30}
                                                                                                      }}
                                                                                                        value={fromProps.values.trips[index].returnStartDate}
                                                                                                        onChange={(e) => props.handleChange(e, `trips[${index}].returnStartDate`)}
                                                                                                        inputProps={{ placeholder: 'Transit Start Date', name: `trips[${index}].returnStartDate`, readOnly:!props.disableForm, disabled: props.disableForm }}
                                                                                                        isValidDate={(current) => current.isAfter(moment().subtract(1, 'day'))}
                                                                                                        dateFormat="DD-MM-YYYY"
                                                                                                        className={props.isInvalidField(fromProps, `trips[${index}].returnStartDate`) ? "is-invalid booking-add-date" : "booking-add-date"}
                                                                                                    /> */}
                                                                                                    {/* <DatePicker
                                                                                                        selected={fromProps.values.trips[index].returnStartDate}
                                                                                                        onChange={(e) => props.handleChange(e, `trips[${index}].returnStartDate`)}
                                                                                                        showTimeSelect
                                                                                                        timeIntervals={15}
                                                                                                        placeholderText="Return Start Date" 
                                                                                                        filterDate={(date) => props.isValidEndDate(date,index, 'returnEndDate')}
                                                                                                        filterTime={(date) => props.isValidEndTime(date)}
                                                                                                        dateFormat="dd-MM-yy h:mm aa"
                                                                                                        className={props.isReturnDatesInvalid(index,'returnStartDate') ? "is-invalid        booking-add-date form-control" : "booking-add-date form-control"}
                                                                                                    />

<div className="invalid-error-text">
                                                                                                        {props.isReturnDatesInvalid(index,'returnStartDate') ? 'Enter Return Start Date' : ''}
                                                                                                    </div> */}
                                                                                                {/* </Form.Group> */}
                                                                                            {/* </Col> */}

                                                                                            {/* <Col md={3}> */}
                                                                                                {/* <Form.Group controlId={`trips[${index}].returnEndDate`}> */}
                                                                                                    {/* <Form.Label className=" col-form-label">Return End Date*</Form.Label> */}

                                                                                                    {/* <Datetime
                                                                                                      ref={datePicker => {dateRef.push(datePicker)}}
                                                                                                     timeConstraints={{
                                                                                                        hours: { min: 0, max: 23 , step:1},
                                                                                                        minutes: { min: 0, max: 59 , step:30}
                                                                                                      }}
                                                                                                        value={fromProps.values.trips[index].returnEndDate}
                                                                                                        onChange={(e) => props.handleChange(e, `trips[${index}].returnEndDate`)}
                                                                                                        inputProps={{ placeholder: 'Transit Start Date', name: `trips[${index}].returnEndDate` , readOnly:!props.disableForm, disabled: props.disableForm}}
                                                                                                        isValidDate={(current) => current.isAfter(moment().subtract(1, 'day'))}
                                                                                                        dateFormat="DD-MM-YYYY"
                                                                                                        className={props.isInvalidField(fromProps, `trips[${index}].returnEndDate`) ? "is-invalid booking-add-date" : "booking-add-date"}
                                                                                                    /> */}
                                                                                                    {/* <DatePicker
                                                                                                        selected={fromProps.values.trips[index].returnEndDate}
                                                                                                        onChange={(e) => props.handleChange(e, `trips[${index}].returnEndDate`)}
                                                                                                        showTimeSelect
                                                                                                        timeIntervals={15}
                                                                                                        placeholderText="Return End Date"
                                                                                                         filterDate={(date) => props.isValidEndDate(date,index, 'returnStartDate')}
                                                                                                        filterTime={(date) => props.isValidEndTime(date)}
                                                                                                        dateFormat="dd-MM-yy h:mm aa"
                                                                                                        className={props.isReturnDatesInvalid(index,'returnEndDate') ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                                                    />
                                                                                                    <div className="invalid-error-text">
                                                                                                        {props.isReturnDatesInvalid(index,'returnEndDate') ? 'Enter Return Start Date' : ''}
                                                                                                    </div> */}
                                                                                                {/* </Form.Group> */}
                                                                                            {/* </Col> */}
                                                                                        </>
                                                                                        : ""
                                                                                }
{
                                                                                    props.isShowField('lrNumber', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].lrNumber`}>
                                                                                                <Form.Label className=" col-form-label">LR Number. </Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="LR Number"
                                                                                                    name={`trips[${index}].lrNumber`}
                                                                                                    maxLength={15}
                                                                                                    value={fromProps.values.trips[index].lrNumber}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].lrNumber`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].lrNumber`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }

                                                                                {
                                                                                    props.isShowField('containerNumber', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].containerNumber`}>
                                                                                                <Form.Label className=" col-form-label">Container No. </Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Container No."
                                                                                                    name={`trips[${index}].containerNumber`}
                                                                                                    maxLength={15}
                                                                                                    value={fromProps.values.trips[index].containerNumber}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].containerNumber`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].containerNumber`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }

                                                                                {
                                                                                    props.isShowField('pingInterval', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].pingInterval`}>
                                                                                                <Form.Label className=" col-form-label">Ping Interval</Form.Label>
                                                                                                <Form.Select value={fromProps.values.trips[index].pingInterval} name={`trips[${index}].pingInterval`} onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].pingInterval`)}>
                                                                                                        <option value="">Select Interval</option>
                                                                                                    {props.pingIntervalsList.map((e, key) => {
                                                                                                       return <option key={e.value} value={e.value}>{e.label}</option>;
                                                                                                    })}
                                                                                                </Form.Select>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }

{
                                                                                    props.isShowField('containerSize', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].containerSize`}>
                                                                                                <Form.Label className=" col-form-label">Container Size</Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Container Size"
                                                                                                    name={`trips[${index}].containerSize`}
                                                                                                    maxLength={15}
                                                                                                    value={fromProps.values.trips[index].containerSize}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].containerSize`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].containerSize`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }
                                                                                 {
                                                                                    props.isShowField('cargoWeight', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].cargoWeight`}>
                                                                                                <Form.Label className=" col-form-label">Cargo Weight</Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Cargo Weight"
                                                                                                    name={`trips[${index}].cargoWeight`}
                                                                                                    maxLength={15}
                                                                                                    value={fromProps.values.trips[index].cargoWeight}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].cargoWeight`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].cargoWeight`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }
                                                                                {
                                                                                    props.isShowField('podUploadURL', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].podUploadURL`}>
                                                                                                <Form.Label className=" col-form-label">Pod Upload URL</Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Pod Upload URL"
                                                                                                    name={`trips[${index}].podUploadURL`}
                                                                                                    maxLength={15}
                                                                                                    value={fromProps.values.trips[index].podUploadURL}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].podUploadURL`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].podUploadURL`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }
                                                                               
                                                                                {
                                                                                    props.isShowField('sealNumber', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].sealNumber`}>
                                                                                                <Form.Label className=" col-form-label">Seal Number</Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Seal Number"
                                                                                                    name={`trips[${index}].sealNumber`}
                                                                                                    maxLength={15}
                                                                                                    value={fromProps.values.trips[index].sealNumber}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].sealNumber`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].sealNumber`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }
                                                                                {
                                                                                    props.isShowField('transporterAdvance', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].transporterAdvance`}>
                                                                                                <Form.Label className=" col-form-label">Transporter Advance</Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Transporter Advance"
                                                                                                    name={`trips[${index}].transporterAdvance`}
                                                                                                    maxLength={15}
                                                                                                    value={fromProps.values.trips[index].transporterAdvance}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].transporterAdvance`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].transporterAdvance`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }
                                                                                {
                                                                                    props.isShowField('customerAdvance', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].customerAdvance`}>
                                                                                                <Form.Label className=" col-form-label">Customer Advance</Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Customer Advance"
                                                                                                    name={`trips[${index}].customerAdvance`}
                                                                                                  
                                                                                                    value={fromProps.values.trips[index].customerAdvance}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].customerAdvance`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].customerAdvance`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }
                                                                                {
                                                                                    props.isShowField('buying', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].buying`}>
                                                                                                <Form.Label className=" col-form-label">Buying</Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Buying"
                                                                                                    name={`trips[${index}].buying`}
                                                                                                   
                                                                                                    value={fromProps.values.trips[index].buying}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].buying`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].buying`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }
                                                                                {
                                                                                    props.isShowField('selling', 'tripFields') ?

                                                                                        <Col md={3}>
                                                                                            <Form.Group controlId={`trips[${index}].selling`}>
                                                                                                <Form.Label className=" col-form-label">Selling</Form.Label>
                                                                                                <Form.Control
                                                                                                    placeholder="Selling"
                                                                                                    name={`trips[${index}].selling`}
                                                                                                   
                                                                                                    value={fromProps.values.trips[index].selling}
                                                                                                    onChange={props.handleChange}
                                                                                                    isInvalid={props.isInvalidField(fromProps, `trips[${index}].selling`)}
                                                                                                />
                                                                                                <Form.Control.Feedback type="invalid">
                                                                                                    {props.getInvalidText(fromProps, `trips[${index}].selling`)}
                                                                                                </Form.Control.Feedback>
                                                                                            </Form.Group>
                                                                                        </Col>
                                                                                        : ""
                                                                                }
                                                                            </Row>
                                                                        </Col>
                                                                        <Col md={1}>

                                                                            {
                                                                                (index === (fromProps.values.trips.length - 1)) ?
                                                                                    (props.isEditPage && trip.id === '') ?
                                                                                        <div className="edit-bookingtrip-ar-icn">
                                                                                            <Button type="button" className="btn btn-dark btn-rounded btn-icon bookingtrip-ar-icn" onClick={() => props.removeTrip(index)}> <i className="ti-minus"></i> </Button>

                                                                                            <Button type="button" className="btn btn-dark btn-rounded btn-icon bookingtrip-ar-icn mt-2" onClick={() => props.addTrip()}> <i className="ti-plus"></i> </Button>
                                                                                        </div>
                                                                                        :
                                                                                        <div className="edit-bookingtrip-ar-icn">
                                                                                            {
                                                                                                (fromProps.values.trips.length - 1>0)?
                                                                                        <Button type="button" className="btn btn-dark btn-rounded btn-icon bookingtrip-ar-icn" onClick={() => props.removeTrip(index)}> <i className="ti-minus"></i> </Button>
                                                                                                :""
                                                                                            }
                                                                                        <Button type="button" className="btn btn-dark btn-rounded btn-icon bookingtrip-ar-icn" onClick={() => props.addTrip()}> <i className="ti-plus"></i> </Button>
                                                                                        </div>
                                                                                    :
                                                                                    (!props.isEditPage || (props.isEditPage && trip.id === '')) ?
                                                                                        <Button type="button" className="btn btn-dark btn-rounded btn-icon bookingtrip-ar-icn" onClick={() => props.removeTrip(index)}> <i className="ti-minus"></i> </Button>
                                                                                        : ""
                                                                            }

                                                                        </Col>
                                                                    </Row>
                                                                </div>
                                                            ))}


                                                        </div>
                                                    )}
                                                />

                                            </Col>

                                        </Col>
                                    </Row>
                                    <Row className=" pb-4 pt-4">
                                        <Col md={12} className=" d-flex flex-column justify-content-center">
                                            <Accordion defaultActiveKey={(props.disableForm)? "0" : "5"} className="addbooking">
                                                <Accordion.Item eventKey="0" className="accourdian-style">
                                                    <Accordion.Header>LR Details</Accordion.Header>
                                                    <Accordion.Body>
                                                        <Row>
                                                    {
                                                        props.isShowField('movementType', 'movementDetailsFields') ?
                                                            <Col md={3}>

                                                                <Form.Group controlId="movementDetails.movementType" >
                                                                    <Form.Label className=" col-form-label">Type </Form.Label>
                                                                    <Form.Select name="movementDetails.movementType"
                                                                        placeholder="Type of Moment"
                                                                        value={fromProps.values.movementDetails.movementType}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.movementType')}
                                                                    >
                                                                        <option value="">Type of Moment</option>
                                                                        <option value={MomentTypeEnum.IMPORT}>Import</option>
                                                                        <option value={MomentTypeEnum.EXPORT}>Export</option>
                                                                        <option value={MomentTypeEnum.INLAND}>Inland</option>
                                                                    </Form.Select>

                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.movementType')}
                                                                    </Form.Control.Feedback>

                                                                </Form.Group>


                                                            </Col>
                                                            : ""
                                                    }

                                                    {
                                                        props.isShowField('consignorSellerName', 'movementDetailsFields') ?

                                                            <Col md={3}>

                                                                <Form.Group controlId="movementDetails.consignorSellerName">
                                                                    <Form.Label className=" col-form-label">Consignor / Seller</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Consignor / Seller Name"
                                                                        name="movementDetails.consignorSellerName"
                                                                        maxLength={20}
                                                                        onChange={props.handleChange}
                                                                        value={fromProps.values.movementDetails.consignorSellerName}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.consignorSellerName')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.consignorSellerName')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>
                                                            </Col>
                                                            : ""


                                                    }

                                                    {
                                                        props.isShowField('consignorSellerAddress', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.consignorSellerAddress">
                                                                    <Form.Label className=" col-form-label">Consignor / Seller Address</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Consignor / Seller Add."
                                                                        name="movementDetails.consignorSellerAddress"
                                                                        maxLength={100}
                                                                        value={fromProps.values.movementDetails.consignorSellerAddress}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.consignorSellerAddress')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.consignorSellerAddress')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>


                                                            </Col>
                                                            : ""
                                                    }

                                                    {
                                                        props.isShowField('consignorSellerGstNumber', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.consignorSellerGstNumber">
                                                                    <Form.Label className=" col-form-label">Consignor / Seller GST No.</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Consignor / Seller GST No."
                                                                        name="movementDetails.consignorSellerGstNumber"
                                                                        maxLength={15}
                                                                        value={fromProps.values.movementDetails.consignorSellerGstNumber}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.consignorSellerGstNumber')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.consignorSellerGstNumber')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col>
                                                            : ""
                                                    }

                                                    {
                                                        props.isShowField('consigneeBuyerName', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.consigneeBuyerName">
                                                                    <Form.Label className=" col-form-label">Consignee / Buyer</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Consignee / Buyer Name"
                                                                        name="movementDetails.consigneeBuyerName"
                                                                        maxLength={20}
                                                                        value={fromProps.values.movementDetails.consigneeBuyerName}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.consigneeBuyerName')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.consigneeBuyerName')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col>
                                                            : ""
                                                    }

                                                    {
                                                        props.isShowField('consigneeBuyerAddress', 'movementDetailsFields') ?

                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.consigneeBuyerAddress">
                                                                    <Form.Label className=" col-form-label">Consignee / Buyer Address</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Consignee / Buyer Add."
                                                                        name="movementDetails.consigneeBuyerAddress"
                                                                        maxLength={100}
                                                                        value={fromProps.values.movementDetails.consigneeBuyerAddress}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.consigneeBuyerAddress')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.consigneeBuyerAddress')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>
                                                            </Col>
                                                            : ""
                                                    }
                                                    {
                                                        props.isShowField('consigneeBuyerGstNumber', 'movementDetailsFields') ?

                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.consigneeBuyerGstNumber">
                                                                    <Form.Label className=" col-form-label">Consignee / Buyer GST No.</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Consignee / Buyer GST No."
                                                                        name="movementDetails.consigneeBuyerGstNumber"
                                                                        maxLength={15}
                                                                        value={fromProps.values.movementDetails.consigneeBuyerGstNumber}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.consigneeBuyerGstNumber')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.consigneeBuyerGstNumber')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col>
                                                            : ""

                                                    }

                                                    {
                                                        props.isShowField('lcNumber', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.lcNumber">
                                                                    <Form.Label className=" col-form-label">LC No.</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="LC No."
                                                                        name="movementDetails.lcNumber"
                                                                        maxLength={20}
                                                                        value={fromProps.values.movementDetails.lcNumber}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.lcNumber')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.lcNumber')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col>
                                                            : ""
                                                    }
                                                    {
                                                        props.isShowField('lcDate', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.lcDate">
                                                                    <Form.Label className=" col-form-label">LC Date</Form.Label>


                                                                    {/* <Form.Control
                                                                    
                                                                        name="movementDetails.lcDate"
                                                                        type="date"
                                                                        // type="text"
                                                                        
                                                                        // onFocus= {(e) => {
                                                                        //     const t = e.currentTarget as any;
                                                                        //     t.type = "date";
                                                                        // }} 
                                                                        // onBlur= {(e) => {
                                                                        //     const t = e.currentTarget as any;
                                                                        //     if(t.value)
                                                                        //     t.type = "text";
                                                                        // }} 
                                                                        placeholder="dd/mm/yyyy"
                                                                        value={fromProps.values.movementDetails.lcDate}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.lcDate')}
                                                                    /> */}
                                                                                                                                                                                        <DatePicker
                                                                        selected={fromProps.values.movementDetails.lcDate}
                                                                        onChange={(e) => props.handleChange(e, `movementDetails.lcDate`)}
                                                                        // showTimeSelect
                                                                        // timeIntervals={15}
                                                                        placeholderText="LC Date" 
                                                                        filterDate={(date) => props.isValidEndDate(date, 'lcDate')}
                                                                        // filterTime={(date) => props.isValidEndTime(date)}
                                                                        dateFormat="dd-MM-yy"
                                                                        name={`movementDetails.lcDate`}
                                                                        id={`movementDetails.lcDate`}
                                                                        
                                                                        className={props.isInvalidField(fromProps, `movementDetails.lcDate`) ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                    /> 
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.lcDate')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>


                                                            </Col>
                                                            : ""
                                                    }
                                                    {
                                                        props.isShowField('packages', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.packages">
                                                                    <Form.Label className=" col-form-label">Packages</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Packages"
                                                                        name="movementDetails.packages"
                                                                        type="text"
                                                                        maxLength={50}
                                                                        value={fromProps.values.movementDetails.packages}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.packages')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.packages')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>


                                                            </Col>
                                                            : ""
                                                    }
                                                    {
                                                        props.isShowField('jobNumber', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.jobNumber">
                                                                    <Form.Label className=" col-form-label">Job No.</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Job No."
                                                                        name="movementDetails.jobNumber"
                                                                        type="text"
                                                                        maxLength={15}
                                                                        value={fromProps.values.movementDetails.jobNumber}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.jobNumber')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.jobNumber')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>


                                                            </Col> : ""
                                                    }
                                                    {
                                                        props.isShowField('port', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.port">
                                                                    <Form.Label className=" col-form-label">Port</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Port"
                                                                        name="movementDetails.port"
                                                                        type="text"
                                                                        maxLength={20}
                                                                        value={fromProps.values.movementDetails.port}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.port')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.port')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col>
                                                            : ""
                                                    }
                                                    {

                                                        props.isShowField('shippingLine', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.shippingLine">
                                                                    <Form.Label className=" col-form-label">Shipping Line</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Shipping Line"
                                                                        name="movementDetails.shippingLine"
                                                                        type="text"
                                                                        maxLength={50}
                                                                        value={fromProps.values.movementDetails.shippingLine}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.shippingLine')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.shippingLine')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>


                                                            </Col> : ""

                                                    }
                                                    {
                                                        props.isShowField('emptyYardGateCfs', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.emptyYardGateCfs">
                                                                    <Form.Label className=" col-form-label">Empty Yard / Gate / CFS</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Address"
                                                                        name="movementDetails.emptyYardGateCfs"
                                                                        type="text"
                                                                        maxLength={100}
                                                                        value={fromProps.values.movementDetails.emptyYardGateCfs}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.emptyYardGateCfs')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.emptyYardGateCfs')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    }
                                                    {
                                                        props.isShowField('additionalInformation', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.additionalInformation">
                                                                    <Form.Label className=" col-form-label">Add. Info.</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Add. Info."
                                                                        name="movementDetails.additionalInformation"
                                                                        type="text"
                                                                        maxLength={50}
                                                                        value={fromProps.values.movementDetails.additionalInformation}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.additionalInformation')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.additionalInformation')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    }
    {
                                                        props.isShowField('pickupDate', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.pickupDate">
                                                                    <Form.Label className=" col-form-label">Pickup Date.</Form.Label>
                                                                    {/* <Form.Control
                                                                        placeholder="Pickup Date."
                                                                        name="movementDetails.pickupDate"
                                                                        type="date"
                                                                    
                                                                        value={fromProps.values.movementDetails.pickupDate}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.pickupDate')}
                                                                    />  */}
                                                                    
                                                                    <DatePicker
                                                                        selected={fromProps.values.movementDetails.pickupDate}
                                                                        onChange={(e) => props.handleChange(e, `movementDetails.pickupDate`)}
                                                                        // showTimeSelect
                                                                        // timeIntervals={15}
                                                                        placeholderText="Pickup Date" 
                                                                        filterDate={(date) => props.isValidEndDate(date, 'pickupDate')}
                                                                        // filterTime={(date) => props.isValidEndTime(date)}
                                                                        dateFormat="dd-MM-yy"
                                                                        name={`movementDetails.pickupDate`}
                                                                        id={`movementDetails.pickupDate`}
                                                                        
                                                                        className={props.isInvalidField(fromProps, `movementDetails.pickupDate`) ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                    /> 
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.pickupDate')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    } {
                                                        props.isShowField('validaityDate', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.validaityDate">
                                                                    <Form.Label className=" col-form-label">Validity Date</Form.Label>
                                                                    {/* <Form.Control
                                                                        placeholder="Validity Date"
                                                                        name="movementDetails.validaityDate"
                                                                        type="date"
                                                                    
                                                                        value={fromProps.values.movementDetails.validaityDate}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.validaityDate')}
                                                                    /> */}
                                                                                                                                                                                        <DatePicker
                                                                        selected={fromProps.values.movementDetails.validaityDate}
                                                                        onChange={(e) => props.handleChange(e, `movementDetails.validaityDate`)}
                                                                        // showTimeSelect
                                                                        // timeIntervals={15}
                                                                        placeholderText="Validity Date" 
                                                                        filterDate={(date) => props.isValidEndDate(date, 'validaityDate')}
                                                                        filterTime={(date) => props.isValidEndTime(date)}
                                                                        dateFormat="dd-MM-yy"
                                                                        name={`movementDetails.validaityDate`}
                                                                        id={`movementDetails.validaityDate`}
                                                                        
                                                                        className={props.isInvalidField(fromProps, `movementDetails.validaityDate`) ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                    /> 
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.validaityDate')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    } {
                                                        props.isShowField('dispatchDate', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.dispatchDate">
                                                                    <Form.Label className=" col-form-label">Dispatch Date</Form.Label>
                                                                    {/* <Form.Control
                                                                        placeholder="Dispatch Date"
                                                                        name="movementDetails.dispatchDate"
                                                                        type="date"
                                                                    
                                                                        value={fromProps.values.movementDetails.dispatchDate}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.dispatchDate')}
                                                                    /> */}
                                                                    <DatePicker
                                                                        selected={fromProps.values.movementDetails.dispatchDate}
                                                                        onChange={(e) => props.handleChange(e, `movementDetails.dispatchDate`)}
                                                                        // showTimeSelect
                                                                        // timeIntervals={15}
                                                                        placeholderText="Dispatch Date" 
                                                                        filterDate={(date) => props.isValidEndDate(date, 'dispatchDate')}
                                                                        // filterTime={(date) => props.isValidEndTime(date)}
                                                                        dateFormat="dd-MM-yy"
                                                                        name={`movementDetails.dispatchDate`}
                                                                        id={`movementDetails.dispatchDate`}
                                                                        
                                                                        className={props.isInvalidField(fromProps, `movementDetails.dispatchDate`) ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                    /> 
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.dispatchDate')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    } {
                                                        props.isShowField('stuffingDate', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.stuffingDate">
                                                                    <Form.Label className=" col-form-label">Stuffing Date</Form.Label>
                                                                    {/* <Form.Control
                                                                        placeholder="Stuffing Date"
                                                                        name="movementDetails.stuffingDate"
                                                                        type="date"
                                                                    
                                                                        value={fromProps.values.movementDetails.stuffingDate}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.stuffingDate')}
                                                                    /> */}
                                                                    <DatePicker
                                                                        selected={fromProps.values.movementDetails.stuffingDate}
                                                                        onChange={(e) => props.handleChange(e, `movementDetails.stuffingDate`)}
                                                                        // showTimeSelect
                                                                        // timeIntervals={15}
                                                                        placeholderText="Stuffing Date" 
                                                                        filterDate={(date) => props.isValidEndDate(date, 'stuffingDate')}
                                                                        // filterTime={(date) => props.isValidEndTime(date)}
                                                                        dateFormat="dd-MM-yy"
                                                                        name={`movementDetails.stuffingDate`}
                                                                        id={`movementDetails.stuffingDate`}
                                                                        
                                                                        className={props.isInvalidField(fromProps, `movementDetails.stuffingDate`) ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.stuffingDate')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    } {
                                                        props.isShowField('cutOffDate', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.cutOffDate">
                                                                    <Form.Label className=" col-form-label">Cut Off Date</Form.Label>
                                                                    {/* <Form.Control
                                                                        placeholder="Cut Off Date"
                                                                        name="movementDetails.cutOffDate"
                                                                        type="date"
                                                                    
                                                                        value={fromProps.values.movementDetails.cutOffDate}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.cutOffDate')}
                                                                    /> */}
                                                                                                                                                                                        <DatePicker
                                                                        selected={fromProps.values.movementDetails.cutOffDate}
                                                                        onChange={(e) => props.handleChange(e, `movementDetails.cutOffDate`)}
                                                                        // showTimeSelect
                                                                        // timeIntervals={15}
                                                                        placeholderText="Cut Off Date" 
                                                                        filterDate={(date) => props.isValidEndDate(date, 'cutOffDate')}
                                                                        // filterTime={(date) => props.isValidEndTime(date)}
                                                                        dateFormat="dd-MM-yy"
                                                                        name={`movementDetails.cutOffDate`}
                                                                        id={`movementDetails.cutOffDate`}
                                                                        
                                                                        className={props.isInvalidField(fromProps, `movementDetails.cutOffDate`) ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.cutOffDate')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    } {
                                                        props.isShowField('doValidityDate', 'movementDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="movementDetails.doValidityDate">
                                                                    <Form.Label className=" col-form-label">Do Validity Date</Form.Label>
                                                                    {/* <Form.Control
                                                                        placeholder="Do Validity Date"
                                                                        name="movementDetails.doValidityDate"
                                                                        type="date"
                                                                    
                                                                        value={fromProps.values.movementDetails.doValidityDate}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'movementDetails.doValidityDate')}
                                                                    /> */}
                                                                    <DatePicker
                                                                        selected={fromProps.values.movementDetails.doValidityDate}
                                                                        onChange={(e) => props.handleChange(e, `movementDetails.doValidityDate`)}
                                                                        // showTimeSelect
                                                                        // timeIntervals={15}
                                                                        placeholderText="Do Validity Date" 
                                                                        filterDate={(date) => props.isValidEndDate(date, 'doValidityDate')}
                                                                        // filterTime={(date) => props.isValidEndTime(date)}
                                                                        dateFormat="dd-MM-yy"
                                                                        name={`movementDetails.doValidityDate`}
                                                                        id={`movementDetails.doValidityDate`}
                                                                        
                                                                        className={props.isInvalidField(fromProps, `movementDetails.doValidityDate`) ? "is-invalid booking-add-date form-control" : "booking-add-date form-control"}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'movementDetails.doValidityDate')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    }
                                                        </Row>
                                                    </Accordion.Body>
                                                </Accordion.Item> 
                                                <div className="borders"></div>
                                            </Accordion>
                                            <Accordion defaultActiveKey={(props.disableForm)? "0" : "5"} className="addbooking setbookingstyle"> 
                                                <Accordion.Item eventKey="0" className="accourdian-style mt-3">
                                                    <Accordion.Header>Additional Details</Accordion.Header>
                                                    <Accordion.Body> 
                                                        <Row>
                                                    {
                                                        props.isShowField('email', 'additionalDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="additionalDetails.email">
                                                                    <Form.Label className=" col-form-label">Add. Email Id</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Email Id"
                                                                        name="additionalDetails.email"
                                                                        type="email"
                                                                        maxLength={255}
                                                                        value={fromProps.values.additionalDetails.email}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'additionalDetails.email')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'additionalDetails.email')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col>
                                                            : ""
                                                    }

                                                    {
                                                        props.isShowField('mobileNumber', 'additionalDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="additionalDetails.mobileNumber">
                                                                    <Form.Label className=" col-form-label">Add. Mobile No.</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Add. Mobile No."
                                                                        name="additionalDetails.mobileNumber"
                                                                        maxLength={15}
                                                                        value={fromProps.values.additionalDetails.mobileNumber}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'additionalDetails.mobileNumber')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'additionalDetails.mobileNumber')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>


                                                            </Col> : ""

                                                    }

                                                    {
                                                        props.isShowField('invoiceNumber', 'additionalDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="additionalDetails.invoiceNumber">
                                                                    <Form.Label className=" col-form-label">Invoice No.</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Invoice No."
                                                                        name="additionalDetails.invoiceNumber"
                                                                        type="text"
                                                                        maxLength={10}
                                                                        value={fromProps.values.additionalDetails.invoiceNumber}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'additionalDetails.invoiceNumber')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'additionalDetails.invoiceNumber')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    }

                                                    {
                                                        props.isShowField('shipmentNumber', 'additionalDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="additionalDetails.shipmentNumber">
                                                                    <Form.Label className=" col-form-label">Shipment No.</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Shipment No."
                                                                        name="additionalDetails.shipmentNumber"
                                                                        type="text"
                                                                        maxLength={20}
                                                                        value={fromProps.values.additionalDetails.shipmentNumber}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'additionalDetails.shipmentNumber')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'additionalDetails.shipmentNumber')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>

                                                            </Col> : ""
                                                    }

                                                    {
                                                        props.isShowField('materialType', 'additionalDetailsFields') ?
                                                            <Col md={3}>
                                                                <Form.Group controlId="additionalDetails.materialType">
                                                                    <Form.Label className=" col-form-label">Material Type</Form.Label>
                                                                    <Form.Control
                                                                        placeholder="Material Type"
                                                                        name="additionalDetails.materialType"
                                                                        type="text"
                                                                        maxLength={20}
                                                                        value={fromProps.values.additionalDetails.materialType}
                                                                        onChange={props.handleChange}
                                                                        isInvalid={props.isInvalidField(fromProps, 'additionalDetails.materialType')}
                                                                    />
                                                                    <Form.Control.Feedback type="invalid">
                                                                        {props.getInvalidText(fromProps, 'additionalDetails.materialType')}
                                                                    </Form.Control.Feedback>
                                                                </Form.Group>


                                                            </Col>
                                                            : ""
                                                    }

                                                        </Row>
                                                    </Accordion.Body>
                                                </Accordion.Item>
                                            </Accordion>
                                        </Col>
                                    </Row>
                                    {/* <Row className=" pb-4 pt-4">
                                        <Col md={12} className=" d-flex flex-column justify-content-center">
                                            <h5>LR Details</h5>

                                        </Col>
                                    </Row>
                                    <Col className="border-bottom-1 pb-4"></Col>
                                    <Row className="pb-4 pt-4">
                                        <Col md={12}>
                                            <h5>Additional Details</h5>

                                        </Col>
                                    </Row> */}
                                    {/* <Col className="border-bottom-1"></Col> */}
                                    {props.disableForm?
                                    <Row className="pb-4 pt-4">
                                        <Col md={12}>
                                            <h5>Tracking Details</h5>
                                            <Row>
                                                <MapDetails path={path} hitPath={hitPath} />
                                            </Row>
                                        </Col>
                                    </Row> : ""
                                    }
                                    <Row className="pb-4 pt-4">
                                        <Col md={12} className=" d-flex flex-column justify-content-center">
                                        {
                                        props.disableForm?"":
                                            <Row className=" align-self-center">

                                                <Button type="submit" onClick={() => props.onSubmitClicked()} className="btn button-theme btn-rounded text-uppercase  col-auto mr-3 " >
                                                    Submit
                                                </Button>
                                                <Button type="button" onClick={() => {
                                                    // if (dateRef && dateRef.length && dateRef.length > 0) {
                                                    //     dateRef.forEach(d => {
                                                    //         if (d && d.state) {
                                                    //             d.state.inputValue = '';
                                                    //         }
                                                    //     });
                                                    // }
                                                    return props.resetForm();
                                                }
                                                } className="btn btn-secondary btn-rounded text-uppercase  col-auto">
                                                    Cancel
                                                </Button>

                                                <Button id="validatebookingForm" className="d-none" onClick={() => fromProps.validateForm()}> </Button>

                                            </Row>
}
                                        </Col>
                                    </Row>

                                </Form>
                            </fieldset>


                       
                    </Col>
                </Row>

            )}
        </Formik>
    );

};