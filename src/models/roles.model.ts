
export interface IRoleModel {
    id: string;
    createdDate: number;
    lastModifiedDate: number;
    active: boolean;
    name: string;
    description: string;
    moduleName?: string;
} 