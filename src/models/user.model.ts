import { IRoleModel } from "./roles.model";

export interface IUserModel {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    role: IRoleModel;
    permissions: IRoleModel[];
}

