export interface IPermissionModel {
    active: boolean;
    createdDate: number | string;
    description: string;
    id: string;
    lastModifiedDate: number | string;
    moduleName: string;
    name: string;
}

export interface IRoleAllModel {
    id: string;
    name: string;
    description: string;
    permissions : IPermissionModel[];
} 

export const defaultRole: IRoleAllModel = {
    id: '',
    name: '',
    description: '',
    permissions : []
}
