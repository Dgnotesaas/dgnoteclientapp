export interface IEmailTemplateModel {
    attachment: string,
    content: string,
    email_bcc: [string],
    email_cc: [string],
    email_to: [string],
    subject: string
} 

export const defaultEmailTemplateModel:IEmailTemplateModel= {
    "attachment": "",
    "content": "",
    "email_bcc": [
        ""
    ],
    "email_cc": [
        ""
    ],
    "email_to": [""],
    "subject": ""
}
  