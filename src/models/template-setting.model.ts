
 export interface ITemplateSettingModel {
    active: boolean;
    additionalDetailsFields: IAdditionalDetailsField[];
    createdDate: Date | string;
    headerFields: IHeaderField[];
    id: string;
    lastModifiedDate: Date | string | number;
    movementDetailsFields: IMovementDetailsField[];
    tripFields: ITripField[];
}

 export interface IAdditionalDetailsField {
    fieldName: string;
    isRequired: boolean;
    isShow: boolean;
}

export interface IHeaderField {
    fieldName: string;
    isRequired: boolean;
    isShow: boolean;
}

export interface IMovementDetailsField {
    fieldName: string;
    isRequired: boolean;
    isShow: boolean;
}

export interface ITripField {
    fieldName: string;
    isRequired: boolean;
    isShow: boolean;
}

export const defaultTemplateSettingValue: ITemplateSettingModel = {
    "id": "",
    "createdDate": "",
    "lastModifiedDate": "",
    "active": true,
    "headerFields": [
      {
        "fieldName": "tripType",
        "isShow": true,
        "isRequired": true
      },
      {
        "fieldName": "fromLocation",
        "isShow": true,
        "isRequired": true
      },
      {
        "fieldName": "toLocation",
        "isShow": true,
        "isRequired": true
      }
    ],
    "tripFields": [
      {
        "fieldName": "customerName",
        "isShow": true,
        "isRequired": true
      },
      {
        "fieldName": "driverMobileNumber",
        "isShow": true,
        "isRequired": true
      },
      {
        "fieldName": "vehicleNumber",
        "isShow": true,
        "isRequired": true
      },
      {
        "fieldName": "transporterName",
        "isShow": true,
        "isRequired": true
      },
      {
        "fieldName": "transitStartDate",
        "isShow": true,
        "isRequired": true
      },
      {
        "fieldName": "transitEstimatedEndDate",
        "isShow": true,
        "isRequired": true
      },
      {
        "fieldName": "transitActualEndDate",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "uploadLRDocumentURL",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "pingInterval",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "containerNumber",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "containerSize",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "cargoWeight",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "podUploadURL",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "sealNumber",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "transporterAdvance",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "customerAdvance",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "buying",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "selling",
        "isShow": false,
        "isRequired": false
      }
    ],
    "movementDetailsFields": [
      {
        "fieldName": "movementType",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "consignorSellerName",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "consignorSellerAddress",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "consignorSellerGstNumber",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "consigneeBuyerName",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "consigneeBuyerAddress",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "consigneeBuyerGstNumber",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "lcNumber",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "lcDate",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "packages",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "jobNumber",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "port",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "shippingLine",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "emptyYardGateCfs",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "additionalInformation",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "pickupDate",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "validaityDate",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "dispatchDate",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "stuffingDate",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "cutOffDate",
        "isShow": false,
        "isRequired": false
      },
      {
        "fieldName": "doValidityDate",
        "isShow": false,
        "isRequired": false
      }
    ],
    "additionalDetailsFields": [
      {
        "fieldName": "email",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "mobileNumber",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "invoiceNumber",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "shipmentNumber",
        "isShow": true,
        "isRequired": false
      },
      {
        "fieldName": "materialType",
        "isShow": true,
        "isRequired": false
      }
    ]
  }