/* eslint-disable import/first */
import { IGenericReducerState } from "../utilities";

//Login model
import {ILoginModel}from "./login.model";
export type {ILoginModel} from "./login.model"

//User model
import {IUserModel} from "./user.model";
export type {IUserModel} from "./user.model";
//Trip model
// import {ITripModel} from "./trip.model";
// export type {ITripModel} from "./trip.model";
//Role model
import {IRoleModel}from "./roles.model";
export type{IRoleModel} from "./roles.model";
//Password model
import {IPasswordModel}from "./password.model";
import {IForgetPasswordModel}from "./password.model";
import {ICreatePasswordModel}from "./createpassword.model";
export type{IPasswordModel} from "./password.model";
export type{IForgetPasswordModel} from "./password.model";
export type{ICreatePasswordModel} from "./createpassword.model";
export {defaultPasswordModel} from "./password.model"
export {defaultForgetPasswordModel} from "./password.model"
export {defaultCreatePasswordModel} from "./createpassword.model"
//Booking models
import {IBookingModel, ITrip} from "./booking.model";
export type{IBookingModel,ITrip} from "./booking.model"
import { IBookingListModel } from "./booking-list.model";
import { ITrackingListModel } from "./tracking-list.model";
import { IListSearchRequstModel } from "./list-search-request.model";
import { ITemplateSettingModel } from "./template-setting.model";
import { ITrackingModel } from "./tracking.model";
import { IAllActiveTripModel } from "./allactivetrips.model";
import { ISingleTrackingByIdModel } from "./allactivetrips.model";
import { IAllHitListModel } from "./allactivetrips.model";
import { ISingleTrackingModel } from "./singletracking.model";
import  {IPermissionModel} from "./permission.model"
import  {IRoleAllModel} from "./roleall.model"
import  {IUsermanagementModel} from "./usermanagement.model"
export {defaultBookingListValue} from "./booking-list.model";
export {defaultTrackingListValue} from "./tracking-list.model";
export {defaultTrackingValue} from "./tracking.model";
export {defaultAllTrackingListValue} from "./allactivetrips.model";
export {defaultAllHitListValue} from "./allactivetrips.model";
export {defaultSingleTrackingValue} from "./singletracking.model";
export {defaultBookingValue} from "./booking.model"
export {defaultListSearchRequestModel} from "./list-search-request.model"
export  {defaultTemplateSettingValue} from "./template-setting.model"
export  {defaultPermission} from "./permission.model"
export  {defaultUsermanagement} from "./usermanagement.model"
export  {defaultRole} from "./roleall.model"

import  {IHealthModel} from "./healthlist.model"
export {defaultHealthValue} from "./healthlist.model";

import  {ICompanyModel} from "./company.model";
export  type {ICompanyModel} from "./company.model";
export {defaultCompanyValue} from "./company.model";

import  {IEmailTemplateModel} from "./emailtemplate.model";
export  {defaultEmailTemplateModel} from "./emailtemplate.model";

import  {IAlertList} from "./alertmodule.model";
import  {IUserRule} from "./alertmodule.model";
export  {defaultAlertList} from "./alertmodule.model";
export  {defaultUserRule} from "./alertmodule.model";
import  {IConsentCountModel, ITransitCountModel} from "./dashboard.model";
export  {defaultConsentCount, defaultTransitCount} from "./dashboard.model";


export interface IAppState {
    Login: IGenericReducerState<ILoginModel>;
    CreatePassword: IGenericReducerState<ICreatePasswordModel>;
    User:IGenericReducerState<IUserModel>;
    Roles:IGenericReducerState<IRoleModel>;
    Password:IGenericReducerState<IPasswordModel>;
    ForgetPassword:IGenericReducerState<IForgetPasswordModel>;
    Booking:IGenericReducerState<IBookingModel>;
    BookingList: IGenericReducerState<IBookingListModel>;
    TrackingList: IGenericReducerState<ITrackingListModel>;
    AllActiveTrip: IGenericReducerState<IAllActiveTripModel[]>;
    SingeTrackingById: IGenericReducerState<ISingleTrackingByIdModel[]>;
    AllHitTripById: IGenericReducerState<IAllHitListModel[]>;
    SearchRequest: IGenericReducerState<IListSearchRequstModel>;
    TemplateSetting: IGenericReducerState<ITemplateSettingModel>;
    Tracking: IGenericReducerState<ITrackingModel[]>;
    SingleTracking: IGenericReducerState<ISingleTrackingModel>;
    UserPermission: IGenericReducerState<IPermissionModel>;
    UserManagement: IGenericReducerState<IUsermanagementModel>;
    RoleAll: IGenericReducerState<IRoleAllModel>;
    HealthList: IGenericReducerState<IHealthModel>;
    Company: IGenericReducerState<ICompanyModel>;
    EmailTemplate: IGenericReducerState<IEmailTemplateModel>;
    AlertList: IGenericReducerState<IAlertList>;
    AlertPost: IGenericReducerState<IUserRule>;
    TransitCount: IGenericReducerState<ITransitCountModel>;
    ConsentCount: IGenericReducerState<IConsentCountModel>;
}