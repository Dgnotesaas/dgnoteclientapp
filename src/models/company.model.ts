export interface ICompanyModel {
    address: Iaddress;
    contactEmailId: string;
    contactMobileNumber: string;
    defaultPingIntervalType: string;
    gstNumber: string;
    id: string;
    name: string;
    pingPriceMapping: IPingPriceMapping;
    pointOfContact: string;
    subdomain: string;
    tenantHeader: string;
}

export interface Iaddress {
    addressLine1: string;
    addressLine2: string;
    city: string;
    country: string;
    landmark: string;
    location: {
      coordinates: [
        number
      ];
      type: string;
      x: number;
      y: number;
    };
    pinCode: string;
    state: string;
}

export interface IPingPriceMapping {
    additionalProp1: number;
    additionalProp2: number;
    additionalProp3: number;
}

export const  defaultCompanyValue: ICompanyModel = {
    address: {
        addressLine1: "",
        addressLine2: "",
        city: "",
        country: "",
        landmark: "",
        location: {
        coordinates: [
            0
        ],
        type: "",
        x: 0,
        y: 0,
        },
        pinCode: "",
        state: "",
    } as Iaddress,
    contactEmailId: "",
    contactMobileNumber: "",
    defaultPingIntervalType: "",
    gstNumber: "",
    id: "",
    name: "",
    pingPriceMapping: {
        additionalProp1: 0,
        additionalProp2: 0,
        additionalProp3: 0,
    } as IPingPriceMapping,
    pointOfContact: "",
    subdomain: "",
    tenantHeader: "",


}