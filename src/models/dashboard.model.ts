export interface IConsentCountModel {
    allowedConsent: number;
    pendingConsent: number;
} 

export const defaultConsentCount: IConsentCountModel = {
    allowedConsent: 0,
    pendingConsent: 0,
}

export interface ITransitCountModel {
    activeTransit: number;
    upcomingTransit: number;
} 

export const defaultTransitCount: ITransitCountModel = {
    activeTransit: 0,
    upcomingTransit: 0
}
