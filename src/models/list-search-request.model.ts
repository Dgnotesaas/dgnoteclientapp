export interface IListSearchRequstModel{
        active: boolean;
        fromDate: number | undefined;
        pageNumber: number;
        pageSize: number;
        searchToken: string;
        showAll: boolean;
        sortDirection: string;
        sortProperty: string | undefined;
        status: string;
         toDate: number | undefined;
         afterTransitEndDateCriteria: number | undefined | string;
}

export const defaultListSearchRequestModel:IListSearchRequstModel= {
    active: true,
    fromDate: undefined,
    pageNumber:0,
    pageSize:5,
    searchToken:"",
    showAll:false,
    sortDirection:"DESC",
    sortProperty:undefined,
    status:"ACTIVE",
    toDate: undefined,
    afterTransitEndDateCriteria: undefined,
}
   