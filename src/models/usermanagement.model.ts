import { IRoleModel } from "./roles.model";
import { IPermissionModel  } from "./permission.model"; 

export interface IUsermanagementModel {
    companyDto: [];
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    role: IRoleModel[];
    permissions: IPermissionModel[];
}

export const defaultUsermanagement: IUsermanagementModel = {
    companyDto: [],
    id: '',
    email: '',
    firstName: '',
    lastName: '',
    password: '',
    role: [],
    permissions: [],
}

