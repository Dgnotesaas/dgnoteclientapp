
// export interface ITrackingListModel  {
//     content: any[],
//     currentPage: number,
//     currentPageSize: number,
//     totalElements: number,
//     totalPages: number
// }
// export const defaultTrackingListValue:ITrackingListModel=
//     {content:[], currentPage:1, currentPageSize:100, totalPages:10,totalElements:10};
    
    export interface ITrackingListModel{
        active: boolean;
        fromDate: number | undefined;
        pageNumber: number;
        pageSize: number;
        searchToken: string;
        showAll: boolean;
        sortDirection: string;
        sortProperty: string;
        status: string;
         toDate: number | undefined;
         afterTransitEndDateCriteria: number | undefined | string;
}

export const defaultTrackingListValue:ITrackingListModel= {
    active: true,
    fromDate: undefined,
    pageNumber:0,
    pageSize:500,
    searchToken:"",
    showAll:false,
    sortDirection:"ASC",
    sortProperty:"lastModifiedDate",
    status:"ACTIVE",
    toDate:undefined,
    afterTransitEndDateCriteria: undefined,
}