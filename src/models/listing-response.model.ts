export interface IListingModel{
    content: any[],
    currentPage: number,
    currentPageSize: number,
    totalElements: number,
    totalPages: number,
}