export interface IPasswordModel {
  existingPassword: string,
  newPassword: string,
  confirmPassword:string
}

export interface IForgetPasswordModel {
  email: string,
}

export interface ICreatePasswordModel {
  code: string,
  email: string,
  newPassword:string
}

export const defaultCreatePasswordModel:ICreatePasswordModel=
  {code: "",
  email: "",
  newPassword:""}

export const defaultForgetPasswordModel:IForgetPasswordModel=
    {email: ""}

export const defaultPasswordModel:IPasswordModel=
    {existingPassword: "",
    newPassword: "",
  confirmPassword:""}
