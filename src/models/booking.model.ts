import { MomentTypeEnum, TripTypeEnum } from "../utilities/enum";


export interface IBookingModel {
    additionalDetails: IAdditionalDetails;
    fromLocation: ILocation;
    id: string;
    movementDetails: IMovementDetails;
    toLocation: ILocation;
    tripType: string;
    trips: ITrip[];
    from_autocomplete: string;
    to_autocomplete: string;
}


export interface IAdditionalDetails {
    email: string;
    invoiceNumber: string;
    materialType: string;
    mobileNumber: string;
    shipmentNumber: string;
}


export interface ITrip {
    bookingDate: Date | string;
    bookingId:string;
    buying: string | number;
    cargoWeight: string;
    containerNumber: string;
    containerSize: string;
    customerAdvance: string | number;
    customerName: string;
    driverMobileNumber: string;
    id: string;
    pingInterval: string;
    podUploadURL: string;
    sealNumber: string;
    selling: string | number;
    transitActualEndDate: Date | string | number;
    transitEstimatedEndDate: Date | string | number;
    transitStartDate:Date | string | number;
    transporterAdvance: string | number;
    transporterName: string;
    uploadLRDocumentURL: string;
    vehicleNumber: string;
    status:string;
    bookingDetails?: IBookingModel,
    returnStartDate: Date | string | number;
    returnEndDate: Date | string | number;
    remarks:string;
    tripNumber:string;
    fromLocation: ILocation;
    toLocation: ILocation;
    distance: number;
}


export interface ILocation {
    addressLine1: string;
    addressLine2: string;
    city: string;
    country: string;
    landmark: string;
    pinCode: string;
    state: string;
    latitude: number;
    longitude: number;
}




export interface IMovementDetails {
    additionalInformation: string;
    consigneeBuyerAddress: string;
    consigneeBuyerGstNumber: string;
    consigneeBuyerName: string;
    consignorSellerAddress: string;
    consignorSellerGstNumber: string;
    consignorSellerName: string;
    cutOffDate: Date | string;
    dispatchDate: Date | string;
    doValidityDate: Date | string;
    emptyYardGateCfs: string;
    jobNumber: string;
    lcDate: Date | string;
    lcNumber: string;
    movementType: string;
    packages: string;
    pickupDate: Date | string;
    pickupYard: string;
    port: string;
    shippingLine: string;
    stuffingDate: Date | string;
    validaityDate: Date | string;
}
export const  defaultBookingValue: IBookingModel = {

    additionalDetails: {
        email: "",
        invoiceNumber: "",
        materialType: "",
        mobileNumber: "",
        shipmentNumber: ""
    } as IAdditionalDetails,

    fromLocation: {
        addressLine1: "",
        addressLine2: "",
        city: "",
        country: "",
        landmark: "",
        pinCode: "",
        state: "",
        latitude:0,
        longitude:0
    } as ILocation,

    id: "",
    from_autocomplete: "",
    to_autocomplete: "",
    movementDetails: {
        additionalInformation: "",
        consigneeBuyerAddress: "",
        consigneeBuyerGstNumber: "",
        consigneeBuyerName: "",
        consignorSellerAddress: "",
        consignorSellerGstNumber: "",
        consignorSellerName: "",
        cutOffDate: "",
        dispatchDate: "",
        doValidityDate: "",
        emptyYardGateCfs: "",
        jobNumber: "",
        lcDate: "",
        lcNumber: "",
        movementType: "IMPORT",
        packages: "",
        pickupDate: "",
        pickupYard: "",
        port: "",
        shippingLine: "",
        stuffingDate: ""
    } as IMovementDetails,
    toLocation: {
        addressLine1: "",
        addressLine2: "",
        city: "",
        country: "",
        landmark: "",
        pinCode: "",
        state: "",
        latitude:0,
        longitude:0
    } as ILocation,
    tripType: "SINGLE_TRIP",
    trips: [{
        bookingDate: "",
        bookingId:"",
        buying: '',
        cargoWeight: "",
        containerNumber: "",
        containerSize: "",
        customerAdvance: '',
        customerName: "",
        driverMobileNumber: "",
        id: "",
        pingInterval: "",
        podUploadURL: "",
        sealNumber: "",
        selling: '',
        transitActualEndDate: "",
        transitEstimatedEndDate: "",
        transitStartDate: "",
        transporterAdvance: '',
        transporterName: "",
        uploadLRDocumentURL: "",
        vehicleNumber: "",
        status:"ACTIVE",
        bookingDetails: {} as IBookingModel,
        returnEndDate:"",
        returnStartDate:"",
        remarks:"",
        tripNumber:"",
        distance:0,
        fromLocation: {
            addressLine1: "",
            addressLine2: "",
            city: "",
            country: "",
            landmark: "",
            pinCode: "",
            state: "",
            latitude:0,
            longitude:0
        } as ILocation,
        toLocation: {
            addressLine1: "",
            addressLine2: "",
            city: "",
            country: "",
            landmark: "",
            pinCode: "",
            state: "",
            latitude:0,
            longitude:0
        } as ILocation
    } as ITrip] as ITrip[]
}

export const defaultTripValue: ITrip = {
    tripNumber:"",
    bookingDate: "",
    bookingId:"",
    buying: '',
    cargoWeight: "",
    containerNumber: "",
    containerSize: "",
    customerAdvance: '',
    customerName: "",
    driverMobileNumber: "",
    id: "",
    pingInterval: "",
    podUploadURL: "",
    sealNumber: "",
    selling: '',
    transitActualEndDate: "",
    transitEstimatedEndDate: "",
    transitStartDate: "" ,
    transporterAdvance: '',
    transporterName: "",
    uploadLRDocumentURL: "",
    vehicleNumber: "",
    status:"ACTIVE",
    bookingDetails: {} as IBookingModel,
    returnEndDate:"",
    returnStartDate:"",
    remarks:"",
    distance:0,
    fromLocation: {
        addressLine1: "",
        addressLine2: "",
        city: "",
        country: "",
        landmark: "",
        pinCode: "",
        state: "",
        latitude:0,
        longitude:0
    } as ILocation,
    toLocation: {
        addressLine1: "",
        addressLine2: "",
        city: "",
        country: "",
        landmark: "",
        pinCode: "",
        state: "",
        latitude:0,
        longitude:0
    } as ILocation
}


export const  emptyBookingValue: IBookingModel = {

    additionalDetails: {
        email: "",
        invoiceNumber: "",
        materialType: "",
        mobileNumber: "",
        shipmentNumber: ""
    } as IAdditionalDetails,

    fromLocation: {
        addressLine1: "",
        addressLine2: "",
        city: "",
        country: "",
        landmark: "",
        pinCode: "",
        state: "",
        latitude:0,
        longitude:0
    } as ILocation,

    id: "",
    from_autocomplete: "",
    to_autocomplete: "",
    movementDetails: {
        additionalInformation: "",
        consigneeBuyerAddress: "",
        consigneeBuyerGstNumber: "",
        consigneeBuyerName: "",
        consignorSellerAddress: "",
        consignorSellerGstNumber: "",
        consignorSellerName: "",
        cutOffDate: "",
        dispatchDate: "",
        doValidityDate: "",
        emptyYardGateCfs: "",
        jobNumber: "",
        lcDate: "",
        lcNumber: "",
        movementType: "IMPORT",
        packages: "",
        pickupDate: "",
        pickupYard: "",
        port: "",
        shippingLine: "",
        stuffingDate: ""
    } as IMovementDetails,
    toLocation: {
        addressLine1: "",
        addressLine2: "",
        city: "",
        country: "",
        landmark: "",
        pinCode: "",
        state: "",
        latitude:0,
        longitude:0
    } as ILocation,
    tripType: "SINGLE_TRIP",
    trips: [{
        bookingDate: "",
        bookingId:"",
        buying: '',
        cargoWeight: "",
        containerNumber: "",
        containerSize: "",
        customerAdvance: '',
        customerName: "",
        driverMobileNumber: "",
        id: "",
        pingInterval: "",
        podUploadURL: "",
        sealNumber: "",
        selling: '',
        transitActualEndDate: "",
        transitEstimatedEndDate: "",
        transitStartDate: "",
        transporterAdvance: '',
        transporterName: "",
        uploadLRDocumentURL: "",
        vehicleNumber: "",
        status:"ACTIVE",
        bookingDetails: {} as IBookingModel,
        returnEndDate:"",
        returnStartDate:"",
        remarks:"",
        tripNumber:"",
        fromLocation: {
            addressLine1: "",
            addressLine2: "",
            city: "",
            country: "",
            landmark: "",
            pinCode: "",
            state: "",
            latitude:0,
            longitude:0
        } as ILocation,
        toLocation: {
            addressLine1: "",
            addressLine2: "",
            city: "",
            country: "",
            landmark: "",
            pinCode: "",
            state: "",
            latitude:0,
            longitude:0
        } as ILocation
    } as ITrip] as ITrip[]
}