
export interface IAllActiveTripModel {
    bookingId: number | string;
    distance: number | string;
    tripNumber: number | string;
    fromLocation: FromLocation,
    toLocation: ToLocation,
    customerName: string;
    driverMobileNumber: number | string;
    vehicleNumber: number | string;
    transporterName: string;
    transitStartDate: number | string;
    transitEstimatedEndDate: number | string;
    transitActualEndDate: number | string;
    uploadLRDocumentURL: number | string;
    pingInterval: string;
    containerNumber: number | string;
    containerSize: number | string;
    cargoWeight: number | string;
    podUploadURL: number | string;
    sealNumber: number | string;
    transporterAdvance: number;
    customerAdvance: number;
    buying: number;
    selling: number;
    status: string;
    remarks: string;
    consentStatus: string;
    lastSuccessfullTrackingHit: LastSuccessfullTrackingHit[];
}

export interface IAllHitListModel {
    bookingId: number | string;
    distance: number | string;
    tripNumber: number | string;
    fromLocation: FromLocation,
    toLocation: ToLocation,
    customerName: string;
    driverMobileNumber: number | string;
    vehicleNumber: number | string;
    transporterName: string;
    transitStartDate: number | string;
    transitEstimatedEndDate: number | string;
    transitActualEndDate: number | string;
    uploadLRDocumentURL: number | string;
    pingInterval: string;
    containerNumber: number | string;
    containerSize: number | string;
    cargoWeight: number | string;
    podUploadURL: number | string;
    sealNumber: number | string;
    transporterAdvance: number;
    customerAdvance: number;
    buying: number;
    selling: number;
    status: string;
    remarks: string;
    consentStatus: string;
    lastSuccessfullTrackingHit: AllHits[];
}

export interface ISingleTrackingByIdModel {
    bookingId: number | string;
    distance: number | string;
    tripNumber: number | string;
    fromLocation: FromLocation[],
    toLocation: ToLocation[],
    customerName: string;
    driverMobileNumber: number | string;
    vehicleNumber: number | string;
    transporterName: string;
    transitStartDate: number | string;
    transitEstimatedEndDate: number | string;
    transitActualEndDate: number | string;
    uploadLRDocumentURL: number | string;
    pingInterval: string;
    containerNumber: number | string;
    containerSize: number | string;
    cargoWeight: number | string;
    podUploadURL: number | string;
    sealNumber: number | string;
    transporterAdvance: number;
    customerAdvance: number;
    buying: number;
    selling: number;
    status: string;
    remarks: string;
    consentStatus: string;
    lastSuccessfullTrackingHit: LastSuccessfullTrackingHit[];
}

export interface LastSuccessfullTrackingHit {
    address: number | string;
    httpResponse: string;
    latitude: number | string;
    locationResultStatusText: string;
    locationRetrievalStatus: string;
    longitude: number | string;
    mobileNumber: number | string;
    status: number | string;
    timeStamp: number | string;
    errorMessageList: string[];
    tripDto: TripDto[];
}

export interface AllHits {
    address: number | string;
    httpResponse: string;
    latitude: number | string;
    locationResultStatusText: string;
    locationRetrievalStatus: string;
    longitude: number | string;
    mobileNumber: number | string;
    status: number | string;
    timeStamp: number | string;
    errorMessageList: string[];
    tripDto: TripDto[];
}

export interface FromLocation {
    addressLine1: string,
    addressLine2: string,
    city: string,
    country: string,
    landmark: string,
    latitude: number | string,
    longitude: number | string,
    pinCode: number | string,
    state: string
}

export interface ToLocation {
    addressLine1: string,
    addressLine2: string,
    city: string,
    country: string,
    landmark: string,
    latitude: number | string,
    longitude: number | string,
    pinCode: number | string,
    state: string
}

export interface TripDto {
    bookingDate: string,
    bookingId: number | string,
    buying: number | string,
    cargoWeight: number | string,
    consentStatus: string,
    containerNumber: string,
    containerSize: string,
    customerAdvance: number | string,
    customerName: string,
    driverMobileNumber: string,
    fromLocation: FromLocation,
    id: number | string,
    pingInterval: string,
    podUploadURL: string,
    remarks: string,
    sealNumber: number | string,
    selling: number | string,
    status: string,
    toLocation: ToLocation,
    trackingUrl: string,
    transitActualEndDate: number | string,
    transitEstimatedEndDate: number | string,
    transitStartDate: number | string,
    transporterAdvance: number | string,
    transporterName: string,
    tripNumber: number | string,
    tripType: string,
    uploadLRDocumentURL: string,
    vehicleNumber: string
}

export const defaultAllTrackingListValue: IAllActiveTripModel[] = []

export const defaultSingleTrackingByIdValue: ISingleTrackingByIdModel =
{
    bookingId: "",
    distance: "",
    tripNumber: "",
    customerName: "",
    driverMobileNumber: "",
    fromLocation: [],
    toLocation: [],
    vehicleNumber: "",
    transporterName: "",
    transitStartDate: 0,
    transitEstimatedEndDate: 0,
    transitActualEndDate: 0,
    uploadLRDocumentURL: "",
    pingInterval: "",
    containerNumber: "",
    containerSize: "",
    cargoWeight: "",
    podUploadURL: "",
    sealNumber: "",
    transporterAdvance: 0,
    customerAdvance: 0,
    buying: 0,
    selling: 0,
    status: "",
    remarks: "",
    consentStatus: "",
    lastSuccessfullTrackingHit: []
}


export const defaultAllHitListValue: IAllHitListModel[] = []
