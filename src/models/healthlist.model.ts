import { HealthListEnum } from "../utilities/enum";


export interface IHealthModel {
    fromLocation: ILocation;
    toLocation: ILocation;
    createTripAPICallLog: ICreateTripAPICallLog;
    driverImportApiCallLogDto: DriverImportApiCallLogDto;
    active: boolean,
    bookingId:string;
    consentStatus: string;
    buying: string | number;
    cargoWeight: string;
    containerNumber: string;
    containerSize: string;
    customerAdvance: string | number;
    customerName: string;
    driverMobileNumber: string;
    id: string;
    createdByUserId: string,
    createdDate: Date | string | number;
    lastModifiedDate: Date | string | number;
    lastTrackingError: string;
    lastTrackingStatus: string;
    trackingUrl: string;
    pingInterval: string;
    lrNumber: string;
    podUploadURL: string;
    sealNumber: string;
    selling: string | number;
    transitActualEndDate: Date | string | number;
    transitEstimatedEndDate: Date | string | number;
    transitStartDate:Date | string | number;
    transporterAdvance: string | number;
    transporterName: string;
    uploadLRDocumentURL: string;
    vehicleNumber: string;
    status:string; 
    remarks:string;
    tripNumber:string; 
    distance: number;
}


export interface ICreateTripAPICallLog { 
    callStatus: string,
    errorMessage: string,
    httpStatusCode: number,
    requestDto: {
    createDate: string,
    district: string,
    expectedDistance: string,
    msisdn: string,
    name: string,
    route: string
    },
    requestJson: string,
    responseDto: {
    name: string,
    status: string,
    tripUrl: string
    },
    responseJson: string
} 

export interface DriverImportApiCallLogDto {
    callStatus: string,
    errorMessage: string,
    httpStatusCode: number,
    requestDto: {
    entityImportList: [
        {
        errorDesc: string,
        firstName: string,
        lastName: string,
        msisdn: string
        }
    ]
    },
    requestJson: string,
    responseDto: {
    failureList: [
        {
        errorDesc: string,
        firstName: string,
        lastName: string,
        msisdn: string
        }
    ],
    successList: [
        {
        errorDesc: string,
        firstName: string,
        lastName: string,
        msisdn: string
        }
    ]
    },
    responseJson: string
}

export interface ILocation {
    addressLine1: string;
    addressLine2: string;
    city: string;
    country: string;
    landmark: string;
    pinCode: string;
    state: string;
    latitude: number;
    longitude: number;
}
 
export const  defaultHealthValue: IHealthModel = { 

    
    active: true,
    bookingId: "",
    buying: 0,
    cargoWeight: "",
    consentStatus: "NOT_INITIATED",
    containerNumber: "",
    containerSize: "",
    createTripAPICallLog: {
        callStatus: "SUCCESS",
        errorMessage: "",
        httpStatusCode: 0,
        requestDto: {
        createDate: "",
        district: "",
        expectedDistance: "",
        msisdn: "",
        name: "",
        route: ""
        },
        requestJson: "",
        responseDto: {
        name: "",
        status: "",
        tripUrl: ""
        },
        responseJson: ""
    },
    createdByUserId: "",
    createdDate: "",
    customerAdvance: 0,
    customerName: "",
    distance: 0,
    driverImportApiCallLogDto: {
        callStatus: "SUCCESS",
        errorMessage: "",
        httpStatusCode: 0,
        requestDto: {
        entityImportList: [
            {
            errorDesc: "",
            firstName: "",
            lastName: "",
            msisdn: ""
            }
        ]
        },
        requestJson: "",
        responseDto: {
        failureList: [
            {
            errorDesc: "",
            firstName: "",
            lastName: "",
            msisdn: ""
            }
        ],
        successList: [
            {
            errorDesc: "",
            firstName: "",
            lastName: "",
            msisdn: ""
            }
        ]
        },
        responseJson: ""
    },
    driverMobileNumber: "",
    fromLocation: {
        addressLine1: "",
        addressLine2: "",
        city: "",
        country: "",
        landmark: "",
        pinCode: "",
        state: "",
        latitude:0,
        longitude:0
    } as ILocation,
    id: "",
    lastModifiedDate: "",
    lastTrackingError: "",
    lastTrackingStatus: "",
    lrNumber: "",
    pingInterval: "HIT_BASED",
    podUploadURL: "",
    remarks: "",
    sealNumber: "",
    selling: 0,
    status: "ACTIVE",
    toLocation: {
        addressLine1: "",
        addressLine2: "",
        city: "",
        country: "",
        landmark: "",
        pinCode: "",
        state: "",
        latitude:0,
        longitude:0
    } as ILocation,
    trackingUrl: "",
    transitActualEndDate: "",
    transitEstimatedEndDate: "",
    transitStartDate: "",
    transporterAdvance: 0,
    transporterName: "",
    tripNumber: "",
    uploadLRDocumentURL: "",
    vehicleNumber: ""
}  