export interface IForgetPasswordModel {
    code: string,
    email: string,
    newPassword:string
  }
  export const defaultForgetPasswordModel:IForgetPasswordModel=
      {code: "",
      email: "",
    newPassword:""}
  