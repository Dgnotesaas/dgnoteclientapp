
export interface IPermissionModel {
    id: string;
    createdDate: number;
    lastModifiedDate: number;
    active: boolean;
    name: string;
    description: string;
    moduleName: string;
} 

export const defaultPermission:  IPermissionModel = {
    id: '',
    createdDate: 0,
    lastModifiedDate: 0,
    active: true,
    name: '',
    description: '',
    moduleName: '',
}