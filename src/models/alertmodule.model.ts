export interface IAlertList {
    description: string,
    id: string,
    rule: string,
    status: string,
    userRule: IUserRule
}

export interface IUserRule {
    companyId: string,
    description: string,
    emailStatus: string,
    id: string,
    rule: string,
    smsStatus: string,
    tenantName: string,
    value: string,
    whatsappStatus: string
}

export const defaultUserRule : IUserRule = {
    companyId: "",
    description: "",
    emailStatus: "",
    id: "",
    rule: "",
    smsStatus: "",
    tenantName: "",
    value: "",
    whatsappStatus: ""    
}

export const defaultAlertList : IAlertList = {
    description: "",
    id: "",
    rule: "",
    status: "",
    userRule: {
        companyId: "",
        description: "",
        emailStatus: "",
        id: "",
        rule: "",
        smsStatus: "",
        tenantName: "",
        value: "",
        whatsappStatus: ""
    } as IUserRule
}