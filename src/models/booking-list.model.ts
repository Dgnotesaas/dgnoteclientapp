
export interface IBookingListModel  {
    content: any[],
    currentPage: number,
    currentPageSize: number,
    totalElements: number,
    totalPages: number
}
export const defaultBookingListValue:IBookingListModel=
    {content:[], currentPage:1, currentPageSize:5, totalPages:0,totalElements:0};
    
    