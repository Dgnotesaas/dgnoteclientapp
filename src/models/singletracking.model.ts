
export interface ISingleTrackingModel {
    address: number | string;
    httpResponse: string;
    latitude: number | string;
    locationResultStatusText: string;
    locationRetrievalStatus: string;
    longitude: number | string;
    mobileNumber: number | string;
    status: number | string;
    timeStamp: number | string;
    errorMessageList: string[];
    tripDto: TripDto[];
}

export interface FromLocation {
    addressLine1: string,
    addressLine2: string,
    city: string,
    country: string,
    landmark: string,
    latitude: number | string,
    longitude: number | string,
    pinCode: number | string,
    state: string
}

export interface ToLocation {
    addressLine1: string,
    addressLine2: string,
    city: string,
    country: string,
    landmark: string,
    latitude: number | string,
    longitude: number | string,
    pinCode: number | string,
    state: string
}

export interface TripDto {
    bookingDate: string,
    bookingId: number | string,
    buying: number | string,
    cargoWeight: number | string,
    consentStatus: string,
    containerNumber: string,
    containerSize: string,
    customerAdvance: number | string,
    customerName: string,
    driverMobileNumber: string,
    fromLocation: FromLocation,
    id: number | string,
    pingInterval: string,
    podUploadURL: string,
    remarks: string,
    sealNumber: number | string,
    selling: number | string,
    status: string,
    toLocation: ToLocation,
    trackingUrl: string,
    transitActualEndDate: number | string,
    transitEstimatedEndDate: number | string,
    transitStartDate: number | string,
    transporterAdvance: number | string,
    transporterName: string,
    tripNumber: number | string,
    tripType: string,
    uploadLRDocumentURL: string,
    vehicleNumber: string
}

export const defaultSingleTrackingValue: ISingleTrackingModel =
{
    address: '',
    httpResponse: '',
    latitude: 0,
    locationResultStatusText: '',
    locationRetrievalStatus: '',
    longitude: 0,
    mobileNumber: '',
    status: '',
    timeStamp: '',
    errorMessageList: [],
    tripDto: [
        // {
        //     "bookingDate": "23-09-2021",
        //     "bookingId": "DG-23231",
        //     "buying": 0,
        //     "cargoWeight": "string",
        //     "consentStatus": "NOT_INITIATED",
        //     "containerNumber": "string",
        //     "containerSize": "string",
        //     "customerAdvance": 0,
        //     "customerName": "Shivam Singh",
        //     "driverMobileNumber": "8934233423",
        //     "fromLocation": {
        //         "addressLine1": "Kanpur",
        //         "addressLine2": "string",
        //         "city": "Kanpur",
        //         "country": "India",
        //         "landmark": "lucknow",
        //         "latitude": 26.449923,
        //         "longitude": 80.331871,
        //         "pinCode": "222121",
        //         "state": "Uttar Pradesh"
        //     },
        //     "id": "61389f87949c947d6d3e8265",
        //     "pingInterval": "string",
        //     "podUploadURL": "string",
        //     "remarks": "string",
        //     "sealNumber": "string",
        //     "selling": 0,
        //     "status": "ACTIVE",
        //     "toLocation": {
        //         "addressLine1": "string",
        //         "addressLine2": "string",
        //         "city": "Varanasi",
        //         "country": "India",
        //         "landmark": "jaunpur",
        //         "latitude": 25.578291,
        //         "longitude": 83.521332,
        //         "pinCode": "222311",
        //         "state": "Uttar Pradesh"
        //     },
        //     "trackingUrl": "string",
        //     "transitActualEndDate": "2021-09-29T06:03:01.002Z",
        //     "transitEstimatedEndDate": "2021-09-29T06:03:01.002Z",
        //     "transitStartDate": "2021-09-29T06:03:01.002Z",
        //     "transporterAdvance": 0,
        //     "transporterName": "string",
        //     "tripNumber": "string",
        //     "tripType": "SINGLE_TRIP",
        //     "uploadLRDocumentURL": "string",
        //     "vehicleNumber": "string"
        // }
    ],
}