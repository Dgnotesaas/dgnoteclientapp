export interface ILoginModel{
    email: string,
    password: string,
    domain: string,
    rememberMe: boolean 
   
}
