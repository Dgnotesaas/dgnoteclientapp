export interface ICreatePasswordModel {
    code: string,
    email: string,
    newPassword:string
  }
  export const defaultCreatePasswordModel:ICreatePasswordModel=
      {code: "",
      email: "",
    newPassword:""}
  