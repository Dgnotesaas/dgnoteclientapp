export interface IDataTableModel<T> {
    page: number ;
    data:T[];
    totalSize: number;
    sizePerPage: number;
}

