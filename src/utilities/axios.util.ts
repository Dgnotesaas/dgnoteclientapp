﻿import axios from "axios";


axios.interceptors.request.use(
  config => {
    
      if (localStorage.getItem("AuthorizationToken")) {
        config.headers["X-AUTH-TOKEN"] =
          localStorage.getItem("AuthorizationToken") || "";

      }
      let tenant = localStorage.getItem("tenant");
      if (!tenant)
        tenant = "dgnote";
      config.headers["X-TENANT"] = tenant;
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  response => {
    CheckStatus(response.status);
    return response;
  },
  error => {
    CheckStatus(error.response.status);
    return Promise.reject(error);
  }
);

export default axios;

const CheckStatus = (status: number) => {
  switch (status) {
    case 401:
      localStorage.removeItem("AuthorizationToken");
      localStorage.removeItem("Authorize");
      window.location.href = "/login";
      break;
    case 404:
      window.location.href = "/pagenotfound";
      break;
    case 500:
     console.log("error:"+ status)
      break;
    default:
      return true;
  }
};
// export const getSubdomain = () => {
//   let hostname = window.location.hostname.toLowerCase();
//   hostname = hostname.replace("-staging", "");
//   const hostnameSections = hostname.split(".");
//   const first = hostnameSections[0];
//   if (reserved.includes(first)) {
//     return hostnameSections[1];
//   } else {
//     return first;
//   }
// };
