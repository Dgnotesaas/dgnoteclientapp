// const apiBaseUrl="http://dev.tmsapi.dgnote.com:8090/api/v1/";
import {apiBaseUrl} from "./apiUrlGet"
// eslint-disable-next-line import/no-anonymous-default-export
export default {
     
    LOGIN_API:apiBaseUrl+"auth/login",
    LOGOUT_API:apiBaseUrl+"auth/logout",
    RESET_PASSWORD_API:apiBaseUrl+"auth/reset-password",
    FORGOT_PASSWORD_API:apiBaseUrl+"auth/forgot-password",
    CHANGE_PASSWORD_API:apiBaseUrl+"auth/change-password",
    VALIDATE_TOKEN_API:apiBaseUrl+"auth/validate-token",

    BOOKING_ADD_API: apiBaseUrl+"booking",
    BOOKING_GET_API: apiBaseUrl+"booking/search-trips-by-criteria",
    BOOKING_UPDATE_API: apiBaseUrl+"booking/",
    BOOKING_GET_SINGLE_API: apiBaseUrl+"booking/",

    TEMPLATE_GET_API: apiBaseUrl+"booking-template/",
    TEMPLATE_UPDATE_API: apiBaseUrl+"booking-template/",
    TRIP_UPDATE_API:apiBaseUrl+"trip/",
    TRIP_GET_API:apiBaseUrl+"trip/",
    TRIP_CONSENT_CHECK:apiBaseUrl+"trip/check-consent/",
    
    TRACKING_GET_LOCATE_ALL_ACTIVE_TRIPS:apiBaseUrl+"trip/locate-all-active-trips/",
    TRACKING_GET_LOCATE_TRIPS:apiBaseUrl+"trip/locate-trips/",
    TRACKING_GET_LOCATE_ACTIVE_TRIPS:apiBaseUrl+"trip/locate-active-trips/",
    TRACKING_GET_LOCATE_TRIPS_BY_ID:apiBaseUrl+"trip/locate-trips-by-ids/",
    TRACKING_GET_ALL_HIT_BY_ID:apiBaseUrl+"trip/locate-trips-all-hits-by-ids/",

    SHARING_TRACKING_GET_ALL_HIT_BY_ID:apiBaseUrl+"sharing/locate-trips-all-hits-by-ids/",
    SHARING_TRACKING_GET_LOCATE_TRIPS_BY_ID:apiBaseUrl+"sharing/locate-trips-by-ids/",

    HEAlth_GET_ALL_TRIP:apiBaseUrl+"trip/active-trips-dashboard/",

    SHARE_TRACKING_API:apiBaseUrl+"smtp-email/index/",

    USER_PERMISSION_GET_API:apiBaseUrl+"permission/",
    USER_ROLE_GET_API:apiBaseUrl+"role/",
    USER_ROLE_POST_API:apiBaseUrl+"role/",
    USER_ROLE_GET_BY_ID_API:apiBaseUrl+"role/",
    USER_ROLE_UPDATE_API:apiBaseUrl+"role/",
    USER_MANAGEMENT_GET_API:apiBaseUrl+"user/fetch-all-user/",
    USER_MANAGEMENT_POST_API:apiBaseUrl+"user/",
    USER_MANAGEMENT_GET_BY_ID_API:apiBaseUrl+"user/fetch-user-info/",
    USER_USER_UPDATE_API:apiBaseUrl+"user/",

    COMPANY_CREATE_POST_API:apiBaseUrl+"company/",
    COMPANY_GET_ALL_LIST_API:apiBaseUrl+"fetch-all-companies/",
    COMPANY_UPDATED_BY_ID_API:apiBaseUrl+"company/",


    GET_CONFIG_SETTINGS: apiBaseUrl+"config",

    GET_ALL_ACTIVE_ALERT_LOG:apiBaseUrl+"alert/listing/", 
    POST_ALL_ACTIVE_ALERT_LOG:apiBaseUrl+"alert/", 
    UPDATE_ALL_ACTIVE_ALERT_LOG:apiBaseUrl+"alert/", 
    GET_TRANSIT_COUNT:apiBaseUrl+"dashboard/transit-count/",
    GET_CONSENT_COUNT:apiBaseUrl+"dashboard/consent-count/",

    
    HEALTH_TRACKING_GET_LOCATE_TRIPS_BY_ID:apiBaseUrl+"health-log/locate-trips-by-ids/",
    HEALTH_TRACKING_GET_ALL_HIT_BY_ID:apiBaseUrl+"health-log/locate-trips-all-hits-by-ids/",

}