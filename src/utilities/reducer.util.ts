import { IGenericActionReturn } from "./action.util";
// export class IGenericReducerState<T> {
//     public Payload: T;
//     public Fetching: boolean = false;
//     public Error: string = "";
//     constructor(model: T) {
//         this.Payload = model;
//     }
// }
export interface IGenericReducerState<T> {
    Payload: T;
    Fetching: boolean;
    Error: string;
    
  }
export type IGenericReducer<T> = (state: IGenericReducerState<T>, action: IGenericActionReturn<T>) => IGenericReducerState<T>;
