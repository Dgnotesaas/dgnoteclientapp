

export interface IGenericActionReturn<T> {
    type: any;
    Payload: T;
    Error?: string;
}

export type IGenericAction<T> = (param: T, error?: string) => IGenericActionReturn<T>;