export enum TrackingEnum {
    GET_TRACKING_REQUEST="GET_TRACKING_REQUEST",
    GET_TRACKING_SUCCESS="GET_TRACKING_SUCCESS",
    GET_TRACKING_FAILURE="GET_TRACKING_FAILED",
}