export enum RoleEnum {
    GET_ROLE_REQUEST="GET_ROLE_REQUEST",
    GET_ROLE_SUCCESS="GET_ROLE_SUCCESS",
    GET_ROLE_FAILURE="GET_ROLE_FAILED",
}

export enum RoleCreateEnum {
    POST_ROLE_REQUEST="POST_ROLE_REQUEST",
    POST_ROLE_SUCCESS="POST_ROLE_SUCCESS",
    POST_ROLE_FAILURE="POST_ROLE_FAILED",
}

export enum RoleSingleEnum {
    GET_SINGLE_ROLE_REQUEST="GET_SINGLE_ROLE_REQUEST",
    GET_SINGLE_ROLE_SUCCESS="GET_SINGLE_ROLE_SUCCESS",
    GET_SINGLE_ROLE_FAILURE="GET_SINGLE_ROLE_FAILED",
}

export enum RoleUpdateEnum {
    UPDATE_ROLE_REQUEST="UPDATE_ROLE_REQUEST",
    UPDATE_ROLE_SUCCESS="UPDATE_ROLE_SUCCESS",
    UPDATE_ROLE_FAILURE="UPDATE_ROLE_FAILED",
}