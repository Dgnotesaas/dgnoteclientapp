export const alphanumericValidate =new RegExp("^[a-zA-Z0-9]+$");
export const onlyNumberValidate =new RegExp("^[0-9]*$");
export const gstNumValidate =new RegExp("^[0-9]{2}[a-zA-Z0-9]+$");
// export const gstNumValidate =new RegExp("^[1-9]{2}[a-zA-Z0-9]+$");
export const combinationAlphanumaricValidate =new RegExp("^(?=.*[a-zA-Z])(?=.*[0-9])");
export const onlyLettersValidate = /^[a-zA-Z\s]+$/;