import {format,parse} from "date-format-parse"
class Helper {
  static getDateFromString(date:string,dateFormat:string){
      return parse(date,dateFormat);
  }
  static ConvertDateTostring(date:Date,dateFormat:string){
   return format(date, dateFormat);
  }
}
export default Helper;
