interface Iterable<T> {
        [Symbol.iterator](): Iterator<T>;
    }

    declare module "react-csv";
    declare module "react-datepicker";