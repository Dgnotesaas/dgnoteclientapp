
import { BookingContainer, BookingListContainer, HealthListContainer, ConsentcheckupContainer, ChangePasswordContainer, ConsentLogContainer, ForgotPasswordContainer, HomeContainer, LoginContainer, ResetPasswordContainer, TemplateSettingContainer, TrackingContainer,TrackingVehicleContainer, TrackingSingleVehicleContainer,RoleContainer, RoleListContainer, UserContainer, UserListContainer, AlertModuleContainer, TrackSharingContainer} from "./components";
import { UserPermissions } from "./utilities/enum/user_permission.enum";


class RouteModel {
    public Path: string = "";
    public Component: any;
    public Name: string = "";
    public protected: boolean = false;
    public permissions?:any=[];
}

export const routes: RouteModel[] = [
    {
        Component: HomeContainer,
        Name: "User Dashboard",
        Path: "/dashboard",
        protected: true,
        permissions:[UserPermissions.User_Dashboard]
    },
    {
        Component: LoginContainer,
        Name: "Login",
        Path: "/login",
        protected: false,
       
    },
    {
        Component: ForgotPasswordContainer,
        Name: "ForgotPassword",
        Path: "/password/forgot",
        protected: false
    },
    {
        Component: ResetPasswordContainer,
        Name: "ResetPassword",
        Path: "/password/reset",
        protected: true,
        permissions:[UserPermissions.Create_User,UserPermissions.Modify_User,UserPermissions.List_Users,UserPermissions.Add_Booking,UserPermissions.Modify_Booking,UserPermissions.List_Bookings,UserPermissions.Send_Consent,UserPermissions.Share_Tracking,UserPermissions.Track_Vehicle]
    
    },
    {
        Component: ResetPasswordContainer,
        Name: "ResetPassword",
        Path: "/password/reset",
        protected: false
    },
    {
        Component: ChangePasswordContainer,
        Name: "ChangePassword",
        Path: "/password/change",
        protected: true,
        permissions:[UserPermissions.Create_User,UserPermissions.Modify_User,UserPermissions.List_Users,UserPermissions.Add_Booking,UserPermissions.Modify_Booking,UserPermissions.List_Bookings,UserPermissions.Send_Consent,UserPermissions.Share_Tracking,UserPermissions.Track_Vehicle]
    
    },
    {
        Component: BookingContainer,
        Name: "Booking",
        Path: "/booking/add",
        protected: true,
        permissions:[UserPermissions.Add_Booking]
    
    },
    {
        Component: BookingContainer,
        Name: "Booking",
        Path: "/booking/:id",
        protected: true,
        permissions:[UserPermissions.Modify_Booking]
    
    },
    {
        Component: BookingContainer,
        Name: "Booking",
        Path: "/booking/view/:id/:tripid",
        protected: true,
        permissions:[UserPermissions.List_Bookings]
    
    },
    {
        Component: BookingListContainer,
        Name: "Booking-List",
        Path: "/booking-list",
        protected: true,
        permissions:[UserPermissions.List_Bookings]
    
    },
    {
        Component: BookingListContainer,
        Name: "Booking-List",
        Path: "/booking-list/active",
        protected: true,
        permissions:[UserPermissions.List_Bookings]
    
    },
    {
        Component: BookingListContainer,
        Name: "Booking-List",
        Path: "/booking-list/planned",
        protected: true,
        permissions:[UserPermissions.List_Bookings]
    
    },
    {
        Component: BookingListContainer,
        Name: "Booking-List",
        Path: "/booking-list/cancelled",
        protected: true,
        permissions:[UserPermissions.List_Bookings]
    
    },
    {
        Component: BookingListContainer,
        Name: "Booking-List",
        Path: "/booking-list/completed",
        protected: true,
        permissions:[UserPermissions.List_Bookings]
    
    },
    {
        Component: HealthListContainer,
        Name: "Health-List",
        Path: "/health-list",
        protected: true,
        permissions:[UserPermissions.Health_List]
    
    },
    {
        Component: HealthListContainer,
        Name: "Health-List",
        Path: "/health-list/healthlist/active",
        protected: true,
        permissions:[UserPermissions.Health_List]
    },
    {
        Component: HealthListContainer,
        Name: "Health-List",
        Path: "/health-list/healthlist/completed",
        protected: true,
        permissions:[UserPermissions.Health_List]
    },
    {
        Component: HealthListContainer,
        Name: "Consent-List",
        Path: "/consent-list/healthlog",
        protected: true,
        permissions:[UserPermissions.Health_Log]
    },
    {
        Component: AlertModuleContainer,
        Name: "Alert-Module", 
        Path: "/alert-module/alertmodule", 
        protected: true,
        permissions:[UserPermissions.Add_alert,UserPermissions.Add_alert,UserPermissions.List_alert]
    
    },
    {
        Component: AlertModuleContainer,
        Name: "Alert-Module", 
        Path: "/alert-module/alertmodule/edit/:id", 
        protected: true,
        permissions:[UserPermissions.Add_alert,UserPermissions.Add_alert,UserPermissions.List_alert]
    
    },
    {
        Component: TemplateSettingContainer,
        Name: "Template-Setting",
        Path: "/template-setting",
        protected: true,
        permissions:[UserPermissions.Template_Settings]
    
    },
    {
        Component: TrackingContainer,
        Name: "Tracking",
        Path: "/tracking",
        protected: true,
        permissions:[UserPermissions.Share_Tracking,UserPermissions.Track_Vehicle]
    
    }, 
    {
        Component: TrackingContainer,
        Name: "Tracking",
        Path: "/tracking/all_vehicle",
        protected: true,
        permissions:[UserPermissions.Share_Tracking,UserPermissions.Track_Vehicle]
    
    }, 
    {
        Component: TrackingVehicleContainer,
        Name: "Trac Vehicle",
        Path: "/tracking/tracking-vehicle",
        protected: true,
        permissions:[UserPermissions.Share_Tracking,UserPermissions.Track_Vehicle]
    
    },  
    {
        Component: ConsentLogContainer,
        Name: "Consent Log",
        Path: "/consentlog",
        protected: true,
        permissions:[UserPermissions.Send_Consent]
    
    }, 
    {
        Component: TrackingSingleVehicleContainer,
        Name: "Single Tracking",
        Path: "/tracking/single-tracking/:id/:type?",
        protected: true,
        permissions:[UserPermissions.Share_Tracking,UserPermissions.Track_Vehicle]
    
    },  
    {
        Component: RoleContainer,
        Name: "Role",
        Path: "/usermanagement/role/create-role/",
        protected: true,
        permissions:[UserPermissions.Create_Role,UserPermissions.Modify_Role]
    
    }, 
    {
        Component: RoleListContainer,
        Name: "Role List", 
        Path: "/usermanagement/role/role-list/", 
        protected: true,
        permissions:[UserPermissions.List_Roles]
    
    }, 
    {
        Component: RoleContainer,
        Name: "Role",
        Path: "/usermanagement/role/edit/:id",
        protected: true,
        permissions:[UserPermissions.Modify_Role]
    
    }, 
    {
        Component: UserContainer,
        Name: "User", 
        Path: "/usermanagement/user/create-user/", 
        protected: true,
        permissions:[UserPermissions.Create_User,UserPermissions.Modify_User]
    
    }, 
    {
        Component: UserListContainer,
        Name: "User List", 
        Path: "/usermanagement/user/user-list/", 
        protected: true,
        permissions:[UserPermissions.List_Users]
    
    }, 
    {
        Component: UserContainer,
        Name: "User",
        Path: "/usermanagement/user/edit/:id",
        protected: true,
        permissions:[UserPermissions.Modify_User]
    
    },
    {
        Component: TrackSharingContainer,
        Name: "User",
        Path: "/tracksharing/:id/:type?",
        protected: false, 
    },  
];
