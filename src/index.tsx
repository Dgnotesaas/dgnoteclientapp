import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import reportWebVitals from './reportWebVitals';


import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from "react-redux";
import { IAppState } from "../src/models";
import {configureStore,history} from "../src/redux/store";
import { ConnectedRouter } from 'connected-react-router';
import "jquery/dist/jquery";
import "bootstrap/dist/js/bootstrap";
import 'react-toastify/dist/ReactToastify.css';
const baseUrl: any = document
    .getElementsByTagName("base")[0]
    .getAttribute("href")!;

// get the application-wide store instance, prepopulating with state from the server where available.
const initialState: IAppState = (window as any)
    .initialReduxState as IAppState;

const {store,persistor}: any = configureStore( initialState);
ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
        <ConnectedRouter history={history}>
  <App/>
  </ConnectedRouter>
  </PersistGate>
  </Provider>
  ,
  document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
