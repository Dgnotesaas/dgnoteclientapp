import { IEmailTemplateModel  } from "../../models/emailtemplate.model";
import { ApiHelper, IGenericActionReturn } from "../../utilities";
import axios from "../../utilities/axios.util";
import { EmailTemplateEnum } from "../../utilities/enum/emailtemplate.enum";
import { toast } from "react-toastify";


function* fetchAction(list: IEmailTemplateModel): IterableIterator<IGenericActionReturn<IEmailTemplateModel>> {
    const value = yield { type: EmailTemplateEnum.GET_EMAILTEMPLATE_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: EmailTemplateEnum.GET_EMAILTEMPLATE_FAILURE, Payload: list, value };
    } else {
        return { type: EmailTemplateEnum.GET_EMAILTEMPLATE_SUCCESS, Payload: value };
    }
} 

export const postEmail = (model) => { 
    debugger
    return (dispatch: any, getState: any) => {
        let fetchEmailTemplateActions = fetchAction({} as IEmailTemplateModel);
        dispatch(fetchEmailTemplateActions.next().value);
        return axios
            .post(ApiHelper.SHARE_TRACKING_API, model)
            .then(response => {
                if (response.data.status) {
                    const user = response.data.data as IEmailTemplateModel;
                    dispatch(fetchEmailTemplateActions.next(user as any).value);
                    toast.success('Send Email successfully')
                } else {
                    toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                    dispatch(
                        fetchEmailTemplateActions.next(response.data.errorMessage).value
                    );
                }
            })
            .catch(error => {
                if (!window.navigator.onLine) {
                    toast.error('Check network connection', {
                        toastId: 'Check network connection'
                    })
                }
                else {
                    let errorMsg = "Soemthing went wrong."
                    if(error.response && error.response.data && error.response.data.errorMessage){
                        errorMsg = error.response.data.errorMessage;
                    }
                    toast.error(errorMsg, {
                        toastId: 'Something went wrong'
                    });
                }
                dispatch(fetchEmailTemplateActions.next(error.message).value);
            });
    };
};   