
import { PasswordEnum as Enum } from "../../utilities/enum";
import { ICreatePasswordModel } from "../../models";
import {  IGenericAction, IGenericActionReturn ,ApiHelper} from "../../utilities";
import axios from "../../utilities/axios.util";
import { push } from "connected-react-router";
import { toast } from "react-toastify";
function* createPasswordAction(login: ICreatePasswordModel): IterableIterator<IGenericActionReturn<ICreatePasswordModel>> {
    const value = yield { type: Enum.RESET_PASSWORD_REQUEST, Payload: login };
  
    if (typeof value === "string") {
        return { type: Enum.RESET_PASSWORD_FAILURE, Payload: login, value };
    } else {
        return { type: Enum.RESET_PASSWORD_SUCCESS, Payload: value };
    }
} 

export const createPassword = (model) => {
    return (dispatch, getState) => {
        const passwordActions = createPasswordAction({} as ICreatePasswordModel);
        dispatch(passwordActions.next().value);
        return axios
            .put(ApiHelper.CHANGE_PASSWORD_API, model)
            .then(response => {
                if (response.data.status) {
                    dispatch(passwordActions.next().value);
                    toast.success('Your password has been updated.')
                    dispatch(push("/login"));
                } else {
                    dispatch(
                        passwordActions.next(response.data.Status.Message).value
                    );
                    toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(passwordActions.next(error.message).value);
            });
    };
};
 
