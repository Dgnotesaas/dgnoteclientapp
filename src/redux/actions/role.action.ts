import { IRoleAllModel } from "../../models/roleall.model";
import { ApiHelper, IGenericActionReturn } from "../../utilities";
import axios from "../../utilities/axios.util";
import { RoleEnum, RoleCreateEnum, RoleSingleEnum, RoleUpdateEnum  } from "../../utilities/enum/roleall.enum";
import { toast } from "react-toastify";


function* fetchAction(list: IRoleAllModel): IterableIterator<IGenericActionReturn<IRoleAllModel>> {
    const value = yield { type: RoleEnum.GET_ROLE_REQUEST, Payload: list };

    if (typeof value === "string") {
        return { type: RoleEnum.GET_ROLE_FAILURE, Payload: list, value };
    } else {
        return { type: RoleEnum.GET_ROLE_SUCCESS, Payload: value };
    }
}

function* fetchCreateAction(list: IRoleAllModel): IterableIterator<IGenericActionReturn<IRoleAllModel>> {
    const value = yield { type: RoleCreateEnum.POST_ROLE_REQUEST, Payload: list };

    if (typeof value === "string") {
        return { type: RoleCreateEnum.POST_ROLE_FAILURE, Payload: list, value };
    } else {
        return { type: RoleCreateEnum.POST_ROLE_SUCCESS, Payload: value };
    }
}

function* fetchSingleAction(list: IRoleAllModel): IterableIterator<IGenericActionReturn<IRoleAllModel>> {
    const value = yield { type: RoleSingleEnum.GET_SINGLE_ROLE_REQUEST, Payload: list };

    if (typeof value === "string") {
        return { type: RoleSingleEnum.GET_SINGLE_ROLE_FAILURE, Payload: list, value };
    } else {
        return { type: RoleSingleEnum.GET_SINGLE_ROLE_SUCCESS, Payload: value };
    }
}

function* updateAction(list: IRoleAllModel): IterableIterator<IGenericActionReturn<IRoleAllModel>> {
    const value = yield { type: RoleUpdateEnum.UPDATE_ROLE_REQUEST, Payload: list };

    if (typeof value === "string") {
        return { type: RoleUpdateEnum.UPDATE_ROLE_FAILURE, Payload: list, value };
    } else {
        return { type: RoleUpdateEnum.UPDATE_ROLE_SUCCESS, Payload: value };
    }
}

export const getAllRole = () => {
    return (dispatch: any) => {
        let fetchPermissionActions = fetchAction({} as IRoleAllModel);
        dispatch(fetchPermissionActions.next().value);

        return axios
            .get(ApiHelper.USER_ROLE_GET_API)
            .then(response => {
                if (response.data.status) {
                    const role = response.data.data as IRoleAllModel;
                    dispatch(fetchPermissionActions.next(role as any).value);
                } else {
                    dispatch(
                        fetchPermissionActions.next(response.data.errorMessage).value
                    );

                }
            })
            .catch(error => {
                if (!window.navigator.onLine) {
                    toast.error('Check network connection', {
                        toastId: 'Check network connection'
                    })
                }
                else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                    });
                }
                dispatch(fetchPermissionActions.next(error.message).value);
            });
    };
};

export const postActiveRole = (model) => {

    return (dispatch: any, getState: any) => {
        let fetchPermissionActions = fetchCreateAction({} as IRoleAllModel);
        dispatch(fetchPermissionActions.next().value);

        return axios
            .post(ApiHelper.USER_ROLE_POST_API, model)
            .then(response => {
                if (response.data.status) {
                    const role = response.data.data as IRoleAllModel;
                    dispatch(fetchPermissionActions.next(role as any).value);
                    toast.success('Role Added successfully')
                } else {
                    dispatch(
                        fetchPermissionActions.next(response.data.errorMessage).value
                    );
                }
            })
            .catch(error => {
                if (!window.navigator.onLine) {
                    toast.error('Check network connection', {
                        toastId: 'Check network connection'
                    })
                }
                else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                    });
                }
                dispatch(fetchPermissionActions.next(error.message).value);
            });
    };
}

export const getSingleRole = (id) => {
  
    return (dispatch, getState) => {
        let fetchRoleActions = fetchAction({} as IRoleAllModel);
        dispatch(fetchRoleActions.next().value);
        return axios
            .get(ApiHelper.USER_ROLE_GET_BY_ID_API + id)
            .then(response => {
                if (response.data.status) {
                    const role = response.data.data as IRoleAllModel;
                    dispatch(fetchRoleActions.next(role as any).value);
                } else {
                    toast.error(response.data.errorMessage);
                }
            })
            .catch(error => {
                console.log(error)
            
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
            });
    };
};

export const updateRole = (roleDetail) => {
  
    return (dispatch, getState) => {
        let fetchRoleActions = updateAction({} as IRoleAllModel);
        dispatch(fetchRoleActions.next().value);
        return axios
            .put(ApiHelper.USER_ROLE_UPDATE_API + roleDetail.id, roleDetail)
            .then(response => {
                if (response.data.status) {
                    const role = response.data.data as IRoleAllModel;
                    dispatch(fetchRoleActions.next(role as any).value);
                    toast.success('Role Update successfully')
                } else {
                    toast.error(response.data.errorMessage);
                }
            })
            .catch(error => {
                console.log(error)
            
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
            });
    };
};
