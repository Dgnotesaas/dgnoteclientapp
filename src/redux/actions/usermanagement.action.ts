import { IUsermanagementModel  } from "../../models/usermanagement.model";
import { ApiHelper, IGenericActionReturn } from "../../utilities";
import axios from "../../utilities/axios.util";
import { UsermanagementEnum, UpdateUsermanagementEnum } from "../../utilities/enum/usermanagement.enum";
import { toast } from "react-toastify";


function* fetchAction(list: IUsermanagementModel): IterableIterator<IGenericActionReturn<IUsermanagementModel>> {
    const value = yield { type: UsermanagementEnum.GET_MANAGEMENT_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: UsermanagementEnum.GET_MANAGEMENT_FAILURE, Payload: list, value };
    } else {
        return { type: UsermanagementEnum.GET_MANAGEMENT_SUCCESS, Payload: value };
    }
}

function* updateAction(list: IUsermanagementModel): IterableIterator<IGenericActionReturn<IUsermanagementModel>> {
    const value = yield { type: UpdateUsermanagementEnum.UPDATE_MANAGEMENT_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: UpdateUsermanagementEnum.UPDATE_MANAGEMENT_FAILURE, Payload: list, value };
    } else {
        return { type: UpdateUsermanagementEnum.UPDATE_MANAGEMENT_SUCCESS, Payload: value };
    }
}

export const getAllUsers = () => { 
    return (dispatch:any) => {
        let fetchUserActions = fetchAction({} as IUsermanagementModel);
        dispatch(fetchUserActions.next().value); 

        return axios
            .get(ApiHelper.USER_MANAGEMENT_GET_API)
            .then(response => {
                if (response.data.status) {
                    const usermng =response.data.data as IUsermanagementModel;
                    dispatch(fetchUserActions.next(usermng as any).value);
                }else {
                    dispatch(
                        fetchUserActions.next(response.data.errorMessage).value
                    );
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchUserActions.next(error.message).value);
            });
    };
}; 

export const postUser = (model) => { 
    return (dispatch: any, getState: any) => {
        let fetchUserActions = fetchAction({} as IUsermanagementModel);
        dispatch(fetchUserActions.next().value);

        return axios
            .post(ApiHelper.USER_MANAGEMENT_POST_API, model)
            .then(response => {
                console.log('response', response);
                
                if (response.data.status) {
                    const user = response.data.data as IUsermanagementModel;
                    dispatch(fetchUserActions.next(user as any).value);
                    toast.success('Role Added successfully')
                } else {
                    toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                    dispatch(
                        fetchUserActions.next(response.data.errorMessage).value
                    );

                }
            })
            .catch(error => {
                if (!window.navigator.onLine) {
                    toast.error('Check network connection', {
                        toastId: 'Check network connection'
                    })
                }
                else {
                    let errorMsg = "Soemthing went wrong."
                    if(error.response && error.response.data && error.response.data.errorMessage){
                        errorMsg = error.response.data.errorMessage;
                    }
                    toast.error(errorMsg, {
                        toastId: 'Something went wrong'
                    });
                }
                dispatch(fetchUserActions.next(error.message).value);
            });
    };
}; 

export const getSingleUser = (id) => {
  
    return (dispatch, getState) => {
        let fetchUserActions = fetchAction({} as IUsermanagementModel);
        dispatch(fetchUserActions.next().value);
        return axios
            .get(ApiHelper.USER_MANAGEMENT_GET_BY_ID_API + id)
            .then(response => {
                console.log('user response', response);
                
                if (response.data.status) {
                    const user = response.data.data as IUsermanagementModel;
                    dispatch(fetchUserActions.next(user as any).value);
                } else {
                    toast.error(response.data.errorMessage);
                }
            })
            .catch(error => {
                console.log(error)
            
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
            });
    };
};

export const updateUser = (userDetail) => {
  
    return (dispatch, getState) => {
        let fetchUserActions = updateAction({} as IUsermanagementModel);
        dispatch(fetchUserActions.next().value);
        console.log('userDetail', userDetail);
        
        return axios
            .put(ApiHelper.USER_USER_UPDATE_API + userDetail.id, userDetail)
            .then(response => {
                console.log('response ', response);
                if (response.data.status) {
                    const role = response.data.data as IUsermanagementModel;
                    dispatch(fetchUserActions.next(role as any).value);
                    toast.success('Role Update successfully')
                } else {
                    toast.error(response.data.errorMessage);
                }
            })
            .catch(error => {
                console.log(error)
            
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
            });
    };
};