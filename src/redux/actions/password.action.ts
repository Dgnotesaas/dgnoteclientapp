
import { PasswordEnum as Enum, CreatePasswordEnum } from "../../utilities/enum";
import { IPasswordModel, IForgetPasswordModel, ICreatePasswordModel} from "../../models";
import {  IGenericAction, IGenericActionReturn ,ApiHelper} from "../../utilities";
import axios from "../../utilities/axios.util";
import { push } from "connected-react-router";
import {toast} from "react-toastify";

function* createPasswordAction(list: ICreatePasswordModel): IterableIterator<IGenericActionReturn<ICreatePasswordModel>> {
    const error = yield { type: CreatePasswordEnum.USER_CREATE_PASSWORD_REQUEST, Payload: list };
    if (typeof error === "string") {
        return { type: CreatePasswordEnum.USER_CREATE_PASSWORD_FAILURE, Payload: list, error };
    } else {
        return { type: CreatePasswordEnum.USER_CREATE_PASSWORD_SUCCESS, Payload: list };
    }
}

function* passwordForgetAction(email: IForgetPasswordModel): IterableIterator<IGenericActionReturn<IForgetPasswordModel>> {
    const error = yield { type: Enum.FORGOT_PASSWORD_REQUEST, Payload: email };
    if (typeof error === "string") {
        return { type: Enum.FORGOT_PASSWORD_FAILURE, Payload: email, error };
    } else {
        return { type: Enum.FORGOT_PASSWORD_SUCCESS, Payload: email };
    }
}

function* passwordResetAction(password: IPasswordModel): IterableIterator<IGenericActionReturn<IPasswordModel>> {
    const error = yield { type: Enum.RESET_PASSWORD_REQUEST, Payload: password };
    if (typeof error === "string") {
        return { type: Enum.RESET_PASSWORD_FAILURE, Payload: password, error };
    } else {
        return { type: Enum.RESET_PASSWORD_SUCCESS, Payload: password };
    }
}
function* passwordChangeAction(password: IPasswordModel): IterableIterator<IGenericActionReturn<IPasswordModel>> {
    const error = yield { type: Enum.CHANGE_PASSWORD_REQUEST, Payload: password };
    if (typeof error === "string") {
        return { type: Enum.CHANGE_PASSWORD_FAILURE, Payload: password, error };
    } else {
        return { type: Enum.CHANGE_PASSWORD_SUCCESS, Payload: password };
    }
}

const updatepasswordState: IGenericAction<IPasswordModel> = Register => {
    return {
        Payload: Register,
        type: Enum.UPDATE_PASSWORD_MODEL
    };
};
const invalidModel: IGenericAction<IPasswordModel> = Register => {
    return {
        Error: "Invalid password Model",
        Payload: Register,
        type: Enum.INVALID_PASSWORD_MODEL
    };
};

const clearPassModel: IGenericAction<IPasswordModel> = PassWordModel => {
    return {
        Error: "Invalid password Model",
        Payload: PassWordModel,
        type: Enum.CLEAR_PASSWORD_MODEL
    };
};

export const passwordForgot = (email: IForgetPasswordModel) => {
    return (dispatch, getState) => {
        const passwordActions = passwordForgetAction(email);
        dispatch(passwordActions.next().value);
        
        return axios
            .put(ApiHelper.FORGOT_PASSWORD_API, email)
            .then(response => {
                if (response.data.status) {
                    dispatch(passwordActions.next().value);
                    toast.success('A Code has been sent to your registered email address!', {toastId:'linkresetpasswrd'})
                    dispatch(push("/password/reset"));
                } else {
                    dispatch(   
                        passwordActions.next(response.data.Status.Message).value
                    );
                    toast.error(response.data.errorMessage);
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                    let errorMsg = "Soemthing went wrong."
                    if(error.response && error.response.data && error.response.data.errorMessage){
                        errorMsg = error.response.data.errorMessage;
                    }
                    toast.error(errorMsg);
                }
                console.log('error', error);
                
                dispatch(passwordActions.next(error.message).value);
            });
    };
};

export const passwordReset = (password: IPasswordModel) => {
    return (dispatch, getState) => {
        const passwordActions = passwordResetAction(password);
        dispatch(passwordActions.next().value);
        return axios
            .put(ApiHelper.RESET_PASSWORD_API, password)
            .then(response => {
                if (response.data.status) {
                    dispatch(passwordActions.next().value);
                    toast.success('Your password has been reset.')
                    dispatch(push("/login"));
                } else {
                    dispatch(
                        passwordActions.next(response.data.Status.Message).value
                    );
                    toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                    let errorMsg = "Soemthing went wrong."
                    if(error.response && error.response.data && error.response.data.errorMessage){
                        errorMsg = error.response.data.errorMessage;
                    }
                    toast.error(errorMsg);
                }
                dispatch(passwordActions.next(error.message).value);
            });
    };
};

export const createPassword = (model) => {
    return (dispatch, getState) => {
        const passwordActions = createPasswordAction({} as ICreatePasswordModel);
        dispatch(passwordActions.next().value);
        return axios
            .put(ApiHelper.CHANGE_PASSWORD_API, model)
            .then(response => {
                if (response.data.status) {
                    dispatch(passwordActions.next().value);
                    toast.success('Your password has been updated.')
                    dispatch(push("/login"));
                } else {
                    dispatch(
                        passwordActions.next(response.data.Status.Message).value
                    );
                    toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                    let errorMsg = "Soemthing went wrong."
                    if(error.response && error.response.data && error.response.data.errorMessage){
                        errorMsg = error.response.data.errorMessage;
                    }
                    toast.error(errorMsg);
                }
                dispatch(passwordActions.next(error.message).value);
            });
    };
};

export const changePassword = (password: IPasswordModel) => {
    return (dispatch, getState) => {
        const passwordActions = passwordChangeAction(password);
        dispatch(passwordActions.next().value);
        return axios
            .put(ApiHelper.RESET_PASSWORD_API, password)
            .then(response => {
                if (response.data.status) {
                    dispatch(passwordActions.next().value);
                    toast.success("Your password has been successfully updated.");
                    localStorage.setItem("Authorize", "False");
                    dispatch(push("/login"));
                    window.location.reload();
                } else {
                    dispatch(
                        passwordActions.next(response.data.Status.Message).value
                    );
                    toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                    let errorMsg = "Soemthing went wrong."
                    if(error.response && error.response.data && error.response.data.errorMessage){
                        errorMsg = error.response.data.errorMessage;
                    }
                toast.error(errorMsg, {
                    toastId: 'Something went wrong',autoClose: 5000,
                  });
                }
                dispatch(passwordActions.next(error.response.data.errorMessage).value);
            });
    };
};

export const updateProperties = (Register: IPasswordModel) => {
    return (dispatch, getState) => {
        dispatch(updatepasswordState(Register));
    };
};
export const invalidPasswordModel = () => {
    return (dispatch, getState) => {
        dispatch(invalidModel({} as IPasswordModel));
    };
};

export const clearPasswordModel = (passwordModel: IPasswordModel) => {
    return (dispatch, getState) => {
        dispatch(clearPassModel(passwordModel));
    };
};
