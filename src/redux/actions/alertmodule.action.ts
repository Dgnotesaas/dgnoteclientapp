import { IAlertList } from "../../models/alertmodule.model";
import { ApiHelper, IGenericActionReturn } from "../../utilities";
import axios from "../../utilities/axios.util";
import { AlertEnum} from "../../utilities/enum/alertmodule.enum";
import { push } from "connected-react-router";
import { RoleEnum, RoleCreateEnum, RoleSingleEnum, RoleUpdateEnum  } from "../../utilities/enum/roleall.enum";
import { toast } from "react-toastify"; 

function* fetchAction(list: IAlertList): IterableIterator<IGenericActionReturn<IAlertList>> {
    const value = yield { type: RoleEnum.GET_ROLE_REQUEST, Payload: list };

    if (typeof value === "string") {
        return { type: RoleEnum.GET_ROLE_FAILURE, Payload: list, value };
    } else {
        return { type: RoleEnum.GET_ROLE_SUCCESS, Payload: value };
    }
}

function* fetchCreateAction(list: IAlertList): IterableIterator<IGenericActionReturn<IAlertList>> {
    const value = yield { type: RoleCreateEnum.POST_ROLE_REQUEST, Payload: list };

    if (typeof value === "string") {
        return { type: RoleCreateEnum.POST_ROLE_FAILURE, Payload: list, value };
    } else {
        return { type: RoleCreateEnum.POST_ROLE_SUCCESS, Payload: value };
    }
}

export const getAlertModule = (status) => {
    return (dispatch: any) => {
        let fetchAlertModuleActions = fetchAction({} as IAlertList);
        dispatch(fetchAlertModuleActions.next().value);
        return axios
            .get(ApiHelper.GET_ALL_ACTIVE_ALERT_LOG + status)
            .then(response => {                 
                if (response.data.status) {

                    let alertlist = response.data.data as IAlertList;
                    console.log('apireturn', alertlist);
                    
                    let getAlertList = Object.values(alertlist).map((item: any)=>{
                        if(item.userRule.length < 0) {
                            return ({
                                "id": item.id,
                                "description": item.description,
                                "rule": item.rule,
                                "status": item.status,
                                "userRule": {
                                  "companyId": "",
                                  "description": "",
                                  "emailSubject": "",
                                  "emailStatus": "INACTIVE",
                                  "rule": "",
                                  "id": "",
                                  "ruleId": "",
                                  "smsStatus": "INACTIVE",
                                  "tenantName": "",
                                  "value": "",
                                  "whatsappStatus": "INACTIVE"
                                }
                            })
                        } else {
                            return ({
                                "id": item.id,
                                "description": item.description,
                                "rule": item.rule,
                                "status": item.status,
                                "userRule": {
                                    "companyId": item.userRule[0].companyId,
                                    "description": item.userRule[0].description,
                                    "emailSubject": item.userRule[0].emailSubject,
                                    "emailStatus": item.userRule[0].emailStatus,
                                    "id": item.userRule[0].id,
                                    "rule": item.userRule[0].rule,
                                    "ruleId": item.userRule[0].ruleId,
                                    "smsStatus": item.userRule[0].smsStatus,
                                    "tenantName": item.userRule[0].tenantName,
                                    "value": item.userRule[0].value,
                                    "whatsappStatus": item.userRule[0].whatsappStatus
                                }
                            })
                        }
                    }) 
                    
                    dispatch(fetchAlertModuleActions.next(getAlertList as any).value);
                } else {
                    dispatch(
                        fetchAlertModuleActions.next(response.data.errorMessage).value
                    );

                }
            })
            .catch(error => {
                if (!window.navigator.onLine) {
                    toast.error('Check network connection', {
                        toastId: 'Check network connection'
                    })
                }
                else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                    });
                }
                dispatch(fetchAlertModuleActions.next(error.message).value);
            });
    };
};  

export const postAlertModule = (model) => {

    return (dispatch: any, getState: any) => {
        let fetchPermissionActions = fetchCreateAction({} as IAlertList);
        dispatch(fetchPermissionActions.next().value);
        let userrule = Object.values(model).map((item: any) => {
            return(item.userRule)
        })
        return axios
            .post(ApiHelper.POST_ALL_ACTIVE_ALERT_LOG, userrule)
            .then(response => {
                if (response.data.status) {
                    const role = response.data.data as IAlertList;
                    dispatch(fetchPermissionActions.next(role as any).value);
                    if(userrule[0].value == '') {
                        toast.success('Alert Module Added successfully')
                    } else {
                        toast.success('Alert Module Updated successfully')
                    }
                    // dispatch(push("/booking-list/active"));
                    // window.location.reload(false);
                } else {
                    dispatch(
                        fetchPermissionActions.next(response.data.errorMessage).value
                    );
                }
            })
            .catch(error => {
                if (!window.navigator.onLine) {
                    toast.error('Check network connection', {
                        toastId: 'Check network connection'
                    })
                }
                else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                    });
                }
                dispatch(fetchPermissionActions.next(error.message).value);
            });
    };
}

export const getAlertModulebyId = (id) => {
 
    return (dispatch: any) => {
        let fetchAlertModuleActions = fetchAction({} as IAlertList);
        dispatch(fetchAlertModuleActions.next().value);
        return axios
            .put(ApiHelper.UPDATE_ALL_ACTIVE_ALERT_LOG + id)
            .then(response => {                 
                if (response.data.status) {

                    const alertlist = response.data.data as IAlertList;

                    // console.log('response', fetchAlertModuleActions.next(tracking as any).value);
                    dispatch(fetchAlertModuleActions.next(alertlist as any).value);
                } else {
                    dispatch(
                        fetchAlertModuleActions.next(response.data.errorMessage).value
                    );

                }
            })
            .catch(error => {
                if (!window.navigator.onLine) {
                    toast.error('Check network connection', {
                        toastId: 'Check network connection'
                    })
                }
                else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                    });
                }
                dispatch(fetchAlertModuleActions.next(error.message).value);
            });
    };
}
 
