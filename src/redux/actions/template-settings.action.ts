import {  TemplateSettingEnum} from "../../utilities/enum";
import {  ApiHelper, axios, IGenericAction, IGenericActionReturn } from "../../utilities";
import { toast } from "react-toastify";
import { ITemplateSettingModel ,defaultTemplateSettingValue} from "../../models/template-setting.model";


function* updateTemplateAction(templateSettings: ITemplateSettingModel): IterableIterator<IGenericActionReturn<ITemplateSettingModel>> {
    const value = yield { type: TemplateSettingEnum.UPDATE_TEMPLATESETTING_REQUST, Payload: templateSettings };
    if (typeof value === "string") {
        
        return { type: TemplateSettingEnum.UPDATE_TEMPLATESETTING_FAILURE, Payload: templateSettings, value };
    } else {
  
        return { type: TemplateSettingEnum.UPDATE_TEMPLATESETTING_SUCCESS, Payload: value };
    }
}

function* fetchTemplateAction(templateSettings: ITemplateSettingModel): IterableIterator<IGenericActionReturn<ITemplateSettingModel>> {
    const value = yield { type: TemplateSettingEnum.GET_TEMPLATESETTING_REQUST, Payload: templateSettings };
    if (typeof value === "string") {
        
        return { type: TemplateSettingEnum.GET_TEMPLATESETTING_FAILURE, Payload: templateSettings, value };
    } else {
  
        return { type: TemplateSettingEnum.GET_TEMPLATESETTING_SUCCESS, Payload: value };
    }
}
const updateTemplateState: IGenericAction<ITemplateSettingModel> = (templateSettings: ITemplateSettingModel) => {
    return {
        Error: "",
        Payload: templateSettings,
        type: TemplateSettingEnum.UPDATE_TEMPLATESETTING_SUCCESS
    };
};
export const updateTemplate = (templateId: string,templateSettings: ITemplateSettingModel) => {

    return (dispatch, getState) => {
        const updateTemplateActions = updateTemplateAction(templateSettings);  
        dispatch(updateTemplateActions.next().value);
        return axios
            .put(ApiHelper.TEMPLATE_UPDATE_API + templateId, templateSettings)
            .then(response => {
              
                if (response.data.status) {
                    
                    dispatch(updateTemplateActions.next(response.data.data).value);
                    toast.success('Template Settings Successfully Updated')
                } else {
                    toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                    dispatch(
                        updateTemplateActions.next(response.data.errorMessage).value
                    );
                }
            })
            .catch(error => {
                console.log(error)
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(updateTemplateActions.next(error.message).value);
            });
    };
};


export const getTemplateSettings = () => {
    
     return (dispatch:any, getState:any) => {
        const fetchTemplateActions = fetchTemplateAction({...defaultTemplateSettingValue});  
        dispatch(fetchTemplateActions.next().value);
        return axios
             .get(ApiHelper.TEMPLATE_GET_API)
             .then(response => {
                if (response.data.status) {
                    dispatch(fetchTemplateActions.next(response.data.data).value);
                 }else {
                     Error(response.data.Status.Message);
                 }
             })
             .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
             });
     };
 }
 

export const updateProperties = (templateSettings: ITemplateSettingModel) => {
    return (dispatch, getState) => {
        dispatch(updateTemplateState(templateSettings));
    };
};