
import {  IGenericAction, IGenericActionReturn ,ApiHelper} from "../../utilities";
import axios from "../../utilities/axios.util";
import { BookingListEnum } from "../../utilities/enum/booking.enum";
import { IBookingListModel } from "../../models/booking-list.model";
import moment from "moment";

import { IBookingModel } from "../../models";
import { toast } from "react-toastify";
import { IListSearchRequstModel } from "../../models/list-search-request.model";


function* fetchAction(list: IBookingListModel): IterableIterator<IGenericActionReturn<IBookingListModel>> {
    const value = yield { type: BookingListEnum.GET_BOOKINGS_LIST_REQUST, Payload: list };
  
    if (typeof value === "string") {
        return { type: BookingListEnum.GET_BOOKINGS_LIST_FAILURE, Payload: list, value };
    } else {
        return { type: BookingListEnum.GET_BOOKINGS_LIST_SUCCESS, Payload: value };
    }
}




const updateBookingState: IGenericAction<IBookingListModel> = (bookingList:any) => {
    return {
        Payload: bookingList,
        type: BookingListEnum.GET_BOOKINGS_LIST_SUCCESS,
    };
};



export const updateBookingListProperties = (bookingList: IBookingListModel) => {
    return (dispatch:any, getState:any) => {
        dispatch(updateBookingState(bookingList));
    };
};


export const getBookings = (searchModel: IListSearchRequstModel) => {

    return (dispatch:any, getState:any) => {
        let fetchBookingsActions = fetchAction({} as IBookingListModel);
        dispatch(fetchBookingsActions.next().value);         
        
        if(searchModel.status == 'ALL') {
            searchModel.status = 'ACTIVE'
            searchModel.afterTransitEndDateCriteria = moment().format("YYYY-MM-DD")
        }

        return axios
            .post(ApiHelper.BOOKING_GET_API, searchModel)
            .then(response => {
                if (response.data.status) {
                    const bookings =response.data.data as IBookingListModel;
                    const ary :IBookingListModel[]=[];
                    ary.push(bookings);
                    dispatch(fetchBookingsActions.next(bookings as any).value);
                }else {
                    dispatch(
                        fetchBookingsActions.next(response.data.errorMessage).value
                    );
                    Error(response.data.Status.Message);
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchBookingsActions.next(error.message).value);
            });
    };
};

export const getBookingById = (id: string) => {
   // return (dispatch:any, getState:any) => {
       

        return axios
            .get(ApiHelper.BOOKING_GET_SINGLE_API + id);
            // .then(response => {
                
            //     if (response.data.status) {
                    
            //     }else {
                   
            //         Error(response.data.Status.Message);
            //     }
            // })
            // .catch(error => {
              
            // });
    //};
}


export const updateExtendedDate = (model:IBookingModel) => {
if(model){

    return (dispatch:any, getState:any) => {

        return axios
            .put(ApiHelper.BOOKING_UPDATE_API+ model.id, model)
            .then(response => {
                if (response.data.status) {
                  
                }else {
                   
                    Error(response.data.Status.Message);
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
            });
    };
}
};


export const getBookingsForexport = (searchModel: IListSearchRequstModel) => {

        return axios
            .post(ApiHelper.BOOKING_GET_API, searchModel);
            
    
};
