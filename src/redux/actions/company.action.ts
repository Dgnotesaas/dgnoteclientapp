import { ICompanyModel  } from "../../models/company.model";
import { ApiHelper, IGenericActionReturn } from "../../utilities";
import axios from "../../utilities/axios.util";
import { CompanyEnum } from "../../utilities/enum/company.enum";
import { toast } from "react-toastify";


function* fetchAction(list: ICompanyModel): IterableIterator<IGenericActionReturn<ICompanyModel>> {
    const value = yield { type: CompanyEnum.GET_COMPANY_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: CompanyEnum.GET_COMPANY_FAILURE, Payload: list, value };
    } else {
        return { type: CompanyEnum.GET_COMPANY_SUCCESS, Payload: value };
    }
}

function* updateAction(list: ICompanyModel): IterableIterator<IGenericActionReturn<ICompanyModel>> {
    const value = yield { type: CompanyEnum.UPDATE_COMPANY_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: CompanyEnum.UPDATE_COMPANY_FAILURE, Payload: list, value };
    } else {
        return { type: CompanyEnum.UPDATE_COMPANY_SUCCESS, Payload: value };
    }
}

export const getAllCompany = () => { 
    return (dispatch:any) => {
        let fetchUserActions = fetchAction({} as ICompanyModel);
        dispatch(fetchUserActions.next().value); 

        return axios
            .get(ApiHelper.COMPANY_GET_ALL_LIST_API)
            .then(response => {
                if (response.data.status) {
                    const companylist =response.data.data as ICompanyModel;
                    dispatch(fetchUserActions.next(companylist as any).value);
                }else {
                    dispatch(
                        fetchUserActions.next(response.data.errorMessage).value
                    );
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchUserActions.next(error.message).value);
            });
    };
}; 

export const createCompany = (model) => { 
    return (dispatch: any, getState: any) => {
        let fetchUserActions = fetchAction({} as ICompanyModel);
        dispatch(fetchUserActions.next().value);

        return axios
            .post(ApiHelper.COMPANY_CREATE_POST_API, model)
            .then(response => {
                console.log('response', response);
                
                if (response.data.status) {
                    const user = response.data.data as ICompanyModel;
                    dispatch(fetchUserActions.next(user as any).value);
                    toast.success('Company Created Successfully')
                } else {
                    dispatch(
                        fetchUserActions.next(response.data.errorMessage).value
                    );

                }
            })
            .catch(error => {
                if (!window.navigator.onLine) {
                    toast.error('Check network connection', {
                        toastId: 'Check network connection'
                    })
                }
                else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                    });
                }
                dispatch(fetchUserActions.next(error.message).value);
            });
    };
}; 

export const getSingleCompany = (id) => {
  
    return (dispatch, getState) => {
        let fetchUserActions = fetchAction({} as ICompanyModel);
        dispatch(fetchUserActions.next().value);
        return axios
            .get(ApiHelper.USER_MANAGEMENT_GET_BY_ID_API + id)
            .then(response => {
                console.log('user response', response);
                
                if (response.data.status) {
                    const user = response.data.data as ICompanyModel;
                    dispatch(fetchUserActions.next(user as any).value);
                } else {
                    toast.error(response.data.errorMessage);
                }
            })
            .catch(error => {
                console.log(error)
            
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
            });
    };
};

export const updateCompany = (companyDetail) => {
  
    return (dispatch, getState) => {
        let fetchUserActions = updateAction({} as ICompanyModel);
        dispatch(fetchUserActions.next().value);
        console.log('userDetail', companyDetail);
        
        return axios
            .put(ApiHelper.COMPANY_UPDATED_BY_ID_API + companyDetail.id, companyDetail)
            .then(response => {
                console.log('response ', response);
                if (response.data.status) {
                    const role = response.data.data as ICompanyModel;
                    dispatch(fetchUserActions.next(role as any).value);
                    toast.success('Company details Update successfully')
                } else {
                    toast.error(response.data.errorMessage);
                }
            })
            .catch(error => {
                console.log(error)
            
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
            });
    };
};