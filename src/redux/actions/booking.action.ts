import { BookingEnum as Enum} from "../../utilities/enum";
import { defaultBookingValue, IBookingModel} from "../../models";
import {  ApiHelper, axios, Helper, IGenericAction, IGenericActionReturn } from "../../utilities";
import { toast } from "react-toastify";
import { push } from "connected-react-router";
import { defaultTripValue } from "../../models/booking.model";


function* createBookingAction(bookingDetail: IBookingModel): IterableIterator<IGenericActionReturn<IBookingModel>> {
    const value = yield { type: Enum.CREATE_BOOKING_REQUEST, Payload: bookingDetail };
    if (typeof value === "string") {
        
        return { type: Enum.CREATE_BOOKING_FAILURE, Payload: bookingDetail, value };
    } else {
  
        return { type: Enum.CREATE_BOOKING_SUCCESS, Payload: value };
    }
}
function* updateBookingAction(bookingDetail: IBookingModel): IterableIterator<IGenericActionReturn<IBookingModel>> {
    const value = yield { type: Enum.CREATE_BOOKING_REQUEST, Payload: bookingDetail };
    if (typeof value === "string") {
        return { type: Enum.CREATE_BOOKING_FAILURE, Payload: bookingDetail, value };
    } else {
  
        return { type: Enum.CREATE_BOOKING_SUCCESS, Payload: value };
    }
}
function* getBookingAction(bookingDetail:IBookingModel): IterableIterator<IGenericActionReturn<IBookingModel>> {
    const value = yield { type: Enum.GET_BOOKING_REQUEST, Payload: bookingDetail };
    if (typeof value === "string") {
        return { type: Enum.GET_BOOKING_FAILURE, Payload: bookingDetail, Error: value };
    } else {
        return { type: Enum.GET_BOOKING_SUCCESS, Payload: value };
    }
}
const updateBookingState: IGenericAction<IBookingModel> = (bookingDetail: IBookingModel) => {
    return {
        Error: "",
        Payload: bookingDetail,
        type: Enum.UPDATE_BOOKING_STATE_SUCCESS
    };
};
export const SaveBooking = (bookingDetail: IBookingModel) => {

bookingDetail.trips = bookingDetail.trips.map(m => { 
    m.transitEstimatedEndDate = new Date(m.transitEstimatedEndDate);
    m.transitActualEndDate = new Date(m.transitEstimatedEndDate);
    m.transitStartDate = new Date(m.transitStartDate);
    return m;
})
    return (dispatch, getState) => {

        const createBookingActions = createBookingAction(bookingDetail);  
        dispatch(createBookingActions.next().value);
        return axios
            .post(ApiHelper.BOOKING_ADD_API, bookingDetail)
            .then(response => {     
                if (response.data.status) {
                
                    const bookingModel:any=JSON.parse(JSON.stringify({...defaultBookingValue}));
                    
                    dispatch(createBookingActions.next(bookingModel).value);
                    toast.success('Booking saved Successfully')
                    dispatch(push("/booking-list/active"));
                } else {
                    toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                    dispatch(
                        createBookingActions.next(response.data.errorMessage).value
                    );
                }
            })
            .catch(error => {
                console.log(error)
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                    let errorMsg = "Soemthing went wrong."
                    if(error.response && error.response.data && error.response.data.errorMessage){
                        errorMsg = error.response.data.errorMessage;
                    }
                toast.error(errorMsg, {
                    toastId: 'Something went wrong',autoClose: 5000,
                  });
                }
                dispatch(createBookingActions.next(error.message).value);
            });
    };
};
export const updateBooking = (bookingDetail: IBookingModel) => {
    // bookingDetail.trips = bookingDetail.trips.map(m => { 
    //     m.transitEstimatedEndDate = Helper.getDateFromString(m.transitEstimatedEndDate as string,"YYYY-MM-DD");
    //     m.transitActualEndDate =   m.transitEstimatedEndDate;//Helper.getDateFromString(m.transitActualEndDate as string,"YYYY-MM-DD");
    //     m.transitStartDate =  Helper.getDateFromString(m.transitStartDate as string,"YYYY-MM-DD");
    //     return m;
    // });

        return (dispatch, getState) => {
    
            const updateBookingActions = updateBookingAction(bookingDetail);  
            dispatch(updateBookingActions.next().value);
            return axios
                .put(ApiHelper.BOOKING_UPDATE_API+bookingDetail.id, bookingDetail)
                .then(response => {
              
                    if (response.data.status) {
                        const bookingModel:any=JSON.parse(JSON.stringify({...defaultBookingValue}));
                        dispatch(updateBookingActions.next(bookingModel).value);
                        toast.success('Booking Updated Successfully')
                        dispatch(push("/booking-list/active"));
                    } else {
                        toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                        dispatch(
                            updateBookingActions.next(response.data.errorMessage).value
                        );
                    }
                })
                .catch(error => {
                    
                    if(!window.navigator.onLine){
                        toast.error('Check network connection',{
                            toastId: 'Check network connection'
                          })
                    }
                    else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                      });
                    }

                    dispatch(updateBookingActions.next(error.message).value);
                });
        };
    };
export const getBooking=(id:string)=>{
    return (dispatch, getState) => {
    const bookingActions = getBookingAction({} as IBookingModel);
    dispatch(bookingActions.next().value);
    return axios
        .get(ApiHelper.BOOKING_GET_SINGLE_API + id)
        .then(response => {
            if (response.data.status) {
                const booking = response.data.data;
                dispatch(bookingActions.next(booking).value);
            } else {
                toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                dispatch(
                    bookingActions.next(response.data.errorMessage).value
                );
               
            }
        })
        .catch(error => {
            if(!window.navigator.onLine){
                toast.error('Check network connection',{
                    toastId: 'Check network connection'
                  })
            }
            else {
            toast.error("Something went wrong", {
                toastId: 'Something went wrong'
              });
            }
            dispatch(bookingActions.next(error.message).value);
            Error(error);
        });
};
}


 export const getConfigSettings= () => {
    return axios.get(ApiHelper.GET_CONFIG_SETTINGS);
   }
  




export const updateBookingProperties = (bookingDetail: IBookingModel) => {
    return (dispatch, getState) => {
        dispatch(updateBookingState(bookingDetail));
    };
};