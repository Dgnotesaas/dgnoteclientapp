import { IAllActiveTripModel, ISingleTrackingByIdModel, IAllHitListModel} from "../../models/allactivetrips.model";
import { IConsentCountModel, ITransitCountModel} from "../../models/dashboard.model";
import { ApiHelper, IGenericActionReturn } from "../../utilities";
import axios from "../../utilities/axios.util";
import { AllActiveTrips } from "../../utilities/enum/allactivetrips";
import { AllEnum, TransitCountEnum, ConsentCountEnum} from "../../utilities/enum/all.enum";
import { AllHitTrips } from "../../utilities/enum/allhittripbyid.enum";
import { toast } from "react-toastify";


function* fetchAction(list: IAllActiveTripModel[]): IterableIterator<IGenericActionReturn<IAllActiveTripModel[]>> {
    const value = yield { type: AllActiveTrips.GET_ALL_ACTIVE_TRIPS_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: AllActiveTrips.GET_ALL_ACTIVE_TRIPS_FAILURE, Payload: list, value };
    } else {
        return { type: AllActiveTrips.GET_ALL_ACTIVE_TRIPS_SUCCESS, Payload: value };
    }
} 

function* getTripsByIdAction(list: ISingleTrackingByIdModel): IterableIterator<IGenericActionReturn<ISingleTrackingByIdModel>> {
    const value = yield { type: AllActiveTrips.GET_ALL_ACTIVE_TRIPS_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: AllActiveTrips.GET_ALL_ACTIVE_TRIPS_FAILURE, Payload: list, value };
    } else {
        return { type: AllActiveTrips.GET_ALL_ACTIVE_TRIPS_SUCCESS, Payload: value };
    }
}

function* fetchHitAction(list: IAllHitListModel[]): IterableIterator<IGenericActionReturn<IAllHitListModel[]>> {
    const value = yield { type: AllHitTrips.GET_ALL_HIT_TRIPS_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: AllHitTrips.GET_ALL_HIT_TRIPS_FAILURE, Payload: list, value };
    } else {
        return { type: AllHitTrips.GET_ALL_HIT_TRIPS_SUCCESS, Payload: value };
    }
}

export const getAllActiveLocateTrips = () => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchAction([] as IAllActiveTripModel[]);
        dispatch(fetchTrackingActions.next().value);

        return axios
            .get(ApiHelper.TRACKING_GET_LOCATE_ACTIVE_TRIPS)
            .then(response => {
                
                if (response.data.status) {
                    const tracking =response.data.data as IAllActiveTripModel[];
                    dispatch(fetchTrackingActions.next(tracking as any).value);
                }else {
                    dispatch(
                        fetchTrackingActions.next(response.data.errorMessage).value
                    );
                  
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
};

export const getActiveLocateTripsById = (model) => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchAction([] as IAllActiveTripModel[]);
        dispatch(fetchTrackingActions.next().value);
        return axios
            .post(ApiHelper.TRACKING_GET_LOCATE_TRIPS_BY_ID, model)
            .then(response => {
                setTimeout(() => { 
                    if (response.data.status) {
                        const tracking =response.data.data as IAllActiveTripModel[];
                        dispatch(fetchTrackingActions.next(tracking as any).value);
                    }else {
                        dispatch(
                            fetchTrackingActions.next(response.data.errorMessage).value
                        );
                    }
                }, 1000);
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
}

export const getAllHitTripsById = (model) => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchHitAction([] as IAllHitListModel[]);
        dispatch(fetchTrackingActions.next().value);
        return axios
            .post(ApiHelper.TRACKING_GET_ALL_HIT_BY_ID, model)
            .then(response => {
                if (response.data.status) {
                    const tracking =response.data.data as IAllHitListModel[];
                    dispatch(fetchTrackingActions.next(tracking as any).value);
                }else {
                    dispatch(
                        fetchTrackingActions.next(response.data.errorMessage).value
                    );
                  
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
}

export const getSharingActiveLocateTripsById = (model) => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchAction([] as IAllActiveTripModel[]);
        dispatch(fetchTrackingActions.next().value);
        // if(localStorage.getItem("Authorize")) {
        //     localStorage.setItem("Authorize", "true");
        // } else {
        //     localStorage.setItem("Authorize", "false");
        // }
        localStorage.setItem("tenant", 'nippon');
        // localStorage.setItem("AuthorizationToken", 'abcd');

        return axios
            .post(ApiHelper.SHARING_TRACKING_GET_LOCATE_TRIPS_BY_ID, model)
            .then(response => {
                setTimeout(() => { 
                    if (response.data.status) {
                        const tracking =response.data.data as IAllActiveTripModel[];
                        dispatch(fetchTrackingActions.next(tracking as any).value);
                    }else {
                        dispatch(
                            fetchTrackingActions.next(response.data.errorMessage).value
                        );
                    }
                }, 1000);
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
}

export const getSharingAllHitTripsById = (model) => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchHitAction([] as IAllHitListModel[]);
        dispatch(fetchTrackingActions.next().value);
        return axios
            .post(ApiHelper.SHARING_TRACKING_GET_ALL_HIT_BY_ID, model)
            .then(response => {
                // console.log('response :',response);
                if (response.data.status) {
                    const tracking =response.data.data as IAllHitListModel[];
                    dispatch(fetchTrackingActions.next(tracking as any).value);
                }else {
                    dispatch(
                        fetchTrackingActions.next(response.data.errorMessage).value
                    );
                  
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
}

export const getHealthActiveLocateTripsById = (model) => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchAction([] as IAllActiveTripModel[]);
        dispatch(fetchTrackingActions.next().value);
        return axios
            .post(ApiHelper.HEALTH_TRACKING_GET_LOCATE_TRIPS_BY_ID, model)
            .then(response => {
                setTimeout(() => { 
                    if (response.data.status) {
                        const tracking =response.data.data as IAllActiveTripModel[];
                        dispatch(fetchTrackingActions.next(tracking as any).value);
                    }else {
                        dispatch(
                            fetchTrackingActions.next(response.data.errorMessage).value
                        );
                    }
                }, 1000);
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
}

export const getHealthAllHitTripsById = (model) => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchHitAction([] as IAllHitListModel[]);
        dispatch(fetchTrackingActions.next().value);
        return axios
            .post(ApiHelper.HEALTH_TRACKING_GET_ALL_HIT_BY_ID, model)
            .then(response => {
                // console.log('response :',response);
                if (response.data.status) {
                    const tracking =response.data.data as IAllHitListModel[];
                    dispatch(fetchTrackingActions.next(tracking as any).value);
                }else {
                    dispatch(
                        fetchTrackingActions.next(response.data.errorMessage).value
                    );
                  
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
}

function* fetchTransitAction(list: ITransitCountModel): IterableIterator<IGenericActionReturn<ITransitCountModel>> {
    const value = yield { type: TransitCountEnum.GET_TRANSIT_COUNT_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: TransitCountEnum.GET_TRANSIT_COUNT_FAILURE, Payload: list, value };
    } else {
        return { type: TransitCountEnum.GET_TRANSIT_COUNT_SUCCESS, Payload: value };
    }
} 

export const getTransitCount = () => {

    return (dispatch:any) => {
        let fetchTrackingActions = fetchTransitAction({} as ITransitCountModel);
        dispatch(fetchTrackingActions.next().value);

        return axios
            .get(ApiHelper.GET_TRANSIT_COUNT)
            .then(response => {
                
                if (response.data.status) {
                    const dashboardchart =response.data.data as ITransitCountModel;
                    
                    dispatch(fetchTrackingActions.next(dashboardchart[0] as any).value);
                }else {
                    dispatch(
                        fetchTrackingActions.next(response.data.errorMessage).value
                    );
                  
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
};

function* fetchConsentCountAction(list: IConsentCountModel): IterableIterator<IGenericActionReturn<IConsentCountModel>> {
    const value = yield { type: ConsentCountEnum.GET_CONSENT_COUNT_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: ConsentCountEnum.GET_CONSENT_COUNT_FAILURE, Payload: list, value };
    } else {
        return { type: ConsentCountEnum.GET_CONSENT_COUNT_SUCCESS, Payload: value };
    }
} 

export const getConsentCount = () => {

    return (dispatch:any) => {
        let fetchTrackingActions = fetchConsentCountAction({} as IConsentCountModel);
        dispatch(fetchTrackingActions.next().value);

        return axios
            .get(ApiHelper.GET_CONSENT_COUNT)
            .then(response => {
                
                if (response.data.status) {
                    const dashboardchart =response.data.data as IConsentCountModel;
                    dispatch(fetchTrackingActions.next(dashboardchart[0] as any).value);
                }else {
                    dispatch(
                        fetchTrackingActions.next(response.data.errorMessage).value
                    );
                  
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
};
