import { IHealthModel } from "../../models/healthlist.model";
import { ApiHelper, IGenericActionReturn } from "../../utilities";
import axios from "../../utilities/axios.util";
import { HealthListEnum  } from "../../utilities/enum/health.enum";
import { toast } from "react-toastify";

function* fetchAction(list: IHealthModel): IterableIterator<IGenericActionReturn<IHealthModel>> {
    const value = yield { type: HealthListEnum.GET_HEALTH_LIST_REQUST, Payload: list };

    if (typeof value === "string") {
        return { type: HealthListEnum.GET_HEALTH_LIST_FAILURE, Payload: list, value };
    } else {
        return { type: HealthListEnum.GET_HEALTH_LIST_SUCCESS, Payload: value };
    }
} 

export const getAllHealth = (status) => {
    return (dispatch: any) => {
        let fetchHealthActions = fetchAction({} as IHealthModel);
        dispatch(fetchHealthActions.next().value);
        return axios
            .get(ApiHelper.HEAlth_GET_ALL_TRIP + status)
            .then(response => {

                if (response.data.status) {
                    const healthlist = response.data.data as IHealthModel;
                    dispatch(fetchHealthActions.next(healthlist as any).value);
                } else {
                    dispatch(
                        fetchHealthActions.next(response.data.errorMessage).value
                    );

                }
            })
            .catch(error => {
                if (!window.navigator.onLine) {
                    toast.error('Check network connection', {
                        toastId: 'Check network connection'
                    })
                }
                else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                    });
                }
                dispatch(fetchHealthActions.next(error.message).value);
            });
    };
}; 
