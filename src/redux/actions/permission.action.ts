import { IPermissionModel  } from "../../models/permission.model";
import { ApiHelper, IGenericActionReturn } from "../../utilities";
import axios from "../../utilities/axios.util";
import { PermissionEnum } from "../../utilities/enum/permission.enum";
import { toast } from "react-toastify";


function* fetchAction(list: IPermissionModel): IterableIterator<IGenericActionReturn<IPermissionModel>> {
    const value = yield { type: PermissionEnum.GET_PERMISSION_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: PermissionEnum.GET_PERMISSION_FAILURE, Payload: list, value };
    } else {
        return { type: PermissionEnum.GET_PERMISSION_SUCCESS, Payload: value };
    }
}

export const getAllPermission = () => { 
    return (dispatch:any) => {
        let fetchPermissionActions = fetchAction({} as IPermissionModel);
        dispatch(fetchPermissionActions.next().value); 

        return axios
            .get(ApiHelper.USER_PERMISSION_GET_API)
            .then(response => {
                if (response.data.status) {
                    const permission =response.data.data as IPermissionModel;
                    dispatch(fetchPermissionActions.next(permission as any).value);
                }else {
                    dispatch(
                        fetchPermissionActions.next(response.data.errorMessage).value
                    );
                  
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchPermissionActions.next(error.message).value);
            });
    };
}; 
