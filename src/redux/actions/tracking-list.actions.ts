import { IGenericActionReturn ,ApiHelper} from "../../utilities";
import axios from "../../utilities/axios.util";
import { TrackingEnum } from "../../utilities/enum/tracking.enum";
import { ITrackingListModel } from "../../models/tracking-list.model";
import { toast } from "react-toastify";

function* fetchAction(list: ITrackingListModel): IterableIterator<IGenericActionReturn<ITrackingListModel>> {
    const value = yield { type: TrackingEnum.GET_TRACKING_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: TrackingEnum.GET_TRACKING_FAILURE, Payload: list, value };
    } else {
        return { type: TrackingEnum.GET_TRACKING_SUCCESS, Payload: value };
    }
}

export const getTracking = (searchModel) => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingsActions = fetchAction({} as ITrackingListModel);
        dispatch(fetchTrackingsActions.next().value);
     
        return axios
            .post(ApiHelper.BOOKING_GET_API, searchModel)
            .then(response => {
                console.log("tracking list_result");
                console.log(response);
                if (response.data.status) {
                    const bookings =response.data.data as ITrackingListModel;
                    const ary :ITrackingListModel[]=[];
                    ary.push(bookings);
                    dispatch(fetchTrackingsActions.next(bookings as any).value);
                }else {
                    dispatch(
                        fetchTrackingsActions.next(response.data.errorMessage).value
                    );
                    Error(response.data.Status.Message);
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingsActions.next(error.message).value);
            });
    };
};