import { TripEnum as Enum} from "../../utilities/enum";
import {  ApiHelper, axios, IGenericActionReturn } from "../../utilities";
import { toast } from "react-toastify";
import { ITrip } from "../../models/booking.model";
import {updateBookingListProperties} from "./booking-list.actions";

function* updateTripAction(tripDetail: ITrip): IterableIterator<IGenericActionReturn<ITrip>> {
    const value = yield { type: Enum.UPDATE_TRIP_REQUEST, Payload: tripDetail };
    if (typeof value === "string") {
        
        return { type: Enum.UPDATE_TRIP_FAILURE, Payload: tripDetail, value };
    } else {
  
        return { type: Enum.UPDATE_TRIP_SUCCESS, Payload: value };
    }
}


export const updateTrip = (tripDetail: ITrip) => {
    tripDetail.transitActualEndDate =  new Date( tripDetail.transitActualEndDate)
        return (dispatch, getState) => {
            debugger
            const updateTripActions = updateTripAction(tripDetail);  
            dispatch(updateTripActions.next().value);
            return axios
                .put(ApiHelper.TRIP_UPDATE_API+tripDetail.id, tripDetail)
                .then(response => {
                    if (response.data.status) {
                        dispatch(updateTripActions.next().value);
                        toast.success('Trip Updated Successfully');
                    } else {
                        toast.error(response.data.errorMessage,{toastId:response.data.errorMessage});
                        dispatch(
                            updateTripActions.next(response.data.errorMessage).value
                        );
                    }
                })
                .catch(error => {
                    if(!window.navigator.onLine){
                        toast.error('Check network connection',{
                            toastId: 'Check network connection'
                          })
                    }
                    else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                      });
                    }
                    dispatch(updateTripActions.next(error.message).value);
                });
        };
    };


    
export const checkConsent = (id) => {
  
        return (dispatch, getState) => {
            return axios
                .get(ApiHelper.TRIP_CONSENT_CHECK+id)
                .then(response => {
                  
                    if (response.data.status) {
                        console.log('consent response', response);
                        
                        const updatedTrip=response.data.data;
                        let trips = getState().BookingList.Payload;
                        let index=trips.content.findIndex(x=>x.id===updatedTrip.id);
                        trips[index]=updatedTrip;
                        dispatch(updateBookingListProperties(trips));
                        toast.success('Consent resend successfully')
                    } else {
                        toast.error(response.data.errorMessage);
                       
                    }
                })
                .catch(error => {
                    console.log(error)
                
                    if(!window.navigator.onLine){
                        toast.error('Check network connection',{
                            toastId: 'Check network connection'
                          })
                    }
                    else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                      });
                    }
                });
        };
    };
