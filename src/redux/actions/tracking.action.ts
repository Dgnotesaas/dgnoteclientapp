import { ITrackingModel,  } from "../../models/tracking.model";
import { ITrackingListModel } from "../../models/tracking-list.model";
import { ApiHelper, IGenericActionReturn } from "../../utilities";
import axios from "../../utilities/axios.util";
import { TrackingEnum } from "../../utilities/enum/tracking.enum";
import { toast } from "react-toastify";


function* fetchAction(list: ITrackingModel[]): IterableIterator<IGenericActionReturn<ITrackingModel[]>> {
    const value = yield { type: TrackingEnum.GET_TRACKING_REQUEST, Payload: list };
  
    if (typeof value === "string") {
        return { type: TrackingEnum.GET_TRACKING_FAILURE, Payload: list, value };
    } else {
        return { type: TrackingEnum.GET_TRACKING_SUCCESS, Payload: value };
    }
}


export const getAllActiveLocateTrips11 = (test) => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchAction([] as ITrackingModel[]);
        dispatch(fetchTrackingActions.next().value);

        return axios
            .get(ApiHelper.TRACKING_GET_LOCATE_ALL_ACTIVE_TRIPS)
            .then(response => {
                if (response.data.status) {
                    const tracking =response.data.data as ITrackingModel;
                    dispatch(fetchTrackingActions.next(tracking as any).value);
                }else {
                    dispatch(
                        fetchTrackingActions.next(response.data.errorMessage).value
                    );
                  
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
};

export const getAllActiveLocateTrips = () => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchAction([] as ITrackingModel[]);
        dispatch(fetchTrackingActions.next().value);
        debugger
        return axios
            .get(ApiHelper.TRACKING_GET_LOCATE_ALL_ACTIVE_TRIPS)
            .then(response => {
                if (response.data.status) {
                    const tracking =response.data.data as ITrackingModel[];
                    dispatch(fetchTrackingActions.next(tracking as any).value);
                } else {
                    dispatch(
                        fetchTrackingActions.next(response.data.errorMessage).value
                    );
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
};

export const getActiveLocateTrips = (model) => {

    return (dispatch:any, getState:any) => {
        let fetchTrackingActions = fetchAction([] as ITrackingModel[]);
        dispatch(fetchTrackingActions.next().value);

        return axios
            .post(ApiHelper.TRACKING_GET_LOCATE_TRIPS, model)
            .then(response => {
                if (response.data.status) {
                    const tracking =response.data.data as ITrackingModel[];
                    dispatch(fetchTrackingActions.next(tracking as any).value);
                }else {
                    dispatch(
                        fetchTrackingActions.next(response.data.errorMessage).value
                    );
                  
                }
            })
            .catch(error => {
                if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                }
                else {
                toast.error("Something went wrong", {
                    toastId: 'Something went wrong'
                  });
                }
                dispatch(fetchTrackingActions.next(error.message).value);
            });
    };
}
