
import { LoginEnum } from "../../utilities/enum/login.enum";
import { ILoginModel, IUserModel } from "../../models";
import {  IGenericAction, IGenericActionReturn ,ApiHelper} from "../../utilities";
import axios from "../../utilities/axios.util";
import { push } from "connected-react-router";
import { toast } from "react-toastify";
function* loginAction(login: ILoginModel): IterableIterator<IGenericActionReturn<ILoginModel>> {
    const value = yield { type: LoginEnum.USER_LOGIN_REQUEST, Payload: login };
  
    if (typeof value === "string") {
        return { type: LoginEnum.USER_LOGIN_FAILURE, Payload: login, value };
    } else {
        return { type: LoginEnum.USER_LOGIN_SUCCESS, Payload: value };
    }
}

function* logoutAction(login: ILoginModel): IterableIterator<IGenericActionReturn<ILoginModel>> {
    const value = yield { type: LoginEnum.USER_LOGOUT_REQUEST, Payload: login };
    if (typeof value === "string") {
        return { type: LoginEnum.USER_LOGOUT_FAILURE, Payload: login, value };
    } else {
        return { type: LoginEnum.USER_LOGOUT_SUCCESS, Payload: value };
    }
}
const setUserDetail: IGenericAction<IUserModel> = (User: any) => {
    return {
        Payload: User,
        type: LoginEnum.SET_USERDETAIL_DETAIL,
    }
}
const updateLoginState: IGenericAction<ILoginModel> = (Register:any) => {
    return {
        Payload: Register,
        type: LoginEnum.UPDATE_LOGIN_MODEL,
    };
};
const invalidModel: IGenericAction<ILoginModel> =( Register:any) => {
    return {
        Error: "Invalid Login Model",
        Payload: Register,
        type: LoginEnum.INVALID_LOGIN_MODEL,
    };
};

export const login = (login: ILoginModel) => {

    return (dispatch:any, getState:any) => {
        const loginActions = loginAction(login);
        dispatch(loginActions.next().value);
       
        return axios
            .post(ApiHelper.LOGIN_API, login)
            .then(response => {
             
                if (response.data.status) {
                    var loginModel = getState().Login.Payload;
                    dispatch(loginActions.next(loginModel).value);
                    localStorage.setItem("Authorize", "True");
                    localStorage.setItem("tenant", response.data.data.user.companyDto.tenantHeader);
                    localStorage.setItem("AuthorizationToken", response.data.data.token);
                    dispatch(setUserDetail(response.data.data.user));

                    if(response.data.data.user.role.name == 'SUPERADMIN') {
                        dispatch(push("/"));
                    } else {
                        dispatch(push("/dashboard"));
                    }
                }else {
                    if(response.status === 403){
                      
                        // toast.error('Invalid credentials');
                       
                    }
                    dispatch(
                        loginActions.next(response.data.errorMessage).value
                    );
                    
                }
            })
            .catch(error => {
                
                if(error.response && error.response.status === 403){
                  //  dispatch(invalidModel(login,'Invalid credentials'));
                    toast.error('Invalid credentials',{toastId: 'Invalid credentials'});
                    dispatch(loginActions.next(error.response.data.errorMessage).value);
                }
                else if(error.response && error.response.data){
                    dispatch(loginActions.next(error.response.data.errorMessage).value);
                }
                else if(!window.navigator.onLine){
                    toast.error('Check network connection',{
                        toastId: 'Check network connection'
                      })
                    dispatch(loginActions.next(error.message).value);
                }
                else {
                    toast.error("Something went wrong", {
                        toastId: 'Something went wrong'
                      });
                }
              

            });
    };
};
export const logout = () => {
  
    return (dispatch:any, getState:any) => {
        const logoutActions = logoutAction({} as ILoginModel);
        dispatch(logoutActions.next().value);
        localStorage.clear();
        localStorage.setItem("Authorize", "False");
        dispatch(push("/login"));
        window.location.reload();
        return axios
            .post(ApiHelper.LOGOUT_API)
            .then(response => {
                if (response.data.status) {
                  
                } else {
                    dispatch(
                        logoutActions.next(response.data.errorMessage).value
                    );
                    
                }
            })
            .catch(error => {
                dispatch(logoutActions.next(error.response.data.errorMessage).value);
            });
    };
};

export const updateProperties = (Register: ILoginModel) => {
    return (dispatch:any, getState:any) => {
        dispatch(updateLoginState(Register));
    };
};
export const invalidLoginModel = () => {
    return (dispatch:any, getState:any) => {
        dispatch(invalidModel({}as  ILoginModel));
    };
};
