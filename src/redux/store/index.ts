
import { createStore, applyMiddleware, compose, combineReducers, StoreEnhancerStoreCreator, StoreEnhancer, Store } from 'redux'
import { Reducers } from '../reducers'
import redux_thunk from 'redux-thunk'
import { IAppState } from '../../models'

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { createBrowserHistory, History } from "history";
import { connectRouter, routerMiddleware } from 'connected-react-router';
export const history = createBrowserHistory()


export function configureStore(

  initialState: IAppState
) {
  const windowIfDefined =
    typeof window === "undefined" ? null : (window as any);
  const allReducers = buildRootReducer(Reducers);
  //If devTools is installed, connect to it
  const devToolsExtension =
    windowIfDefined &&
    (windowIfDefined.devToolsExtension);
    const persistConfig = {
      key: 'root',
      storage: storage,
      whitelist: ['User'] //  persisted
    };
    const persistedReducer = persistReducer(persistConfig, allReducers)
  let store = createStore(
    persistedReducer,
    initialState,
    compose(
      applyMiddleware(redux_thunk, routerMiddleware(history)),
      devToolsExtension
        ? devToolsExtension()
        : <S>(next: StoreEnhancerStoreCreator<S>) => next
    )
  ) as Store<IAppState>;
  let persistor = persistStore(store)
  return {store,persistor};
}
function buildRootReducer(allReducers: any) {
  return combineReducers<IAppState>(
    Object.assign({}, allReducers, { router: connectRouter(history) })
  );
}