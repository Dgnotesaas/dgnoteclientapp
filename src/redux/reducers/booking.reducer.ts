import { BookingEnum as Enum } from "../../utilities/enum";
import { defaultBookingValue, IBookingModel } from "../../models";
import { IGenericReducerState, IGenericReducer } from "../../utilities";

const defaultState : IGenericReducerState<IBookingModel>={
    Error:"",
    Fetching:false,
    Payload: JSON.parse(JSON.stringify(defaultBookingValue))
}as IGenericReducerState<IBookingModel>;
export const BookingReducer: IGenericReducer<IBookingModel> = (state = defaultState, action) => {
    switch (action.type) {
        case Enum.GET_BOOKING_REQUEST:
        case Enum.CREATE_BOOKING_REQUEST:{
            return { ...state, Fetching: true, Error: "" };
        }
        case Enum.GET_BOOKING_SUCCESS:
        case Enum.UPDATE_BOOKING_STATE_SUCCESS:
        case Enum.CREATE_BOOKING_SUCCESS: {
            return { ...state, Fetching: false, Payload: { ...action.Payload } };
        }
        case Enum.GET_BOOKING_FAILURE:
        case Enum.CREATE_BOOKING_FAILURE:{
            return { ...state, Fetching: false, Error: action.Error || "" };
        }
    }
    return state;
};