import { PasswordEnum as Enum } from "../../utilities/enum";
import { ICreatePasswordModel,defaultCreatePasswordModel } from "../../models";
import { IGenericReducerState, IGenericReducer } from "../../utilities";
 
const defaultState : IGenericReducerState<ICreatePasswordModel>={
    Error:"",
    Fetching:false,
    Payload: JSON.parse(JSON.stringify(defaultCreatePasswordModel)) 
};

export const CreatePasswordReducer: IGenericReducer<ICreatePasswordModel> = (state = defaultState, action) => {
    switch (action.type) {
        case Enum.RESET_PASSWORD_REQUEST: {
            return { ...state, Fetching: true, Payload: { ...action.Payload }};
        }
        case Enum.RESET_PASSWORD_SUCCESS: {
            return { ...state, Fetching: false, Payload: { ...action.Payload } };
        }
        case Enum.RESET_PASSWORD_FAILURE: {
            return { ...state, Fetching: false, Error: action.Error || "" };
        } 
    }
    return state;
};