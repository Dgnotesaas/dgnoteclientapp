import { CompanyEnum as Enum } from "../../utilities/enum";
import { defaultCompanyValue, ICompanyModel } from "../../models";
import { IGenericReducerState, IGenericReducer } from "../../utilities";

const defaultState : IGenericReducerState<ICompanyModel>={
    Error:"",
    Fetching:false,
    Payload: JSON.parse(JSON.stringify(defaultCompanyValue))
}as IGenericReducerState<ICompanyModel>;
export const CompanyReducer: IGenericReducer<ICompanyModel> = (state = defaultState, action) => {
    switch (action.type) {
        case Enum.GET_COMPANY_REQUEST:
        case Enum.CREATE_COMPANY_REQUEST:{
            return { ...state, Fetching: true, Error: "" };
        }
        case Enum.GET_COMPANY_SUCCESS:
        case Enum.CREATE_COMPANY_SUCCESS: {
            return { ...state, Fetching: false, Payload: { ...action.Payload } };
        }
        case Enum.GET_COMPANY_FAILURE:
        case Enum.CREATE_COMPANY_FAILURE:{
            return { ...state, Fetching: false, Error: action.Error || "" };
        }
    }
    return state;
};