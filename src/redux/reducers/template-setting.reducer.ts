import { BookingEnum as Enum, TemplateSettingEnum } from "../../utilities/enum";

import { defaultTemplateSettingValue, ITemplateSettingModel } from "../../models/template-setting.model";
import { IGenericReducer, IGenericReducerState } from "../../utilities";

const defaultState : IGenericReducerState<ITemplateSettingModel>={
    Error:"",
    Fetching:false,
    Payload: JSON.parse(JSON.stringify(defaultTemplateSettingValue))
}as IGenericReducerState<ITemplateSettingModel>;
export const TemplateSettingReducer: IGenericReducer<ITemplateSettingModel> = (state = defaultState, action) => {
    switch (action.type) {
        case TemplateSettingEnum.GET_TEMPLATESETTING_REQUST:{
            return { ...state, Fetching: true, Error: "" };
        }
        
        case TemplateSettingEnum.UPDATE_TEMPLATESETTING_REQUST:{
            return { ...state, Fetching: true, Error: "" };
        }
        
        case TemplateSettingEnum.UPDATE_TEMPLATESETTING_SUCCESS: {
            return { ...state, Fetching: false, Payload: { ...action.Payload } };
        }
        case TemplateSettingEnum.GET_TEMPLATESETTING_FAILURE:{
            return { ...state, Fetching: false, Error: action.Error || "" };
        }

        case TemplateSettingEnum.GET_TEMPLATESETTING_SUCCESS: {
            return { ...state, Fetching: false, Payload: { ...action.Payload } };
        }
        case TemplateSettingEnum.UPDATE_TEMPLATESETTING_FAILURE:{
            return { ...state, Fetching: false, Error: action.Error || "" };
        }
    }
    return state;
};