
import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { IBookingListModel } from "../../models/booking-list.model";
import { defaultBookingListValue } from "../../models";
import { BookingListEnum } from "../../utilities/enum/booking.enum";

const defaultState : IGenericReducerState<IBookingListModel>={
    Error:"",
    Fetching:false,
    Payload: JSON.parse(JSON.stringify(defaultBookingListValue))
}as IGenericReducerState<IBookingListModel>;
export const BookingListReducer: IGenericReducer<IBookingListModel> = (state = defaultState, action) => {
    switch (action.type) {
        case BookingListEnum.GET_BOOKINGS_LIST_REQUST:{
            return { ...state, Fetching: true, Error: "" };
        }
        case BookingListEnum.GET_BOOKINGS_LIST_FAILURE:{
            return { ...state, Fetching: false, Error: action.Error || "" };
        }
        case BookingListEnum.GET_BOOKINGS_LIST_SUCCESS: {
            return {
                ...state, Fetching: false, Payload: { ...action.Payload }, Error: ""
            };
        }
    }
    return {...state};
};