

import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultSingleTrackingValue, ISingleTrackingModel } from "../../models/singletracking.model";
import { TrackingEnum } from "../../utilities/enum/tracking.enum";

const defaultState: IGenericReducerState<ISingleTrackingModel> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultSingleTrackingValue))
} as IGenericReducerState<ISingleTrackingModel>;
export const SingleTrackingReducer: IGenericReducer<ISingleTrackingModel> = (state = defaultState, action) => {
    switch (action.type) {
        case TrackingEnum.GET_TRACKING_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case TrackingEnum.GET_TRACKING_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case TrackingEnum.GET_TRACKING_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};