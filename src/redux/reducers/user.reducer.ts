import { LoginEnum } from "../../utilities/enum";
import {  IUserModel } from "../../models";
import { IGenericReducerState, IGenericReducer } from "../../utilities";

const defaultState :  IGenericReducerState<IUserModel>={
    Error:"",
    Fetching:false,
    Payload:{} as IUserModel


} as IGenericReducerState<IUserModel>;

export const UserReducer: IGenericReducer<IUserModel> = (state = defaultState, action) => {

    switch (action.type) {
      
        case LoginEnum.SET_USERDETAIL_DETAIL: {
            return {
                ...state, Fetching: false, Payload: { ...action.Payload }, Error: ""
            };
        }
     
    }
    return state;
};