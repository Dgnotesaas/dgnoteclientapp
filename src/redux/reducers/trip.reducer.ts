import { TripEnum as Enum } from "../../utilities/enum";
import {  ITrip } from "../../models";
import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultTripValue } from "../../models/booking.model";


const defaultState : IGenericReducerState<ITrip>={
    Error:"",
    Fetching:false,
    Payload: JSON.parse(JSON.stringify(defaultTripValue))
}as IGenericReducerState<ITrip>;
export const TripReducer: IGenericReducer<ITrip> = (state = defaultState, action) => {
    switch (action.type) {
     
        case Enum.UPDATE_TRIP_REQUEST:{
            return { ...state, Fetching: true, Error: "" };
        }
        
        case Enum.UPDATE_TRIP_SUCCESS: {
            return { ...state, Fetching: false, Payload: { ...action.Payload } };
        }
       
        case Enum.UPDATE_TRIP_FAILURE:{
            return { ...state, Fetching: false, Error: action.Error || "" };
        }
    }
    return state;
};