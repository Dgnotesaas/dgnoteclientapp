import { PasswordEnum as Enum } from "../../utilities/enum";
import { IPasswordModel,defaultPasswordModel } from "../../models";
import { IGenericReducerState, IGenericReducer } from "../../utilities";

const defaultState : IGenericReducerState<IPasswordModel>={
    Error:"",
    Fetching:false,
    Payload: JSON.parse(JSON.stringify(defaultPasswordModel)) 
};

export const PasswordReducer: IGenericReducer<IPasswordModel> = (state = defaultState, action) => {
    switch (action.type) {
        case Enum.FORGOT_PASSWORD_REQUEST:
        case Enum.RESET_PASSWORD_REQUEST:
        case Enum.CHANGE_PASSWORD_REQUEST: {
            return { ...state, Fetching: true, Error: "" };
        }
        case Enum.FORGOT_PASSWORD_SUCCESS:
        case Enum.RESET_PASSWORD_SUCCESS:
        case Enum.CHANGE_PASSWORD_SUCCESS: {
            return { ...state, Fetching: false, Error: "" };
        }
        case Enum.FORGOT_PASSWORD_FAILURE:
        case Enum.RESET_PASSWORD_FAILURE:
        case Enum.CHANGE_PASSWORD_FAILURE: {
            return { ...state, Fetching: false, Error: action.Error || "" };
        }
        case Enum.UPDATE_PASSWORD_MODEL: {
            return { ...state, Fetching: false, Payload: { ...action.Payload } };
        }
        case Enum.INVALID_PASSWORD_MODEL: {
            return { ...state, Fetching: false, Error: "invalid model" };
        }
        case Enum.CLEAR_PASSWORD_MODEL: {
            return { ...state, Fetching: false, Payload: { ...action.Payload } };
        }
    }
    return state;
};