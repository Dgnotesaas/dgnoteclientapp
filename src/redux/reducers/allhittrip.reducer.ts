

import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultAllHitListValue, IAllHitListModel } from "../../models/allactivetrips.model";
import { AllHitTrips } from "../../utilities/enum/allhittripbyid.enum";

const defaultState: IGenericReducerState<IAllHitListModel[]> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultAllHitListValue))
} as IGenericReducerState<IAllHitListModel[]>;
export const GetAllHitTrips: IGenericReducer<IAllHitListModel[]> = (state = defaultState, action) => {
    switch (action.type) {
        case AllHitTrips.GET_ALL_HIT_TRIPS_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case AllHitTrips.GET_ALL_HIT_TRIPS_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case AllHitTrips.GET_ALL_HIT_TRIPS_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};