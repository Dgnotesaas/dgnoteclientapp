import { PasswordEnum as Enum } from "../../utilities/enum";
import { IForgetPasswordModel,defaultForgetPasswordModel } from "../../models";
import { IGenericReducerState, IGenericReducer } from "../../utilities";

const defaultState : IGenericReducerState<IForgetPasswordModel>={
    Error:"",
    Fetching:false,
    Payload: JSON.parse(JSON.stringify(defaultForgetPasswordModel)) 
};

export const ForgetPasswordReducer: IGenericReducer<IForgetPasswordModel> = (state = defaultState, action) => {
    switch (action.type) {
        case Enum.FORGOT_PASSWORD_REQUEST: {
            return { ...state, Fetching: true, Error: "" };
        }
        case Enum.FORGOT_PASSWORD_SUCCESS: {
            return { ...state, Fetching: false, Error: "" };
        }
        case Enum.FORGOT_PASSWORD_FAILURE: {
            return { ...state, Fetching: false, Error: action.Error || "" };
        } 
    }
    return state;
};