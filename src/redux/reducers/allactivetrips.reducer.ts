

import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultAllTrackingListValue, IAllActiveTripModel } from "../../models/allactivetrips.model";
import { AllActiveTrips } from "../../utilities/enum/allactivetrips";

const defaultState: IGenericReducerState<IAllActiveTripModel[]> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultAllTrackingListValue))
} as IGenericReducerState<IAllActiveTripModel[]>;
export const GetAllActiveTrips: IGenericReducer<IAllActiveTripModel[]> = (state = defaultState, action) => {
    switch (action.type) {
        case AllActiveTrips.GET_ALL_ACTIVE_TRIPS_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case AllActiveTrips.GET_ALL_ACTIVE_TRIPS_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case AllActiveTrips.GET_ALL_ACTIVE_TRIPS_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};