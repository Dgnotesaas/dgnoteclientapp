import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultHealthValue, IHealthModel } from "../../models/healthlist.model";
import { HealthListEnum } from "../../utilities/enum/health.enum";

const defaultState: IGenericReducerState<IHealthModel> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultHealthValue))
} as IGenericReducerState<IHealthModel>;
export const HealthReducer: IGenericReducer<IHealthModel> = (state = defaultState, action) => {
    switch (action.type) {
        case HealthListEnum.GET_HEALTH_LIST_REQUST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case HealthListEnum.GET_HEALTH_LIST_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case HealthListEnum.GET_HEALTH_LIST_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};