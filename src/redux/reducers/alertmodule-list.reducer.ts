

import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultRole, IRoleAllModel } from "../../models/roleall.model";
import { RoleEnum } from "../../utilities/enum/roleall.enum";

const defaultState: IGenericReducerState<IRoleAllModel> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultRole))
} as IGenericReducerState<IRoleAllModel>;
export const AlertModuleReducer: IGenericReducer<IRoleAllModel> = (state = defaultState, action) => {
    switch (action.type) {
        case RoleEnum.GET_ROLE_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case RoleEnum.GET_ROLE_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case RoleEnum.GET_ROLE_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};