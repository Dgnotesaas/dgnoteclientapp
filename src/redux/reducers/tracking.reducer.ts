

import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultTrackingValue, ITrackingModel } from "../../models/tracking.model";
import { TrackingEnum } from "../../utilities/enum/tracking.enum";

const defaultState: IGenericReducerState<ITrackingModel[]> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultTrackingValue))
} as IGenericReducerState<ITrackingModel[]>;
export const TrackingReducer: IGenericReducer<ITrackingModel[]> = (state = defaultState, action) => {
    switch (action.type) {
        case TrackingEnum.GET_TRACKING_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case TrackingEnum.GET_TRACKING_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case TrackingEnum.GET_TRACKING_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};