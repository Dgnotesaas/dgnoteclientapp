

import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultPermission, IPermissionModel } from "../../models/permission.model";
import { PermissionEnum } from "../../utilities/enum/permission.enum";

const defaultState: IGenericReducerState<IPermissionModel> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultPermission))
} as IGenericReducerState<IPermissionModel>;
export const PermissionReducer: IGenericReducer<IPermissionModel> = (state = defaultState, action) => {
    switch (action.type) {
        case PermissionEnum.GET_PERMISSION_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case PermissionEnum.GET_PERMISSION_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case PermissionEnum.GET_PERMISSION_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};