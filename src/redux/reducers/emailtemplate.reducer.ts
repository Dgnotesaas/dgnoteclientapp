import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultEmailTemplateModel, IEmailTemplateModel } from "../../models/emailtemplate.model";
import { EmailTemplateEnum } from "../../utilities/enum/emailtemplate.enum";

const defaultState: IGenericReducerState<IEmailTemplateModel> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultEmailTemplateModel))
} as IGenericReducerState<IEmailTemplateModel>;
export const EmailTemplateReducer: IGenericReducer<IEmailTemplateModel> = (state = defaultState, action) => {
    switch (action.type) {
        case EmailTemplateEnum.GET_EMAILTEMPLATE_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case EmailTemplateEnum.GET_EMAILTEMPLATE_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case EmailTemplateEnum.GET_EMAILTEMPLATE_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};