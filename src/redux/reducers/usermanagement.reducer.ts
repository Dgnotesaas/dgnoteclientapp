

import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultUsermanagement, IUsermanagementModel } from "../../models/usermanagement.model";
import { UsermanagementEnum } from "../../utilities/enum/usermanagement.enum";

const defaultState: IGenericReducerState<IUsermanagementModel> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultUsermanagement))
} as IGenericReducerState<IUsermanagementModel>;
export const UsermanagementReducer: IGenericReducer<IUsermanagementModel> = (state = defaultState, action) => {
    switch (action.type) {
        case UsermanagementEnum.GET_MANAGEMENT_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case UsermanagementEnum.GET_MANAGEMENT_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case UsermanagementEnum.GET_MANAGEMENT_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};