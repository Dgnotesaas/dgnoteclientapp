

import {LoginReducer} from "./login.reducer";
import { PasswordReducer } from "./password.reducer";
import { UserReducer } from "./user.reducer";
import { BookingReducer } from "./booking.reducer";
import { BookingListReducer } from "./booking-list.reducer";
import { TrackingListReducer } from "./tracking-list.reducer";
import { TemplateSettingReducer } from "./template-setting.reducer";
import { TripReducer } from "./trip.reducer";
import { GetAllActiveTrips } from "./allactivetrips.reducer";
import { GetAllHitTrips } from "./allhittrip.reducer";
import { LoaderReducer } from "./loader.reducer";
import { TrackingReducer } from "./tracking.reducer";
import { SingleTrackingReducer } from "./singletracking.reducer";
import { PermissionReducer } from "./permission.reducer";
import { UsermanagementReducer } from "./usermanagement.reducer";
import { RoleReducer } from "./role.reducer";
import { ForgetPasswordReducer } from "./forgetpassword.reducer";
import { CreatePasswordReducer } from "./createpassword.reducer";
import { HealthReducer } from "./healthlist.reducer";
import { CompanyReducer } from "./company.reducer";
import { EmailTemplateReducer } from "./emailtemplate.reducer";
import { AlertModuleReducer } from "./alertmodule-list.reducer";
import { AlertModulePostReducer } from "./alertmodule.reducer";
import { DashboardReducer, ConsentCountReducer } from "./dashboard.reducer";
export const Reducers={
Login:LoginReducer,
Password:PasswordReducer,
User:UserReducer,
Booking:BookingReducer,
BookingList:BookingListReducer,
TrackingList:TrackingListReducer,
TemplateSetting: TemplateSettingReducer,
Trip:TripReducer,
AllActiveTrip: GetAllActiveTrips,
AllHitTripById: GetAllHitTrips,
Loader:LoaderReducer,
Tracking: TrackingReducer,
SingleTracking: SingleTrackingReducer,
UserPermission: PermissionReducer,
UserManagement: UsermanagementReducer,
RoleAll: RoleReducer,
ForgetPassword: ForgetPasswordReducer,
CreatePassword: CreatePasswordReducer,
HealthList: HealthReducer,
Company: CompanyReducer,
EmailTemplate: EmailTemplateReducer,
AlertList: AlertModuleReducer,
AlertPost: AlertModulePostReducer,
TransitCount: DashboardReducer,
ConsentCount: ConsentCountReducer,
}
  