
import { IGenericReducerState, IGenericReducer } from "../../utilities";

const defaultState : IGenericReducerState<boolean>={
    Error:"",
    Fetching:false,
    Payload: {}
}as IGenericReducerState<boolean>;
export const LoaderReducer: IGenericReducer<boolean> = (state = defaultState, action) => {
    return {...state};
};