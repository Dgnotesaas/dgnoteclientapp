import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { ITrackingListModel } from "../../models/tracking-list.model";
import { defaultTrackingListValue } from "../../models";
import { TrackingEnum } from "../../utilities/enum/tracking.enum";

const defaultState : IGenericReducerState<ITrackingListModel>={
    Error:"",
    Fetching:false,
    Payload: JSON.parse(JSON.stringify(defaultTrackingListValue))
}as IGenericReducerState<ITrackingListModel>;

export const TrackingListReducer: IGenericReducer<ITrackingListModel> = (state = defaultState, action) => {
    switch (action.type) {
        case TrackingEnum.GET_TRACKING_REQUEST:{
            return { ...state, Fetching: true, Error: "" };
        }
        case TrackingEnum.GET_TRACKING_SUCCESS:{
            return { ...state, Fetching: false, Error: action.Error || "" };
        }
        case TrackingEnum.GET_TRACKING_FAILURE: {
            return {
                ...state, Fetching: false, Payload: { ...action.Payload }, Error: ""
            };
        }
    }
    return {...state};
};