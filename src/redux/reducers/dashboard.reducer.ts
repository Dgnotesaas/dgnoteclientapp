import { IGenericReducerState, IGenericReducer } from "../../utilities";
import { defaultConsentCount, defaultTransitCount, IConsentCountModel, ITransitCountModel } from "../../models/dashboard.model";
import { AllEnum, TransitCountEnum, ConsentCountEnum} from "../../utilities/enum/all.enum";

const defaultState: IGenericReducerState<ITransitCountModel> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultTransitCount))
} as IGenericReducerState<ITransitCountModel>;
export const DashboardReducer: IGenericReducer<ITransitCountModel> = (state = defaultState, action) => {
    switch (action.type) {
        case TransitCountEnum.GET_TRANSIT_COUNT_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case TransitCountEnum.GET_TRANSIT_COUNT_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case TransitCountEnum.GET_TRANSIT_COUNT_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};


const defaultConsentState: IGenericReducerState<IConsentCountModel> = {
    Error: "",
    Fetching: false,
    Payload: JSON.parse(JSON.stringify(defaultConsentCount))
} as IGenericReducerState<IConsentCountModel>;
export const ConsentCountReducer: IGenericReducer<IConsentCountModel> = (state = defaultConsentState, action) => {
    switch (action.type) {
        case ConsentCountEnum.GET_CONSENT_COUNT_REQUEST:
            {
                return { ...state, Fetching: true, Payload: { ...action.Payload } };
            }
        case ConsentCountEnum.GET_CONSENT_COUNT_SUCCESS:
            {
                return { ...state, Fetching: false, Payload: { ...action.Payload } };
            }
        case ConsentCountEnum.GET_CONSENT_COUNT_FAILURE:
            {
                return { ...state, Fetching: false, Error: action.Error || "" };
            }
    }
    return state;
};
