import { LoginEnum } from "../../utilities/enum";
import { ILoginModel } from "../../models";
import { IGenericReducerState, IGenericReducer } from "../../utilities";

const defaultState :  IGenericReducerState<ILoginModel>={
    Error:"",
    Fetching:false,
    Payload:{email:"",password:"",rememberMe:false} as ILoginModel


} as IGenericReducerState<ILoginModel>;

export const LoginReducer: IGenericReducer<ILoginModel> = (state = defaultState, action) => {

    switch (action.type) {
        case LoginEnum.USER_LOGIN_REQUEST: {
            return { ...state, Fetching: true, Error: "" };
        }
        case LoginEnum.USER_LOGIN_SUCCESS: {
            return {
                ...state, Fetching: false, Payload: { ...action.Payload }, Error: ""
            };
        }
        case LoginEnum.USER_LOGIN_FAILURE: {
            return { ...state, Fetching: false, Error: action.Error || "" };
        }
        case LoginEnum.UPDATE_LOGIN_MODEL: {
            return { ...state, Fetching: false, Payload: { ...action.Payload } };
        }
        case LoginEnum.INVALID_LOGIN_MODEL: {
            return { ...state, Fetching: false, Error: "invalid model" };
        }
        case LoginEnum.USER_LOGOUT_REQUEST: {
            return { ...state, Fetching: true, Error: "" };
        }
        case LoginEnum.USER_LOGOUT_SUCCESS: {
            return { ...state, Fetching: false, Error: "" };
        }
        case LoginEnum.USER_LOGOUT_FAILURE: {
            return { ...state, Fetching: false, Error: action.Error || "" };
        }
    }
    return state;
};