
# Step 1
FROM node:13.12.0-alpine as build-step

RUN mkdir /app

WORKDIR /app

COPY package.json /app
COPY package-lock.json /app


RUN npm install

COPY . /app

RUN npm run build

# Stage 2

#FROM nginx:stable-alpine
#COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80

CMD ["npm", "start"]
#CMD ["nginx", "-g", "daemon off;"]